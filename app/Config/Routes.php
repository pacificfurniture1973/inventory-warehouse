<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'auth/c_auth::index');
$routes->get('/signUp', 'auth/c_auth::signUp');
$routes->get('/signOut', 'auth/c_auth::signOut');
$routes->get('/forgotPassword', 'auth/c_auth::forgotPassword');

$routes->get('/dashboard', 'warehouse/c_dashboard::index');
$routes->get('/main/products', 'main/c_products::index');
$routes->get('/main/assembly', 'main/c_assembly::index');
$routes->get('/main/customer', 'main/c_customer::index');
$routes->get('/main/customer/ajaxList', 'main/c_customer::ajaxList');
$routes->get('/main/vendor', 'main/c_vendor::index');
$routes->get('/main/jobOrder', 'main/c_jobOrder::index');
$routes->get('/main/receivementReport', 'main/c_receivementReport::index');
$routes->get('/main/masterPlan', 'main/c_masterPlan::index');

$routes->get('/warehouse/dashboard', 'warehouse/c_dashboard::index');
$routes->get('/warehouse/products', 'warehouse/c_products::index');
$routes->get('/warehouse/stockAdjustment', 'warehouse/c_stockAdjustment::index');
$routes->get('/warehouse/stockAdjustmentLog', 'warehouse/c_stockAdjustmentLog::index');
$routes->get('/warehouse/c_stockAdjustment/getTerm', 'warehouse/c_stockAdjustment::getTerm');
$routes->get('/warehouse/withdrawal', 'warehouse/c_withdrawal::index');
$routes->get('/warehouse/withdrawalProcess', 'warehouse/c_withdrawalProcess::index');
$routes->get('/warehouse/statusOverbudget', 'warehouse/c_withdrawalProcess::statusOverbuget');
$routes->get('/warehouse/withdrawalHistory', 'warehouse/c_withdrawalHistory::index');
$routes->get('/warehouse/withdrawalFinish', 'warehouse/c_withdrawalFinish::index');

// Management
$routes->get('management/userManagement', 'management/c_userManagement::index');
$routes->get('management/userRequest', 'management/c_userRequest::index');
$routes->get('management/otherManagement', 'management/c_otherManagement::index');

// PDE
$routes->get('pde/bom', 'pde/c_bom::index');
$routes->get('pde/buildAssembly', 'pde/c_buildAssembly::index');
$routes->get('pde/processAssembly', 'pde/c_processAssembly::index');
$routes->get('pde/finishAssembly', 'pde/c_finishAssembly::index');

// Log
$routes->get('log/productsLog', 'log/c_productsLog::index');
$routes->get('log/assemblyLog', 'log/c_assemblyLog::index');
$routes->get('log/customerLog', 'log/c_customerLog::index');
$routes->get('log/vendorLog', 'log/c_vendorLog::index');
$routes->get('log/joLog', 'log/c_joLog::index');
$routes->get('log/rrLog', 'log/c_rrLog::index');
$routes->get('log/withdrawalLog', 'log/c_withdrawalLog::index');
$routes->get('log/bomLog', 'log/c_bomLog::index');



/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
