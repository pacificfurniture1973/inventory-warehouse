<?php

namespace App\Controllers\auth;
use App\Controllers\BaseController;
use Config\Services;
use App\Models\warehouse\models;

class c_auth extends BaseController
{
    protected $table = 'user';
    protected $database = 'main';
    protected $column_order = [];
    protected $column_search = [];
    protected $order = [];
    
    public function __construct() {
        $this->request = Services::request();
        $this->models = new models($this->request, $this->table, $this->column_order, $this->column_search, $this->order, $this->database);
        $this->session = session();
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index()
    {        
        if($this->session->get('login')) {
            return redirect()->to('/dashboard');
        }

        return view('login/v_login.php');
    }

    public function signIn() {

        // terima inputan dari web
        $username = $this->request->getVar('username');
        $password = $this->request->getVar('password');
        // cek di database ada apa ngga
        if($this->models->getUser($username) != null) {
        // jika ada cek password apakah benar
            $user = $this->models->getUser($username);

            if(password_verify($password, $user['user_password'])) {
                // set session 
                $jam = intval(date('H'));
                if($jam < 12) {
                    $pesan = ["Good Morning", "Selamat Pagi"];
                    $urutan = array_rand($pesan, 1);
                } elseif($jam >= 12 && $jam <= 17) {
                    $pesan = ["Good Afternoon", "Selamat Siang"];
                    $urutan = array_rand($pesan, 1);
                } elseif($jam >= 18 && $jam < 19) {
                    $pesan = ["Good Evening", "Selamat Sore"];
                    $urutan = array_rand($pesan, 1);
                } else {
                    $pesan = ['Selamat Malam'];
                    $urutan = array_rand($pesan, 1);
                }

                $sessionUser = [
                    'login'     => True,
                    'user_id'   => $user['user_id'],
                    'username'  => $user['user_username'],
                    'fullname'  => $user['user_fullname'],
                    'position'  => $user['user_position'],
                    'level'     => $user['user_level'],
                    'password'  => $user['user_password'],
                    'telp'      => $user['user_telp'],
                    'address'   => $user['user_address'],
                    'email'     => $user['user_email'],
                    'status'    => $user['user_status'],
                    'dept'      => $user['user_dept'],
                    'deptHandle'  => $user['user_dept_handle'],
                    'pesan'     => $pesan[$urutan],

                ]; 
                $this->session->set($sessionUser);
				
                return redirect()->to('/dashboard');
            } else {
                // set pesan
                $this->session->setFlashdata('error_message', '<strong>Failed !</strong> wrong password !!');
                return redirect()->to('/');
            }
            return redirect()->to('/dashboard');
        } else {
            // set pesan
            $this->session->setFlashdata('error_message', '<strong>Failed !</strong> user not found/not active !!');
            return redirect()->to('/');
        }

    }

    public function signUp() {
        // dd('masuk');
        $data = [
            'validation'    => \Config\Services::validation(),
            'dept'          => $this->models->getAllDataDept(),
            'level'         => $this->models->getAllDataLevel(),
            'status'        => $this->models->getAllDataStatus(),
        ];

        return view('login/v_register.php', $data);
    }

    public function doSignUp() {
        // get data from web
        $regUsername    = $this->request->getVar('regUsername');
        $regFullname    = $this->request->getVar('regFullname');
        $regPassword    = $this->request->getVar('regPassword');
        $regConfirmPassword = $this->request->getVar('regConfirmPassword');
        $regPosition    = $this->request->getVar('regPosition');
        $regTelp        = $this->request->getVar('regTelp');
        $regDept        = $this->request->getVar('regDept');
        $regDeptHandle  = $this->request->getVar('regDeptHandle');
        $regEmail       = $this->request->getVar('regEmail');
        $regLevel       = 5;   
        $regStatus      = 3;
        $regAddress     = $this->request->getVar('regAddress');

        // general validation
        if(!$this->validate([
            'regUsername'   => 'required|is_unique[user.user_username]',
            'regFullname'   => 'required',
            'regPassword'   => 'required|min_length[5]',
            'regConfirmPassword'    => 'required|matches[regPassword]',
            'regPosition'   => 'required',
            'regTelp'       => 'required',
            'regDept'       => 'required',
            'regDeptHandle' => 'required',
            'regEmail'      => 'required',
            'regAddress'    => 'required',
        ], [    
            'regUsername'  => [
                'required'  => 'Value is required !',
                'is_unique' => 'Value is already exists !'
            ],
            'regFullname'   => [
                'required'  => 'Value is required !',
            ],  
            'regPassword'   => [
                'required'  => 'Value is required !',
                'min_length' => 'Min 5 character !'
            ],
            'regConfirmPassword'    => [
                'required'  => 'Value is required !',
                'matches'   => 'Must be same with password !'
            ],
            'regPosition'   => [
                'required'  => 'Value is required !'
            ],
            'regTelp'   => [
                'required'  => 'Value is required !'
            ],
            'regDept'   => [
                'required'  => 'Value is required !'
            ],
            'regDeptHandle'    => [
                'required'  => 'Value is required !'
            ],
            'regEmail'  => [
                'required'  => 'Value is required !'
            ],
            'regAddress'    => [
                'required'  => 'Value is required !'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $this->session->setFlashdata('error_message', '<strong>Failed !</strong> register new user, please check & try again !');

            // ngirim data dept handle buat old di modals
            $regDeptHandle = implode(',',$regDeptHandle);
            $this->session->setFlashdata('regDeptHandle',$regDeptHandle);
            return redirect()->to('/signUp')->withInput()->with('validation', $validation);
        } else {

            // encrypt password
            $regPassword = password_hash($regPassword, PASSWORD_DEFAULT);
            $regDeptHandle = implode(',', $regDeptHandle);

            // masukan ke database
            $this->models->registerUser($regUsername, $regFullname, $regPassword, $regPosition, $regTelp, $regDept, $regDeptHandle, $regEmail, $regLevel, $regStatus, $regAddress);

            // set session sukses
            $this->session->setFlashdata('success_message', '<strong>Successfully !!</strong> register new user, thanks..');
            return redirect()->to('/');
        }
    }

    public function signOut() {
        $this->session->destroy();

        $this->session->setFlashdata('success_message', '<strong>Successfully !</strong> Sign out !, have a nice day...');
        return redirect()->to('/');
    }

    public function changePassword() {
        $userId = $this->request->getVar('changePasswordId');
        $oldPassword = $this->request->getVar('changePasswordOld');
        $newPassword = $this->request->getVar('changePasswordNew');
        $ConfirmationPassword = $this->request->getVar('changePasswordConfirmation');

        // dd($userId, $oldPassword, $newPassword, $ConfirmationPassword);

        // general rule
        if(!$this->validate([
            'changePasswordId'  => 'required',
            'changePasswordOld' => 'required',
            'changePasswordNew' => 'required|min_length[5]',
            'changePasswordConfirmation'    => 'matches[changePasswordNew]'
        ], [
            'changePasswordOld' => [
                'required'  => 'Must be filled !'
            ],
            'changePasswordNew' => [
                'required'  => 'Must be filled !',
                'min_length'    => "Min 5 Character !"
            ],
            'changePasswordConfirmation'    => [
                'matches'   => 'Confirmation password not same with new password !'
            ]
        ])) {
            // return failed
            $validation = \Config\Services::validation();
            $this->session->setFlashdata('error_message', '<strong>Failed !!</strong> Change password ! Please check try again...');
            return redirect()->to('/dashboard')->withInput()->with('validation', $validation);
        } else {
            // sukses validasi
            $getOldData = $this->models->getUserData($userId);

            if(password_verify($oldPassword, $getOldData['user_password'])) {
                if(!password_verify($newPassword, $getOldData['user_password'])) {
                    
                    // update password
                    // encrypt password
                    $encPass = password_hash($ConfirmationPassword, PASSWORD_DEFAULT);

                    $this->models->updatePassword($userId, $encPass);

                    // set sukses
                    $this->session->setFlashdata('success_message', '<strong>Successfully !!</strong> Change password ! Thanks...');
                    $this->session->destroy();
                    return redirect()->to('/');
                } else {
                    // return failed
                    $validation = \Config\Services::validation();
                    $this->session->setFlashdata('error_message', '<strong>Failed !!</strong> Password must be different from the old one ! Please check try again...');
                    return redirect()->to('/dashboard')->withInput()->with('validation', $validation);
                }
            } else {
                // return failed
                $validation = \Config\Services::validation();
                $this->session->setFlashdata('error_message', '<strong>Failed !!</strong> Wrong old password ! Please check try again...');
                return redirect()->to('/dashboard')->withInput()->with('validation', $validation);
            }
        } 
    }
}
