<?php

namespace App\Controllers\log;
use App\Controllers\BaseController;
use Config\Services;
use App\Models\warehouse\models;
// use App\Models\models;

class c_assemblyLog extends BaseController
{

    protected $table = 'assembly_log';
    protected $database = 'log';
    protected $column_order = [];
    protected $column_search = ['import_code', 'import_assembly_filename', 'al.assembly_code'];
    protected $order = ['import_assembly_id  ' => 'DESC'];
    
    public function __construct() {
        $this->request = Services::request();
        $this->models = new models($this->request, $this->table, $this->column_order, $this->column_search, $this->order, $this->database, 'assemblyLog');
        $this->session = session();
    }

    public function index()
    {  

        if(!$this->session->get('login')) {
            return redirect()->to('/');
        }
        
        $data = [
            'validation'    => \Config\Services::validation(),
            // add new main product
            'multipleUom'   => $this->models->getAllDataMultipleUom(),
            'uomSchema'     => $this->models->getAllDataUomSchema(),
            'currency'      => $this->models->getAllDataCurrency(),
            'status'        => $this->models->getAllDataStatus(),
            // add new user management
            'dept'          => $this->models->getAllDataDept(),
            'level'         => $this->models->getAllDataLevel(),
            // add new assembly
            'whs'           => $this->models->getAllDataWhs(),
        ];

        return view('log/v_assemblyLog.php', $data); 
    }

    public function ajaxList()
    {
        if ($this->request->getMethod(true) === 'POST') {
            $lists = $this->models->getDatatables();
            $data = [];
            $no = $this->request->getPost('start');

            foreach ($lists as $list) {
                $no++;
                $status = $list->assembly_status == 1 ? 'text-success' : 'text-danger';
                $row = [];
                $row[] = $no;
                $row[] = "<a class='fas fa-file-export' href='c_assemblyLog/exportAssemblyLog/$list->import_code'></a>";
                $row[] = "<p class='fw-bold text-primary'>$list->import_code</p>";
                $row[] = $list->import_assembly_filename;
                $row[] = "<p class='fw-bold text-info'>$list->user_fullname</p><small>$list->import_assembly_create</small>";
                $row[] = "<p class='fw-bold text-primary'>$list->assembly_code</p><small>$list->assembly_name</small>";
                $row[] = "<p class='fw-bold text-primary'>$list->assembly_category</p><small>$list->assembly_type</small>";
                $row[] = $list->assembly_description;
                $row[] = $list->assembly_uom;
                $row[] = $list->currency_name;
                $row[] = floatval($list->assembly_unit_price);
                $row[] = $list->assembly_item_spec;
                $row[] = $list->assembly_packaging;
                $row[] = "<p class='fw-bold $status'>$list->status_name</p>";
                $data[] = $row;
            }

            $output = [
                'draw' => $this->request->getPost('draw'),
                'recordsTotal' => $this->models->countAll($this->database),
                'recordsFiltered' => $this->models->countFiltered(),
                'data' => $data
            ];

            echo json_encode($output);
        }
    }

    public function exportAssemblyLog($importCode) {
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $dataTemplate = $this->models->getDataAssemblyLog($importCode);

        $sheet->setCellValue('A1', 'Log Code');
        $sheet->setCellValue('B1', 'Filename');
        $sheet->setCellValue('C1', 'Created By');
        $sheet->setCellValue('D1', 'Created At');
        $sheet->setCellValue('E1', 'Assembly Code');
        $sheet->setCellValue('F1', 'Assembly Name');
        $sheet->setCellValue('G1', 'Category');
        $sheet->setCellValue('H1', 'Assembly Type');
        $sheet->setCellValue('I1', 'Description');
        $sheet->setCellValue('J1', 'Uom');
        $sheet->setCellValue('K1', 'Currency');
        $sheet->setCellValue('L1', 'Unit Price');
        $sheet->setCellValue('M1', 'Item Spec');
        $sheet->setCellValue('N1', 'Packaging');
        $sheet->setCellValue('O1', 'Status');
        $rows = 2;

        foreach ($dataTemplate as $dt){
            $sheet->setCellValue('A'.$rows, $dt['import_code']);
            $sheet->setCellValue('B'.$rows, $dt['import_assembly_filename']);
            $sheet->setCellValue('C'.$rows, $dt['user_fullname']);
            $sheet->setCellValue('D'.$rows, $dt['import_assembly_create']);
            $sheet->setCellValue('E'.$rows, $dt['assembly_code']);
            $sheet->setCellValue('F'.$rows, $dt['assembly_name']);
            $sheet->setCellValue('G'.$rows, $dt['assembly_category']);
            $sheet->setCellValue('H'.$rows, $dt['assembly_type']);
            $sheet->setCellValue('I'.$rows, $dt['assembly_description']);
            $sheet->setCellValue('J'.$rows, $dt['assembly_uom']);
            $sheet->setCellValue('K'.$rows, $dt['currency_name']);
            $sheet->setCellValue('L'.$rows, $dt['assembly_unit_price']);
            $sheet->setCellValue('M'.$rows, $dt['assembly_item_spec']);
            $sheet->setCellValue('N'.$rows, $dt['assembly_packaging']);
            $sheet->setCellValue('O'.$rows, $dt['status_name']);
            $rows++;
        }

        // buat excelnya, fyi inisialisasi spreadsheet itu buat file excel kosong baru dan writer itu mengisi file kosong itu dengan data diatas
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $date = date('j M Y');
        $fileName = 'assembly report log - ' . $dataTemplate[0]['import_code'];
    
        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName.'.xlsx');
        header('Cache-Control: max-age=0');
    
        // auto download disini, gaperlu dibalikin ke redirect lagi
        $writer->save('php://output');

        // return redirect()->to('/warehouse/products');
    }
}
