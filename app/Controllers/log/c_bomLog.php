<?php

namespace App\Controllers\log;
use App\Controllers\BaseController;
use Config\Services;
use App\Models\warehouse\models;
// use App\Models\models;

class c_bomLog extends BaseController
{

    protected $table = 'bom_log';
    protected $database = 'log';
    protected $column_order = [];
    protected $column_search = ['bl.bom_code', 'import_bom_filename', 'import_product_code', 'import_assembly_code'];
    protected $order = ['import_bom_id' => 'DESC'];
    
    public function __construct() {
        $this->request = Services::request();
        $this->models = new models($this->request, $this->table, $this->column_order, $this->column_search, $this->order, $this->database, 'bomLog');
        $this->session = session();
    }

    public function index()
    {  

        if(!$this->session->get('login')) {
            return redirect()->to('/');
        }
        
        $data = [
            'validation'    => \Config\Services::validation(),
            // add new main product
            'multipleUom'   => $this->models->getAllDataMultipleUom(),
            'uomSchema'     => $this->models->getAllDataUomSchema(),
            'currency'      => $this->models->getAllDataCurrency(),
            'status'        => $this->models->getAllDataStatus(),
            // add new user management
            'dept'          => $this->models->getAllDataDept(),
            'level'         => $this->models->getAllDataLevel(),
            // add new assembly
            'whs'           => $this->models->getAllDataWhs(),
        ];

        return view('log/v_bomLog.php', $data); 
    }

    public function ajaxList()
    {
        if ($this->request->getMethod(true) === 'POST') {
            $lists = $this->models->getDatatables();
            $data = [];
            $no = $this->request->getPost('start');

            foreach ($lists as $list) {
                $no++;
                $row = [];
                $row[] = $no;
                $row[] = "<a class='fas fa-file-export' href='c_bomLog/exportBomLog/$list->bom_code'></a>";
                $row[] = "<p class='fw-bold text-primary'>$list->bom_code</p>";
                $row[] = $list->import_bom_filename;
                $row[] = "<p class='fw-bold text-success'>$list->createBy</p>";
                $row[] = "<p class='fw-bold text-info'>$list->lastUpdateBy</p><small>$list->import_bom_create</small>";
                $row[] = "<p class='fw-bold text-dark'>$list->import_assembly_code</p>";
                $row[] = "<p class='fw-bold text-primary'>$list->import_product_code</p><small>$list->product_name</small>";
                $row[] = $list->bom_type;
                $row[] = floatval($list->bom_quantity_needed);
                $data[] = $row;
            }

            $output = [
                'draw' => $this->request->getPost('draw'),
                'recordsTotal' => $this->models->countAll($this->database),
                'recordsFiltered' => $this->models->countFiltered(),
                'data' => $data
            ];

            echo json_encode($output);
        }
    }

    public function exportBomLog($bomCode) {
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $dataTemplate = $this->models->getDataBomLog($bomCode);

        $sheet->setCellValue('A1', 'BOM Code');
        $sheet->setCellValue('B1', 'Filename');
        $sheet->setCellValue('C1', 'Created By');
        $sheet->setCellValue('D1', 'Last Updated By');
        $sheet->setCellValue('E1', 'Last Updated At');
        $sheet->setCellValue('F1', 'Assembly Code');
        $sheet->setCellValue('G1', 'Product Code');
        $sheet->setCellValue('H1', 'Product Name');
        $sheet->setCellValue('I1', 'BOM Type');
        $sheet->setCellValue('J1', 'Quantity Required');
        $rows = 2;

        foreach ($dataTemplate as $dt){
            $sheet->setCellValue('A'.$rows, $dt['bom_code']);
            $sheet->setCellValue('B'.$rows, $dt['import_bom_filename']);
            $sheet->setCellValue('C'.$rows, $dt['createBy']);
            $sheet->setCellValue('D'.$rows, $dt['lastUpdateBy']);
            $sheet->setCellValue('E'.$rows, $dt['import_bom_create']);
            $sheet->setCellValue('F'.$rows, $dt['import_assembly_code']);
            $sheet->setCellValue('G'.$rows, $dt['import_product_code']);
            $sheet->setCellValue('H'.$rows, $dt['product_name']);
            $sheet->setCellValue('I'.$rows, $dt['bom_type']);
            $sheet->setCellValue('J'.$rows, $dt['bom_quantity_needed']);
            $rows++;
        }

        // buat excelnya, fyi inisialisasi spreadsheet itu buat file excel kosong baru dan writer itu mengisi file kosong itu dengan data diatas
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $date = date('j M Y');
        $fileName = 'bom report log - ' . $dataTemplate[0]['bom_code'];
    
        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName.'.xlsx');
        header('Cache-Control: max-age=0');
    
        // auto download disini, gaperlu dibalikin ke redirect lagi
        $writer->save('php://output');

        // return redirect()->to('/warehouse/products');
    }
}
