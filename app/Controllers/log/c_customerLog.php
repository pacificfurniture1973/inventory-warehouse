<?php

namespace App\Controllers\log;
use App\Controllers\BaseController;
use Config\Services;
use App\Models\warehouse\models;
// use App\Models\models;

class c_customerLog extends BaseController
{

    protected $table = 'customer_log';
    protected $database = 'log';
    protected $column_order = [];
    protected $column_search = ['import_code', 'import_customer_filename', 'cl.customer_code'];
    protected $order = ['import_customer_id   ' => 'DESC'];
    
    public function __construct() {
        $this->request = Services::request();
        $this->models = new models($this->request, $this->table, $this->column_order, $this->column_search, $this->order, $this->database, 'customerLog');
        $this->session = session();
    }

    public function index()
    {  

        if(!$this->session->get('login')) {
            return redirect()->to('/');
        }
        
        $data = [
            'validation'    => \Config\Services::validation(),
            // add new main product
            'multipleUom'   => $this->models->getAllDataMultipleUom(),
            'uomSchema'     => $this->models->getAllDataUomSchema(),
            'currency'      => $this->models->getAllDataCurrency(),
            'status'        => $this->models->getAllDataStatus(),
            // add new user management
            'dept'          => $this->models->getAllDataDept(),
            'level'         => $this->models->getAllDataLevel(),
            // add new assembly
            'whs'           => $this->models->getAllDataWhs(),
        ];

        return view('log/v_customerLog.php', $data); 
    }

    public function ajaxList()
    {
        if ($this->request->getMethod(true) === 'POST') {
            $lists = $this->models->getDatatables();
            $data = [];
            $no = $this->request->getPost('start');

            foreach ($lists as $list) {
                $no++;
                $status = $list->customer_status == 1 ? 'text-success' : 'text-danger';
                $row = [];
                $row[] = $no;
                $row[] = "<a class='fas fa-file-export' href='c_customerLog/exportCustomerLog/$list->import_code'></a>";
                $row[] = "<p class='fw-bold text-primary'>$list->import_code</p>";
                $row[] = $list->import_customer_filename;
                $row[] = "<p class='fw-bold text-info'>$list->lastUpdateBy</p><small>$list->import_customer_create</small>";
                $row[] = "<p class='fw-bold text-primary'>$list->customer_code</p><small>$list->customer_agency</small>";
                $row[] = $list->customer_address;
                $row[] = "<p class='fw-bold text-primary'>$list->customer_name</p><small>$list->customer_email</small>";
                $row[] = $list->customer_telp;
                $row[] = "<p class='fw-bold text-primary'>$list->customer_bank_account</p><small>$list->customer_bank_name</small>";
                $row[] = $list->customer_bank_number;
                $row[] = $list->customer_credit_term_default;
                $row[] = "<p class='fw-bold $status'>$list->status_name</p>";
                $data[] = $row;
            }

            $output = [
                'draw' => $this->request->getPost('draw'),
                'recordsTotal' => $this->models->countAll($this->database),
                'recordsFiltered' => $this->models->countFiltered(),
                'data' => $data
            ];

            echo json_encode($output);
        }
    }
    
    public function exportCustomerLog($importCode) {
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $dataTemplate = $this->models->getDataCustomerLog($importCode);

        $sheet->setCellValue('A1', 'Log Code');
        $sheet->setCellValue('B1', 'Filename');
        $sheet->setCellValue('C1', 'Created By');
        $sheet->setCellValue('D1', 'Created At');
        $sheet->setCellValue('E1', 'Customer Code');
        $sheet->setCellValue('F1', 'Customer Agency');
        $sheet->setCellValue('G1', 'Address');
        $sheet->setCellValue('H1', 'Customer Name');
        $sheet->setCellValue('I1', 'Email');
        $sheet->setCellValue('J1', 'Contact');
        $sheet->setCellValue('K1', 'Bank Name');
        $sheet->setCellValue('L1', 'Bank Account');
        $sheet->setCellValue('M1', 'Bank Number');
        $sheet->setCellValue('N1', 'Credit Term');
        $sheet->setCellValue('O1', 'Status');
        $rows = 2;

        foreach ($dataTemplate as $dt){
            $sheet->setCellValue('A'.$rows, $dt['import_code']);
            $sheet->setCellValue('B'.$rows, $dt['import_customer_filename']);
            $sheet->setCellValue('C'.$rows, $dt['lastUpdateBy']);
            $sheet->setCellValue('D'.$rows, $dt['import_customer_create']);
            $sheet->setCellValue('E'.$rows, $dt['customer_code']);
            $sheet->setCellValue('F'.$rows, $dt['customer_agency']);
            $sheet->setCellValue('G'.$rows, $dt['customer_address']);
            $sheet->setCellValue('H'.$rows, $dt['customer_name']);
            $sheet->setCellValue('I'.$rows, $dt['customer_email']);
            $sheet->setCellValue('J'.$rows, $dt['customer_telp']);
            $sheet->setCellValue('K'.$rows, $dt['customer_bank_name']);
            $sheet->setCellValue('L'.$rows, $dt['customer_bank_account']);
            $sheet->setCellValue('M'.$rows, $dt['customer_bank_number']);
            $sheet->setCellValue('N'.$rows, $dt['customer_credit_term_default']);
            $sheet->setCellValue('O'.$rows, $dt['status_name']);
            $rows++;
        }

        // buat excelnya, fyi inisialisasi spreadsheet itu buat file excel kosong baru dan writer itu mengisi file kosong itu dengan data diatas
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $date = date('j M Y');
        $fileName = 'customer report log - ' . $dataTemplate[0]['import_code'];
    
        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName.'.xlsx');
        header('Cache-Control: max-age=0');
    
        // auto download disini, gaperlu dibalikin ke redirect lagi
        $writer->save('php://output');

        // return redirect()->to('/warehouse/products');
    }
}
