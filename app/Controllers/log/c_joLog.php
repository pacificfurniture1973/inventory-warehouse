<?php

namespace App\Controllers\log;
use App\Controllers\BaseController;
use Config\Services;
use App\Models\warehouse\models;
// use App\Models\models;

class c_joLog extends BaseController
{

    protected $table = 'job_order_log';
    protected $database = 'log';
    protected $column_order = [];
    protected $column_search = ['import_code', 'jo_filename', 'jl.jo_number', 'jl.jo_assembly_code'];
    protected $order = ['jo_id' => 'DESC'];
    
    public function __construct() {
        $this->request = Services::request();
        $this->models = new models($this->request, $this->table, $this->column_order, $this->column_search, $this->order, $this->database, 'joLog');
        $this->session = session();
    }

    public function index()
    {  

        if(!$this->session->get('login')) {
            return redirect()->to('/');
        }
        
        $data = [
            'validation'    => \Config\Services::validation(),
            // add new main product
            'multipleUom'   => $this->models->getAllDataMultipleUom(),
            'uomSchema'     => $this->models->getAllDataUomSchema(),
            'currency'      => $this->models->getAllDataCurrency(),
            'status'        => $this->models->getAllDataStatus(),
            // add new user management
            'dept'          => $this->models->getAllDataDept(),
            'level'         => $this->models->getAllDataLevel(),
            // add new assembly
            'whs'           => $this->models->getAllDataWhs(),
        ];

        return view('log/v_joLog.php', $data); 
    }

    public function ajaxList()
    {
        if ($this->request->getMethod(true) === 'POST') {
            $lists = $this->models->getDatatables();
            $data = [];
            $no = $this->request->getPost('start');

            foreach ($lists as $list) {
                $no++;
                $row = [];
                $row[] = $no;
                $row[] = "<a class='fas fa-file-export' href='c_joLog/exportJOLog/$list->import_code'></a>";
                $row[] = "<p class='fw-bold text-primary'>$list->import_code</p>";
                $row[] = $list->jo_filename;
                $row[] = "<p class='fw-bold text-info'>$list->user_fullname</p><small>$list->jo_create</small>";
                $row[] = "<p class='fw-bold text-success'>$list->jo_number</p>";
                $row[] = "<p class='fw-bold text-warning'>$list->jo_assembly_code</p>";
                $row[] = "<p class='fw-bold text-primary'>$list->customer_agency - $list->jo_customer_code</p><small>$list->jo_customer_po</small>";
                $row[] = "<p class='text-dark'>Date: $list->jo_date</p><p class='fw-bold text-dark'>Due Date: $list->jo_due_date</p>";
                $row[] = "<p>Quantity: $list->jo_quantity</p><p>Balance Qty: $list->jo_balance_quantity</p>";
                $row[] = "<p class='fw-bold text-dark'>$list->currency_name</p>";
                $row[] = $list->jo_memo;
                $row[] = $list->jo_discount;
                $row[] = $list->jo_amount;
                $row[] = $list->jo_model_code;
                $data[] = $row;
            }

            $output = [
                'draw' => $this->request->getPost('draw'),
                'recordsTotal' => $this->models->countAll($this->database),
                'recordsFiltered' => $this->models->countFiltered(),
                'data' => $data
            ];

            echo json_encode($output);
        }
    }

    public function exportJOLog($importCode) {
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $dataTemplate = $this->models->getDataJOLog($importCode);

        $sheet->setCellValue('A1', 'Log Code');
        $sheet->setCellValue('B1', 'Filename');
        $sheet->setCellValue('C1', 'Created By');
        $sheet->setCellValue('D1', 'Created At');
        $sheet->setCellValue('E1', 'JO Number');
        $sheet->setCellValue('F1', 'Assembly Code');
        $sheet->setCellValue('G1', 'Customer Code');
        $sheet->setCellValue('H1', 'Customer Name');
        $sheet->setCellValue('I1', 'JO Date');
        $sheet->setCellValue('J1', 'JO Due Date');
        $sheet->setCellValue('K1', 'Quantity');
        $sheet->setCellValue('L1', 'Balance Quantity');
        $sheet->setCellValue('M1', 'Currency');
        $sheet->setCellValue('N1', 'Memo');
        $sheet->setCellValue('O1', 'Discont');
        $sheet->setCellValue('P1', 'Amount');
        $sheet->setCellValue('Q1', 'Model Code');
        $rows = 2;

        foreach ($dataTemplate as $dt){
            $sheet->setCellValue('A'.$rows, $dt['import_code']);
            $sheet->setCellValue('B'.$rows, $dt['jo_filename']);
            $sheet->setCellValue('C'.$rows, $dt['user_fullname']);
            $sheet->setCellValue('D'.$rows, $dt['jo_create']);
            $sheet->setCellValue('E'.$rows, $dt['jo_number']);
            $sheet->setCellValue('F'.$rows, $dt['jo_assembly_code']);
            $sheet->setCellValue('G'.$rows, $dt['jo_customer_code']);
            $sheet->setCellValue('H'.$rows, $dt['customer_agency']);
            $sheet->setCellValue('I'.$rows, $dt['jo_date']);
            $sheet->setCellValue('J'.$rows, $dt['jo_due_date']);
            $sheet->setCellValue('K'.$rows, $dt['jo_quantity']);
            $sheet->setCellValue('L'.$rows, $dt['jo_balance_quantity']);
            $sheet->setCellValue('M'.$rows, $dt['currency_name']);
            $sheet->setCellValue('N'.$rows, $dt['jo_memo']);
            $sheet->setCellValue('O'.$rows, $dt['jo_discount']);
            $sheet->setCellValue('P'.$rows, $dt['jo_amount']);
            $sheet->setCellValue('Q'.$rows, $dt['jo_model_code']);
            $rows++;
        }

        // buat excelnya, fyi inisialisasi spreadsheet itu buat file excel kosong baru dan writer itu mengisi file kosong itu dengan data diatas
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $date = date('j M Y');
        $fileName = 'jo report log - ' . $dataTemplate[0]['import_code'];
    
        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName.'.xlsx');
        header('Cache-Control: max-age=0');
    
        // auto download disini, gaperlu dibalikin ke redirect lagi
        $writer->save('php://output');

        // return redirect()->to('/warehouse/products');
    }
}
