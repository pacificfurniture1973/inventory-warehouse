<?php

namespace App\Controllers\log;
use App\Controllers\BaseController;
use Config\Services;
use App\Models\warehouse\models;
// use App\Models\models;

class c_productsLog extends BaseController
{

    protected $table = 'product_log';
    protected $database = 'log';
    protected $column_order = [];
    protected $column_search = ['import_code', 'import_product_filename', 'pl.product_code'];
    protected $order = ['import_product_id ' => 'DESC'];
    
    public function __construct() {
        $this->request = Services::request();
        $this->models = new models($this->request, $this->table, $this->column_order, $this->column_search, $this->order, $this->database, 'productsLog');
        $this->session = session();
    }

    public function index()
    {  

        if(!$this->session->get('login')) {
            return redirect()->to('/');
        }
        
        $data = [
            'validation'    => \Config\Services::validation(),
            // add new main product
            'multipleUom'   => $this->models->getAllDataMultipleUom(),
            'uomSchema'     => $this->models->getAllDataUomSchema(),
            'currency'      => $this->models->getAllDataCurrency(),
            'status'        => $this->models->getAllDataStatus(),
            // add new user management
            'dept'          => $this->models->getAllDataDept(),
            'level'         => $this->models->getAllDataLevel(),
            // add new assembly
            'whs'           => $this->models->getAllDataWhs(),
        ];

        return view('log/v_productsLog.php', $data); 
    }

    public function ajaxList()
    {
        if ($this->request->getMethod(true) === 'POST') {
            $lists = $this->models->getDatatables();
            $data = [];
            $no = $this->request->getPost('start');

            foreach ($lists as $list) {
                $no++;
                $status = $list->product_status == 1 ? 'text-success' : 'text-danger';
                $row = [];
                $row[] = $no;
                $row[] = "<a class='fas fa-file-export' href='c_productsLog/exportProductsLog/$list->import_code'></a>";
                $row[] = "<p class='fw-bold text-primary'>$list->import_code</p>";
                $row[] = $list->import_product_filename;
                $row[] = "<p class='fw-bold text-info'>$list->user_fullname</p><small>$list->import_product_create</small>";
                $row[] = "<p class='fw-bold text-primary'>$list->product_code</p><small>$list->product_name</small>";
                $row[] = "<p class='fw-bold text-primary'>$list->product_category</p><small>$list->product_type</small>";
                $row[] = $list->product_description;
                $row[] = $list->product_uom;
                $row[] = $list->currency_name;
                $row[] = floatval($list->product_unit_price);
                $row[] = $list->multiple_uom_name;
                $row[] = $list->uom_schema_name;
                $row[] = "<p class='fw-bold $status'>$list->status_name</p>";
                $data[] = $row;
            }

            $output = [
                'draw' => $this->request->getPost('draw'),
                'recordsTotal' => $this->models->countAll($this->database),
                'recordsFiltered' => $this->models->countFiltered(),
                'data' => $data
            ];

            echo json_encode($output);
        }
    }

    public function exportProductsLog($importCode) {
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $dataTemplate = $this->models->getDataProductsLog($importCode);

        $sheet->setCellValue('A1', 'Log Code');
        $sheet->setCellValue('B1', 'Filename');
        $sheet->setCellValue('C1', 'Created By');
        $sheet->setCellValue('D1', 'Created At');
        $sheet->setCellValue('E1', 'Product Code');
        $sheet->setCellValue('F1', 'Product Name');
        $sheet->setCellValue('G1', 'Category');
        $sheet->setCellValue('H1', 'Description');
        $sheet->setCellValue('I1', 'Uom');
        $sheet->setCellValue('J1', 'Currency');
        $sheet->setCellValue('K1', 'Unit Price');
        $sheet->setCellValue('L1', 'Multiple Uom');
        $sheet->setCellValue('M1', 'Uom Schema');
        $sheet->setCellValue('N1', 'Status');
        $rows = 2;

        foreach ($dataTemplate as $dt){
            $sheet->setCellValue('A'.$rows, $dt['import_code']);
            $sheet->setCellValue('B'.$rows, $dt['import_product_filename']);
            $sheet->setCellValue('C'.$rows, $dt['user_fullname']);
            $sheet->setCellValue('D'.$rows, $dt['import_product_create']);
            $sheet->setCellValue('E'.$rows, $dt['product_code']);
            $sheet->setCellValue('F'.$rows, $dt['product_name']);
            $sheet->setCellValue('G'.$rows, $dt['product_category']);
            $sheet->setCellValue('H'.$rows, $dt['product_description']);
            $sheet->setCellValue('I'.$rows, $dt['product_uom']);
            $sheet->setCellValue('J'.$rows, $dt['currency_name']);
            $sheet->setCellValue('K'.$rows, $dt['product_unit_price']);
            $sheet->setCellValue('L'.$rows, $dt['product_multiple_uom']);
            $sheet->setCellValue('M'.$rows, $dt['product_uom_schema']);
            $sheet->setCellValue('N'.$rows, $dt['status_name']);
            $rows++;
        }

        // buat excelnya, fyi inisialisasi spreadsheet itu buat file excel kosong baru dan writer itu mengisi file kosong itu dengan data diatas
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $date = date('j M Y');
        $fileName = 'products report log - ' . $dataTemplate[0]['import_code'];
    
        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName.'.xlsx');
        header('Cache-Control: max-age=0');
    
        // auto download disini, gaperlu dibalikin ke redirect lagi
        $writer->save('php://output');

        // return redirect()->to('/warehouse/products');
    }
}
