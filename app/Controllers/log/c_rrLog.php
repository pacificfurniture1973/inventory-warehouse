<?php

namespace App\Controllers\log;
use App\Controllers\BaseController;
use Config\Services;
use App\Models\warehouse\models;
// use App\Models\models;

class c_rrLog extends BaseController
{

    protected $table = 'receivement_report_log';
    protected $database = 'log';
    protected $column_order = [];
    protected $column_search = ['import_code', 'rr_filename', 'rl.rr_number', 'rl.rr_vendor_code'];
    protected $order = ['rr_id' => 'DESC'];
    
    public function __construct() {
        $this->request = Services::request();
        $this->models = new models($this->request, $this->table, $this->column_order, $this->column_search, $this->order, $this->database, 'rrLog');
        $this->session = session();
    }

    public function index()
    {  

        if(!$this->session->get('login')) {
            return redirect()->to('/');
        }
        
        $data = [
            'validation'    => \Config\Services::validation(),
            // add new main product
            'multipleUom'   => $this->models->getAllDataMultipleUom(),
            'uomSchema'     => $this->models->getAllDataUomSchema(),
            'currency'      => $this->models->getAllDataCurrency(),
            'status'        => $this->models->getAllDataStatus(),
            // add new user management
            'dept'          => $this->models->getAllDataDept(),
            'level'         => $this->models->getAllDataLevel(),
            // add new assembly
            'whs'           => $this->models->getAllDataWhs(),
        ];

        return view('log/v_rrLog.php', $data); 
    }

    public function ajaxList()
    {
        if ($this->request->getMethod(true) === 'POST') {
            $lists = $this->models->getDatatables();
            $data = [];
            $no = $this->request->getPost('start');

            foreach ($lists as $list) {
                $no++;
                $row = [];
                $row[] = $no;
                $row[] = "<a class='fas fa-file-export' href='c_rrLog/exportRRLog/$list->import_code'></a>";
                $row[] = "<p class='fw-bold text-primary'>$list->import_code</p>";
                $row[] = $list->rr_filename;
                $row[] = "<p class='fw-bold text-info'>$list->user_fullname</p><small>$list->rr_created</small>";
                $row[] = "<p class='fw-bold text-success'>$list->rr_number</p><small>$list->rr_date</small>";
                $row[] = "<p class='fw-bold text-primary'>$list->rr_product_code</p><small>$list->rr_product_name</small>";
                $row[] = "<p class='fw-bold text-primary'>$list->rr_vendor_code</p><small>$list->vendor_name</small>";
                $row[] = $list->rr_quantity;
                $data[] = $row;
            }

            $output = [
                'draw' => $this->request->getPost('draw'),
                'recordsTotal' => $this->models->countAll($this->database),
                'recordsFiltered' => $this->models->countFiltered(),
                'data' => $data
            ];

            echo json_encode($output);
        }
    }

    public function exportRRLog($importCode) {
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $dataTemplate = $this->models->getDataRRLog($importCode);

        $sheet->setCellValue('A1', 'Log Code');
        $sheet->setCellValue('B1', 'Filename');
        $sheet->setCellValue('C1', 'Created By');
        $sheet->setCellValue('D1', 'Created At');
        $sheet->setCellValue('E1', 'RR Number');
        $sheet->setCellValue('F1', 'RR Date');
        $sheet->setCellValue('G1', 'Product Code');
        $sheet->setCellValue('H1', 'Product Name');
        $sheet->setCellValue('I1', 'Vendor Code');
        $sheet->setCellValue('J1', 'Vendor Name');
        $sheet->setCellValue('K1', 'Quantity');
        $rows = 2;

        foreach ($dataTemplate as $dt){
            $sheet->setCellValue('A'.$rows, $dt['import_code']);
            $sheet->setCellValue('B'.$rows, $dt['rr_filename']);
            $sheet->setCellValue('C'.$rows, $dt['user_fullname']);
            $sheet->setCellValue('D'.$rows, $dt['rr_created']);
            $sheet->setCellValue('E'.$rows, $dt['rr_number']);
            $sheet->setCellValue('F'.$rows, $dt['rr_date']);
            $sheet->setCellValue('G'.$rows, $dt['rr_product_code']);
            $sheet->setCellValue('H'.$rows, $dt['rr_product_name']);
            $sheet->setCellValue('I'.$rows, $dt['rr_vendor_code']);
            $sheet->setCellValue('J'.$rows, $dt['vendor_name']);
            $sheet->setCellValue('K'.$rows, $dt['rr_quantity']);
            $rows++;
        }

        // buat excelnya, fyi inisialisasi spreadsheet itu buat file excel kosong baru dan writer itu mengisi file kosong itu dengan data diatas
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $date = date('j M Y');
        $fileName = 'receivement report log - ' . $dataTemplate[0]['import_code'];
    
        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName.'.xlsx');
        header('Cache-Control: max-age=0');
    
        // auto download disini, gaperlu dibalikin ke redirect lagi
        $writer->save('php://output');

        // return redirect()->to('/warehouse/products');
    }
}
