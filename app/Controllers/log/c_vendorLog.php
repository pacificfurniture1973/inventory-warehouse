<?php

namespace App\Controllers\log;
use App\Controllers\BaseController;
use Config\Services;
use App\Models\warehouse\models;
// use App\Models\models;

class c_vendorLog extends BaseController
{

    protected $table = 'vendor_log';
    protected $database = 'log';
    protected $column_order = [];
    protected $column_search = ['import_code', 'import_vendor_filename', 'vl.vendor_code'];
    protected $order = ['import_customer_id   ' => 'DESC'];
    
    public function __construct() {
        $this->request = Services::request();
        $this->models = new models($this->request, $this->table, $this->column_order, $this->column_search, $this->order, $this->database, 'vendorLog');
        $this->session = session();
    }

    public function index()
    {  

        if(!$this->session->get('login')) {
            return redirect()->to('/');
        }
        
        $data = [
            'validation'    => \Config\Services::validation(),
            // add new main product
            'multipleUom'   => $this->models->getAllDataMultipleUom(),
            'uomSchema'     => $this->models->getAllDataUomSchema(),
            'currency'      => $this->models->getAllDataCurrency(),
            'status'        => $this->models->getAllDataStatus(),
            // add new user management
            'dept'          => $this->models->getAllDataDept(),
            'level'         => $this->models->getAllDataLevel(),
            // add new assembly
            'whs'           => $this->models->getAllDataWhs(),
        ];

        return view('log/v_vendorLog.php', $data); 
    }

    public function ajaxList()
    {
        if ($this->request->getMethod(true) === 'POST') {
            $lists = $this->models->getDatatables();
            $data = [];
            $no = $this->request->getPost('start');

            foreach ($lists as $list) {
                $no++;
                $status = $list->vendor_status == 1 ? 'text-success' : 'text-danger';
                $row = [];
                $row[] = $no;
                $row[] = "<a class='fas fa-file-export' href='c_vendorLog/exportVendorLog/$list->import_code'></a>";
                $row[] = "<p class='fw-bold text-primary'>$list->import_code</p>";
                $row[] = $list->import_vendor_filename;
                $row[] = "<p class='fw-bold text-info'>$list->user_fullname</p><small>$list->import_vendor_create</small>";
                $row[] = "<p class='fw-bold text-primary'>$list->vendor_code</p><small>$list->vendor_agency</small>";
                $row[] = $list->vendor_address;
                $row[] = "<p class='fw-bold text-primary'>$list->vendor_name</p><small>$list->vendor_email</small>";
                $row[] = $list->vendor_telp;
                $row[] = "<p class='fw-bold text-primary'>$list->vendor_bank_account</p><small>$list->vendor_bank_name</small>";
                $row[] = $list->vendor_bank_number;
                $row[] = $list->vendor_debit_term_default;
                $row[] = "<p class='fw-bold $status'>$list->status_name</p>";
                $data[] = $row;
            }

            $output = [
                'draw' => $this->request->getPost('draw'),
                'recordsTotal' => $this->models->countAll($this->database),
                'recordsFiltered' => $this->models->countFiltered(),
                'data' => $data
            ];

            echo json_encode($output);
        }
    }

    public function exportVendorLog($importCode) {
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $dataTemplate = $this->models->getDataVendorLog($importCode);

        $sheet->setCellValue('A1', 'Log Code');
        $sheet->setCellValue('B1', 'Filename');
        $sheet->setCellValue('C1', 'Created By');
        $sheet->setCellValue('D1', 'Created At');
        $sheet->setCellValue('E1', 'Vendor Code');
        $sheet->setCellValue('F1', 'Vendor Agency');
        $sheet->setCellValue('G1', 'Address');
        $sheet->setCellValue('H1', 'Vendor Name');
        $sheet->setCellValue('I1', 'Email');
        $sheet->setCellValue('J1', 'Contact');
        $sheet->setCellValue('K1', 'Bank Name');
        $sheet->setCellValue('L1', 'Bank Account');
        $sheet->setCellValue('M1', 'Bank Number');
        $sheet->setCellValue('N1', 'Debit Term');
        $sheet->setCellValue('O1', 'Status');
        $rows = 2;

        foreach ($dataTemplate as $dt){
            $sheet->setCellValue('A'.$rows, $dt['import_code']);
            $sheet->setCellValue('B'.$rows, $dt['import_vendor_filename']);
            $sheet->setCellValue('C'.$rows, $dt['user_fullname']);
            $sheet->setCellValue('D'.$rows, $dt['import_vendor_create']);
            $sheet->setCellValue('E'.$rows, $dt['vendor_code']);
            $sheet->setCellValue('F'.$rows, $dt['vendor_agency']);
            $sheet->setCellValue('G'.$rows, $dt['vendor_address']);
            $sheet->setCellValue('H'.$rows, $dt['vendor_name']);
            $sheet->setCellValue('I'.$rows, $dt['vendor_email']);
            $sheet->setCellValue('J'.$rows, $dt['vendor_telp']);
            $sheet->setCellValue('K'.$rows, $dt['vendor_bank_name']);
            $sheet->setCellValue('L'.$rows, $dt['vendor_bank_account']);
            $sheet->setCellValue('M'.$rows, $dt['vendor_bank_number']);
            $sheet->setCellValue('N'.$rows, $dt['vendor_debit_term_default']);
            $sheet->setCellValue('O'.$rows, $dt['status_name']);
            $rows++;
        }

        // buat excelnya, fyi inisialisasi spreadsheet itu buat file excel kosong baru dan writer itu mengisi file kosong itu dengan data diatas
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $date = date('j M Y');
        $fileName = 'vendor report log - ' . $dataTemplate[0]['import_code'];
    
        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName.'.xlsx');
        header('Cache-Control: max-age=0');
    
        // auto download disini, gaperlu dibalikin ke redirect lagi
        $writer->save('php://output');

        // return redirect()->to('/warehouse/products');
    }
}
