<?php

namespace App\Controllers\log;
use App\Controllers\BaseController;
use Config\Services;
use App\Models\warehouse\models;
// use App\Models\models;

class c_withdrawalLog extends BaseController
{

    protected $table = 'withdrawal_log';
    protected $database = 'log';
    protected $column_order = [];
    protected $column_search = ['insert_wd_number', 'insert_wd_filename', 'insert_wd_product_code', 'insert_wd_jo_number'];
    protected $order = ['insert_wd_id' => 'DESC'];
    
    public function __construct() {
        $this->request = Services::request();
        $this->models = new models($this->request, $this->table, $this->column_order, $this->column_search, $this->order, $this->database, 'withdrawalLog');
        $this->session = session();
    }

    public function index()
    {  

        if(!$this->session->get('login')) {
            return redirect()->to('/');
        }
        
        $data = [
            'validation'    => \Config\Services::validation(),
            // add new main product
            'multipleUom'   => $this->models->getAllDataMultipleUom(),
            'uomSchema'     => $this->models->getAllDataUomSchema(),
            'currency'      => $this->models->getAllDataCurrency(),
            'status'        => $this->models->getAllDataStatus(),
            // add new user management
            'dept'          => $this->models->getAllDataDept(),
            'level'         => $this->models->getAllDataLevel(),
            // add new assembly
            'whs'           => $this->models->getAllDataWhs(),
        ];

        return view('log/v_withdrawalLog.php', $data); 
    }

    public function ajaxList()
    {
        if ($this->request->getMethod(true) === 'POST') {
            $lists = $this->models->getDatatables();
            $data = [];
            $no = $this->request->getPost('start');

            foreach ($lists as $list) {
                $no++;
                $row = [];
                $approved = $list->wd_approve_head == 1 ? 'Yes' : 'Not yet';
                $approvedWh = $list->wd_approve_head == 1 ? 'Yes' : 'Not yet';
                $status = $list->wd_status == 7 ? 'text-success' : ($list->wd_status == '6' ? 'text-danger' : 'text-warning');
                $row[] = $no;
                $row[] = "<a class='fas fa-file-export' href='c_withdrawalLog/exportWithdrawalLog/$list->insert_wd_number'></a>";
                $row[] = "<p class='fw-bold text-primary'>$list->insert_wd_number</p><small>$list->wd_date</small>";
                $row[] = $list->insert_wd_filename;
                $row[] = "<p class='fw-bold text-info'>$list->user_fullname</p><small>Department : $list->dept_name</small>";
                $row[] = $list->insert_wd_date;
                $row[] = "<p class='text-dark'>$list->fromWhs >> $list->toWhs</p>";
                $row[] = "<p class='fw-bold text-primary'>$list->insert_wd_product_code</p><small>$list->product_name</small>";
                $row[] = "<p class='fw-bold text-warning'>$list->wd_assembly_product</p>";
                $row[] = "<p class='fw-bold text-success'>$list->insert_wd_jo_number </p>";
                $row[] = $list->wd_request_quantity;
                $row[] = $list->wd_remark;
                $row[] = "<p class='fw-blod text-success'>$approved</p>";
                $row[] = $list->wd_request_quantity;
                $row[] = $list->wd_remark_warehouse;
                $row[] = "<p class='fw-blod text-success'>$approvedWh</p>";
                $row[] = $list->wd_issue_date;
                $row[] = "<p class='fw-blod $status'>$list->status_name</p>";
                $data[] = $row;
            }

            $output = [
                'draw' => $this->request->getPost('draw'),
                'recordsTotal' => $this->models->countAll($this->database),
                'recordsFiltered' => $this->models->countFiltered(),
                'data' => $data
            ];

            echo json_encode($output);
        }
    }

    public function exportWithdrawalLog($wdNumber) {
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $dataTemplate = $this->models->getDataWithdrawalLog($wdNumber);

        $sheet->setCellValue('A1', 'Withdrawal Number');
        $sheet->setCellValue('B1', 'Date');
        $sheet->setCellValue('C1', 'Filename');
        $sheet->setCellValue('D1', 'Created By');
        $sheet->setCellValue('E1', 'Department Handle');
        $sheet->setCellValue('F1', 'From Warehouse');
        $sheet->setCellValue('G1', 'To Warehouse');
        $sheet->setCellValue('H1', 'Product Code');
        $sheet->setCellValue('I1', 'Product Name');
        $sheet->setCellValue('J1', 'Assembly Code');
        $sheet->setCellValue('K1', 'JO Number');
        $sheet->setCellValue('L1', 'Quantity');
        $sheet->setCellValue('M1', 'Remark');
        $sheet->setCellValue('N1', 'Approved');
        $sheet->setCellValue('O1', 'Receipt Quantity');
        $sheet->setCellValue('P1', 'Remark Warehouse');
        $sheet->setCellValue('Q1', 'Approved Warehouse');
        $sheet->setCellValue('R1', 'Issue Date');
        $sheet->setCellValue('S1', 'Status');
        $rows = 2;

        foreach ($dataTemplate as $dt){
            $sheet->setCellValue('A'.$rows, $dt['insert_wd_number']);
            $sheet->setCellValue('B'.$rows, $dt['insert_wd_date']);
            $sheet->setCellValue('C'.$rows, $dt['insert_wd_filename']);
            $sheet->setCellValue('D'.$rows, $dt['user_fullname']);
            $sheet->setCellValue('E'.$rows, $dt['dept_name']);
            $sheet->setCellValue('F'.$rows, $dt['fromWhs']);
            $sheet->setCellValue('G'.$rows, $dt['toWhs']);
            $sheet->setCellValue('H'.$rows, $dt['insert_wd_product_code']);
            $sheet->setCellValue('I'.$rows, $dt['product_name']);
            $sheet->setCellValue('J'.$rows, $dt['wd_assembly_product']);
            $sheet->setCellValue('K'.$rows, $dt['wd_jo_number']);
            $sheet->setCellValue('L'.$rows, $dt['wd_request_quantity']);
            $sheet->setCellValue('M'.$rows, $dt['wd_remark']);
            $sheet->setCellValue('N'.$rows, $dt['wd_approve_head']);
            $sheet->setCellValue('O'.$rows, $dt['wd_receipt_quantity_warehouse']);
            $sheet->setCellValue('P'.$rows, $dt['wd_remark_warehouse']);
            $sheet->setCellValue('Q'.$rows, $dt['wd_approve_head_warehouse']);
            $sheet->setCellValue('R'.$rows, $dt['wd_issue_date']);
            $sheet->setCellValue('S'.$rows, $dt['status_name']);
            $rows++;
        }

        // buat excelnya, fyi inisialisasi spreadsheet itu buat file excel kosong baru dan writer itu mengisi file kosong itu dengan data diatas
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $date = date('j M Y');
        $fileName = 'withdrawal report log - ' . $dataTemplate[0]['insert_wd_number'];
    
        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName.'.xlsx');
        header('Cache-Control: max-age=0');
    
        // auto download disini, gaperlu dibalikin ke redirect lagi
        $writer->save('php://output');

        // return redirect()->to('/warehouse/products');
    }
}
