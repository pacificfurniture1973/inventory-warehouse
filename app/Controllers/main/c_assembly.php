<?php

namespace App\Controllers\main;
use App\Controllers\BaseController;
use Config\Services;
use App\Models\warehouse\models;
// use App\Models\models;

class c_assembly extends BaseController
{
    protected $table = 'assembly';
    protected $database = 'main';
    protected $column_order = ['assembly_id', 'assembly_code', null, null, null, null, 'assembly_status', 'assembly_category', null]; 
    protected $column_search = ['assembly_id', 'assembly_code'];
    protected $order = ['assembly_id' => 'DESC'];

    public function __construct() {
        $this->session = session();
        $this->request = Services::request();
        $this->models = new models($this->request, $this->table, $this->column_order, $this->column_search, $this->order, $this->database, 'mainAssembly');
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index($id = null)
    {

        if(!$this->session->get('login')) {
            return redirect()->to('/');
        }
        
        $data = [
            'validation'    => \Config\Services::validation(),
            // add new main product
            'multipleUom'   => $this->models->getAllDataMultipleUom(),
            'uomSchema'     => $this->models->getAllDataUomSchema(),
            'currency'      => $this->models->getAllDataCurrency(),
            'status'        => $this->models->getAllDataStatus(),
            // add new user management
            'dept'          => $this->models->getAllDataDept(),
            'level'         => $this->models->getAllDataLevel(),
            // add new assembly
            'whs'           => $this->models->getAllDataWhs(),
        ];
        
        return view('main/v_assembly.php', $data); 
    }

    public function ajaxList()
    {   
        if ($this->request->getMethod(true) === 'POST') {
            $lists = $this->models->getDatatables();
            $data = [];
            $no = $this->request->getPost('start');

            foreach ($lists as $list) {
                $no++; 
                $row = [];
                $row[] = $no;
                $row[] = "<p class='fw-bold text-success'>$list->assembly_code</p>";
                $row[] = $list->assembly_type;
                $row[] = $list->assembly_name;
                $row[] = "<p class='fw-bold text-primary'>$list->assembly_description</p>";
                $row[] = $list->assembly_uom;
                $row[] = ($list->assembly_status == 1) ? "<p class='fw-bold text-success'>$list->status_name</p>" : "<p class='fw-bold text-danger'>$list->status_name</p>";
                $row[] = "<p class='fw-bold text-warning'>$list->assembly_category</p>";
                $row[] = "
                <a href='#' data-bs-toggle='modal' data-bs-target='#modalEditAssembly' id='editAssembly' class='text-decoration-none text-warning far fa-edit fa-1x fa-fw'
                data-id='$list->assembly_id' data-code='$list->assembly_code' data-type='$list->assembly_type' data-name='$list->assembly_name' data-desc='$list->assembly_description' data-uom='$list->assembly_uom'
                data-currency='$list->assembly_currency' data-status='$list->assembly_status' data-uprice='$list->assembly_unit_price' data-item_spec='$list->assembly_item_spec'
                data-package='$list->assembly_packaging' data-category='$list->assembly_category' data-import='$list->assembly_import_code'></a>
                <a href='#' data-bs-toggle='modal' data-bs-target='#modalDelete' data-id='$list->assembly_id' id='deleteItemAssembly' class='text-decoration-none text-danger fas fa-trash-alt fa-1x fa-fw'></a>";
                $data[] = $row;
            }

            $output = [
                'draw' => $this->request->getPost('draw'),
                'recordsTotal' => $this->models->countAll($this->database),
                'recordsFiltered' => $this->models->countFiltered(),
                'data' => $data
            ];

            echo json_encode($output);
        }
    }

    public function importAssembly() {
        $file = $this->request->getFile('importAssembly');
        $filename = $file->getName();
        $userId = $this->session->get('user_id');
        $ext = $file->getClientExtension();
        
        if($ext == 'xls') {
            $render = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
        } else {
            $render = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        }

        // jika error = 0 maka file ada, jika error = 4 maka file null
        if($file->getError() == 4) {
            $validation = \Config\Services::validation();
            $this->session->setFlashdata('error_message', '<strong>Failed !!</strong> file not found ! Please try again...');
            return redirect()->to('main/assembly')->withInput()->with('validation', $validation);
        }

        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file);

        $sheet = $spreadsheet->getActiveSheet()->toArray();
        $log = array('success' => 0, 'failed' => 0);

        // auto increment import code
        if($this->models->getLastIdDataAssemblyLog() != null) {
            $lastAssembly = $this->models->getLastIdDataAssemblyLog();
            $number = explode('-', $lastAssembly['import_code']);
            if(substr($number[2],0,4) == date('Y')) {                
                $importCode = 'IA-PTPF-' . intval($number[2] + 1);
            } else {
                $importCode = 'IA-PTPF-' . date('Y') . '00001';
            }
        } else {
            $importCode = 'IA-PTPF-' . date('Y') . '00001';
        }

        if($spreadsheet != null) {
            foreach($sheet as $baris => $kolom) {
                if($baris == 0) {
                    continue;
                }

                // validasi duplicate
                // dd($this->models);
                $check = $this->models->checkDuplicateAssembly($kolom['0'], $kolom['2']);

                // jika semua ngga diinput langsung continue biar yg note informatiaon ngga kebaca failed
                if($kolom['0'] == null && $kolom['1'] == null && $kolom['2'] == null && $kolom['3'] == null && $kolom['4'] == null && $kolom['5'] == null && $kolom['6'] == null && $kolom['7'] === null && $kolom['8'] == null && $kolom['9'] == null && $kolom['10'] == null) {
                    continue;
                }

                if($check != null) {
                    $log['failed'] += 1;
                    // dd($log);
                    continue;
                } elseif($kolom['0'] == null || $kolom['2'] == null || $kolom['3'] == null || $kolom['4'] == null || $kolom['5'] == null || $kolom['6'] == null) {
                    $log['failed'] += 1;
                    continue;
                } else {
                    $assemblyCode       = $kolom['0'];
                    $assemblyType       = $kolom['1'];
                    $assemblyName       = $kolom['2'];
                    $assemblyDescription = $kolom['3'];
                    $assemblyUom        = $kolom['4'];
                    $assemblyCurrency   = $kolom['5'];
                    $assemblyStatus     = $kolom['6'];
                    $assemblyUnitPrice  = floatval($kolom['7']);
                    $assemblyCategory   = $kolom['8'];
                    $assemblyPackage    = $kolom['9'];
                    $assemblyItemSpec   = $kolom['10'];
    
                    // masukan ke database
                    $this->models->importAssembly($assemblyCode, $assemblyType, $assemblyName, $assemblyDescription, $assemblyUom, $assemblyCurrency, $assemblyStatus, 
                        $assemblyUnitPrice, $assemblyCategory, $assemblyPackage, $assemblyItemSpec, $importCode);

                    // import log sekalian 
                    $this->models->importAssemblyLog($importCode, $assemblyCode, $filename, $userId);
                    $log['success'] += 1;
                }
            }  

            $this->session->setFlashdata('success_message', '<strong>Successfully !!</strong> import ' . $log['success'] . ' and ' . $log['failed'] . ' <span class="text-danger">failure</span>');
            return redirect()->to('main/assembly');
        }
    }

    public function exportTemplateAssembly() {
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $dataCurrency = $this->models->getAllDataCurrency();
        $dataStatus = $this->models->getAllDataStatus();
        $dataWhs = $this->models->getAllDataWhs();

        $sheet->setCellValue('A1', 'Assembly Code/Id*');
        $sheet->setCellValue('B1', 'Assembly Type');
        $sheet->setCellValue('C1', 'Assembly Name*');
        $sheet->setCellValue('D1', 'Description*');
        $sheet->setCellValue('E1', 'Uom*');
        $sheet->setCellValue('F1', 'Currency*');
        $sheet->setCellValue('G1', 'Status*');
        $sheet->setCellValue('H1', 'Unit Price');
        $sheet->setCellValue('I1', 'Category');
        $sheet->setCellValue('J1', 'Packaging');
        $sheet->setCellValue('K1', 'Item Spec');

        $sheet->setCellValue('A2', 'PK-00015-00');
        $sheet->setCellValue('B2', 'Inventory Assembly');
        $sheet->setCellValue('C2', 'MONTGOMERY COFFEE TABLE');
        $sheet->setCellValue('D2', 'MONTGOMERY COFFEE TABLE');
        $sheet->setCellValue('E2', 'pc(s)');
        $sheet->setCellValue('F2', '1');
        $sheet->setCellValue('G2', '1');
        $sheet->setCellValue('H2', '500.000');
        $sheet->setCellValue('I2', 'Table');
        $sheet->setCellValue('J2', 'Packaging');
        $sheet->setCellValue('K2', '60""W X 29""D X 17""H * Mahogany solids * Crush...');

        // informasi untuk template 
        $sheet->setCellValue('N2', "# Information");
        $sheet->setCellValue('N3', "# Currency");

        $rowCurr = 4;

        foreach($dataCurrency as $dc) {
            $sheet->setCellValue('N'.$rowCurr, $dc['currency_id'] . ". " . $dc['currency_name']);
            $rowCurr++;
        }

        $sheet->setCellValue('N'.$rowCurr + 2, "# Location");
        $rowWhs = $rowCurr + 3;

        foreach($dataWhs as $dw) {
            $sheet->setCellValue('N'.$rowWhs, $dw['whs_code'] . ". " . $dw['whs_name']);
            $rowWhs++;
        }

        $sheet->setCellValue('P3', "# Status");
        $rowStatus = 4;

        foreach($dataStatus as $ds) {
            $sheet->setCellValue('P'.$rowStatus, $ds['status_id'] . ". " . $ds['status_name']);
            $rowStatus++;
        }

        // buat excelnya, fyi inisialisasi spreadsheet itu buat file excel kosong baru dan writer itu mengisi file kosong itu dengan data diatas
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $fileName = 'Template import assembly products';
    
        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName.'.xlsx');
        header('Cache-Control: max-age=0');
    
        // auto download disini, gaperlu dibalikin ke redirect lagi
        $writer->save('php://output');

        // return redirect()->to('/warehouse/products');
    }

    public function exportDataAssembly() {
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $dataTemplate = $this->models->getAllDataAssembly();

        $sheet->setCellValue('A1', 'Assembly Code/Id');
        $sheet->setCellValue('B1', 'Assembly Type');
        $sheet->setCellValue('C1', 'Assembly Name');
        $sheet->setCellValue('D1', 'Description');
        $sheet->setCellValue('E1', 'Uom');
        $sheet->setCellValue('F1', 'Currency');
        $sheet->setCellValue('G1', 'Status');
        $sheet->setCellValue('H1', 'Location');
        $sheet->setCellValue('I1', 'Unit Price');
        $sheet->setCellValue('J1', 'Category');
        $sheet->setCellValue('K1', 'Packaging');
        $sheet->setCellValue('L1', 'Item Spec');
        $rows = 2;

        foreach ($dataTemplate as $dt){
            $sheet->setCellValue('A' . $rows, $dt['assembly_code']);
            $sheet->setCellValue('B' . $rows, $dt['assembly_type']);
            $sheet->setCellValue('C' . $rows, $dt['assembly_name']);
            $sheet->setCellValue('D' . $rows, $dt['assembly_description']);
            $sheet->setCellValue('E' . $rows, $dt['assembly_uom']);
            $sheet->setCellValue('F' . $rows, $dt['currency_name']);
            $sheet->setCellValue('G' . $rows, $dt['status_name']);
            $sheet->setCellValue('H' . $rows, 'Finish Good');
            $sheet->setCellValue('I' . $rows, $dt['assembly_unit_price']);
            $sheet->setCellValue('J' . $rows, $dt['assembly_category']);
            $sheet->setCellValue('K' . $rows, $dt['assembly_packaging']);
            $sheet->setCellValue('L' . $rows, $dt['assembly_item_spec']);
            $rows++;
        }

        // buat excelnya, fyi inisialisasi spreadsheet itu buat file excel kosong baru dan writer itu mengisi file kosong itu dengan data diatas
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $date = date('j M Y');
        $fileName = 'All data assembly products - ' . $date;
    
        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName.'.xlsx');
        header('Cache-Control: max-age=0');
    
        // auto download disini, gaperlu dibalikin ke redirect lagi
        $writer->save('php://output');

        // return redirect()->to('/warehouse/products');
    }

    public function insertAssembly() {
        // get data from modals
        $assemblyCode   = $this->request->getVar('assemblyCode');
        $assemblyType   = $this->request->getVar('assemblyType');
        $assemblyName   = $this->request->getVar('assemblyName');
        $description    = $this->request->getVar('assemblyDescription');
        $uom            = $this->request->getVar('assemblyUom');
        $currency       = $this->request->getVar('assemblyCurrency');
        $status         = $this->request->getVar('assemblyStatus');
        $unitPrice      = $this->request->getVar('assemblyUnitPrice');
        $category       = $this->request->getVar('assemblyCategory');
        $package        = $this->request->getVar('assemblyPackage');
        $itemSpec       = $this->request->getVar('assemblyItemSpec');

        $userId = $this->session->get('user_id');
        $filename = 'Manual Insert';

        // dd($assemblyCode, $assemblyType, $assemblyName, $description, $uom, $currency, $status, $unitPrice, $whs, $category, $package, $itemSpec, $userId, $filename);

        // auto increment add code
        if($this->models->getLastIdDataAssemblyLog() != null) {
            $lastAssembly = $this->models->getLastIdDataAssemblyLog();
            $number = explode('-', $lastAssembly['import_code']);
            if(substr($number[2],0,4) == date('Y')) {                
                $addCode = 'AA-PTPF-' . intval($number[2] + 1);
            } else {
                $addCode = 'AA-PTPF-' . date('Y') . '00001';
            }
        } else {
            $addCode = 'AA-PTPF-' . date('Y') . '00001';
        }

        // validasi
        if(!$this->validate([
            'assemblyCode'          => 'required|is_unique[assembly.assembly_code]',
            'assemblyName'          => 'required|is_unique[assembly.assembly_name]',
            'assemblyDescription'   => 'required',
            'assemblyUom'           => 'required',
            'assemblyCurrency'      => 'required',
            'assemblyStatus'        => 'required',
        ], [
            'assemblyCode'  => [
                'required'  => 'Must be filled !',
                'is_unique' => 'Values is already exists !'
            ],
            'assemblyName'  => [
                'required'  => 'Must be filled !',
                'is_unique' => 'Values is already exists !'
            ],
            'assemblyDescription'   => [
                'required'  => 'Must be filled !'
            ],
            'assemblyUom'   => [
                'required'  => 'Must be filled !'
            ],
            'assemblyCurrency'  => [
                'required'  => 'Must be filled !'
            ],
            'assemblyStatus'    => [
                'required'  => 'Must be filled !'
            ]
        ])) {
            // set session failed
            $validation = \Config\Services::validation();
            $this->session->setFlashdata('error_message', '<strong>Failed !</strong> insert new data, please check & try again !');
            return redirect()->to('main/assembly')->withInput()->with('validation', $validation);
        } else {
            // sukses validasi

            // masukan ke database
            $this->models->insertAssembly($assemblyCode, $assemblyType, $assemblyName, $description, $uom, $currency, $status, $unitPrice, $category,
                $package, $itemSpec, $addCode);

            // masukan ke log
            $this->models->insertAssemblyLog($addCode, $assemblyCode, $filename, $userId);

            // set session sukses
            $this->session->setFlashdata('success_message', '<strong>Successfully !!</strong> insert new data, thanks..');
            return redirect()->to('main/assembly');
        }
    }

    public function editAssembly() {
        $assemblyId     = $this->request->getVar('editAssemblyId');
        $assemblyCode   = $this->request->getVar('editAssemblyCode');
        $assemblyType   = $this->request->getVar('editAssemblyType');
        $assemblyName   = $this->request->getVar('editAssemblyName');
        $importCode     = $this->request->getVar('editAssemblyImportCode');
        $description    = $this->request->getVar('editAssemblyDescription');
        $uom            = $this->request->getVar('editAssemblyUom');
        $currency       = $this->request->getVar('editAssemblyCurrency');
        $status         = $this->request->getVar('editAssemblyStatus');
        $unitPrice      = floatval($this->request->getVar('editAssemblyUnitPrice'));
        $category       = $this->request->getVar('editAssemblyCategory');
        $package        = $this->request->getVar('editAssemblyPackage');
        $itemSpec       = $this->request->getVar('editAssemblyItemSpec');

        // dd($assemblyId, $assemblyCode, $assemblyType, $assemblyName, $importCode, $description, $uom, $currency, $status, $unitPrice, $whs, $category, $package, $itemSpec);

        $userId = $this->session->get('user_id');
        $filename = 'Manual Update';

        if($this->models->getDataForEditAssembly($assemblyId) != null) {
            $getOldData     = $this->models->getDataForEditAssembly($assemblyId);
        } else {
            $validation = \Config\Services::validation();
            $this->session->setFlashdata('error_message', '<strong>Failed !!</strong> update existing data, please try again later..');
            return redirect()->to('main/assembly');
        }

        // setup specific rule
        if($getOldData['assembly_code'] == $assemblyCode) {
            $rule1 = 'required';
        } else {
            $rule1 = 'required|is_unique[assembly.assembly_code]';
        }

        if($getOldData['assembly_name'] == $assemblyName) {
            $rule2 = 'required';
        } else {
            $rule2 = 'required|is_unique[assembly.assembly_name]';
        }

        // general validation
        if(!$this->validate([
            'editAssemblyCode'          => $rule1,
            'editAssemblyName'          => $rule2,
            'editAssemblyImportCode'    => 'required',
            'editAssemblyDescription'   => 'required',
            'editAssemblyUom'           => 'required',
            'editAssemblyCurrency'      => 'required',
            'editAssemblyStatus'        => 'required',
        ], [
            'editAssemblyCode'  => [
                'required'  => 'Must be filled !',
                'is_unique' => 'Values is already exists !'
            ],
            'editAssemblyName'  => [
                'required'  => 'Must be filled !',
                'is_unique' => 'Values is already exists !'
            ],
            'editAssemblyImportCode'    => [
                'required'  => 'Must be filled !'
            ],
            'editAssemblyDescription'   => [
                'required'  => 'Must be filled !'
            ],
            'editAssemblyUom'   => [
                'required'  => 'Must be filled !'
            ],
            'editAssemblyCurrency'  => [
                'required'  => 'Must be filled !'
            ],
            'editAssemblyStatus'    => [
                'required'  => 'Must be filled !'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $this->session->setFlashdata('error_message', '<strong>Failed !!</strong> update existing data, please try again later..');
            return redirect()->to('main/assembly')->withInput()->with('validation', $validation);
        } else {
            // auto increment update code
            if($this->models->getLastIdDataAssemblyLog() != null) {
                $lastAssembly = $this->models->getLastIdDataAssemblyLog();
                $number = explode('-', $lastAssembly['import_code']);
                if(substr($number[2],0,4) == date('Y')) {                
                    $updateCode = 'EA-PTPF-' . intval($number[2] + 1);
                } else {
                    $updateCode = 'EA-PTPF-' . date('Y') . '00001';
                }
            } else {
                $updateCode = 'EA-PTPF-' . date('Y') . '00001';
            }

            $assemblyId     = $this->request->getVar('editAssemblyId');
            $assemblyCode   = $this->request->getVar('editAssemblyCode');
            $assemblyType   = $this->request->getVar('editAssemblyType');
            $assemblyName   = $this->request->getVar('editAssemblyName');
            $importCode     = $this->request->getVar('editAssemblyImportCode');
            $description    = $this->request->getVar('editAssemblyDescription');
            $uom            = $this->request->getVar('editAssemblyUom');
            $currency       = $this->request->getVar('editAssemblyCurrency');
            $status         = $this->request->getVar('editAssemblyStatus');
            $unitPrice      = $this->request->getVar('editAssemblyUnitPrice');
            $category       = $this->request->getVar('editAssemblyCategory');
            $package        = $this->request->getVar('editAssemblyPackage');
            $itemSpec       = $this->request->getVar('editAssemblyItemSpec');

            // update assembly
            $this->models->updateDataAssembly($assemblyId, $assemblyCode, $assemblyType, $assemblyName, $importCode, $description, $uom, $currency, $status, $unitPrice,
                $category, $package, $itemSpec, $updateCode);

            // create log
            $this->models->updateDataAssemblyLog($updateCode, $assemblyCode, $filename, $userId);

            $this->session->setFlashdata('success_message', '<strong>Successfully !!</strong> update existing data, thanks..');
            return redirect()->to('main/assembly');
        }
    }

    public function deleteAssembly() {
        $deleteId = $this->request->getVar('deleteId');

        // cek data apakah ada
        if($this->models->checkDataForDeleteAssembly($deleteId) != null) {
            // auto increment update code
            if($this->models->getLastIdDataAssemblyLog() != null) {
                $lastAssembly = $this->models->getLastIdDataAssemblyLog();
                $number = explode('-', $lastAssembly['import_code']);
                if(substr($number[2],0,4) == date('Y')) {                
                    $deleteCode = 'DA-PTPF-' . intval($number[2] + 1);
                } else {
                    $deleteCode = 'DA-PTPF-' . date('Y') . '00001';
                }
            } else {
                $deleteCode = 'DA-PTPF-' . date('Y') . '00001';
            }

            $getOldData  = $this->models->checkDataForDeleteAssembly($deleteId);
            $filename = "Manual Delete";
            $userId = $this->session->get('user_id');

            // masukan ke log
            $this->models->deleteDataAssemblyLog($deleteCode, $getOldData['assembly_code'], $filename, $userId);

            // delete dari database
            $this->models->deleteDataAssembly($deleteId);

            $this->session->setFlashdata('success_message', '<strong>Successfully !!</strong> delete existing data, thanks..');
            return redirect()->to('main/assembly');

        } else {
            $validation = \Config\Services::validation();
            $this->session->setFlashdata('error_message', '<strong>Failed !!</strong> delete data, data not found detected ! please try again later..');
            return redirect()->to('main/assembly');
        }
    }
}
