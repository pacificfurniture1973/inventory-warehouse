<?php

namespace App\Controllers\main;
use App\Controllers\BaseController;
use Config\Services;
use App\Models\warehouse\models;
// use App\Models\models;

class c_customer extends BaseController
{
    protected $table = 'customer';
    protected $database = 'main';
    protected $column_order = ['customer_id', 'customer_code', null, 'customer_agency', null, null, 'customer_name', null, null, null, null, 'customer_status', 'customer_id']; 
    protected $column_search = ['customer_code', 'customer_title', 'customer_agency', 'customer_address', 'customer_email', 'customer_name', 'customer_telp', 'customer_bank_name', 'customer_bank_account', 'customer_bank_number', 'customer_credit_term_default'];
    protected $order = ['customer_id' => 'DESC'];

    public function __construct() {
        $this->session = session();
        $this->request = Services::request();
        $this->models = new models($this->request, $this->table, $this->column_order, $this->column_search, $this->order, $this->database, 'mainCustomer');
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index()
    {

        if(!$this->session->get('login')) {
            return redirect()->to('/');
        }
        
        $products = $this->models->getAllProducts();

        $data = [
            'products'      => $products,
            'validation'    => \Config\Services::validation(),
            // add new main product
            'multipleUom'   => $this->models->getAllDataMultipleUom(),
            'uomSchema'     => $this->models->getAllDataUomSchema(),
            'currency'      => $this->models->getAllDataCurrency(),
            'status'        => $this->models->getAllDataStatus(),
            // add new user management
            'dept'          => $this->models->getAllDataDept(),
            'level'         => $this->models->getAllDataLevel(),
            // add new assembly
            'whs'           => $this->models->getAllDataWhs(),
        ];
        
        return view('main/v_customer.php', $data); 
    }

    public function ajaxList()
    {   
        if ($this->request->getMethod()) {
            $lists = $this->models->getDatatables();
            $data = [];
            $no = $this->request->getPost('start');

            foreach ($lists as $list) {
                $no++; 
                $row = [];
                $row[] = $no;
                $row[] = $list->customer_code;
                $row[] = $list->customer_title;
                $row[] = $list->customer_agency;
                $row[] = $list->customer_address;
                $row[] = $list->customer_email;
                $row[] = $list->customer_name;
                $row[] = $list->customer_telp;
                $row[] = $list->customer_bank_account;
                $row[] = $list->customer_bank_number;
                $row[] = $list->customer_credit_term_default;
                $row[] = $list->status_name;
                $row[] = "
                <a href='#' data-bs-toggle='modal' data-bs-target='#modalEditCustomer' id='editCustomer'
                data-id='$list->customer_id' data-code='$list->customer_code' data-title='$list->customer_title' data-agency='$list->customer_agency' data-address='$list->customer_address' data-email='$list->customer_email'
                data-name='$list->customer_name' data-telp='$list->customer_telp' data-test='$list->customer_import_code' data-bank_name='$list->customer_bank_name' data-bank_account='$list->customer_bank_account' 
                data-bank_number='$list->customer_bank_number' data-credit_term='$list->customer_credit_term_default' data-status='$list->customer_status' 
                class='text-decoration-none text-warning far fa-edit fa-1x fa-fw'></a>
                <a href='#' data-bs-toggle='modal' data-bs-target='#modalDelete' data-id='$list->customer_id' data-import='$list->customer_import_code' id='deleteItemCustomer' class='text-decoration-none text-danger fas fa-trash-alt fa-1x fa-fw'></a>";
                $data[] = $row;
            }

            $output = [
                'draw' => $this->request->getPost('draw'),
                'recordsTotal' => $this->models->countAll($this->database),
                'recordsFiltered' => $this->models->countFiltered(),
                'data' => $data
            ];

            echo json_encode($output);
        }
    }

    public function importCustomer() {
        $file = $this->request->getFile('importCustomer');
        $filename = $file->getName();
        $userId = $this->session->get('user_id');
        $ext = $file->getClientExtension();
        
        if($ext == 'xls') {
            $render = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
        } else {
            $render = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        }

        // jika error = 0 maka file ada, jika error = 4 maka file null
        if($file->getError() == 4) {
            $validation = \Config\Services::validation();
            $this->session->setFlashdata('error_message', '<strong>Failed !!</strong> file not found ! Please try again...');
            return redirect()->to('main/customer')->withInput()->with('validation', $validation);
        }

        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file);

        $sheet = $spreadsheet->getActiveSheet()->toArray();
        $log = array('success' => 0, 'failed' => 0);

        // auto increment import code
        if($this->models->getLastIdDataCustomerLog() != null) {
            $lastCustomer = $this->models->getLastIdDataCustomerLog();
            $number = explode('-', $lastCustomer['import_code']);
            if(substr($number[2],0,4) == date('Y')) {                
                $importCode = 'IC-PTPF-' . intval($number[2] + 1);
            } else {
                $importCode = 'IC-PTPF-' . date('Y') . '00001';
            }
        } else {
            $importCode = 'IC-PTPF-' . date('Y') . '00001';
        }

        if($spreadsheet != null) {
            foreach($sheet as $baris => $kolom) {
                if($baris == 0) {
                    continue;
                }

                // validasi duplicate
                $check = $this->models->checkDuplicateCustomer($kolom['0'], $kolom['2']);

                // jika semua ngga diinput langsung continue biar yg note informatiaon ngga kebaca failed
                if($kolom['0'] == null && $kolom['1'] == null && $kolom['2'] == null && $kolom['3'] == null && $kolom['4'] == null && $kolom['5'] == null && $kolom['6'] == null &&
                   $kolom['7'] == null && $kolom['8'] == null && $kolom['9'] == null && $kolom['10'] == null && $kolom['11'] == null) {
                    continue;
                }

                if($check != null) {
                    $log['failed'] += 1;
                    // dd($log);
                    continue;
                } elseif($kolom['0'] == null || $kolom['2'] == null || $kolom['11'] == null) {
                    $log['failed'] += 1;
                    // dd($log);
                    continue;
                } else {
                    $customerCode       = $kolom['0'];
                    $customerTitle      = $kolom['1'];
                    $customerAgency     = $kolom['2'];
                    $customerAddress    = $kolom['3'];
                    $customerEmail      = $kolom['4'];
                    $customerName       = $kolom['5'];
                    $customerTelp       = $kolom['6'];
                    $customerBankName   = $kolom['7'];
                    $customerBankAccount = $kolom['8'];
                    $customerBankNumber  = $kolom['9'];
                    $customerCreditTermDefault = $kolom['10'];
                    $customerStatus      = $kolom['11'];
    
                    // masukan ke database
                    $this->models->importCustomer($customerCode, $customerTitle, $customerAgency, $customerAddress, $customerEmail, $customerName, $customerTelp, $customerBankName, $customerBankAccount, $customerBankNumber, $customerCreditTermDefault, $customerStatus,
                        $importCode);

                    // import log sekalian 
                    $this->models->importCustomerLog($importCode, $customerCode, $filename, $userId);

                    $log['success'] += 1;
                }
            }

            $this->session->setFlashdata('success_message', '<strong>Successfully !!</strong> import ' . $log['success'] . ' and ' . $log['failed'] . ' <span class="text-danger">failure</span>');
            return redirect()->to('main/customer');
        }
    }

    public function exportTemplateCustomer() {
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValue('A1', 'Customer Code*');
        $sheet->setCellValue('B1', 'Customer Title');
        $sheet->setCellValue('C1', 'Customer Agency*');
        $sheet->setCellValue('D1', 'Address');
        $sheet->setCellValue('E1', 'Customer Email');
        $sheet->setCellValue('F1', 'Customer Name');
        $sheet->setCellValue('G1', 'Customer Telp');
        $sheet->setCellValue('H1', 'Customer Bank Name');
        $sheet->setCellValue('I1', 'Customer Bank Account');
        $sheet->setCellValue('J1', 'Customer Bank Number');
        $sheet->setCellValue('K1', 'Customer Credit Term');
        $sheet->setCellValue('L1', 'Customer Status*');

        $sheet->setCellValue('A2', 'PTPF');
        $sheet->setCellValue('B2', 'PT');
        $sheet->setCellValue('C2', 'Pacific Furniture');
        $sheet->setCellValue('D2', 'Semarang, Tugu Wijaya III');
        $sheet->setCellValue('E2', 'ptpacificfurniture@pacificfurniture.id');
        $sheet->setCellValue('F2', 'Nuryadi');
        $sheet->setCellValue('G2', '083111222333');
        $sheet->setCellValue('H2', 'BCA');
        $sheet->setCellValue('I2', 'Nuryadi');
        $sheet->setCellValue('J2', '0098887654');
        $sheet->setCellValue('K2', 'Cash');
        $sheet->setCellValue('L2', '1');

        $sheet->setCellValue('O3', '# Information');
        $sheet->setCellValue('O4', '# Status');
        $sheet->setCellValue('O5', '1. Active');
        $sheet->setCellValue('O6', '2. Deactive');

        // buat excelnya, fyi inisialisasi spreadsheet itu buat file excel kosong baru dan writer itu mengisi file kosong itu dengan data diatas
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $fileName = 'Template import customer';
    
        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName.'.xlsx');
        header('Cache-Control: max-age=0');
    
        // auto download disini, gaperlu dibalikin ke redirect lagi
        $writer->save('php://output');

        // return redirect()->to('/warehouse/products');
    }

    public function exportDataCustomer() {
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $dataTemplate = $this->models->getAllDataCustomer();

        $sheet->setCellValue('A1', 'Customer Code');
        $sheet->setCellValue('B1', 'Title');
        $sheet->setCellValue('C1', 'Customer Agency');
        $sheet->setCellValue('D1', 'Address');
        $sheet->setCellValue('E1', 'Email');
        $sheet->setCellValue('F1', 'Customer Name');
        $sheet->setCellValue('G1', 'Telp');
        $sheet->setCellValue('H1', 'Bank Name');
        $sheet->setCellValue('I1', 'Bank Account');
        $sheet->setCellValue('J1', 'Bank Number');
        $sheet->setCellValue('K1', 'Credit Term');
        $sheet->setCellValue('L1', 'Status');
        $rows = 2;

        foreach ($dataTemplate as $dt){
            $sheet->setCellValue('A' . $rows, $dt['customer_code']);
            $sheet->setCellValue('B' . $rows, $dt['customer_title']);
            $sheet->setCellValue('C' . $rows, $dt['customer_agency']);
            $sheet->setCellValue('D' . $rows, $dt['customer_address']);
            $sheet->setCellValue('E' . $rows, $dt['customer_email']);
            $sheet->setCellValue('F' . $rows, $dt['customer_name']);
            $sheet->setCellValue('G' . $rows, $dt['customer_telp']);
            $sheet->setCellValue('H' . $rows, $dt['customer_telp']);
            $sheet->setCellValue('I' . $rows, $dt['customer_bank_account']);
            $sheet->setCellValue('J' . $rows, $dt['customer_bank_number']);
            $sheet->setCellValue('K' . $rows, $dt['customer_credit_term_default']);
            $sheet->setCellValue('L' . $rows, $dt['status_name']);
            $rows++;
        }

        // buat excelnya, fyi inisialisasi spreadsheet itu buat file excel kosong baru dan writer itu mengisi file kosong itu dengan data diatas
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $date = date('j M Y');
        $fileName = 'All data customer - ' . $date;
    
        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName.'.xlsx');
        header('Cache-Control: max-age=0');
    
        // auto download disini, gaperlu dibalikin ke redirect lagi
        $writer->save('php://output');

        // return redirect()->to('/warehouse/products');
    }

    public function insertCustomer() {
        // get data from modals
        $customerCode       = $this->request->post('customerCode');
        $customerTitle      = $this->request->post('customerTitle');
        $customerAgency     = $this->request->post('customerAgency');
        $customerAddress    = $this->request->post('customerAddress');
        $customerEmail      = $this->request->post('customerEmail');
        $customerName       = $this->request->post('customerName');
        $customerTelp       = $this->request->post('customerTelp');
        $customerBankName   = $this->request->post('customerBankName');
        $customerBankAccount = $this->request->post('customerBankAccount');
        $customerBankNumber = $this->request->post('customerBankNumber');
        $customerCreditTerm = $this->request->post('customerCreditTerm');
        $customerStatus     = $this->request->post('customerStatus');

        // auto increment import code
        if($this->models->getLastIdDataCustomerLog() != null) {
            $lastCustomer = $this->models->getLastIdDataCustomerLog();
            $number = explode('-', $lastCustomer['import_code']);
            if(substr($number[2],0,4) == date('Y')) {                
                $addCode = 'AC-PTPF-' . intval($number[2] + 1);
            } else {
                $addCode = 'AC-PTPF-' . date('Y') . '00001';
            }
        } else {
            $addCode = 'AC-PTPF-' . date('Y') . '00001';
        }

        // for log
        $userId = $this->session->get('user_id');
        $filename = 'Manual Insert';

        // general validation
        if(!$this->validate([
            'customerCode'      => 'required|is_unique[customer.customer_code]',
            'customerAgency'    => 'required|is_unique[customre.customer_agency]',
            'customerStatus'    => 'required',
        ], [
            'customerCode'   => [
                'required'  => 'Value is required !',
                'is_unique' => 'Value is already exists !'
            ], 
            'customerAgency'   => [
                'required'  => 'Value is required !',
                'is_unique' => 'Value is already exists !'
            ], 
            'customerStatus'  => [
                'required'  => 'Value is required'
            ]
        ])) {
            // set session failed
            $validation = \Config\Services::validation();
            $this->session->setFlasdata('error_message', '<strong>Failed !</strong> insert new data, please check & try again !');
            return redirect()->to('main/customer')->withInput()->with('validation', $validation);
        } else {
            // sukses validasi

            // masukan ke database customer
            $this->models->insertCustomer($customerCode, $customerTitle , $customerAgency, $customerAddress, $customerEmail, $customerName, $customerTelp, 
                $customerBankName, $customerBankAccount, $customerBankNumber, $customerCreditTerm, $customerStatus, $addCode);

            // create log
            $this->models->insertCustomerLog($addCode, $customerCode, $filename, $userId);

            // set session sukses
            $this->session->setFlashdata('success_message', '<strong>Successfully !!</strong> insert new data, thanks..');
            return redirect()->to('main/customer');
        }
    }

    public function editCustomer() {
        // get data from modals
        $editCustomerId         = $this->request->getVar('editCustomerId');
        $editCustomerCode       = $this->request->getVar('editCustomerCode');
        $editCustomerTitle      = $this->request->getVar('editCustomerTitle');
        $editCustomerAgency     = $this->request->getVar('editCustomerAgency');
        $editCustomerAddress    = $this->request->getVar('editCustomerAddress');
        $editCustomerEmail      = $this->request->getVar('editCustomerEmail');
        $editCustomerName       = $this->request->getVar('editCustomerName');
        $editCustomerBankName   = $this->request->getVar('editCustomerBankName');
        $editCustomerBankAccount = $this->request->getVar('editCustomerBankAccount');
        $editCustomerBankNumber = $this->request->getVar('editCustomerBankNumber');
        $editCustomerTelp       = $this->request->getVar('editCustomerTelp');
        $editCustomerCreditTerm = $this->request->getVar('editCustomerCreditTerm');
        $editCustomerStatus     = $this->request->getVar('editCustomerStatus');
        $editCustomerImportCode = $this->request->getVar('editCustomerImportCode');

        $filename = 'Manual Update';
        $userId = $this->session->get('user_id');

        // dd($editCustomerId, $editCustomerCode, $edittCustomerTitle, $editCustomerAgency, $editCustomerAddress, $editCustomerEmail, $editCustomerName, $editCustomerBankName, $editCustomerBankAccount,
        // $editCustomerBankNumber, $editCustomerTelp, $editCustomerCreditTerm, $editCustomerStatus);

        if($this->models->getDataForEditCustomer($editCustomerId) != null) {
            $getOldData     = $this->models->getDataForEditCustomer($editCustomerId);
        } else {
            $validation = \Config\Services::validation();
            $this->session->setFlashdata('error_message', '<strong>Failed !!</strong> update existing data, please try again later..');
            return redirect()->to('main/customer');
        }

        // specific rules
        if($getOldData['customer_code'] == $editCustomerCode) {
            $rule1 = 'required';
        } else {
            $rule1 = 'required|is_unique[customer.customer_code]';
        }

        if($getOldData['customer_agency'] == $editCustomerAgency) {
            $rule2 = 'required';
        } else {
            $rule2 = 'required|is_unique[customer.customer_agency]';
        }

        // general validation
        if(!$this->validate([
            'editCustomerCode'      => $rule1,
            'editCustomerAgency'    => $rule2,
            'editCustomerStatus'        => 'required',
            'editCustomerImportCode'    => 'required',
        ], [
            'editCustomerCode'  => [
                'required'  => 'Value is required !',
                'is_unique' => 'Values is already exists !'
            ],
            'editCustomerAgency'    => [
                'required'  => 'Value is required !',
                'is_unique' => 'Value is already exists !'
            ],
            'editCustomerStatus'    => [
                'required'  => 'Value is required !'
            ],
            'editCustomerImportCode'    => [
                'required'  => 'Value is required !'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $this->session->setFlashdata('error_message', '<strong>Failed !</strong> update existing data, please check & try again !');
            return redirect()->to('main/customer')->withInput()->with('validation', $validation);
        } else {
            // auto increment update code
            // auto increment import code
            if($this->models->getLastIdDataCustomerLog() != null) {
                $lastCustomer = $this->models->getLastIdDataCustomerLog();
                $number = explode('-', $lastCustomer['import_code']);
                if(substr($number[2],0,4) == date('Y')) {                
                    $updateCode = 'UC-PTPF-' . intval($number[2] + 1);
                } else {
                    $updateCode = 'UC-PTPF-' . date('Y') . '00001';
                }
            } else {
                $updateCode = 'UC-PTPF-' . date('Y') . '00001';
            }

            // update customer
            $this->models->updateDataCustomer($editCustomerId, $editCustomerCode, $editCustomerTitle, $editCustomerAgency, $editCustomerAddress, $editCustomerEmail, $editCustomerName,
                $editCustomerBankName, $editCustomerBankAccount, $editCustomerBankNumber, $editCustomerTelp, $editCustomerCreditTerm, $editCustomerStatus, $updateCode);

            // create log
            $this->models->updateDataCustomerLog($updateCode, $editCustomerCode, $filename, $userId);

            $this->session->setFlashdata('success_message', '<strong>Successfully !!</strong> update existing data, thanks..');
            return redirect()->to('main/customer');
        }
    }

    public function deleteCustomer() {
        $deleteId = $this->request->getVar('deleteId');
        $importCode = $this->request->getVar('importId');

        // cek data apakah ada
        if($this->models->checkDataForDeleteCustomer($deleteId) != null) {
            // auto increment update code
            if($this->models->getLastIdDataCustomerLog() != null) {
                $lastCustomer = $this->models->getLastIdDataCustomerLog();
                $number = explode('-', $lastCustomer['import_code']);
                if(substr($number[2],0,4) == date('Y')) {                
                    $deleteCode = 'DC-PTPF-' . intval($number[2] + 1);
                } else {
                    $deleteCode = 'DC-PTPF-' . date('Y') . '00001';
                }
            } else {
                $deleteCode = 'DC-PTPF-' . date('Y') . '00001';
            }

            $getOldData  = $this->models->checkDataForDeleteCustomer($deleteId);
            $filename = "Manual Delete";
            $userId = $this->session->get('user_id');

            // masukan ke log
            $this->models->deleteDataCustomerLog($deleteCode, $getOldData['customer_code'], $filename, $userId);

            // delete dari database
            $this->models->deleteDataCustomer($deleteId);

            $this->session->setFlashdata('success_message', '<strong>Successfully !!</strong> delete existing data, thanks..');
            return redirect()->to('main/customer');

        } else {
            $validation = \Config\Services::validation();
            $this->session->setFlashdata('error_message', '<strong>Failed !!</strong> delete data, data not found detected ! please try again later..');
            return redirect()->to('main/customer');
        }
    }
}
