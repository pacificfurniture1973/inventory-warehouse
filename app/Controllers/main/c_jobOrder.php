<?php

namespace App\Controllers\main;
use App\Controllers\BaseController;
use Config\Services;
use App\Models\warehouse\models;
// use App\Models\models;

class c_jobOrder extends BaseController
{
    protected $table = 'job_order';
    protected $database = 'main';
    protected $column_order = ['jo_id', 'jo_number', null, 'jo_customer_po', 'jo_assembly_code', null, 'jo_model_code', 'jo_id']; 
    protected $column_search = ['jo_number', 'jo_customer_po', 'jo_assembly_code', 'jo_model_code'];
    protected $order = ['jo_id' => 'DESC'];

    public function __construct() {
        $this->session = session();
        $this->request = Services::request();
        $this->models = new models($this->request, $this->table, $this->column_order, $this->column_search, $this->order, $this->database, 'mainJobOrder');
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index($id = null)
    {
        if(!$this->session->get('login')) {
            return redirect()->to('/');
        }

        $products = $this->models->getAllProducts();

        $data = [
            'products'      => $products,
            'validation'    => \Config\Services::validation(),
            // add new main product
            'multipleUom'   => $this->models->getAllDataMultipleUom(),
            'uomSchema'     => $this->models->getAllDataUomSchema(),
            'currency'      => $this->models->getAllDataCurrency(),
            'status'        => $this->models->getAllDataStatus(),
            // add new user management
            'dept'          => $this->models->getAllDataDept(),
            'level'         => $this->models->getAllDataLevel(),
            // add new assembly
            'whs'           => $this->models->getAllDataWhs(),
        ];
        
        return view('main/v_jobOrder.php', $data); 
    }

    public function ajaxList()
    {   
        if ($this->request->getMethod(true) === 'POST') {

            $fromDate = $this->request->getVar('fromDate');
            $toDate = $this->request->getVar('toDate');

            $lists = $this->models->getDatatables($fromDate, $toDate, 'jo');

            $data = [];
            $no = $this->request->getPost('start');

            foreach ($lists as $list) {
                $no++; 
                $row = [];
                $row[] = $no;
                $row[] = "<p class='fw-bold text-success'>$list->jo_number</p><small class='text-muted'>$list->jo_date</small>";
                $row[] = "<p class='fw-bold text-dark'>$list->user_username</p>";
                $row[] = "<p class='fw-bold text-warning'>$list->jo_customer_po</p><small class='text-muted'>$list->customer_agency</small>";
                $row[] = $list->jo_assembly_code;
                $row[] = floatval($list->jo_quantity);
                $row[] = floatval($list->jo_balance_quantity);
                $row[] = $list->assembly_uom;
                $row[] = $list->jo_model_code;
                $row[] = floatval($list->jo_balance_quantity) > 0 ? "<p class='fw-bold text-info'>Process</p>" : "<p class='fw-bold text-success'>Finish</p>";
                $row[] = "
                <a href='#' data-bs-toggle='modal' data-bs-target='#modalDelete' data-id='$list->jo_id' data-import='$list->jo_import_code' id='deleteItemJo' class='text-decoration-none text-danger fas fa-trash-alt fa-1x fa-fw'></a>";
                $data[] = $row;
            }

            $output = [
                'draw' => $this->request->getPost('draw'),
                'recordsTotal' => $this->models->countAll($this->database),
                'recordsFiltered' => $this->models->countFiltered($fromDate, $toDate, 'jo'),
                'data' => $data
            ];

            echo json_encode($output);
        }
    }

    public function importJobOrder() {
        $file = $this->request->getFile('importJobOrder');
        $filename = $file->getName();
        $userId = $this->session->get('user_id');
        $ext = $file->getClientExtension();
        
        if($ext == 'xls') {
            $render = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
        } else {
            $render = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        }

        // jika error = 0 maka file ada, jika error = 4 maka file null
        if($file->getError() == 4) {
            $validation = \Config\Services::validation();
            $this->session->setFlashdata('error_message', '<strong>Failed !!</strong> file not found ! Please try again...');
            return redirect()->to('main/jobOrder')->withInput()->with('validation', $validation);
        }

        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file);

        $sheet = $spreadsheet->getActiveSheet()->toArray();
        $log = array('success' => 0, 'failed' => 0);

        // auto increment import code
        if($this->models->getLastIdDataJobOrderLog() != null) {
            $lastJo = $this->models->getLastIdDataJobOrderLog();
            $number = explode('-', $lastJo['import_code']);
            if(substr($number[2],0,4) == date('Y')) {                
                $importCode = 'IJ-PTPF-' . intval($number[2] + 1);
            } else {
                $importCode = 'IJ-PTPF-' . date('Y') . '00001';
            }
        } else {
            $importCode = 'IJ-PTPF-' . date('Y') . '00001';
        }

        if($spreadsheet != null) {
            foreach($sheet as $baris => $kolom) {
                if($baris == 0) {
                    continue;
                }

                if($kolom['0'] == null && $kolom['1'] == null && $kolom['2'] == null && $kolom['3'] == null && $kolom['4'] == null && $kolom['5'] == null && $kolom['6'] === null && 
                $kolom['7'] == null && $kolom['8'] == null && $kolom['9'] == null && $kolom['10'] == null && $kolom['11'] == null && $kolom['12'] == null && $kolom['13'] == null) {
                    continue;
                }
                
                // validasi duplicate
                if($this->models->checkCustomerAndAssembly($kolom['2'], $kolom['9']) != null) {
                    $check = $this->models->checkDuplicateJobOrder($kolom['0'], $kolom['9']);

                    if($kolom['0'] != null && $kolom['1'] != null && $kolom['2'] != null && $kolom['3'] != null && $kolom['5'] != null && $kolom['9'] != null && $kolom['10'] !== null && $kolom['11'] !== null) {
                                                
                        if($check != null) {
                            $log['failed'] += 1;
                            // dd($log);
                            continue;
                        } else {
                            $joBy           = $userId;
                            $joNumber       = $kolom['0'];
                            $joDate         = $kolom['1'];
                            $joCustomerCode = $kolom['2'];
                            $joCustomerPo   = $kolom['3'];
                            $joDueDate      = $kolom['4'];
                            $joCurrency     = $kolom['5'];
                            $joDiscount     = $kolom['6'];
                            $joMemo         = $kolom['7'];
                            $joCreditTerm   = $kolom['8'];
                            $joAssemblyCode = $kolom['9'];
                            $joQuantity     = $kolom['10'];
                            $joBalance      = $kolom['11'];
                            $joAmount       = $kolom['12'];
                            $joModelCode    = $kolom['13'];
            
                            // masukan ke database
                            $this->models->importJobOrder($joNumber, $joDate, $joBy, $joCustomerCode, $joCustomerPo, $joDueDate, $joCurrency, $joDiscount, $joMemo, $joCreditTerm, 
                                $joAssemblyCode, $joQuantity, $joBalance, $joAmount, $joModelCode, $importCode);

                            // import log sekalian 
                            $this->models->importJobOrderLog($importCode, $joNumber, $joAssemblyCode, $filename, $userId);
                            $log['success'] += 1;
                        }
                    } else {
                        $log['failed'] += 1;
                        continue;
                    }
                } else {
                    $log['failed'] += 1;
                    continue;
                }
            }  


            $this->session->setFlashdata('success_message', '<strong>Successfully !!</strong> import ' . $log['success'] . ' and ' . $log['failed'] . ' <span class="text-danger">failure</span>');
            return redirect()->to('main/jobOrder');
        }
    }

    public function exportTemplateJobOrder() {
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $dataCurrency = $this->models->getAllDataCurrency();

        $sheet->setCellValue('A1', 'Job Order Number*');
        $sheet->setCellValue('B1', 'Date*');
        $sheet->setCellValue('C1', 'Customer Code*');
        $sheet->setCellValue('D1', 'Customer PO*');
        $sheet->setCellValue('E1', 'Due Date');
        $sheet->setCellValue('F1', 'Currency*');
        $sheet->setCellValue('G1', 'Discount');
        $sheet->setCellValue('H1', 'Memo');
        $sheet->setCellValue('I1', 'Credit Term');
        $sheet->setCellValue('J1', 'Assembly Code*');
        $sheet->setCellValue('K1', 'Quantity*');
        $sheet->setCellValue('L1', 'Balance Quantity*');
        $sheet->setCellValue('M1', 'Amount');
        $sheet->setCellValue('N1', 'Model Code');

        $sheet->setCellValue('A2', 'JO-PTPF-21000506');
        $sheet->setCellValue('B2', '2021-12-30');
        $sheet->setCellValue('C2', 'BV');
        $sheet->setCellValue('D2', '317331');
        $sheet->setCellValue('E2', '2021-12-30');
        $sheet->setCellValue('F2', '1');
        $sheet->setCellValue('G2', '0');
        $sheet->setCellValue('H2', 'Memo');
        $sheet->setCellValue('I2', 'NET7');
        $sheet->setCellValue('J2', 'PK-1437-00');
        $sheet->setCellValue('K2', '10');
        $sheet->setCellValue('L2', '3');
        $sheet->setCellValue('M2', '1.197.0000');
        $sheet->setCellValue('N2', 'Model Code');

        // informasi untuk template 
        $sheet->setCellValue('P2', "# Information");
        $sheet->setCellValue('P3', "# Currency");
        $row = 4;

        foreach($dataCurrency as $dc) {
            $sheet->setCellValue('P'.$row, $dc['currency_id'] . ". " . $dc['currency_name']);
            $row++;
        }

        // buat excelnya, fyi inisialisasi spreadsheet itu buat file excel kosong baru dan writer itu mengisi file kosong itu dengan data diatas
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $fileName = 'Template import job order';
    
        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName.'.xlsx');
        header('Cache-Control: max-age=0');
    
        // auto download disini, gaperlu dibalikin ke redirect lagi
        $writer->save('php://output');

        // return redirect()->to('/warehouse/products');
    }

    public function exportDataJobOrder() {
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $dataTemplate = $this->models->getAllDataJobOrder();

        $sheet->setCellValue('A1', 'Job Order Number');
        $sheet->setCellValue('B1', 'Date');
        $sheet->setCellValue('C1', 'By');
        $sheet->setCellValue('D1', 'Customer Code');
        $sheet->setCellValue('E1', 'Customer PO');
        $sheet->setCellValue('F1', 'Due Date');
        $sheet->setCellValue('G1', 'Currency');
        $sheet->setCellValue('H1', 'Discount');
        $sheet->setCellValue('I1', 'Memo');
        $sheet->setCellValue('J1', 'Credit Term');
        $sheet->setCellValue('K1', 'Assembly Code');
        $sheet->setCellValue('L1', 'Quantity');
        $sheet->setCellValue('M1', 'Balance Quantity');
        $sheet->setCellValue('N1', 'Model Code');
        $rows = 2;

        foreach ($dataTemplate as $dt){
            $sheet->setCellValue('A'.$rows, $dt['jo_number']);
            $sheet->setCellValue('B'.$rows, $dt['jo_date']);
            $sheet->setCellValue('C'.$rows, $dt['user_username']);
            $sheet->setCellValue('D'.$rows, $dt['jo_customer_code']);
            $sheet->setCellValue('E'.$rows, $dt['jo_customer_po']);
            $sheet->setCellValue('F'.$rows, $dt['jo_due_date']);
            $sheet->setCellValue('G'.$rows, $dt['currency_name']);
            $sheet->setCellValue('H'.$rows, $dt['jo_discount']);
            $sheet->setCellValue('I'.$rows, $dt['jo_memo']);
            $sheet->setCellValue('J'.$rows, $dt['jo_credit_term']);
            $sheet->setCellValue('K'.$rows, $dt['jo_assembly_code']);
            $sheet->setCellValue('L'.$rows, $dt['jo_quantity']);
            $sheet->setCellValue('M'.$rows, $dt['jo_balance_quantity']);
            $sheet->setCellValue('N'.$rows, $dt['jo_model_code']);
            $rows++;
        }

        // buat excelnya, fyi inisialisasi spreadsheet itu buat file excel kosong baru dan writer itu mengisi file kosong itu dengan data diatas
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $date = date('j M Y');
        $fileName = 'All data job order - ' . $date;
    
        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName.'.xlsx');
        header('Cache-Control: max-age=0');
    
        // auto download disini, gaperlu dibalikin ke redirect lagi
        $writer->save('php://output');

        // return redirect()->to('/warehouse/products');
    }

    public function deleteJobOrder() {
        $deleteId = $this->request->getVar('deleteId');
        $importCode = $this->request->getVar('importId');

        // cek data apakah ada
        if($this->models->checkDataForDeleteJo($deleteId) != null) {
            // auto increment delete code
            if($this->models->getLastIdDataJobOrderLog() != null) {
                $lastJo = $this->models->getLastIdDataJobOrderLog();
                $number = explode('-', $lastJo['import_code']);
                if(substr($number[2],0,4) == date('Y')) {                
                    $deleteCode = 'DJO-PTPF-' . intval($number[2] + 1);
                } else {
                    $deleteCode = 'DJO-PTPF-' . date('Y') . '00001';
                }
            } else {
                $deleteCode = 'DJO-PTPF-' . date('Y') . '00001';
            }

            $getOldData  = $this->models->checkDataForDeleteJo($deleteId);
            $filename = "Manual Delete";
            $userId = $this->session->get('user_id');

            // masukan ke log
            $this->models->deleteDataJoLog($deleteCode, $getOldData['jo_number'], $getOldData['jo_assembly_code'], $filename, $userId);

            // delete dari database
            $this->models->deleteDataJo($deleteId);

            $this->session->setFlashdata('success_message', '<strong>Successfully !!</strong> delete existing data, thanks..');
            return redirect()->to('main/jobOrder');

        } else {
            $validation = \Config\Services::validation();
            $this->session->setFlashdata('error_message', '<strong>Failed !!</strong> delete data, data not found detected ! please try again later..');
            return redirect()->to('main/jobOrder');
        }
    }
}
