<?php

namespace App\Controllers\main;
use App\Controllers\BaseController;
use Config\Services;
use App\Models\warehouse\models;
// use App\Models\models;

class c_masterPlan extends BaseController
{
    protected $table = 'job_order';
    protected $database = 'main';
    protected $column_order = ['jo_id', 'jo_number']; 
    protected $column_search = ['jo_number', 'jo_assembly_code', 'mp.product_code'];
    protected $order = ['jo_id' => 'DESC'];

    public function __construct() {
        $this->session = session();
        $this->request = Services::request();
        $this->models = new models($this->request, $this->table, $this->column_order, $this->column_search, $this->order, $this->database, 'masterPlan');
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index($id = null)
    {
        if(!$this->session->get('login')) {
            return redirect()->to('/');
        }

        $products = $this->models->getAllProducts();

        $data = [
            'products'      => $products,
            'validation'    => \Config\Services::validation(),
            // add new main product
            'multipleUom'   => $this->models->getAllDataMultipleUom(),
            'uomSchema'     => $this->models->getAllDataUomSchema(),
            'currency'      => $this->models->getAllDataCurrency(),
            'status'        => $this->models->getAllDataStatus(),
            // add new user management
            'dept'          => $this->models->getAllDataDept(),
            'level'         => $this->models->getAllDataLevel(),
            // add new assembly
            'whs'           => $this->models->getAllDataWhs(),
        ];
        
        return view('main/v_masterPlan.php', $data); 
    }

    public function ajaxList()
    {   
        if ($this->request->getMethod(true) === 'POST') {
            $lists = $this->models->getDatatables();
            $data = [];
            $no = $this->request->getPost('start');

            foreach ($lists as $list) {
                $no++; 
                $statusColor = floatval($list->totalWithdrawal) > floatval($list->totalRequired) ? 'badge-danger' : 'badge-warning';
                $row = [];
                $row[] = $no;
                $row[] = "<p class='fw-bold text-success'>$list->jo_number</p><small class='text-muted'>$list->jo_date</small>";
                $row[] = "<p class='fw-bold text-warning'>$list->jo_assembly_code</p><small class='text-muted'>$list->assembly_name</small>";
                $row[] = "<p class='fw-bold text-primary'>$list->bom_product_code</p><small class='text-muted'>$list->product_name</small>";
                $row[] = "<p class='fw-bold text-dark'>$list->stock_quantity</p>";
                $row[] = "<p class='fw-bold text-info'>Jo Qty: ".round($list->jo_quantity,2)."</p><small>Balance Qty: ".round($list->jo_balance_quantity,2)."</small>";
                $row[] = "<p class='fw-bold text-info'>Total: ".round($list->totalWithdrawal,2)."</p><small>Withdrawal need: ".round($list->totalRequired,2)." (".round($list->needWithdrawal,2).")</small>";
                $row[] = "<p class='badge $statusColor'>$list->status</p>";
                $row[] = "<p class='fw-bold text-info'>Total: ".round($list->totalPrice,4)."</p><small>Unit Price /Item: ".round($list->product_unit_price,4)."</small>";
                $data[] = $row;
            }

            $output = [
                'draw' => $this->request->getPost('draw'),
                'recordsTotal' => $this->models->countAll('custom', 'masterPlan'),
                'recordsFiltered' => $this->models->countFiltered(),
                'data' => $data
            ];

            echo json_encode($output);
        }
    }
}
