<?php

namespace App\Controllers\main;
use App\Controllers\BaseController;
use Config\Services;
use App\Models\warehouse\models;
// use App\Models\models;

class c_products extends BaseController
{
    protected $table = 'product';
    protected $database = 'main';
    protected $column_order = ['product_id', 'product_code', null, null, null, null, 'product_status', 'product_id']; 
    protected $column_search = ['product_code', 'product_name', 'product_description', 'product_status'];
    protected $order = ['product_id' => 'DESC'];

    public function __construct() {
        $this->session = session();
        $this->request = Services::request();
        $this->models = new models($this->request, $this->table, $this->column_order, $this->column_search, $this->order, $this->database, 'mainProduct');
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index($id = null)
    {

        if(!$this->session->get('login')) {
            return redirect()->to('/');
        }
        
        $products = $this->models->getAllProducts();

        $data = [
            'products'      => $products,
            'validation'    => \Config\Services::validation(),
            // add new main product
            'multipleUom'   => $this->models->getAllDataMultipleUom(),
            'uomSchema'     => $this->models->getAllDataUomSchema(),
            'currency'      => $this->models->getAllDataCurrency(),
            'status'        => $this->models->getAllDataStatus(),
            // add new user management
            'dept'          => $this->models->getAllDataDept(),
            'level'         => $this->models->getAllDataLevel(),
            // add new assembly
            'whs'           => $this->models->getAllDataWhs(),
        ];
        
        return view('main/v_products.php', $data); 
    }

    public function ajaxList()
    {   
        if ($this->request->getMethod(true) === 'POST') {
            $lists = $this->models->getDatatables();
            $data = [];
            $no = $this->request->getPost('start');

            foreach ($lists as $list) {
                $no++; 
                $status = $list->product_status == 1 ? 'text-success' : 'text-danger';
                $row = [];
                $row[] = $no;
                $row[] = "<p class='fw-bold text-success'>$list->product_code</p><small>$list->product_type</small>";
                $row[] = $list->product_name;
                $row[] = $list->product_description;
                $row[] = $list->product_uom;
                $row[] = $list->currency_name . ' ' . floatval($list->product_unit_price);
                $row[] = $list->product_valuation_method;
                $row[] = "<p class='fw-bold $status'>$list->status_name</p>";
                $row[] = "
                <a href='#' data-bs-toggle='modal' data-bs-target='#modalEditProducts' id='editProducts' class='text-decoration-none text-warning far fa-edit fa-1x fa-fw'
                data-id='$list->product_id' data-code='$list->product_code' data-type='$list->product_type' data-name='$list->product_name' data-desc='$list->product_description' data-uom='$list->product_uom'
                data-muom='$list->product_multiple_uom' data-uoms='$list->product_uom_schema' data-currency='$list->product_currency' data-status='$list->product_status'
                data-uprice='$list->product_unit_price' data-valuation='$list->product_valuation_method' data-category='$list->product_category' data-import='$list->product_import_code'></a>
                <a href='#' data-bs-toggle='modal' data-bs-target='#modalDelete' data-id='$list->product_id' data-import='$list->product_import_code' id='deleteItemProducts' class='text-decoration-none text-danger fas fa-trash-alt fa-1x fa-fw'></a>";
                $data[] = $row;
            }

            $output = [
                'draw' => $this->request->getPost('draw'),
                'recordsTotal' => $this->models->countAll($this->database),
                'recordsFiltered' => $this->models->countFiltered(),
                'data' => $data
            ];

            echo json_encode($output);
        }
    }

    public function importProducts() {
        $file = $this->request->getFile('importProducts');
        $filename = $file->getName();
        $userId = $this->session->get('user_id');
        $ext = $file->getClientExtension();
        
        if($ext == 'xls') {
            $render = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
        } else {
            $render = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        }

        // jika error = 0 maka file ada, jika error = 4 maka file null
        if($file->getError() == 4) {
            $validation = \Config\Services::validation();
            $this->session->setFlashdata('error_message', '<strong>Failed !!</strong> file not found ! Please try again...');
            return redirect()->to('main/products')->withInput()->with('validation', $validation);
        }

        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file);

        $sheet = $spreadsheet->getActiveSheet()->toArray();
        $log = array('success' => 0, 'failed' => 0);

        // auto increment import code
        if($this->models->getLastIdDataProductsLog() != null) {
            $lastProducts = $this->models->getLastIdDataProductsLog();
            $number = explode('-', $lastProducts['import_code']);
            if(substr($number[2],0,4) == date('Y')) {                
                $importCode = 'IP-PTPF-' . intval($number[2] + 1);
            } else {
                $importCode = 'IP-PTPF-' . date('Y') . '00001';
            }
        } else {
            $importCode = 'IP-PTPF-' . date('Y') . '00001';
        }

        if($spreadsheet != null) {
            foreach($sheet as $baris => $kolom) {
                if($baris == 0) {
                    continue;
                }
				
                // validasi duplicate
                $check = $this->models->checkDuplicateProducts($kolom['0'], $kolom['2']);

                if($kolom['0'] == null && $kolom['1'] == null && $kolom['2'] == null && $kolom['3'] == null && $kolom['4'] == null && $kolom['5'] == null && $kolom['6'] == null && $kolom['7'] == null && $kolom['8'] == null && $kolom['9'] === null && $kolom['10'] == null) {
                    continue;
                }

                if($check != null) {
                    $log['failed'] += 1;
                    // dd($log);
                    continue;
                } elseif($kolom['0'] == null || $kolom['2'] == null || $kolom['3'] == null || $kolom['4'] == null || $kolom['7'] == null || $kolom['8'] == null || $kolom['9'] === null || $kolom['10'] == null) {
                    $log['failed'] += 1;
                    continue;
                } else {

                    $productCode = $kolom['0'];
                    $productType = $kolom['1'];
                    $productName = $kolom['2'];
                    $productDescription = $kolom['3'];
                    $productUom = $kolom['4'];
                    $productMultipleUom = $kolom['5'];
                    $productUomSchema = $kolom['6'];
                    $productCurrency = $kolom['7'];
                    $productStatus = $kolom['8'];
                    $productUnitPrice = floatval($kolom['9']);
                    $productValuation = $kolom['10'];
                    $productCategory = $kolom['11'];
                            
                    // masukan ke database
                    $this->models->importProducts($productCode, $productType, $productName, $productDescription, $productUom, $productMultipleUom, $productUomSchema, $productCurrency, $productStatus, $productUnitPrice, $productValuation, $productCategory,
                        $importCode);

                    // import product ke stock 
                    $this->models->insertToStockWarehouse($productCode, 1, 0);

                    // import log sekalian 
                    $this->models->importProductsLog($importCode, $productCode, $filename, $userId);
                    $log['success'] += 1;
                }
            }  

            $this->session->setFlashdata('success_message', '<strong>Successfully !!</strong> import ' . $log['success'] . ' and ' . $log['failed'] . ' <span class="text-danger">failure</span>');
            return redirect()->to('main/products');
        }
    }

    public function exportTemplateProducts() {
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $dataMultipleUom = $this->models->getAllDataMultipleUom();
        $dataUomSchema = $this->models->getAllDataUomSchema();
        $dataCurrency = $this->models->getAllDataCurrency();
        $dataStatus = $this->models->getAllDataStatus();

        $sheet->setCellValue('A1', 'Product Code/Id*');
        $sheet->setCellValue('B1', 'Product Type');
        $sheet->setCellValue('C1', 'Product Name*');
        $sheet->setCellValue('D1', 'Description*');
        $sheet->setCellValue('E1', 'Uom*');
        $sheet->setCellValue('F1', 'Multiple Uom');
        $sheet->setCellValue('G1', 'Uom Schema');
        $sheet->setCellValue('H1', 'Currency*');
        $sheet->setCellValue('I1', 'Status*');
        $sheet->setCellValue('J1', 'Unit Price*');
        $sheet->setCellValue('K1', 'Valuation Method*');
        $sheet->setCellValue('L1', 'Inventory Account');

        $sheet->setCellValue('A2', 'WIP-1909-00344');
        $sheet->setCellValue('B2', 'Inventory Assembly');
        $sheet->setCellValue('C2', 'JB1-WV-LF-0027-01');
        $sheet->setCellValue('D2', 'JOB OUT2-ATTACH WEAVING-COLOMBIER CHR');
        $sheet->setCellValue('E2', 'Pc(s)');
        $sheet->setCellValue('F2', '1');
        $sheet->setCellValue('G2', '1');
        $sheet->setCellValue('H2', '1');
        $sheet->setCellValue('I2', '1');
        $sheet->setCellValue('J2', '500000');
        $sheet->setCellValue('K2', 'Moving Average');
        $sheet->setCellValue('L2', 'Raw Materials Inventory - Hardware (Local)');

        // informasi untuk template 
        $sheet->setCellValue('N2', "# Information");
        $sheet->setCellValue('N3', "# Multiple Uom");

        $rowMu = 4;

        foreach($dataMultipleUom as $dmu) {
            $sheet->setCellValue('N'.$rowMu, $dmu['multiple_uom_id'] . ". " . $dmu['multiple_uom_name']);
            $rowMu++;
        }

        $sheet->setCellValue('N'.$rowMu + 2, "# Uom Schema");
        $rowUs = $rowMu + 3;

        foreach($dataUomSchema as $dus) {
            $sheet->setCellValue('N'.$rowUs, $dus['uom_schema_id'] . ". " . $dus['uom_schema_name']);
            $rowMu++;
        }

        $sheet->setCellValue('P3', "# Currency");
        $rowCurrency = 4;

        foreach($dataCurrency as $dc) {
            $sheet->setCellValue('P'.$rowCurrency, $dc['currency_id'] . ". " . $dc['currency_name']);
            $rowCurrency++;
        }

        $sheet->setCellValue('P'. $rowCurrency + 2, "# Status");
        $rowStatus = $rowCurrency + 3;

        foreach($dataStatus as $ds) {
            $sheet->setCellValue('P'.$rowStatus, $ds['status_id'] . ". " . $ds['status_name']);
            $rowStatus++;
        }

        // buat excelnya, fyi inisialisasi spreadsheet itu buat file excel kosong baru dan writer itu mengisi file kosong itu dengan data diatas
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $fileName = 'Template import products';
    
        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName.'.xlsx');
        header('Cache-Control: max-age=0');
    
        // auto download disini, gaperlu dibalikin ke redirect lagi
        $writer->save('php://output');

        // return redirect()->to('/warehouse/products');
    }

    public function exportDataProducts() {
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $dataTemplate = $this->models->getAllDataMainProducts();

        $sheet->setCellValue('A1', 'Product Code/Id');
        $sheet->setCellValue('B1', 'Product Type');
        $sheet->setCellValue('C1', 'Product Name');
        $sheet->setCellValue('D1', 'Description');
        $sheet->setCellValue('E1', 'Uom');
        $sheet->setCellValue('F1', 'Multiple Uom');
        $sheet->setCellValue('G1', 'Uom Schema');
        $sheet->setCellValue('H1', 'Currency');
        $sheet->setCellValue('I1', 'Status');
        $sheet->setCellValue('J1', 'Unit Price');
        $sheet->setCellValue('K1', 'Valuation Method');
        $sheet->setCellValue('L1', 'Inventory Account');
        $rows = 2;

        foreach ($dataTemplate as $dt){
            $sheet->setCellValue('A' . $rows, $dt['product_code']);
            $sheet->setCellValue('B' . $rows, $dt['product_type']);
            $sheet->setCellValue('C' . $rows, $dt['product_name']);
            $sheet->setCellValue('D' . $rows, $dt['product_description']);
            $sheet->setCellValue('E' . $rows, $dt['product_uom']);
            $sheet->setCellValue('F' . $rows, $dt['multiple_uom_name']);
            $sheet->setCellValue('G' . $rows, $dt['uom_schema_name']);
            $sheet->setCellValue('H' . $rows, $dt['currency_name']);
            $sheet->setCellValue('I' . $rows, $dt['status_name']);
            $sheet->setCellValue('J' . $rows, $dt['product_unit_price']);
            $sheet->setCellValue('K' . $rows, $dt['product_valuation_method']);
            $sheet->setCellValue('L' . $rows, $dt['product_category']);
            $rows++;
        }

        // buat excelnya, fyi inisialisasi spreadsheet itu buat file excel kosong baru dan writer itu mengisi file kosong itu dengan data diatas
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $date = date('j M Y');
        $fileName = 'All data products - ' . $date;
    
        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName.'.xlsx');
        header('Cache-Control: max-age=0');
    
        // auto download disini, gaperlu dibalikin ke redirect lagi
        $writer->save('php://output');

        // return redirect()->to('/warehouse/products');
    }

    public function insertProducts() {
        // get data from modals
        $productCode        = $this->request->getVar('productCode');
        $productType        = $this->request->getVar('productType');
        $productName        = $this->request->getVar('productName');
        $productDescription = $this->request->getVar('productDescription');
        $productUom         = $this->request->getVar('productUom');
        $productMultipleUom = $this->request->getVar('productMultipleUom');
        $productUomSchema   = $this->request->getVar('productUomSchema');
        $productCurrency    = $this->request->getVar('productCurrency');
        $productStatus      = $this->request->getVar('productStatus');
        $productUnitPrice   = floatval($this->request->getVar('productUnitPrice'));
        $productValuation   = $this->request->getVar('productValuation');
        $productCategory   = $this->request->getVar('productCategory');

        $userId = $this->session->get('user_id');
        $filename = 'Manual Insert';

        // auto increment add code
        if($this->models->getLastIdDataProductsLog() != null) {
            $lastProducts = $this->models->getLastIdDataProductsLog();
            $number = explode('-', $lastProducts['import_code']);
            if(substr($number[2],0,4) == date('Y')) {                
                $addCode = 'AP-PTPF-' . intval($number[2] + 1);
            } else {
                $addCode = 'AP-PTPF-' . date('Y') . '00001';
            }
        } else {
            $addCode = 'AP-PTPF-' . date('Y') . '00001';
        }

        // validasi
        if(!$this->validate([
            'productCode'       => 'required|is_unique[product.product_code]',
            'productName'       => 'required|is_unique[product.product_name]',
            'productDescription' => 'required',
            'productUom'        => 'required',
            'productCurrency'   => 'required',
            'productStatus'     => 'required',
            'productUnitPrice'  => 'required',
            'productValution'   => 'required',
        ], [
            'productCode'   => [
                'required'  => 'Value is required !',
                'is_unique' => 'Value is already exists !'
            ], 
            'productName'   => [
                'required'  => 'Value is required !',
                'is_unique' => 'Value is already exists'
            ], 
            'productDescription'    => [
                'required'  => 'Value is required !'
            ], 
            'productUom'    => [
                'required'  => 'Value is required !'
            ], 
            'productCurrency' => [
                'required'  => 'Value is required !'
            ], 
            'productStatus' => [
                'required'  => 'Value is required !'
            ],
            'productUnitPrice'  => [
                'required'  => 'Value is required'
            ],
            'productValution'  => [
                'required'  => 'Value is required'
            ]
        ])) {
            // set session failed
            $validation = \Config\Services::validation();
            $this->session->setFlashdata('error_message', '<strong>Failed !</strong> insert new data, please check & try again !');
            return redirect()->to('main/products')->withInput()->with('validation', $validation);
        } else {
            // sukses validasi

            // masukan ke database
            $this->models->insertProducts($productCode, $productType, $productName, $productDescription, $productUom, $productMultipleUom, $productUomSchema, $productCurrency,
            $productStatus, $productUnitPrice, $productValution, $productCategory, $addCode);

            // masukan ke log
            $this->models->insertProductsLog($addCode, $productCode, $filename, $userId);

            // set session sukses
            $this->session->setFlashdata('success_message', '<strong>Successfully !!</strong> insert new data, thanks..');
            return redirect()->to('main/products');
        }
    }

    public function editProducts() {
        $productId      = $this->request->getVar('editProductId');
        $productCode    = $this->request->getVar('editProductCode');
        $productType    = $this->request->getVar('editProductType');
        $productName    = $this->request->getVar('editProductName');
        $importCode     = $this->request->getVar('editImportCode');
        $desc           = $this->request->getVar('editProductDescription');
        $uom            = $this->request->getVar('editProductUom');
        $mUom           = $this->request->getVar('editProductMultipleUom');
        $uomSchema      = $this->request->getVar('editProductUomSchema');
        $status         = $this->request->getVar('editProductStatus');
        $currency       = $this->request->getVar('editProductCurrency');
        $unitPrice      = floatval($this->request->getVar('editProductUnitPrice'));
        $productValuation = $this->request->getVar('editProductValuation');
        $category       = $this->request->getVar('editProductCategory');

        // dd($productId, $productCode, $productType, $productName, $importCode, $desc, $mUom, $uom, $uomSchema, $status, $currency, $unitPrice);

        $userId = $this->session->get('user_id');
        $filename = 'Manual Update';

        if($this->models->getDataForEditProduct($productId) != null) {
            $getOldData     = $this->models->getDataForEditProduct($productId);
        } else {
            $validation = \Config\Services::validation();
            $this->session->setFlashdata('error_message', '<strong>Failed !!</strong> update existing data, please try again later..');
            return redirect()->to('main/products');
        }

        // setup specific rule
        if($getOldData['product_code'] == $productCode) {
            $rule1 = 'required';
        } else {
            $rule1 = 'required|is_unique[product.product_code]';
        }

        if($getOldData['product_name'] == $productName) {
            $rule2 = 'required';
        } else {
            $rule2 = 'required|is_unique[product.product_name]';
        }

        // general validation
        if(!$this->validate([
            'editProductId'     => 'required',
            'editProductCode'   => $rule1,
            'editProductName'   => $rule2,
            'editImportCode'    => 'required',
            'editProductDescription'    => 'required',
            'editProductUom'    => 'required',
            'editProductStatus' => 'required',
            'editProductCurrency'       => 'required',
            'editProductUnitPrice'      => 'required',
            'editProductValuation'      => 'required',
        ], [
            'editProductId' => [
                'required'  => 'Must be filled !'
            ],
            'editProductCode'   => [
                'required'  => 'Must be filled !',
                'is_unique' => 'Values is already exists !'
            ],
            'editProductName'   => [
                'required'  => 'Must be filled !',
                'is_unique' => 'Values is already exists !'
            ],
            'editImportCode'    => [
                'required'  => 'Must be filled !'
            ],
            'editProductDescription'    => [
                'required'  => 'Must be filled !'
            ],
            'editProductUom'    => [
                'required'  => 'Must be filled !'
            ],
            'editProductStatus' => [
                'required'  => 'Must be filled !'
            ],
            'editProductCurrency'   => [
                'required'  => 'Must be filled !'
            ],
            'editProductUnitPrice'  => [
                'required'  => 'Must be filled !'
            ],
            'editProductValuation'  => [
                'required'  => 'Must be filled !'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $this->session->setFlashdata('error_message', '<strong>Failed !!</strong> update existing data, please try again later..');
            return redirect()->to('main/products')->withInput()->with('validation', $validation);
        } else {
            // auto increment update code
            if($this->models->getLastIdDataProductsLog() != null) {
                $lastProducts = $this->models->getLastIdDataProductsLog();
                $number = explode('-', $lastProducts['import_code']);
                if(substr($number[2],0,4) == date('Y')) {                
                    $updateCode = 'EP-PTPF-' . intval($number[2] + 1);
                } else {
                    $updateCode = 'EP-PTPF-' . date('Y') . '00001';
                }
            } else {
                $updateCode = 'EP-PTPF-' . date('Y') . '00001';
            }

            // update products
            $this->models->updateDataProducts($productId, $productCode, $productType, $productName, $updateCode, $desc, $uom, $mUom, $uomSchema, $status, $currency, $unitPrice, $productValuation, $category);

            // create log
            $this->models->updateDataProductsLog($updateCode, $productCode, $filename, $userId);

            $this->session->setFlashdata('success_message', '<strong>Successfully !!</strong> update existing data, thanks..');
            return redirect()->to('main/products');
        }
    }

    public function deleteProducts() {
        $deleteId = $this->request->getVar('deleteId');
        $importCode = $this->request->getVar('importId');

        // cek data apakah ada
        if($this->models->checkDataForDeleteProducts($deleteId) != null) {
            // auto increment update code
            if($this->models->getLastIdDataProductsLog() != null) {
                $lastProducts = $this->models->getLastIdDataProductsLog();
                $number = explode('-', $lastProducts['import_code']);
                if(substr($number[2],0,4) == date('Y')) {                
                    $deleteCode = 'DP-PTPF-' . intval($number[2] + 1);
                } else {
                    $deleteCode = 'DP-PTPF-' . date('Y') . '00001';
                }
            } else {
                $deleteCode = 'DP-PTPF-' . date('Y') . '00001';
            }

            $getOldData  = $this->models->checkDataForDeleteProducts($deleteId);
            $filename = "Manual Delete";
            $userId = $this->session->get('user_id');

            // masukan ke log
            $this->models->deleteDataProductsLog($deleteCode, $getOldData['product_code'], $filename, $userId);

            // delete dari database
            $this->models->deleteDataProducts($deleteId);

            $this->session->setFlashdata('success_message', '<strong>Successfully !!</strong> delete existing data, thanks..');
            return redirect()->to('main/products');

        } else {
            $validation = \Config\Services::validation();
            $this->session->setFlashdata('error_message', '<strong>Failed !!</strong> delete data, data not found detected ! please try again later..');
            return redirect()->to('main/products');
        }
    }

    public function exportTemplateUnitPrice() {
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValue('A1', 'Product Code/Id*');
        $sheet->setCellValue('B1', 'Old Unit Price*');
        $sheet->setCellValue('C1', 'New Unit Price*');

        $sheet->setCellValue('A2', 'RWM-2010-02387');
        $sheet->setCellValue('B2', '350800.000000');
        $sheet->setCellValue('C2', '360800.000000');


        // buat excelnya, fyi inisialisasi spreadsheet itu buat file excel kosong baru dan writer itu mengisi file kosong itu dengan data diatas
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $fileName = 'Template import products unit price';
    
        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName.'.xlsx');
        header('Cache-Control: max-age=0');
    
        // auto download disini, gaperlu dibalikin ke redirect lagi
        $writer->save('php://output');

        // return redirect()->to('/warehouse/products');
    }

    public function importUnitPrice() {
        $file = $this->request->getFile('importUnitPrice');
        $filename = $file->getName();
        $userId = $this->session->get('user_id');
        $ext = $file->getClientExtension();
        
        if($ext == 'xls') {
            $render = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
        } else {
            $render = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        }

        // jika error = 0 maka file ada, jika error = 4 maka file null
        if($file->getError() == 4) {
            $validation = \Config\Services::validation();
            $this->session->setFlashdata('error_message', '<strong>Failed !!</strong> file not found ! Please try again...');
            return redirect()->to('main/products')->withInput()->with('validation', $validation);
        }

        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file);

        $sheet = $spreadsheet->getActiveSheet()->toArray();
        $log = array('success' => 0, 'failed' => 0);

        // auto increment import code
        if($this->models->getLastIdDataProductsLog() != null) {
            $lastProducts = $this->models->getLastIdDataProductsLog();
            $number = explode('-', $lastProducts['import_code']);
            if(substr($number[2],0,4) == date('Y')) {                
                $importCode = 'IUP-PTPF-' . intval($number[2] + 1);
            } else {
                $importCode = 'IUP-PTPF-' . date('Y') . '00001';
            }
        } else {
            $importCode = 'IUP-PTPF-' . date('Y') . '00001';
        }

        if($spreadsheet != null) {
            foreach($sheet as $baris => $kolom) {
                if($baris == 0) {
                    continue;
                }
				
                // validasi duplicate
                $check = $this->models->checkDuplicateProductsUnitPrice($kolom['0'], $importCode);

                if($kolom['0'] == null && $kolom['1'] === null && $kolom['2'] === null) {
                    continue;
                }

                if($check != null) {
                    $log['failed'] += 1;
                    // dd($log);
                    continue;
                } elseif($kolom['0'] == null || $kolom['1'] === null || $kolom['2'] === null) {
                    $log['failed'] += 1;
                    continue;
                } else {

                    $productCode = $kolom['0'];
                    $unitPrice = $kolom['2'];

                    $oldData = $this->models->getDataForUnitPrice($productCode);
                    if(floatval($kolom['1']) == $oldData['product_unit_price']) {
                        // masukan ke database
                        $this->models->importProductsUnitPrice($productCode, floatval($unitPrice), $importCode);
    
                        // import log sekalian 
                        $this->models->importProductsUnitPriceLog($importCode, $productCode, $filename, $userId);
                        $log['success'] += 1;
                    } else {
                        $log['failed'] += 1;
                        continue;
                    }    
                }
            }  

            $this->session->setFlashdata('success_message', '<strong>Successfully !!</strong> import ' . $log['success'] . ' and ' . $log['failed'] . ' <span class="text-danger">failure</span>');
            return redirect()->to('main/products');
        }
    }
}
