<?php

namespace App\Controllers\main;
use App\Controllers\BaseController;
use Config\Services;
use App\Models\warehouse\models;
// use App\Models\models;

class c_receivementReport extends BaseController
{
    protected $table = 'receivement_report';
    protected $database = 'main';
    protected $column_order = ['rr_id', 'rr_number', 'rr_product_code', null, 'rr_vendor_code', null, 'rr_by', 'rr_id']; 
    protected $column_search = ['rr_date', 'rr_product_code', 'rr_number', 'rr_vendor_code'];
    protected $order = ['rr_id' => 'DESC'];

    public function __construct() {
        $this->session = session();
        $this->request = Services::request();
        $this->models = new models($this->request, $this->table, $this->column_order, $this->column_search, $this->order, $this->database, 'mainReceivementReport');
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index($id = null)
    {
        if(!$this->session->get('login')) {
            return redirect()->to('/');
        }

        $products = $this->models->getAllProducts();

        $data = [
            'products'      => $products,
            'validation'    => \Config\Services::validation(),
            // add new main product
            'multipleUom'   => $this->models->getAllDataMultipleUom(),
            'uomSchema'     => $this->models->getAllDataUomSchema(),
            'currency'      => $this->models->getAllDataCurrency(),
            'status'        => $this->models->getAllDataStatus(),
            // add new user management
            'dept'          => $this->models->getAllDataDept(),
            'level'         => $this->models->getAllDataLevel(),
            // add new assembly
            'whs'           => $this->models->getAllDataWhs(),
        ];
        
        return view('main/v_receivementReport.php', $data); 
    }

    public function ajaxList()
    {   
        if ($this->request->getMethod(true) === 'POST') {
            $fromDate = $this->request->getVar('fromDate');
            $toDate = $this->request->getVar('toDate');

            $lists = $this->models->getDatatables($fromDate, $toDate, 'rr');

            $data = [];
            $no = $this->request->getPost('start');

            foreach ($lists as $list) {
                $no++; 
                $row = [];
                $row[] = $no;
                $row[] = "<p class='fw-bold text-success'>$list->rr_number</p><small class='text-muted'>$list->rr_date</small>";
                $row[] = "<p class='fw-bold text-dark'>$list->rr_product_code</p>";
                $row[] = "<p class='fw-bold text-dark'>$list->rr_product_name</p>";
                $row[] = "<p class='fw-bold text-success'>$list->rr_vendor_code</p><small class='text-muted'>$list->vendor_agency</small>";
                $row[] = floatval($list->rr_quantity);
                $row[] = $list->product_uom;
                $row[] = $list->currency_name . ' ' . $list->product_unit_price;
                $row[] = $list->user_username;
                $row[] = "
                <a href='#' data-bs-toggle='modal' data-bs-target='#modalDelete' data-id='$list->rr_id' data-import='$list->rr_import_code' id='deleteItemRR' class='text-decoration-none text-danger fas fa-trash-alt fa-1x fa-fw'></a>";
                $data[] = $row;
            }

            $output = [
                'draw' => $this->request->getPost('draw'),
                'recordsTotal' => $this->models->countAll($this->database),
                'recordsFiltered' => $this->models->countFiltered($fromDate, $toDate, 'rr'),
                'data' => $data
            ];

            echo json_encode($output);
        }
    }

    public function importRR() {
        $file = $this->request->getFile('importRR');
        $filename = $file->getName();
        $userId = $this->session->get('user_id');
        $ext = $file->getClientExtension();
        
        if($ext == 'xls') {
            $render = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
        } else {
            $render = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        }

        // jika error = 0 maka file ada, jika error = 4 maka file null
        if($file->getError() == 4) {
            $validation = \Config\Services::validation();
            $this->session->setFlashdata('error_message', '<strong>Failed !!</strong> file not found ! Please try again...');
            return redirect()->to('main/receivementReport')->withInput()->with('validation', $validation);
        }

        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file);

        $sheet = $spreadsheet->getActiveSheet()->toArray();
        $log = array('success' => 0, 'failed' => 0);

        // auto increment import code
        if($this->models->getLastIdDataRRLog() != null) {
            $lastRR = $this->models->getLastIdDataRRLog();
            $number = explode('-', $lastRR['import_code']);
            if(substr($number[2],0,4) == date('Y')) {                
                $importCode = 'IRR-PTPF-' . intval($number[2] + 1);
            } else {
                $importCode = 'IRR-PTPF-' . date('Y') . '00001';
            }
        } else {
            $importCode = 'IRR-PTPF-' . date('Y') . '00001';
        }

        if($spreadsheet != null) {
            foreach($sheet as $baris => $kolom) {
                if($baris == 0) {
                    continue;
                }

                if($this->models->checkProductForRR($kolom['2']) != null) {
                    // validasi duplicate
                    $check = $this->models->checkDuplicateRR($kolom['0'], $kolom['2'], $kolom['4']);

                    if($kolom['0'] == null && $kolom['1'] == null && $kolom['2'] == null && $kolom['3'] == null && $kolom['4'] == null && $kolom['5'] === null) {
                        continue;
                    }

                    if($check != null) {
                        $log['failed'] += 1;
                        // dd($log);
                        continue;
                    } elseif($kolom['0'] == null || $kolom['1'] == null || $kolom['2'] == null || $kolom['3'] == null || $kolom['4'] == null || $kolom['5'] === null) {
                        $log['failed'] += 1;
                        // dd($log);
                        continue;
                    } else {
                        $rrNumber       = $kolom['0'];
                        $date           = $kolom['1'];
                        $productCode    = $kolom['2'];
                        $productName    = $kolom['3'];
                        $vendorCode     = $kolom['4'];
                        $quantity       = $kolom['5'];
                        $by             = $userId;
                        
                        // update stock warehouse
                        if($this->models->getDataForRR($kolom['2'], 1) != null) {
                            // masukan ke database
                            $this->models->importRR($rrNumber, $date, $productCode, $productName, $vendorCode, $quantity, $by, $importCode);

                            // // jika price dan currency ngga kosong update di product data nya
                            // if($price !== null && $currency != null) {
                            //     $this->models->updateProductPriceCurrencyFromRR($productCode, $price, $currency);

                            //     // import log update di product log
                            //     $this->models->importProductsUnitPriceLog($importCode, $productCode, 'Update Unit Price & Currency from Import RR', $userId);
                            // } else {
                            //     if($price !== null) {
                            //         // update data product unit price
                            //         $this->models->updateProductUnitPriceFromRR($productCode, $price);

                            //         // import log update di product log
                            //         $this->models->importProductsUnitPriceLog($importCode, $productCode, 'Update Unit Price from Import RR', $userId);
                            //     } elseif($currency != null) {
                            //         // update data product currency
                            //         $this->models->updateProductCurrencyFromRR($productCode, $currency);

                            //         // import log update di product log
                            //         $this->models->importProductsUnitPriceLog($importCode, $productCode, 'Update Currency from Import RR', $userId);
                            //     }
                            // }

                            $getOldData = $this->models->getDataForRR($kolom['2'], 1);
                            $newStock = floatval($getOldData['stock_quantity']) + floatval($kolom['4']);
                            $this->models->updateStockWarehouse($productCode, floatval($newStock));
                            
                            // import log sekalian 
                            $this->models->importRRLog($importCode, $rrNumber, $productCode, $vendorCode, $filename, $userId);
                            $log['success'] += 1;
                        } else {
                            $log['failed'] += 1;
                            continue;
                        }
                    }
                } else {
                    $log['failed'] += 1;
                    continue;
                }
            }  

            $this->session->setFlashdata('success_message', '<strong>Successfully !!</strong> import RR ' . $log['success'] . ' and ' . $log['failed'] . ' <span class="text-danger">failure</span>');
            return redirect()->to('main/receivementReport');
        }
    }

    public function exportTemplateRR() {
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $dataCurrency = $this->models->getAllDataCurrency();

        $sheet->setCellValue('A1', 'RR Number*');
        $sheet->setCellValue('B1', 'Date*');
        $sheet->setCellValue('C1', 'Product Code*');
        $sheet->setCellValue('D1', 'Product Name*');
        $sheet->setCellValue('E1', 'Vendor Code*');
        $sheet->setCellValue('F1', 'Quantity*');

        $sheet->setCellValue('A2', 'RR-PTPF-21007259');
        $sheet->setCellValue('B2', '2021-12-30');
        $sheet->setCellValue('C2', 'MNT-1910-00354');
        $sheet->setCellValue('D2', 'MT-SKF Explorer 6008-2RSH / C3');
        $sheet->setCellValue('E2', 'A00070');
        $sheet->setCellValue('F2', '2');

        // informasi untuk template 
        $sheet->setCellValue('H2', "# Information");
        $sheet->setCellValue('H3', "# Currency");

        $row = 4;

        foreach($dataCurrency as $dc) {
            $sheet->setCellValue('H'.$row, $dc['currency_id'] . ". " . $dc['currency_name']);
            $row++;
        }

        // buat excelnya, fyi inisialisasi spreadsheet itu buat file excel kosong baru dan writer itu mengisi file kosong itu dengan data diatas
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $fileName = 'Template import receiving report';
    
        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName.'.xlsx');
        header('Cache-Control: max-age=0');
    
        // auto download disini, gaperlu dibalikin ke redirect lagi
        $writer->save('php://output');

        // return redirect()->to('/warehouse/products');
    }

    public function exportDataRR() {
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $dataTemplate = $this->models->getAllDataRR();

        $sheet->setCellValue('A1', 'RR Number');
        $sheet->setCellValue('B1', 'Date');
        $sheet->setCellValue('C1', 'Product Code');
        $sheet->setCellValue('D1', 'Product Name');
        $sheet->setCellValue('E1', 'Vendor Code');
        $sheet->setCellValue('F1', 'Vendor Name');
        $sheet->setCellValue('G1', 'Quantity');
        $sheet->setCellValue('H1', 'Created By');
        $rows = 2;

        foreach ($dataTemplate as $dt){
            $sheet->setCellValue('A'.$rows, $dt['rr_number']);
            $sheet->setCellValue('B'.$rows, $dt['rr_date']);
            $sheet->setCellValue('C'.$rows, $dt['rr_product_code']);
            $sheet->setCellValue('D'.$rows, $dt['rr_product_name']);
            $sheet->setCellValue('E'.$rows, $dt['rr_vendor_code']);
            $sheet->setCellValue('F'.$rows, $dt['vendor_agency']);
            $sheet->setCellValue('G'.$rows, $dt['rr_quantity']);
            $sheet->setCellValue('H'.$rows, $dt['user_username']);
            $rows++;
        }

        // buat excelnya, fyi inisialisasi spreadsheet itu buat file excel kosong baru dan writer itu mengisi file kosong itu dengan data diatas
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $date = date('j M Y');
        $fileName = 'All data receivement report - ' . $date;
    
        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName.'.xlsx');
        header('Cache-Control: max-age=0');
    
        // auto download disini, gaperlu dibalikin ke redirect lagi
        $writer->save('php://output');

        // return redirect()->to('/warehouse/products');
    }

    public function deleteRR() {
        $deleteId = $this->request->getVar('deleteId');
        $importCode = $this->request->getVar('importId');

        // cek data apakah ada
        if($this->models->checkDataForDeleteRR($deleteId) != null) {
            // auto increment delete code
            if($this->models->getLastIdDataRRLog() != null) {
                $lastVendor = $this->models->getLastIdDataRRLog();
                $number = explode('-', $lastVendor['import_code']);
                if(substr($number[2],0,4) == date('Y')) {                
                    $deleteCode = 'DRR-PTPF-' . intval($number[2] + 1);
                } else {
                    $deleteCode = 'DRR-PTPF-' . date('Y') . '00001';
                }
            } else {
                $deleteCode = 'DRR-PTPF-' . date('Y') . '00001';
            }

            $getOldData  = $this->models->checkDataForDeleteRR($deleteId);
            $filename = "Manual Delete";
            $userId = $this->session->get('user_id');

            // masukan ke log
            $this->models->deleteDataRRLog($deleteCode, $getOldData['rr_number'], $getOldData['rr_product_code'], $getOldData['rr_vendor_code'], $filename, $userId);

            // delete ngurangi stock di product available
            $oldDataProduct = $this->models->getDataProductAvailable($getOldData['rr_product_code']);
            $newStock = floatval($oldDataProduct['stock_quantity']) - floatval($getOldData['rr_quantity']);
            $this->models->updateStockWarehouse($getOldData['rr_product_code'], $newStock);

            // delete dari database
            $this->models->deleteDataRR($deleteId);

            $this->session->setFlashdata('success_message', '<strong>Successfully !!</strong> delete existing data, thanks..');
            return redirect()->to('main/receivementReport');

        } else {
            $validation = \Config\Services::validation();
            $this->session->setFlashdata('error_message', '<strong>Failed !!</strong> delete data, data not found detected ! please try again later..');
            return redirect()->to('main/receivementReport');
        }
    }
}
