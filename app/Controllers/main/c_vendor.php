<?php

namespace App\Controllers\main;
use App\Controllers\BaseController;
use Config\Services;
use App\Models\warehouse\models;
// use App\Models\models;

class c_vendor extends BaseController
{
    protected $table = 'vendor';
    protected $database = 'main';
    protected $column_order = ['vendor_id', 'vendor_code', null, 'vendor_agency', null, null, 'vendor_name', null, null, null, null, 'vendor_status', 'vendor_id']; 
    protected $column_search = ['vendor_code', 'vendor_title', 'vendor_agency', 'vendor_address', 'vendor_email', 'vendor_name', 'vendor_telp', 'vendor_bank_name', 'vendor_bank_account', 'vendor_bank_number', 'vendor_debit_term_default'];
    protected $order = ['vendor_id' => 'DESC'];

    public function __construct() {
        $this->session = session();
        $this->request = Services::request();
        $this->models = new models($this->request, $this->table, $this->column_order, $this->column_search, $this->order, $this->database, 'mainVendor');
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index()
    {
        if(!$this->session->get('login')) {
            return redirect()->to('/');
        }

        $products = $this->models->getAllProducts();

        $data = [
            'products'      => $products,
            'validation'    => \Config\Services::validation(),
            // add new main product
            'multipleUom'   => $this->models->getAllDataMultipleUom(),
            'uomSchema'     => $this->models->getAllDataUomSchema(),
            'currency'      => $this->models->getAllDataCurrency(),
            'status'        => $this->models->getAllDataStatus(),
            // add new user management
            'dept'          => $this->models->getAllDataDept(),
            'level'         => $this->models->getAllDataLevel(),
            // add new assembly
            'whs'           => $this->models->getAllDataWhs(),
        ];
        
        return view('main/v_vendor.php', $data); 
    }

    public function ajaxList()
    {   
        if ($this->request->getMethod()) {
            $lists = $this->models->getDatatables();
            $data = [];
            $no = $this->request->getPost('start');

            foreach ($lists as $list) {
                $no++; 
                $row = [];
                $row[] = $no;
                $row[] = $list->vendor_code;
                $row[] = $list->vendor_title;
                $row[] = $list->vendor_agency;
                $row[] = $list->vendor_address;
                $row[] = $list->vendor_email;
                $row[] = $list->vendor_name;
                $row[] = $list->vendor_telp;
                $row[] = $list->vendor_bank_account;
                $row[] = $list->vendor_bank_number;
                $row[] = $list->vendor_debit_term_default;
                $row[] = $list->status_name;
                $row[] = "
                <a href='#' data-bs-toggle='modal' data-bs-target='#modalEditVendor' id='editVendor'
                data-id='$list->vendor_id' data-code='$list->vendor_code' data-title='$list->vendor_title' data-agency='$list->vendor_agency' data-address='$list->vendor_address'
                data-email='$list->vendor_email' data-name='$list->vendor_name' data-bank_name='$list->vendor_bank_name' data-bank_account='$list->vendor_bank_account' data-bank_number='$list->vendor_bank_number'
                data-telp='$list->vendor_telp' data-debit_term='$list->vendor_debit_term_default' data-status='$list->vendor_status' data-import='$list->vendor_import_code'
                class='text-decoration-none text-warning far fa-edit fa-1x fa-fw'></a>
                <a href='#' data-bs-toggle='modal' data-bs-target='#modalDelete' data-id='$list->vendor_id' data-import='$list->vendor_import_code' id='deleteItemVendor' class='text-decoration-none text-danger fas fa-trash-alt fa-1x fa-fw'></a>";
                $data[] = $row;
            }

            $output = [
                'draw' => $this->request->getPost('draw'),
                'recordsTotal' => $this->models->countAll($this->database),
                'recordsFiltered' => $this->models->countFiltered(),
                'data' => $data
            ];

            echo json_encode($output);
        }
    }

    public function importVendor() {
        $file = $this->request->getFile('importVendor');
        $filename = $file->getName();
        $userId = $this->session->get('user_id');
        $ext = $file->getClientExtension();
        
        if($ext == 'xls') {
            $render = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
        } else {
            $render = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        }

        // jika error = 0 maka file ada, jika error = 4 maka file null
        if($file->getError() == 4) {
            $validation = \Config\Services::validation();
            $this->session->setFlashdata('error_message', '<strong>Failed !!</strong> file not found ! Please try again...');
            return redirect()->to('main/vendor')->withInput()->with('validation', $validation);
        }

        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file);

        $sheet = $spreadsheet->getActiveSheet()->toArray();
        $log = array('success' => 0, 'failed' => 0);

        // auto increment import code
        if($this->models->getLastIdDataVendorLog() != null) {
            $lastVendor = $this->models->getLastIdDataVendorLog();
            $number = explode('-', $lastVendor['import_code']);
            if(substr($number[2],0,4) == date('Y')) {                
                $importCode = 'IV-PTPF-' . intval($number[2] + 1);
            } else {
                $importCode = 'IV-PTPF-' . date('Y') . '00001';
            }
        } else {
            $importCode = 'IV-PTPF-' . date('Y') . '00001';
        }


        if($spreadsheet != null) {
            foreach($sheet as $baris => $kolom) {
                if($baris == 0) {
                    continue;
                }

                // validasi duplicate
                $check = $this->models->checkDuplicateVendor($kolom['0'], $kolom['2']);

                // jika semua ngga diinput langsung continue biar yg note informatiaon ngga kebaca failed
                if($kolom['0'] == null && $kolom['1'] == null && $kolom['2'] == null && $kolom['3'] == null && $kolom['4'] == null && $kolom['5'] == null && $kolom['6'] == null &&
                $kolom['7'] == null && $kolom['8'] == null && $kolom['9'] == null && $kolom['10'] == null && $kolom['11'] == null) {
                    continue;
                }

                if($check != null) {
                    $log['failed'] += 1;
                    // dd($log);
                    continue;
                } elseif($kolom['0'] == null ||$kolom['2'] == null || $kolom['11'] == null) {
                    $log['failed'] += 1;
                    // dd($log);
                    continue;
                } else {
                    $vendorCode       = $kolom['0'];
                    $vendorTitle      = $kolom['1'];
                    $vendorAgency     = $kolom['2'];
                    $vendorAddress    = $kolom['3'];
                    $vendorEmail      = $kolom['4'];
                    $vendorName       = $kolom['5'];
                    $vendorTelp       = $kolom['6'];
                    $vendorBankName   = $kolom['7'];
                    $vendorBankAccount = $kolom['8'];
                    $vendorBankNumber = $kolom['9'];
                    $vendorDebitTermDefault = $kolom['10'];
                    $vendorStatus     = $kolom['11'];

                    // dd($vendorCode, $vendorTitle, $vendorAgency, $vendorAddress, $vendorEmail, $vendorName, $vendorTelp, $vendorBankName, $vendorBankAccount, $vendorBankNumber, $vendorDebitTermDefault, 
                    // $vendorStatus, $importCode);

                    $this->models->importVendor($vendorCode, $vendorTitle, $vendorAgency, $vendorAddress, $vendorEmail, $vendorName, $vendorTelp, $vendorBankName, $vendorBankAccount, $vendorBankNumber, $vendorDebitTermDefault, 
                        $vendorStatus, $importCode);

                    // import log sekalian 
                    $this->models->importVendorLog($importCode, $vendorCode, $filename, $userId);
                    $log['success'] += 1;
                }
            }  

            $this->session->setFlashdata('success_message', '<strong>Successfully !!</strong> import ' . $log['success'] . ' and ' . $log['failed'] . ' <span class="text-danger">failure</span>');
            return redirect()->to('main/vendor');
        }
    }

    public function exportTemplateVendor() {
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValue('A1', 'Vendor Code/Id*');
        $sheet->setCellValue('B1', 'Title');
        $sheet->setCellValue('C1', 'Agency*');
        $sheet->setCellValue('D1', 'Address');
        $sheet->setCellValue('E1', 'Email');
        $sheet->setCellValue('F1', 'Name');
        $sheet->setCellValue('G1', 'Telp');
        $sheet->setCellValue('H1', 'Bank Name');
        $sheet->setCellValue('I1', 'Bank Account');
        $sheet->setCellValue('J1', 'Bank Number');
        $sheet->setCellValue('K1', 'Debit Term');
        $sheet->setCellValue('L1', 'Status*');

        $sheet->setCellValue('A2', 'PTPF');
        $sheet->setCellValue('B2', 'PT');
        $sheet->setCellValue('C2', 'Pacific Furniture');
        $sheet->setCellValue('D2', 'Semarang, Tugu Wijaya III');
        $sheet->setCellValue('E2', 'ptpacificfurniture@pacificfurniture.id');
        $sheet->setCellValue('F2', 'Nuryadi');
        $sheet->setCellValue('G2', '083111222333');
        $sheet->setCellValue('H2', 'BCA');
        $sheet->setCellValue('I2', 'Nuryadi');
        $sheet->setCellValue('J2', '0098887654');
        $sheet->setCellValue('K2', 'Cash');
        $sheet->setCellValue('L2', '1');

        $sheet->setCellValue('N3', '# Information');
        $sheet->setCellValue('N4', '# Status');
        $sheet->setCellValue('N5', '1. Active');
        $sheet->setCellValue('N6', '2. Deactive');


        // buat excelnya, fyi inisialisasi spreadsheet itu buat file excel kosong baru dan writer itu mengisi file kosong itu dengan data diatas
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $fileName = 'Template import vendor';
    
        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName.'.xlsx');
        header('Cache-Control: max-age=0');
    
        // auto download disini, gaperlu dibalikin ke redirect lagi
        $writer->save('php://output');

        // return redirect()->to('/warehouse/products');
    }

    public function exportDataVendor() {
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $dataTemplate = $this->models->getAllDataVendor();

        $sheet->setCellValue('A1', 'Vendor Code/Id');
        $sheet->setCellValue('B1', 'Title');
        $sheet->setCellValue('C1', 'Agency');
        $sheet->setCellValue('D1', 'Address');
        $sheet->setCellValue('E1', 'Email');
        $sheet->setCellValue('F1', 'Name');
        $sheet->setCellValue('G1', 'Telp');
        $sheet->setCellValue('H1', 'Bank Name');
        $sheet->setCellValue('I1', 'Bank Account');
        $sheet->setCellValue('J1', 'Bank Number');
        $sheet->setCellValue('K1', 'Debit Term');
        $sheet->setCellValue('L1', 'Status');
        $rows = 2;

        foreach ($dataTemplate as $dt){
            $sheet->setCellValue('A' . $rows, $dt['vendor_code']);
            $sheet->setCellValue('B' . $rows, $dt['vendor_title']);
            $sheet->setCellValue('C' . $rows, $dt['vendor_agency']);
            $sheet->setCellValue('D' . $rows, $dt['vendor_address']);
            $sheet->setCellValue('E' . $rows, $dt['vendor_email']);
            $sheet->setCellValue('F' . $rows, $dt['vendor_name']);
            $sheet->setCellValue('G' . $rows, $dt['vendor_telp']);
            $sheet->setCellValue('H' . $rows, $dt['vendor_bank_name']);
            $sheet->setCellValue('I' . $rows, $dt['vendor_bank_account']);
            $sheet->setCellValue('J' . $rows, $dt['vendor_bank_number']);
            $sheet->setCellValue('K' . $rows, $dt['vendor_debit_term_default']);
            $sheet->setCellValue('L' . $rows, $dt['status_name']);
            $rows++;
        }

        // buat excelnya, fyi inisialisasi spreadsheet itu buat file excel kosong baru dan writer itu mengisi file kosong itu dengan data diatas
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $date = date('j M Y');
        $fileName = 'All data vendor - ' . $date;
    
        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName.'.xlsx');
        header('Cache-Control: max-age=0');
    
        // auto download disini, gaperlu dibalikin ke redirect lagi
        $writer->save('php://output');

        // return redirect()->to('/warehouse/products');
    }

    public function insertVendor() {
        $vendorCode         = $this->request->getVar('vendorCode');
        $vendorTitle        = $this->request->getVar('vendorTitle');
        $vendorAgency       = $this->request->getVar('vendorAgency');
        $vendorAddress      = $this->request->getVar('vendorAddress');
        $vendorEmail        = $this->request->getVar('vendorEmail');
        $vendorName         = $this->request->getVar('vendorName');
        $vendorImportCode   = $this->request->getVar('vendorImportCode');
        $vendorBankName     = $this->request->getVar('vendorBankName');
        $vendorBankAccount  = $this->request->getVar('vendorBankAccount');
        $vendorBankNumber   = $this->request->getVar('vendorBankNumber');
        $vendorTelp         = $this->request->getVar('vendorTelp');
        $vendorDebitTerm    = $this->request->getVar('vendorDebitTerm');
        $vendorStatus       = $this->request->getVar('vendorStatus');

        // auto increment import code
        if($this->models->getLastIdDataVendorLog() != null) {
            $lastVendor = $this->models->getLastIdDataVendorLog();
            $number = explode('-', $lastVendor['vendor_import_code']);
            if(substr($number[2],0,4) == date('Y')) {                
                $addCode = 'AV-PTPF-' . intval($number[2] + 1);
            } else {
                $addCode = 'AV-PTPF-' . date('Y') . '00001';
            }
        } else {
            $addCode = 'AV-PTPF-' . date('Y') . '00001';
        }

        $filename = 'Manual Insert';
        $userId = $this->session->get('user_id');

        // general validation
        if(!$this->validate([
            'vendorCode'    => 'required|is_unique[vendor.vendor_code]',
            'vendorAgency'  => 'required|is_unique[vendor.vendor_agency]',
            'vendorStatus'  => 'required'
        ], [
            'vendorCode'    => [
                'required'  => 'Value is required !',
                'is_unique' => 'Value is already exists !'
            ],
            'vendorAgency'  => [
                'required'  => 'Value is required !',  
                'is_unique' => 'Value is already exists !'
            ],
            'vendorStatus'  => [
                'required'  => 'Value is required !'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $this->session->setFlashdata('error_message', '<strong>Failed !</strong> insert new data, please check & try again !');
            return redirect()->to('main/vendor')->withInput()->with('validation', $validation);
        } else {
            // sukses validasi
            $vendorCode         = $this->request->getVar('vendorCode');
            $vendorTitle        = $this->request->getVar('vendorTitle');
            $vendorAgency       = $this->request->getVar('vendorAgency');
            $vendorAddress      = $this->request->getVar('vendorAddress');
            $vendorEmail        = $this->request->getVar('vendorEmail');
            $vendorName         = $this->request->getVar('vendorName');
            $vendorImportCode   = $this->request->getVar('vendorImportCode');
            $vendorBankName     = $this->request->getVar('vendorBankName');
            $vendorBankAccount  = $this->request->getVar('vendorBankAccount');
            $vendorBankNumber   = $this->request->getVar('vendorBankNumber');
            $vendorTelp         = $this->request->getVar('vendorTelp');
            $vendorDebitTerm    = $this->request->getVar('vendorDebitTerm');
            $vendorStatus       = $this->request->getVar('vendorStatus');

            // masukan ke database
            $this->models->insertVendor($vendorCode, $vendorTitle, $vendorAgency, $vendorAddress, $vendorEmail, $vendorName, $vendorImportCode, $vendorBankName, $vendorBankAccount,
                $vendorBankNumber, $vendorTelp, $vendorDebitTerm, $vendorStatus, $addCode);

            // masukan log nya
            $this->models->insertVendorLog($addCode, $vendorCode, $filename, $userId);

            // set session sukses
            $this->session->setFlashdata('success_message', '<strong>Successfully !!</strong> insert new data, thanks..');
            return redirect()->to('main/vendor');
        }
    }
    
    public function editVendor() {
        // get data from modals
        $editVendorId         = $this->request->getVar('editVendorId');
        $editVendorCode       = $this->request->getVar('editVendorCode');
        $editVendorTitle      = $this->request->getVar('editVendorTitle');
        $editVendorAgency     = $this->request->getVar('editVendorAgency');
        $editVendorAddress    = $this->request->getVar('editVendorAddress');
        $editVendorEmail      = $this->request->getVar('editVendorEmail');
        $editVendorName       = $this->request->getVar('editVendorName');
        $editVendorBankName   = $this->request->getVar('editVendorBankName');
        $editVendorBankAccount = $this->request->getVar('editVendorBankAccount');
        $editVendorBankNumber = $this->request->getVar('editVendorBankNumber');
        $editVendorTelp       = $this->request->getVar('editVendorTelp');
        $editVendorDebitTerm  = $this->request->getVar('editVendorDebitTerm');
        $editVendorStatus     = $this->request->getVar('editVendorStatus');
        $editVendorImportCode = $this->request->getVar('editVendorImportCode');

        $filename = 'Manual Update';
        $userId = $this->session->get('user_id');

        // dd($editVendorId, $editVendorCode, $editVendorTitle, $editVendorAgency, $editVendorAddress, $editVendorEmail, $editVendorName, $editVendorBankName, $editVendorBankAccount,
        // $editVendorBankNumber, $editVendorTelp, $editVendorDebitTerm, $editVendorStatus);

        if($this->models->getDataForEditVendor($editVendorId) != null) {
            $getOldData     = $this->models->getDataForEditVendor($editVendorId);
        } else {
            $validation = \Config\Services::validation();
            $this->session->setFlashdata('error_message', '<strong>Failed !!</strong> update existing data, please try again later..');
            return redirect()->to('main/vendor');
        }

        // specific rules
        if($getOldData['vendor_code'] == $editVendorCode) {
            $rule1 = 'required';
        } else {
            $rule1 = 'required|is_unique[vendor.vendor_code]';
        }

        if($getOldData['vendor_agency'] == $editVendorAgency) {
            $rule2 = 'required';
        } else {
            $rule2 = 'required|is_unique[vendor.vendor_agency]';
        }

        // general validation
        if(!$this->validate([
            'editVendorCode'      => $rule1,
            'editVendorAgency'    => $rule2,
            'editVendorStatus'        => 'required',
            'editVendorImportCode'    => 'required',
        ], [
            'editVendorCode'  => [
                'required'  => 'Value is required !',
                'is_unique' => 'Values is already exists !'
            ],
            'editVendorAgency'    => [
                'required'  => 'Value is required !',
                'is_unique' => 'Value is already exists !'
            ],
            'editVendorStatus'    => [
                'required'  => 'Value is required !'
            ],
            'editVendorImportCode'    => [
                'required'  => 'Value is required !'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $this->session->setFlashdata('error_message', '<strong>Failed !</strong> update existing data, please check & try again !');
            return redirect()->to('main/vendor')->withInput()->with('validation', $validation);
        } else {
            // auto increment update code
            if($this->models->getLastIdDataVendorLog() != null) {
                $lastVendor = $this->models->getLastIdDataVendorLog();
                $number = explode('-', $lastVendor['import_code']);
                if(substr($number[2],0,4) == date('Y')) {                
                    $updateCode = 'EV-PTPF-' . intval($number[2] + 1);
                } else {
                    $updateCode = 'EV-PTPF-' . date('Y') . '00001';
                }
            } else {
                $updateCode = 'EV-PTPF-' . date('Y') . '00001';
            }

            // update customer
            $this->models->updateDataVendor($editVendorId, $editVendorCode, $editVendorTitle, $editVendorAgency, $editVendorAddress, $editVendorEmail, $editVendorName,
                $editVendorBankName, $editVendorBankAccount, $editVendorBankNumber, $editVendorTelp, $editVendorDebitTerm, $editVendorStatus, $updateCode);

            // create log
            $this->models->updateDataVendorLog($updateCode, $editVendorCode, $filename, $userId);

            $this->session->setFlashdata('success_message', '<strong>Successfully !!</strong> update existing data, thanks..');
            return redirect()->to('main/vendor');
        }
    }

    public function deleteVendor() {
        $deleteId = $this->request->getVar('deleteId');
        $importCode = $this->request->getVar('importId');

        // cek data apakah ada
        if($this->models->checkDataForDeleteVendor($deleteId) != null) {
            // auto increment delete code
            if($this->models->getLastIdDataVendorLog() != null) {
                $lastVendor = $this->models->getLastIdDataVendorLog();
                $number = explode('-', $lastVendor['import_code']);
                if(substr($number[2],0,4) == date('Y')) {                
                    $deleteCode = 'DV-PTPF-' . intval($number[2] + 1);
                } else {
                    $deleteCode = 'DV-PTPF-' . date('Y') . '00001';
                }
            } else {
                $deleteCode = 'DV-PTPF-' . date('Y') . '00001';
            }

            $getOldData  = $this->models->checkDataForDeleteVendor($deleteId);
            $filename = "Manual Delete";
            $userId = $this->session->get('user_id');

            // masukan ke log
            $this->models->deleteDataVendorLog($deleteCode, $getOldData['vendor_code'], $filename, $userId);

            // delete dari database
            $this->models->deleteDataVendor($deleteId);

            $this->session->setFlashdata('success_message', '<strong>Successfully !!</strong> delete existing data, thanks..');
            return redirect()->to('main/vendor');

        } else {
            $validation = \Config\Services::validation();
            $this->session->setFlashdata('error_message', '<strong>Failed !!</strong> delete data, data not found detected ! please try again later..');
            return redirect()->to('main/vendor');
        }
    }
}
