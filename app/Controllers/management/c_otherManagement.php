<?php

namespace App\Controllers\management;
use App\Controllers\BaseController;
use Config\Services;
use App\Models\warehouse\models;

class c_otherManagement extends BaseController
{
    protected $table = 'currency';
    protected $database = 'main';
    protected $column_order = []; 
    protected $column_search = [];
    protected $order = ['currency_id' => 'DESC'];
    
    public function __construct() {
        $this->request = Services::request();
        $this->models = new models($this->request, $this->table, $this->column_order, $this->column_search, $this->order, $this->database, 'otherManagement');
        $this->session = session();
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index()
    {
        if(!$this->session->get('login')) {
            return redirect()->to('/');
        }

        $data = [
            'validation'    => \Config\Services::validation(),
            // add new main product
            'multipleUom'   => $this->models->getAllDataMultipleUom(),
            'uomSchema'     => $this->models->getAllDataUomSchema(),
            'currency'      => $this->models->getAllDataCurrency(),
            'status'        => $this->models->getAllDataStatus(),
            // add new user management
            'dept'          => $this->models->getAllDataDept(),
            'level'         => $this->models->getAllDataLevel(),
            // add new assembly
            'whs'           => $this->models->getAllDataWhs(),
        ];
        
        return view('management/v_otherManagement.php', $data); 
    }

    public function getDataFeature() {
        $feature = $this->request->getVar('feature');

        $data = $this->models->getDataFeature($feature);
        echo json_encode($data);
    }

    public function insertFeature() {
        $feature = $this->request->getVar('addFeature');
        $featureName = $this->request->getVar('addFeatureName');

        $getOldData = $this->models->getLastDataFeature($feature);
        if($feature == 'reason' || $feature == 'whs') {
            $featureId = $getOldData[$feature . '_code'];
            $realFeatureName = $feature . '_name';
        } elseif($feature == 'department') {
            $featureId = $getOldData['dept_id'];
            $realFeatureName = 'dept_name';
        } else {
            $featureId = $getOldData[$feature . '_id'];
            $realFeatureName = $feature . '_name';
        }
        
        $featureId = intval($featureId) + 1;
        $dataForIsUnique = $feature.'.'.$realFeatureName;

        if(!$this->validate([
            'addFeature'    => 'required',
            'addFeatureName'    => "required|is_unique[$dataForIsUnique]",
        ], [
            'addFeatureName' => [
                'required'  => 'Must be filled !',
                'is_unique' => 'Already exists !'
            ]
        ])) {
            // set session failed
            $validation = \Config\Services::validation();
            $this->session->setFlashdata('error_message', '<strong>Failed !</strong> insert new data, please check & try again !');
            return redirect()->to('management/otherManagement')->withInput()->with('validation', $validation);
        }

        // insert to database
        $this->models->insertNewFeature($feature, $featureId, $featureName);

        $this->session->setFlashdata('success_message', '<strong>Successfully !</strong> insert new data, thanks...');
        return redirect()->to('management/otherManagement');
    }

    public function deleteFeature() {
        $deleteId = $this->request->getVar('deleteId');
        // aku make inputan import Id untuk nampung nama database
        $dataBase = $this->request->getVar('importId');

        if($this->models->checkDeleteFeature($dataBase, $deleteId) != null) {
            // delete data
            $this->models->deleteFeature($dataBase, $deleteId);

            $this->session->setFlashdata('success_message', '<strong>Successfully !</strong> delete data, thanks...');
            return redirect()->to('management/otherManagement');
        } else {
            $this->session->setFlashdata('error_message', '<strong>Failed !</strong> delete data, data not found !');
            return redirect()->to('management/otherManagement');
        }
    }

    public function editFeature() {
        $dataBase = $this->request->getVar('editDataBaseFeature');
        $featureId = $this->request->getVar('editFeatureId');
        $realFeatureId = $this->request->getVar('editRealFeatureId');
        $featureName = $this->request->getVar('editFeatureName');
        $realFeatureName = $this->request->getVar('editRealFeatureName');

        // specific rule
        $getOldData = $this->models->getSpecificDataFeature($dataBase, $realFeatureId, $realFeatureName);
        
        if($dataBase == 'reason' || $dataBase == 'whs') {
            $oldDataId          = $getOldData[$dataBase . '_code'];
            $oldDataName = $getOldData[$dataBase . '_name'];
            $featureIdColumn      = $dataBase . '_code';
            $featureNameColumn  = $dataBase . '_name';
        } elseif($dataBase == 'department') {
            $oldDataId          = $getOldData['dept_id'];
            $oldDataName = $getOldData['dept_name'];
            $featureIdColumn      = 'dept_id';
            $featureNameColumn  = 'dept_name';
        } else {
            $oldDataId          = $getOldData[$dataBase . '_id'];
            $oldDataName = $getOldData[$dataBase . '_name'];
            $featureIdColumn      = $dataBase . '_id';
            $featureNameColumn  = $dataBase . '_name';
        }

        $isUniqueId = $dataBase.'.'.$featureIdColumn;
        $isUniqueName = $dataBase.'.'.$featureNameColumn;

        // specific rule feature id
        if($featureId == $oldDataId) {
            $rule2 = 'required';
        } else {
            $rule2 = "required|is_unique[$isUniqueId]";
        }

        // specific rule feature name
        if($featureName == $oldDataName) {
            $rule1 = 'required';
        } else {
            $rule1 = "required|is_unique[$isUniqueName]";
        }


        // dd($featureName, $oldDataName);
        // general validation
        if(!$this->validate([
            'editDataBaseFeature'   => 'required',
            'editFeatureId'     => $rule2,
            'editFeatureName'   => $rule1,
        ], [
            'editFeatureId' => [
                'required'  => 'Must be filled !',
                'is_unique' => 'Already exists !'
            ],
            'editFeatureName'   => [
                'required'  => 'Must be filled !',
                'is_unique' => 'Already exists !'
            ]
        ])) {
            // set session failed
            $validation = \Config\Services::validation();
            $this->session->setFlashdata('error_message', '<strong>Failed !</strong> update existing data, please check & try again !');
            // dd($validation);
            return redirect()->to('management/otherManagement')->withInput()->with('validation', $validation);
        }

        // insert to database
        $this->models->updateDataFeature($dataBase, $featureId, $featureName, $oldDataName);

        $this->session->setFlashdata('success_message', '<strong>Successfully !</strong> update existing data, thanks...');
        return redirect()->to('management/otherManagement');
    }
}
