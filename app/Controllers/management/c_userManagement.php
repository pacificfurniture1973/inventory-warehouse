<?php

namespace App\Controllers\management;
use App\Controllers\BaseController;
use Config\Services;
use App\Models\warehouse\models;

class c_userManagement extends BaseController
{
    protected $table = 'user';
    protected $database = 'main';
    protected $column_order = ['user_id', null, 'user_username', 'user_fullname', null, null, 'user_status']; 
    protected $column_search = ['user_username', 'user_fullname', 'user_email'];
    protected $order = ['user_id' => 'DESC'];
    
    public function __construct() {
        $this->request = Services::request();
        $this->models = new models($this->request, $this->table, $this->column_order, $this->column_search, $this->order, $this->database, 'userManagement');
        $this->session = session();
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index()
    {
        if(!$this->session->get('login')) {
            return redirect()->to('/');
        }

        $data = [
            'validation'    => \Config\Services::validation(),
            // add new main product
            'multipleUom'   => $this->models->getAllDataMultipleUom(),
            'uomSchema'     => $this->models->getAllDataUomSchema(),
            'currency'      => $this->models->getAllDataCurrency(),
            'status'        => $this->models->getAllDataStatus(),
            // add new user management
            'dept'          => $this->models->getAllDataDept(),
            'level'         => $this->models->getAllDataLevel(),
            // add new assembly
            'whs'           => $this->models->getAllDataWhs(),
        ];
        
        return view('management/v_userManagement.php', $data); 
    }

    public function ajaxList()
    {   
        if ($this->request->getMethod()) {
            $lists = $this->models->getDatatables();
            $data = [];
            $no = $this->request->getPost('start');

            foreach ($lists as $list) {
                $no++; 
                $status = $list->user_status == 1 ? 'text-success' : 'text-danger';
                $row = [];
                $row[] = $no;
                $row[] = "
                <a href='#' data-bs-toggle='modal' data-bs-target='#modalEditUser' onclick='triggerSelect2()' id='editUser' data-id='$list->user_id' data-user='$list->user_username' data-fullname='$list->user_fullname'
                data-position='$list->user_position' data-level='$list->user_level' data-password='$list->user_password' data-telp='$list->user_telp' data-address='$list->user_address' data-email='$list->user_email'
                data-status='$list->user_status' data-dept='$list->user_dept' data-dept_handle='$list->user_dept_handle'
                class='text-decoration-none text-warning far fa-edit fa-1x fa-fw'></a>
                <a href='#' data-bs-toggle='modal' data-bs-target='#modalDelete' data-id='$list->user_id' id='deleteUser' class='text-decoration-none text-danger fas fa-trash-alt fa-1x fa-fw'></a>";
                $row[] = "<p class='fw-bold text-success'>$list->user_username</p><small class='text-muted'>Level $list->user_level - $list->level_name</small>";
                $row[] = $list->user_fullname;
                $row[] = "<p class='fw-bold text-success'>$list->dept_name</p><small class='text-muted'>$list->user_position</small>";
                $row[] = $list->user_email;
                $row[] = "<p class='fw-bold $status'>$list->status_name</p>";
                $data[] = $row;
            }

            $output = [
                'draw' => $this->request->getPost('draw'),
                'recordsTotal' => $this->models->countAll($this->database),
                'recordsFiltered' => $this->models->countFiltered(),
                'data' => $data
            ];

            echo json_encode($output);
        }
    }

    public function insertUser() {
        $username               = $this->request->getVar('username');
        $userLevel              = $this->request->getVar('userLevel');
        $fullname               = $this->request->getVar('fullname');
        $password               = $this->request->getVar('password');
        $passwordConfirmation   = $this->request->getVar('passwordConfirmation');
        $userDept               = $this->request->getVar('userDept');
        $userPosition           = $this->request->getVar('userPosition');
        $userEmail              = $this->request->getVar('userEmail');
        $deptHandle             = $this->request->getVar('deptHandle');
        $userStatus             = $this->request->getVar('userStatus');
        $userTelp               = $this->request->getVar('userTelp');
        $userAddress            = $this->request->getVar('userAddress');

        // dd($username, $userLevel, $fullname, $password, $passwordConfirmation, $userDept, $userPosition, $userEmail, $deptHandle, $userStatus, $userTelp, $userAddress);

        // general validation
        if(!$this->validate([
            'username'  => 'required|is_unique[user.user_username]',
            'userLevel' => 'required',
            'fullname'  => 'required',
            'password'  => 'required|min_length[5]',
            'passwordConfirmation'  => 'matches[password]',
            'userDept'  => 'required',
            'userPosition'  => 'required',
            'userEmail' => 'required|valid_email',
            'deptHandle'    => 'required',
            'userStatus'    => 'required',
            'userTelp'  => 'required',
            'userAddress'   => 'required',
        ], [
            'username'  => [
                'required'  => 'Value is required !',
                'is_unique' => 'Value is already exists !'
            ],
            'userLevel' => [
                'required'  => 'Value is required !'
            ],
            'fullname'  => [
                'required'  => 'Value is required !'
            ],
            'password'  => [
                'required'  => 'Value is required !',
                'min_length'    => 'Min 5 Character !'
            ],
            'passwordConfirmation'  => [
                'matches'   => 'Confirmation password must be same with password !'
            ],
            'userDept'  => [
                'required'  => 'Value is required !'
            ],
            'userPosition'  => [
                'required'  => 'Value is required !'
            ],
            'userEmail' => [
                'required'  => 'Value is required !'
            ],
            'deptHandle'    => [
                'required'  => 'Value is required !'
            ],
            'userStatus'    => [
                'required'  => 'Value is required !'
            ],
            'userTelp'  => [
                'required'  => 'Value is required !'
            ],
            'userAddress'   => [
                'required'  => 'Value is required !'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $this->session->setFlashdata('error_message', '<strong>Failed !</strong> insert new user, please check & try again !');

            // ngirim data dept handle buat old di modals
            $deptHandle = implode(',',$deptHandle);
            $this->session->setFlashdata('deptHandle',$deptHandle);
            return redirect()->to('management/userManagement')->withInput()->with('validation', $validation);
        } else {

            // encrypt password
            $password = password_hash($password, PASSWORD_DEFAULT);
            $deptHandle = implode(',', $deptHandle);

            // masukan ke database
            $this->models->insertUser($username, $userLevel, $fullname, $password, $userDept, $userPosition, $userEmail, $deptHandle, $userStatus, $userTelp, $userAddress);

            // set session sukses
            $this->session->setFlashdata('success_message', '<strong>Successfully !!</strong> insert new user, thanks..');
            return redirect()->to('management/userManagement');
        }
    }
    
    public function editUser() {
        // get data from modals
        $edituserId         = $this->request->getVar('editUserId');
        $editUsername       = $this->request->getVar('editUsername');
        $editUserLevel      = $this->request->getVar('editUserLevel');
        $editFullname       = $this->request->getVar('editFullname');
        $editPassword       = $this->request->getVar('editPassword');
        $editPasswordConfirmation   = $this->request->getVar('editPasswordConfirmation');
        $editUserDept       = $this->request->getVar('editUserDept');
        $editUserPosition   = $this->request->getVar('editUserPosition');
        $editUserEmail      = $this->request->getVar('editUserEmail');
        $editDeptHandle     = $this->request->getVar('editDeptHandle');
        $editUserStatus     = $this->request->getVar('editUserStatus');
        $editUserTelp       = $this->request->getVar('editUserTelp');
        $editUserAddress    = $this->request->getVar('editUserAddress');

        // dd($edituserId, $editUsername, $editUserLevel, $editFullname, $editPassword, $editPasswordConfirmation, $editUserDept, $editUserPosition, $editUserEmail,
        // $editDeptHandle, $editUserStatus, $editUserTelp, $editUserAddress);

        $getOldData = $this->models->getDataForEditUser($edituserId);

        // specific rules
        if($getOldData['user_username'] == $editUsername) {
            $rule1 = 'required';
        } else {
            $rule1 = 'required|is_unique[user.user_username]';
        }

        // general validation
        if(!$this->validate([
            'editUsername'      => $rule1,
            'editUserLevel'     => 'required',
            'editFullname'      => 'required',
            'editPasswordConfirmation'  => 'required_with[editPassword]|matches[editPassword]',
            'editUserDept'      => 'required',
            'editUserPosition'  => 'required',
            'editUserEmail'     => 'required',
            'editDeptHandle'    => 'required',
            'editUserStatus'    => 'required',
            'editUserTelp'      => 'required',
            'editUserAddress'   => 'required',
        ], [
            'editUsername'  => [
                'required'  => 'Value is required !',
                'is_unique' => 'Values is already exists !'
            ],
            'editUserLevel'    => [
                'required'  => 'Value is required !'
            ],
            'editFullname'    => [
                'required'  => 'Value is required !',
            ],
            'editPasswordConfirmation' => [
                'required_with'  => 'Value is required if you want to change password !',
                'matches'        => 'Confirmation Password must be same with password !'
            ],
            'editUserDept' => [
                'required'  => 'Value is required !'
            ],
            'editUserPosition'  => [
                'required'  => 'Value is required !'
            ],
            'editUserEmail'  => [
                'required'  => 'Value is required'
            ],
            'editDeptHandle'   => [
                'required'  => 'Value is required !'
            ],
            'editUserStatus'    => [
                'required'  => 'Value is required !'
            ],
            'editUserTelp'  => [
                'required'  => 'Value is required !'
            ],
            'editUserAddress'    => [
                'required'  => 'Value is required !'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $this->session->setFlashdata('error_message', '<strong>Failed !</strong> update existing user, please check & try again !');
            return redirect()->to('management/c_userManagement')->withInput()->with('validation', $validation);
        } else {

            // encrypt password
            if($editPassword != null) {
                $editPassword = password_hash($password, PASSWORD_DEFAULT);
            } else {
                $editPassword = $getOldData['user_password'];
            }

            $editDeptHandle = implode(',', $editDeptHandle);

            // masukan ke database
            $this->models->updateUser($edituserId, $editUsername, $editUserLevel, $editFullname, $editPassword, $editUserDept, $editUserPosition, $editUserEmail, $editDeptHandle, $editUserStatus, $editUserTelp, $editUserAddress);

            // set session sukses
            $this->session->setFlashdata('success_message', '<strong>Successfully !!</strong> update existing user, thanks..');
            return redirect()->to('management/userManagement');
        }
    }

    public function deleteUser() {
        $deleteId = $this->request->getVar('deleteId');
        
        // cek data apakah ada (pakai yg buat edit bisa)
        if($this->models->getDataForEditUser($deleteId) != null) {

            $getOldData  = $this->models->getDataForEditUser($deleteId);

            // delete dari database
            $this->models->deleteDataUser($deleteId);

            $this->session->setFlashdata('success_message', '<strong>Successfully !!</strong> delete existing user, thanks..');
            return redirect()->to('management/c_userManagement');

        } else {
            $validation = \Config\Services::validation();
            $this->session->setFlashdata('error_message', '<strong>Failed !!</strong> delete user, user not found detected ! please try again later..');
            return redirect()->to('management/c_userManagement');
        }
    }

    public function exportDataUser() {
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $dataTemplate = $this->models->getAllDataUser();

        $sheet->setCellValue('A1', 'Username');
        $sheet->setCellValue('B1', 'Fullname');
        $sheet->setCellValue('C1', 'Position');
        $sheet->setCellValue('D1', 'Level');
        $sheet->setCellValue('E1', 'Telp');
        $sheet->setCellValue('F1', 'Address');
        $sheet->setCellValue('G1', 'Email');
        $sheet->setCellValue('H1', 'Dept');
        $sheet->setCellValue('I1', 'Status');
        $rows = 2;

        foreach ($dataTemplate as $dt){
            $sheet->setCellValue('A' . $rows, $dt['user_username']);
            $sheet->setCellValue('B' . $rows, $dt['user_fullname']);
            $sheet->setCellValue('C' . $rows, $dt['user_position']);
            $sheet->setCellValue('D' . $rows, $dt['user_level']);
            $sheet->setCellValue('E' . $rows, $dt['user_telp']);
            $sheet->setCellValue('F' . $rows, $dt['user_address']);
            $sheet->setCellValue('G' . $rows, $dt['user_email']);
            $sheet->setCellValue('H' . $rows, $dt['dept_name']);
            $sheet->setCellValue('I' . $rows, $dt['status_name']);
            $rows++;
        }

        // buat excelnya, fyi inisialisasi spreadsheet itu buat file excel kosong baru dan writer itu mengisi file kosong itu dengan data diatas
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $date = date('j M Y');
        $fileName = 'All data user - ' . $date;
    
        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName.'.xlsx');
        header('Cache-Control: max-age=0');
    
        // auto download disini, gaperlu dibalikin ke redirect lagi
        $writer->save('php://output');

        // return redirect()->to('/warehouse/products');
    }
}
