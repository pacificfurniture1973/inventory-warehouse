<?php

namespace App\Controllers\management;
use App\Controllers\BaseController;
use Config\Services;
use App\Models\warehouse\models;

class c_userRequest extends BaseController
{
    protected $table = 'user';
    protected $database = 'main';
    protected $column_order = []; 
    protected $column_search = [];
    protected $order = ['user_id' => 'DESC'];
    
    public function __construct() {
        $this->request = Services::request();
        $this->models = new models($this->request, $this->table, $this->column_order, $this->column_search, $this->order, $this->database, 'userRequest');
        $this->session = session();
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index()
    {
        if(!$this->session->get('login')) {
            return redirect()->to('/');
        }

        $data = [
            'validation'    => \Config\Services::validation(),
            // add new main product
            'multipleUom'   => $this->models->getAllDataMultipleUom(),
            'uomSchema'     => $this->models->getAllDataUomSchema(),
            'currency'      => $this->models->getAllDataCurrency(),
            'status'        => $this->models->getAllDataStatus(),
            // add new user management
            'dept'          => $this->models->getAllDataDept(),
            'level'         => $this->models->getAllDataLevel(),
            // add new assembly
            'whs'           => $this->models->getAllDataWhs(),
        ];
        
        return view('management/v_userRequest.php', $data); 
    }

    public function ajaxList()
    {   
        if ($this->request->getMethod()) {
            $lists = $this->models->getDatatables();
            $data = [];
            $no = $this->request->getPost('start');

            foreach ($lists as $list) {
                $no++; 
                $status = $list->user_status == 1 ? 'text-success' : 'text-danger';
                $row = [];
                $row[] = $no;
                $row[] = "
                <a href='#' data-bs-toggle='modal' data-bs-target='#modalApproveUser' onclick='triggerSelect2()' id='approveUser' 
                data-id='$list->user_id' data-user='$list->user_username' data-fullname='$list->user_fullname'
                data-position='$list->user_position' data-level='$list->user_level' data-password='$list->user_password' data-telp='$list->user_telp' data-address='$list->user_address' data-email='$list->user_email'
                data-status='$list->user_status' data-dept='$list->user_dept' data-dept_handle='$list->user_dept_handle'
                class='text-decoration-none text-warning far fa-edit fa-1x fa-fw'></a>
                <a href='#' data-bs-toggle='modal' data-bs-target='#modalApproveRejectUser' id='ApproveRejectUser' data-info='Approve' data-status='$list->user_status' data-id='$list->user_id' data-level='$list->user_level' class='far fa-check-square text-decoration-none text-success mx-2'></a>
                <a href='#' data-bs-toggle='modal' data-bs-target='#modalApproveRejectUser' id='ApproveRejectUser' data-info='Reject' data-status='$list->user_status' data-id='$list->user_id' data-level='$list->user_level' class='fas fa-times text-decoration-none text-danger'></a>";
                $row[] = "<p class='fw-bold text-success'>$list->user_username</p><small class='text-muted'>Level $list->user_level - $list->level_name</small>";
                $row[] = $list->user_fullname;
                $row[] = "<p class='fw-bold text-success'>$list->dept_name</p><small class='text-muted'>$list->user_position</small>";
                $row[] = $list->user_email;
                $row[] = "<p class='fw-bold $status'>$list->status_name</p>";
                $data[] = $row;
            }

            $output = [
                'draw' => $this->request->getPost('draw'),
                'recordsTotal' => $this->models->countAll($this->database),
                'recordsFiltered' => $this->models->countFiltered(),
                'data' => $data
            ];

            echo json_encode($output);
        }
    }

    public function processUser() {
        $infoUser   = $this->request->getVar('infoUser');
        $statusUser = $this->request->getVar('statusUser');
        $userId     = $this->request->getVar('idUser');
        $userLevel  = $this->request->getVar('levelUser');

        if($infoUser == 'Approve') {
            // jika statusnya masih pending head maka di update ke pending IT
            if($statusUser == 3) {
                $this->models->updateProcessUser($userId, 8, $userLevel);

                $this->session->setFlashdata('success_message', '<strong>Successfully !!</strong> approve step 1 !, thanks..');
                return redirect()->to('management/userRequest');
            } elseif($statusUser == 8) {
                // jika statusnya pending IT maka di update ke active
                $this->models->updateProcessUser($userId, 1, $userLevel);
                $this->session->setFlashdata('success_message', '<strong>Successfully !!</strong> your user is activated !, thanks..');
                return redirect()->to('management/userManagement');
            }
        } elseif($infoUser == 'Reject') {
            // jika di reject maka buat deactive
            $this->models->updateProcessUser($userId, 2);

            $this->session->setFlashdata('success_message', '<strong>Successfully !!</strong> reject this user !, thanks..');
            return redirect()->to('management/userManagement');
        }
    }
}
