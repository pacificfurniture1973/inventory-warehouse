<?php

namespace App\Controllers\pde;
use App\Controllers\BaseController;
use Config\Services;
use App\Models\warehouse\models;
// use App\Models\models;

class c_bom extends BaseController
{
    protected $table = 'bom';
    protected $database = 'pde';
    protected $column_order = ['bom_id', null, 'bom_assembly_code', 'bom_product_code', null, null, null, null, null, null]; 
    protected $column_search = ['bom_type', 'bom_assembly_code', 'bom_product_code'];
    protected $order = ['bom_id' => 'DESC'];

    public function __construct() {
        $this->session = session();
        $this->request = Services::request();
        $this->models = new models($this->request, $this->table, $this->column_order, $this->column_search, $this->order, $this->database, 'bom');
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index()
    {  

        if(!$this->session->get('login')) {
            return redirect()->to('/');
        }
        
        $data = [
            'validation'     => \Config\Services::validation(),
            // add new main product
            'multipleUom'   => $this->models->getAllDataMultipleUom(),
            'uomSchema'     => $this->models->getAllDataUomSchema(),
            'currency'      => $this->models->getAllDataCurrency(),
            'status'        => $this->models->getAllDataStatus(),
            // add new user management
            'dept'          => $this->models->getAllDataDept(),
            'level'         => $this->models->getAllDataLevel(),
            // add new assembly
            'whs'           => $this->models->getAllDataWhs(),
        ];

        return view('pde/v_bom.php', $data);
    }

    public function ajaxList() {
        if ($this->request->getMethod(true) === 'POST') {
            $lists = $this->models->getDatatables();
            $data = [];
            $no = $this->request->getPost('start');

            foreach ($lists as $list) {
                $no++; 
                $row = [];
                $row[] = $no;
                $row[] = "<p class='fw-bold text-success'>$list->bom_type</p>";
                $row[] = "<p class='fw-bold text-warning'>$list->bom_assembly_code</p>";
                $row[] = "<p class='fw-bold text-dark'>$list->bom_product_code</p>";
                $row[] = $list->product_name;
                $row[] = $list->bom_quantity_needed;
                $row[] = $list->product_uom;
                $row[] = $list->product_unit_price;
                $row[] = "<p class='fw-bold text-info'>$list->user_username</p><small class='text-muted'>$list->dept_name</small>";
                $row[] = $list->lastEdit;
                $row[] = "
                        <a href='#' data-bs-toggle='modal' data-bs-target='#modalEditBom' id='editBom'
                        data-id='$list->bom_id' data-type='$list->bom_type' data-assembly='$list->bom_assembly_code' data-product='$list->bom_product_code' data-qty='$list->bom_quantity_needed' class='far fa-edit text-decoration-none text-warning mx-2'></a>
                        <a href='#' data-bs-toggle='modal' data-bs-target='#modalDelete' id='deleteBom' data-id='$list->bom_id' class='fas fa-trash-alt text-decoration-none text-danger'></a>";
                $data[] = $row;
            }

            $output = [
                'draw' => $this->request->getPost('draw'),
                'recordsTotal' => $this->models->countAll($this->database),
                'recordsFiltered' => $this->models->countFiltered(),
                'data' => $data
            ];

            echo json_encode($output);
        }
    }

    public function exportBom() {

        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet()->setTitle('Inventory');

        $dataTemplate = $this->models->getAllBom();

        $sheet->setCellValue('A1', 'BOM Type');
        $sheet->setCellValue('B1', 'Assembly Code');
        $sheet->setCellValue('C1', 'Product Code');
        $sheet->setCellValue('D1', 'Product Name');
        $sheet->setCellValue('E1', 'Uom');
        $sheet->setCellValue('F1', 'Unit Price');
        $sheet->setCellValue('G1', 'Quantity Required');
        $sheet->setCellValue('H1', 'Created By');
        $rows = 2;


        foreach ($dataTemplate as $dt){
            $sheet->setCellValue('A' . $rows, $dt['bom_type']);
            $sheet->setCellValue('B' . $rows, $dt['bom_assembly_code']);
            $sheet->setCellValue('C' . $rows, $dt['bom_product_code']);
            $sheet->setCellValue('D' . $rows, $dt['product_name']);
            $sheet->setCellValue('E' . $rows, $dt['product_uom']);
            $sheet->setCellValue('F' . $rows, $dt['product_unit_price']);
            $sheet->setCellValue('G' . $rows, $dt['bom_quantity_needed']);
            $sheet->setCellValue('H' . $rows, $dt['user_username']);
            $rows++;
        }

        // buat excelnya, fyi inisialisasi spreadsheet itu buat file excel kosong baru dan writer itu mengisi file kosong itu dengan data diatas
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $date = date('j M Y');
        $fileName = 'all data bom - ' . $date;
    
        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName.'.xlsx');
        header('Cache-Control: max-age=0');
    
        // auto download disini, gaperlu dibalikin ke redirect lagi
        $writer->save('php://output');
    }

    public function exportTemplateBom() {
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet()->setTitle("Inventory");

        $dataMultipleUom = $this->models->getAllDataMultipleUom();
        $dataUomSchema = $this->models->getAllDataUomSchema();
        $dataCurrency = $this->models->getAllDataCurrency();
        $dataStatus = $this->models->getAllDataStatus();

        $sheet->setCellValue('A1', 'Bom Type*');
        $sheet->setCellValue('B1', 'Assembly Code*');
        $sheet->setCellValue('C1', 'Product Code*');
        $sheet->setCellValue('D1', 'Quantity Required*');

        $sheet->setCellValue('A2', 'Default Bom');
        $sheet->setCellValue('B2', 'EN-0343-01');
        $sheet->setCellValue('C2', 'RWM-2102-02657');
        $sheet->setCellValue('D2', '5');

        // buat excelnya, fyi inisialisasi spreadsheet itu buat file excel kosong baru dan writer itu mengisi file kosong itu dengan data diatas
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $fileName = 'Template import bom';
    
        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName.'.xlsx');
        header('Cache-Control: max-age=0');
    
        // auto download disini, gaperlu dibalikin ke redirect lagi
        $writer->save('php://output');

        // return redirect()->to('/warehouse/products');
    }

    public function importBom() {
        $file = $this->request->getFile('importBom');
        $filename = $file->getName();
        $userId = $this->session->get('user_id');
        $ext = $file->getClientExtension();
        
        if($ext == 'xls') {
            $render = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
        } else {
            $render = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        }

        // jika error = 0 maka file ada, jika error = 4 maka file null
        if($file->getError() == 4) {
            $validation = \Config\Services::validation();
            $this->session->setFlashdata('error_message', '<strong>Failed !!</strong> file not found ! Please try again...');
            return redirect()->to('pde/bom')->withInput()->with('validation', $validation);
        }

        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file);

        if($spreadsheet->getSheetByName('Inventory') == null) {
            $validation = \Config\Services::validation();
            $this->session->setFlashdata('error_message', '<strong>Failed !</strong> sheet inventory not found !, please check & try again !');
            return redirect()->to('pde/bom')->withInput()->with('validation', $validation);
        }
        
        $sheet = $spreadsheet->getSheetByName('Inventory')->toArray();
        $log = array('success' => 0, 'failed' => 0);

        // auto increment import code
        if($this->models->getLastIdDataBomLog() != null) {
            $lastBom = $this->models->getLastIdDataBomLog();
            $number = explode('-', $lastBom['bom_code']);
            if(substr($number[2],0,4) == date('Y')) {                
                $importCode = 'IB-PTPF-' . intval($number[2] + 1);
            } else {
                $importCode = 'IB-PTPF-' . date('Y') . '00001';
            }
        } else {
            $importCode = 'IB-PTPF-' . date('Y') . '00001';
        }

        if($spreadsheet != null) {
            foreach($sheet as $baris => $kolom) {
                if($baris == 0) {
                    continue;
                }

                if($kolom['0'] == null && $kolom['1'] == null && $kolom['2'] == null && $kolom['3'] === null) {
                    continue;
                }
                
                if($this->models->checkAssemblyAndProduct($kolom['1'], $kolom['2']) != null) {

                    $check = $this->models->checkDuplicateBom($kolom['1'], $kolom['2']);

                    if($check != null) {
                        $log['failed'] += 1;
                        continue;
                    } elseif($kolom['0'] == null || $kolom['1'] == null || $kolom['2'] == null || $kolom['3'] === null) {
                        $log['failed'] += 1;
                        continue;
                    } else {
                        $bomType        = $kolom['0'];
                        $bomAssembly    = $kolom['1'];
                        $bomProduct     = $kolom['2'];
                        $bomQuantity    = $kolom['3'];
        
                        // masukan ke database
                        $this->models->importBom($importCode, $bomType, $bomAssembly, $bomProduct, $bomQuantity, $userId); 

                        // import log sekalian 
                        $this->models->importBomLog($importCode, $bomAssembly, $bomProduct, $filename, $userId);
                        $log['success'] += 1;
                    }
                } else {
                    $log['failed'] += 1;
                    continue;
                }
            }  

            $this->session->setFlashdata('success_message', '<strong>Successfully !!</strong> import ' . $log['success'] . ' and ' . $log['failed'] . ' <span class="text-danger">failure</span>');
            return redirect()->to('pde/bom');
        }
    }

    public function editBom() {
        $bomId          = $this->request->getVar('editBomId');
        $bomType        = $this->request->getVar('editBomType');
        $bomAssembly    = $this->request->getVar('editBomAssemblyCode');
        $bomProduct     = $this->request->getVar('editBomProductCode');
        $bomQty         = $this->request->getVar('editBomQuantityNeeded');

        // dd($bomId, $bomType, $bomAssembly, $bomProduct, $bomQty);

        $userId = $this->session->get('user_id');
        $filename = 'Manual Update';

        if($this->models->checkAssemblyAndProduct($bomAssembly, $bomProduct) != null) {
            if($this->models->checkDuplicateBom($bomAssembly, $bomProduct) == null) {
                // general validation
                if(!$this->validate([
                    'editBomId'     => 'required',
                    'editBomType'   => 'required',
                    'editBomAssemblyCode'   => 'required',
                    'editBomProductCode'    => 'required',
                    'editBomQuantityNeeded' => 'required',
                ], [
                    'editBomId' => [
                        'required'  => 'Must be filled !'
                    ],
                    'editBomType'   => [
                        'required'  => 'Must be filled !',
                    ],
                    'editBomAssemblyCode'   => [
                        'required'  => 'Must be filled !'
                    ],
                    'editBomProductCode'   => [
                        'required'  => 'Must be filled !',
                    ],
                    'editBomQuantityNeeded'    => [
                        'required'  => 'Must be filled !'
                    ]
                ])) {
                    $validation = \Config\Services::validation();
                    $this->session->setFlashdata('error_message', '<strong>Failed !!</strong> update existing data, please try again later..');
                    return redirect()->to('pde/bom')->withInput()->with('validation', $validation);
                } else {
                    // auto increment update code
                    if($this->models->getLastIdDataBomLog() != null) {
                        $lastBom = $this->models->getLastIdDataBomLog();
                        $number = explode('-', $lastBom['bom_code']);
                        if(substr($number[2],0,4) == date('Y')) {                
                            $updateCode = 'EB-PTPF-' . intval($number[2] + 1);
                        } else {
                            $updateCode = 'EB-PTPF-' . date('Y') . '00001';
                        }
                    } else {
                        $updateCode = 'EB-PTPF-' . date('Y') . '00001';
                    }            

                    // update products
                    $this->models->updateDataBom($updateCode, $bomId, $bomType, $bomAssembly, $bomProduct, $bomQty);

                    // create log
                    $this->models->updateDataBomLog($updateCode, $bomAssembly, $bomProduct, $filename, $userId);

                    $this->session->setFlashdata('success_message', '<strong>Successfully !!</strong> update existing data, thanks..');
                    return redirect()->to('pde/bom');
                }
            } else {
                $validation = \Config\Services::validation();
                $this->session->setFlashdata('error_message', '<strong>Failed !!</strong> data already exist !, please try again..');
                return redirect()->to('pde/bom')->withInput()->with('validation', $validation);
            }
        } else {
            $validation = \Config\Services::validation();
            $this->session->setFlashdata('error_message', '<strong>Failed !!</strong> assembly or product code not found !, please try again..');
            return redirect()->to('pde/bom')->withInput()->with('validation', $validation);
        }
    }

    public function deleteBom() {
        $deleteId = $this->request->getVar('deleteId');

        // cek data apakah ada
        if($this->models->checkDataForDeleteBom($deleteId) != null) {
            // auto increment update code
            if($this->models->getLastIdDataBomLog() != null) {
                $lastBom = $this->models->getLastIdDataBomLog();
                $number = explode('-', $lastBom['bom_code']);
                if(substr($number[2],0,4) == date('Y')) {                
                    $deleteCode = 'DB-PTPF-' . intval($number[2] + 1);
                } else {
                    $deleteCode = 'DB-PTPF-' . date('Y') . '00001';
                }
            } else {
                $deleteCode = 'DB-PTPF-' . date('Y') . '00001';
            }

            $getOldData  = $this->models->checkDataForDeleteBom($deleteId);
            $filename = "Manual Delete";
            $userId = $this->session->get('user_id');

            // masukan ke log
            $this->models->deleteDataBomLog($deleteCode, $getOldData['bom_assembly_code'], $getOldData['bom_product_code'], $filename, $userId);

            // delete dari database
            $this->models->deleteDataBom($deleteId);

            $this->session->setFlashdata('success_message', '<strong>Successfully !!</strong> delete existing data, thanks..');
            return redirect()->to('pde/bom');

        } else {
            $validation = \Config\Services::validation();
            $this->session->setFlashdata('error_message', '<strong>Failed !!</strong> data not found ! please try again..');
            return redirect()->to('pde/bom');
        }
    }

    public function insertBom() {
        // get data from modals
        $bomType     = $this->request->getVar('addBomType');
        $bomQty      = $this->request->getVar('addBomQty');
        $bomAssembly = $this->request->getVar('addBomAssembly');
        $bomProduct  = $this->request->getVar('addBomProduct');

        $userId = $this->session->get('user_id');
        $filename = 'Manual Insert';

        if($this->models->checkAssemblyAndProduct($bomAssembly, $bomProduct) != null) {
            if($this->models->checkDuplicateBom($bomAssembly, $bomProduct) == null) {
                // auto increment add code
                if($this->models->getLastIdDataBomLog() != null) {
                    $lastBom = $this->models->getLastIdDataBomLog();
                    $number = explode('-', $lastBom['bom_code']);
                    if(substr($number[2],0,4) == date('Y')) {                
                        $addCode = 'AB-PTPF-' . intval($number[2] + 1);
                    } else {
                        $addCode = 'AB-PTPF-' . date('Y') . '00001';
                    }
                } else {
                    $addCode = 'AB-PTPF-' . date('Y') . '00001';
                }

                // validasi
                if(!$this->validate([
                    'addBomType'        => 'required',
                    'addBomQty'         => 'required',
                    'addBomAssembly'    => 'required',
                    'addBomProduct'     => 'required',
                ], [
                    'addBomType'   => [
                        'required'  => 'Value is required !',
                    ], 
                    'addBomQty'   => [
                        'required'  => 'Value is required !'
                    ], 
                    'addBomAssembly'   => [
                        'required'  => 'Value is required !',
                    ], 
                    'addBomProduct'    => [
                        'required'  => 'Value is required !'
                    ]
                ])) {
                    // set session failed
                    $validation = \Config\Services::validation();
                    $this->session->setFlashdata('error_message', '<strong>Failed !</strong> insert new data, please check & try again !');
                    return redirect()->to('pde/bom')->withInput()->with('validation', $validation);
                } else {
                    // sukses validasi

                    $bomType     = $this->request->getVar('addBomType');
                    $bomQty      = $this->request->getVar('addBomQty');
                    $bomAssembly = $this->request->getVar('addBomAssembly');
                    $bomProduct  = $this->request->getVar('addBomProduct');

                    // masukan ke database
                    $this->models->insertBom($addCode, $bomType, $bomQty, $bomAssembly, $bomProduct, $userId);

                    // masukan ke log
                    $this->models->insertBomLog($addCode, $bomAssembly, $bomProduct, $filename, $userId);

                    // set session sukses
                    $this->session->setFlashdata('success_message', '<strong>Successfully !!</strong> insert new data, thanks..');
                    return redirect()->to('pde/bom');
                }
            } else {
                $validation = \Config\Services::validation();
                $this->session->setFlashdata('error_message', '<strong>Failed !!</strong> data already exist !, please try again..');
                return redirect()->to('pde/bom')->withInput()->with('validation', $validation);
            }
        } else {
            $validation = \Config\Services::validation();
            $this->session->setFlashdata('error_message', '<strong>Failed !!</strong> assembly or product code not found !, please try again..');
            return redirect()->to('pde/bom')->withInput()->with('validation', $validation);
        }
    }
}
