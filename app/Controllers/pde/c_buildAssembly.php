<?php

namespace App\Controllers\pde;
use App\Controllers\BaseController;
use Config\Services;
use App\Models\warehouse\models;
// use App\Models\models;

class c_buildAssembly extends BaseController
{
    protected $table = 'bom';
    protected $database = 'pde';
    protected $column_order = ['bom_id', null, 'bom_assembly_code', 'bom_product_code', null, null, null, null, null, null]; 
    protected $column_search = ['bom_type', 'bom_assembly_code', 'bom_product_code'];
    protected $order = ['bom_id' => 'DESC'];

    public function __construct() {
        $this->session = session();
        $this->request = Services::request();
        $this->models = new models($this->request, $this->table, $this->column_order, $this->column_search, $this->order, $this->database, 'buildAssembly');
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index()
    {  

        if(!$this->session->get('login')) {
            return redirect()->to('/');
        }
        
        $data = [
            'validation'     => \Config\Services::validation(),
            // add new main product
            'multipleUom'   => $this->models->getAllDataMultipleUom(),
            'uomSchema'     => $this->models->getAllDataUomSchema(),
            'currency'      => $this->models->getAllDataCurrency(),
            'status'        => $this->models->getAllDataStatus(),
            // add new user management
            'dept'          => $this->models->getAllDataDept(),
            'level'         => $this->models->getAllDataLevel(),
            // add new assembly
            'whs'           => $this->models->getAllDataWhs(),
        ];

        return view('pde/v_buildAssembly.php', $data);
    }

    public function ajaxList() {
        if ($this->request->getMethod(true) === 'POST') {
            $lists = $this->models->getDatatables();
            $data = [];
            $no = $this->request->getPost('start');

            foreach ($lists as $list) {
                $no++; 
                $row = [];
                $row[] = $no;
                $row[] = "<p class='fw-bold text-success'>$list->bom_type</p>";
                $row[] = "<p class='fw-bold text-warning'>$list->bom_assembly_code</p>";
                $row[] = "<p class='fw-bold text-dark'>$list->bom_product_code</p>";
                $row[] = $list->product_name;
                $row[] = $list->product_uom;
                $row[] = $list->product_unit_price;
                $row[] = $list->bom_quantity_needed;
                $row[] = "<p class='fw-bold text-info'>$list->user_username</p><small class='text-muted'>$list->dept_name</small>";
                $row[] = $list->lastEdit;
                $row[] = "
                        <a href='#' data-bs-toggle='modal' data-bs-target='#modalEditBom' id='editBom'
                        data-id='$list->bom_id' data-type='$list->bom_type' data-assembly='$list->bom_assembly_code' data-product='$list->bom_product_code' data-qty='$list->bom_quantity_needed' class='far fa-edit text-decoration-none text-warning mx-2'></a>
                        <a href='#' data-bs-toggle='modal' data-bs-target='#modalDelete' id='deleteBom' data-id='$list->bom_id' class='fas fa-trash-alt text-decoration-none text-danger'></a>";
                $data[] = $row;
            }

            $output = [
                'draw' => $this->request->getPost('draw'),
                'recordsTotal' => $this->models->countAll($this->database),
                'recordsFiltered' => $this->models->countFiltered(),
                'data' => $data
            ];

            echo json_encode($output);
        }
    }

    public function getTermAssembly() {
        $dataAssembly = $this->request->getVar('assemblyCode');
        $dataJo = $this->request->getVar('joNumber');
        $data = $this->models->getTermAssembly($dataAssembly, $dataJo);
        echo json_encode($data);
    }

    public function getTermJo() {
        $data = $this->request->getVar('joNumber');
        $data = $this->models->getTermJo($data);
        echo json_encode($data);
    }

    public function getDataForAssembly() {
        $assemblyCode   = $this->request->getVar('buildAssemblyCode'); 
        $joNumber       = $this->request->getVar('buildJoNumber');

        // dd($assemblyCode, $joNumber);

        $data = $this->models->getDataForBuildAssembly($assemblyCode, $joNumber);
        echo json_encode($data);
    }

    public function buildAssembly() {
        $processQty = $this->request->getVar('processQty');
        $assemblyCode = $this->request->getVar('buildAssemblyCodeHidden');
        $joNumber = $this->request->getVar('buildJoNumberHidden');
        $dataProductCode = $this->request->getVar('buildProductCode');
        $status = 9;
        $userId = $this->session->get('user_id');
        $log = array('success' => 0, 'failed' => 0);

        // auto build assembly number
        if($this->models->getLastIdDataBuildAssembly() != null) {
            $lastBuildAssembly = $this->models->getLastIdDataBuildAssembly();
            $number = explode('-', $lastBuildAssembly['ba_code']);
            if(substr($number[2],0,4) == date('Y')) {                
                $baCode = 'ABA-PTPF-' . intval($number[2] + 1);
            } else {
                $baCode = 'ABA-PTPF-' . date('Y') . '00001';
            }
        } else {
            $baCode = 'ABA-PTPF-' . date('Y') . '00001';
        }

        // dd($processQty, $assemblyCode, $joNumber, $productCode);

        if($this->models->checkJoAndAssembly($joNumber, $assemblyCode) != null) {
            // general validation
            if(!$this->validate([
                'processQty' => 'required',
                'buildAssemblyCodeHidden'   => 'required',
                'buildJoNumberHidden'   => 'required',
                'buildProductCode'   => 'required'
                ], [
                'processQty'    => [
                    'required' => 'Value is required !'
                ],
                'buildAssemblyCodeHidden' => [
                    'required'  => 'Value is required !'
                ],
                'buildJoNumberHidden'   => [
                    'required'  => 'Value is required !'
                ],
                'buildProductCode'  => [
                    'required'  => 'Values is required !'
                ]
            ])) {
                $validation = \Config\Services::validation();
                $this->session->setFlashdata('error_message', '<strong>Failed !!</strong> process data (required data not success validation) !, please check try again..');
                return redirect()->to('pde/buildAssembly')->withInput()->with('validation', $validation);
            } else {
                // jika process quantity lebih dari balance qty di jo return failed
                $getOldDataJoAndAssembly = $this->models->getDataJoAndAssembly($joNumber, $assemblyCode);
                if(floatval($processQty) > floatval($getOldDataJoAndAssembly['jo_balance_quantity'])) {
                    $validation = \Config\Services::validation();
                    $this->session->setFlashdata('error_message', '<strong>Failed !!</strong> process quantity cant bigger than jo balance quantity ! Please check & try again...');
                    return redirect()->to('pde/buildAssembly')->withInput()->with('validation', $validation);
                }

                // validasi jika lebih besar dari qty di production warehouse maka failed dan ada apa ndak nya data
                $logVal = array('success' => 0, 'failed' => 0);
                for ($j = 0; $j < count($dataProductCode);) {
                    $productCode = $dataProductCode[$j];
                    if($this->models->checkJoAndAssemblyAndProduct($joNumber, $assemblyCode, $productCode) != null) {
                        // jika kombinasi ba code, jo number, assmbly, product code ada yg sama return failed
                        if($this->models->checkDuplicateJoAssemblyProductBaCode($baCode, $joNumber, $assemblyCode, $productCode) == null) {
                            // jika kosong maka lanjut
                            if($this->models->getStockProduction($productCode) != null) {
                                $getOldDataStockProduction = $this->models->getStockProduction($productCode);
                            } else {
                                // add new stock for production first then get it
                                $this->models->insertNewStockWithdrawal($productCode, '2', 0);
                                $getOldDataStockProduction = $this->models->getStockProduction($productCode);
                            }

                            $getOldDataQtyNeed = $this->models->getQtyNeedBom($assemblyCode, $productCode);

                            if(floatval($getOldDataQtyNeed['bom_quantity_needed']) * floatval($processQty) <= floatval($getOldDataStockProduction['stock_quantity'])) {
                                $logVal['success'] += 1;
                            } else {
                                $logVal['failed'] += 1;
                            }
                        } else {
                            // return failed
                            $validation = \Config\Services::validation();
                            $this->session->setFlashdata('error_message', '<strong>Failed !!</strong> data already exists ! Please check & try again...');
                            return redirect()->to('pde/buildAssembly')->withInput()->with('validation', $validation);
                        }
                    } else {
                        // return failed
                        $validation = \Config\Services::validation();
                        $this->session->setFlashdata('error_message', '<strong>Failed !!</strong> data jo/assembly/product not found ! Please check & try again...');
                        return redirect()->to('pde/buildAssembly')->withInput()->with('validation', $validation);
                    }

                    $j++;
                }

                // jika ada logVal yg failed satu saja maka return failed
                if($logVal['failed'] == 0) {
                    // validasi sukses

                    // looping untuk memasukan data sesuai panjang data product code
                    for ($i = 0; $i < count($dataProductCode);) {
                        $productCode = $dataProductCode[$i];
                        $getOldDataQtyNeed = $this->models->getQtyNeedBom($assemblyCode, $productCode);
                        $productQuantity = floatval($getOldDataQtyNeed['bom_quantity_needed']) * floatval($processQty);

                        // insert to database build assembly
                        $this->models->insertDataBuildAssembly($baCode, $userId, $assemblyCode, $productCode, $joNumber, $processQty, $productQuantity, $status);
                        $log['success'] += 1;

                        $i++;
                    }

                    // pesan sukses or failed
                    $this->session->setFlashdata('success_message', '<strong>Successfully !!</strong> insert for build assembly data ' . $log['success'] . ' and ' . $log['failed'] . ' <span class="text-danger">failure</span>');
                    return redirect()->to('pde/buildAssembly');
                } else {
                    // return failed
                    $validation = \Config\Services::validation();
                    $this->session->setFlashdata('error_message', '<strong>Failed !!</strong> process quantity x quantity needed must less than quantity production ! Please try again...');
                    return redirect()->to('pde/buildAssembly')->withInput()->with('validation', $validation);
                }
            }
        } else {
            // return failed
            $validation = \Config\Services::validation();
            $this->session->setFlashdata('error_message', '<strong>Failed !!</strong> jo number or assembly product not found ! Please try again...');
            return redirect()->to('pde/buildAssembly')->withInput()->with('validation', $validation);
        }

    }
}
