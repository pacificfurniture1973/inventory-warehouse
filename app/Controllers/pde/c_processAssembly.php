<?php

namespace App\Controllers\pde;
use App\Controllers\BaseController;
use Config\Services;
use App\Models\warehouse\models;
// use App\Models\models;

class c_processAssembly extends BaseController
{
    protected $table = 'build_assembly';
    protected $database = 'pde';
    protected $column_order = ['ba_id', null, 'ba_code', 'ba_assembly_code', 'ba_product_code', 'ba_jo_number', null, null, null, null, 'ba_user_id', 'ba_status']; 
    protected $column_search = ['ba_code', 'ba_assembly_code', 'ba_product_code', 'ba_jo_number'];
    protected $order = ['ba_id' => 'DESC'];

    public function __construct() {
        $this->session = session();
        $this->request = Services::request();
        $this->models = new models($this->request, $this->table, $this->column_order, $this->column_search, $this->order, $this->database, 'processAssembly');
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index()
    {  

        if(!$this->session->get('login')) {
            return redirect()->to('/');
        }
        
        $data = [
            'validation'     => \Config\Services::validation(),
            // add new main product
            'multipleUom'   => $this->models->getAllDataMultipleUom(),
            'uomSchema'     => $this->models->getAllDataUomSchema(),
            'currency'      => $this->models->getAllDataCurrency(),
            'status'        => $this->models->getAllDataStatus(),
            // add new user management
            'dept'          => $this->models->getAllDataDept(),
            'level'         => $this->models->getAllDataLevel(),
            // add new assembly
            'whs'           => $this->models->getAllDataWhs(),
        ];

        return view('pde/v_processAssembly.php', $data);
    }

    public function ajaxList() {
        if ($this->request->getMethod(true) === 'POST') {
            $lists = $this->models->getDatatables();
            $data = [];
            $no = $this->request->getPost('start');

            foreach ($lists as $list) {
                $no++; 
                $status = $list->ba_status == 9 ? 'text-danger' : 'text-success';
                $row = [];
                $row[] = $no;
                $row[] = "
                    <a href='#' data-bs-toggle='modal' data-bs-target='#modalProcessAssembly' id='approveRejectProcessAssembly' data-info='Approve' data-id=$list->ba_id data-jo_number=$list->ba_jo_number data-assembly_code=$list->ba_assembly_code data-product_code=$list->ba_product_code data-balance_process_quantity=$list->ba_balance_process_quantity data-product_quantity=$list->ba_product_quantity class='far fa-check-square text-decoration-none text-success mx-2'></a>
                    <a href='#' data-bs-toggle='modal' data-bs-target='#modalProcessAssembly' id='approveRejectProcessAssembly' data-info='Reject' data-id=$list->ba_id class='fas fa-times text-decoration-none text-danger'></a>";
                $row[] = "<p class='fw-bold text-dark'>$list->ba_code</p><small class='text-muted'>$list->ba_create</small>";
                $row[] = "<p class='fw-bold text-warning'>$list->ba_assembly_code</p>";
                $row[] = "<p class='fw-bold text-info'>$list->ba_product_code</p>";
                $row[] = "<p class='fw-bold text-success'>$list->ba_jo_number</p>";
                $row[] = $list->ba_process_quantity;
                $row[] = $list->stock_quantity;
                $row[] = "<p class='fw-bold text-dark'>$list->ba_product_quantity</p><small class='text-muted'>Qty per item: $list->bom_quantity_needed</small>";
                $row[] = $list->ba_status == 9 ? round($list->bom_quantity_needed * ($list->jo_balance_quantity - $list->ba_process_quantity),2) : round($list->bom_quantity_needed * $list->jo_balance_quantity,2) ;
                $row[] = $list->user_username;
                $row[] = "<p class='fw-bold $status'>$list->status_name</p>";
                $data[] = $row;
            }

            $output = [
                'draw' => $this->request->getPost('draw'),
                'recordsTotal' => $this->models->countAll('custom', 'processAssembly'),
                'recordsFiltered' => $this->models->countFiltered(),
                'data' => $data
            ];

            echo json_encode($output);
        }
    }

    public function processAssembly() {
        $baId       = $this->request->getVar('idProcessAssembly');
        $approveAt  = date("F j, Y, g:i a");
        $approveBy  = $this->session->get('user_id');
        $joNumber = $this->request->getVar('joNumberProcessAssembly');
        $assemblyCode = $this->request->getVar('assemblyCodeProcessAssembly');
        $productCode = $this->request->getVar('productCodeProcessAssembly');
        $balanceProcessQuantity = $this->request->getVar('balanceProcessQty');
        $productQuantity = $this->request->getVar('productQty');
        $info = $this->request->getVar('infoProcessAssembly');
        
        if($this->models->checkBuildAssemblyId($baId) != null) {
            if($info == 'Approve') {
                $baStatus   = 10;
                $checkApprove = $this->models->checkApproveAndTimeApprove($baId);
                if($checkApprove['ba_approve_by'] == null && $checkApprove['ba_approve_at'] == null) {
                    // validasi sukses
                    // kurangi stock di warehouse production
                    $getOldDataProduction = $this->models->getStockProduction($productCode);
                    $newStockProduction = floatval($getOldDataProduction['stock_quantity']) - floatval($productQuantity);
                    $this->models->updateDataProductionWarehouse($productCode, 2, $newStockProduction);

                    // kurangi stock di jo balance
                    $getOldDataJoBalance = $this->models->getDataJoBalance($joNumber, $assemblyCode);
                    $newStockJoBalance = floatval($getOldDataJoBalance['jo_balance_quantity']) - floatval($balanceProcessQuantity);
                    $this->models->updateDataJoBalance($joNumber, $assemblyCode, $newStockJoBalance);
    
                    // tambah stock di finish good  
                    if($this->models->getDataStockFinishGood($assemblyCode, 4) == null) {
                        // insert new
                        $this->models->insertDataFinishGoodWarehouse($assemblyCode, 4, $balanceProcessQuantity);
                    } else {
                        // update datanya
                        $getOldDataFinishGood = $this->models->getDataStockFinishGood($assemblyCode, 4);
                        $newStockFinishGood = floatval($getOldDataFinishGood['stock_quantity']) + floatval($balanceProcessQuantity);
    
                        $this->models->updateDataFinishGoodWarehouse($assemblyCode, 4, $newStockFinishGood);
                    }
    
                    // update data build assembly
                    $this->models->updateDataBuildAssembly($baId, $baStatus, $approveBy, $approveAt);

                    // renew data balance process quantity
                    $newStockBalanceQuantity = floatval($balanceProcessQuantity) - floatval($balanceProcessQuantity);
                    $this->models->updateDataBalanceProcessQuantity($newStockBalanceQuantity, $joNumber, $assemblyCode);
    
                    // sukses message
                    $this->session->setFlashdata('success_message', '<strong>Successfully !!</strong> approve this data !');
                    return redirect()->to('pde/processAssembly');
                } else {
                    // return failed
                    $validation = \Config\Services::validation();
                    $this->session->setFlashdata('error_message', '<strong>Failed !!</strong> data already approved !');
                    return redirect()->to('pde/processAssembly')->withInput()->with('validation', $validation);
                }
            } else {
                $baStatus   = 7;
                // update data build assembly
                $this->models->updateDataBuildAssembly($baId, $baStatus, $approveBy, $approveAt);

                // sukses message
                $this->session->setFlashdata('success_message', '<strong>Successfully !!</strong> reject this data !');
                return redirect()->to('pde/processAssembly');
        }
        } else {
            // return failed
            $validation = \Config\Services::validation();
            $this->session->setFlashdata('error_message', '<strong>Failed !!</strong> data not found ! Please check & try again...');
            return redirect()->to('pde/processAssembly')->withInput()->with('validation', $validation);
        }
    }
}
