<?php

namespace App\Controllers\warehouse;
use App\Controllers\BaseController;
use Config\Services;
use App\Models\warehouse\models;

class c_dashboard extends BaseController
{
    protected $table = 'job_order';
    protected $database = 'main';
    protected $column_order = ['jo_id', 'jo_number']; 
    protected $column_search = ['jo_number', 'jo_assembly_code', 'mp.product_code'];
    protected $order = ['jo_id' => 'DESC'];

    public function __construct() {
        $this->session = session();
        $this->request = Services::request();
        // tabel overbudget aku ambil dari tabel master plan biar ngga ngulang2
        $this->models = new models($this->request, $this->table, $this->column_order, $this->column_search, $this->order, $this->database, 'masterPlan');
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index()
    {

        if(!$this->session->get('login')) {
            return redirect()->to('/');
        }

        $data = [
            'validation'    => \Config\Services::validation(),
            'totalProducts' => $this->models->getTotalProducts('total'),
            'totalProductsThisMonth' => $this->models->getTotalProducts('month'),
            'totalCustomer' => $this->models->getTotalCustomer('total'),
            'totalCustomerThisMonth' => $this->models->getTotalCustomer('month'),
            'totalVendor'   => $this->models->getTotalVendor('total'),
            'totalVendorThisMonth'   => $this->models->getTotalVendor('month'),
            'totalAssembly' => $this->models->getTotalAssembly('total'),
            'totalAssemblyThisMonth' => $this->models->getTotalAssembly('month'),
            // 'totalJo'       => $this->models->getTotalJo('total'),
            // 'totalJoThisMonth'       => $this->models->getTotalJo('month'),
            // 'totalRR'       => $this->models->getTotalRR('total'),
            // 'totalRRThisMonth'       => $this->models->getTotalRR('month'),
            'totalWithdrawal'   => $this->models->getTotalWithdrawal('day'),
            'totalWithdrawalMonth'  => $this->models->getTotalWithdrawal('month'),
            'totalPendingJo'    => $this->models->getTotalPendingJo('pending'),
            'totalTotalJoMonth'    => $this->models->getTotalPendingJo('month'),
            'totalPendingWithdrawal'    => $this->models->getWithdrawalForDashboard('pending'),
            'totalPendingWithdrawalList'    => $this->models->getWithdrawalForDashboard('list'),
            // add new main product
            'multipleUom'   => $this->models->getAllDataMultipleUom(),
            'uomSchema'     => $this->models->getAllDataUomSchema(),
            'currency'      => $this->models->getAllDataCurrency(),
            'status'        => $this->models->getAllDataStatus(),
            // add new user management
            'dept'          => $this->models->getAllDataDept(),
            'level'         => $this->models->getAllDataLevel(),
            // add new assembly
            'whs'           => $this->models->getAllDataWhs(),
        ];
        
        return view('warehouse/v_dashboard.php', $data);
    }
}
