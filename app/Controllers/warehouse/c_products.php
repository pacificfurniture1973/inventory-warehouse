<?php

namespace App\Controllers\warehouse;
use App\Controllers\BaseController;
use Config\Services;
use App\Models\warehouse\models;
// use App\Models\models;

class c_products extends BaseController
{

    protected $table = 'stock';
    protected $database = 'warehouse';
    protected $column_order = ['product_id', 'product_code', null, null, null, 'stock_quantity', 'whs_name'];
    protected $column_search = ['product_assembly_code'];
    protected $order = ['stock_id' => 'DESC'];

    public function __construct() {
        $this->session = session();
        $this->request = Services::request();
        $this->models = new models($this->request, $this->table, $this->column_order, $this->column_search, $this->order, $this->database, 'warehouseProduct');
    }

    public function index()
    {

        if(!$this->session->get('login')) {
            return redirect()->to('/');
        }
        
        $data = [
            'validation'    => \Config\Services::validation(),
            // add new main product
            'multipleUom'   => $this->models->getAllDataMultipleUom(),
            'uomSchema'     => $this->models->getAllDataUomSchema(),
            'currency'      => $this->models->getAllDataCurrency(),
            'status'        => $this->models->getAllDataStatus(),
            // add new user management
            'dept'          => $this->models->getAllDataDept(),
            'level'         => $this->models->getAllDataLevel(),
            // add new assembly
            'whs'           => $this->models->getAllDataWhs(),
        ];
        
        return view('warehouse/v_products.php', $data); 
    }

    public function ajaxList()
    {           
        if ($this->request->getMethod(true) === 'POST') {
            $lists = $this->models->getDatatables();
            $data = [];
            $no = $this->request->getPost('start');

            foreach ($lists as $list) {
                $no++; 
                $row = [];
                $row[] = $no;
                $row[] = "<p class='fw-bold text-success'>$list->product_code</p>";
                $row[] = $list->product_name;
                $row[] = $list->product_description;
                $row[] = "<p class='fw-bold text-info'>" . floatval($list->stock_quantity) . "</p>";
                $row[] = "<p class='fw-bold text-info'>" . number_format($list->totalPrice,4,',','.') . "</p><small>Per item: " . number_format($list->product_unit_price,2,',','.') . "</small>";
                $row[] = $list->product_uom;
                $row[] = "<p class='fw-bold text-warning'>$list->whs_name</p>";
                $data[] = $row;
            }

            $output = [
                'draw' => $this->request->getPost('draw'),
                'recordsTotal' => $this->models->countAll($this->database),
                'recordsFiltered' => $this->models->countFiltered(),
                'data' => $data
            ];

            echo json_encode($output);
        }
    }

    public function exportDataProducts() {
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $dataTemplate = $this->models->getAllDataWarehouseProducts();

        $sheet->setCellValue('A1', 'Product Code/Id');
        $sheet->setCellValue('B1', 'Product Name');
        $sheet->setCellValue('C1', 'Description');
        $sheet->setCellValue('D1', 'uom_name');
        $sheet->setCellValue('E1', 'Available Quantity');
        $sheet->setCellValue('F1', 'Default Location');
        $rows = 2;

        foreach ($dataTemplate as $dt){
            $sheet->setCellValue('A' . $rows, $dt['product_assembly_code']);
            $sheet->setCellValue('B' . $rows, $dt['product_name']);
            $sheet->setCellValue('C' . $rows, $dt['product_description']);
            $sheet->setCellValue('D' . $rows, $dt['product_description']);
            $sheet->setCellValue('E' . $rows, $dt['stock_quantity']);
            $sheet->setCellValue('F' . $rows, $dt['whs_name']);
            $rows++;
        }

        // buat excelnya, fyi inisialisasi spreadsheet itu buat file excel kosong baru dan writer itu mengisi file kosong itu dengan data diatas
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $date = date('j M Y');
        $fileName = 'All data products - ' . $date;
    
        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName.'.xlsx');
        header('Cache-Control: max-age=0');
    
        // auto download disini, gaperlu dibalikin ke redirect lagi
        $writer->save('php://output');

        // return redirect()->to('/warehouse/products');
    }

    // public function insertProducts() {
    //     // get data from modals
    //     $productCode        = $this->request->post('productCode');
    //     $productType        = $this->request->post('productType');
    //     $productName        = $this->request->post('productName');
    //     $productDescription = $this->request->post('productDescription');
    //     $productUom         = $this->request->post('productUom');
    //     $productMultipleUom = $this->request->post('productMultipleUom');
    //     $productUomSchema   = $this->request->post('productUomSchema');
    //     $productCurrency    = $this->request->post('productCurrency');
    //     $productStatus      = $this->request->post('productStatus');
    //     $productDefaultLocation = $this->request->post('productDefaultLocation');
    //     $productUnitPrice   = $this->request->post('productUnitPrice');

    //     // validasi
    //     if(!$this->validate([
    //         'productCode'       => 'required|is_unique[product.product_code]',
    //         'productType'       => 'required',
    //         'productName'       => 'required|is_unique[product.product_name]',
    //         'productDescription' => 'required',
    //         'productUom'        => 'required',
    //         'productMultipleUom' => 'required',
    //         'productUomSchema'  => 'required',
    //         'productCurrency'   => 'required',
    //         'productStatus'     => 'required',
    //         'productDefaultLocation' => 'required',
    //         'productUnitPrice'  => 'required',
    //     ], [
    //         'productCode'   => [
    //             'required'  => 'Value is required !',
    //             'is_unique' => 'Value is already exists !'
    //         ], 
    //         'productType'   => [
    //             'required'  => 'Value is required !'
    //         ], 
    //         'productName'   => [
    //             'required'  => 'Value is required !',
    //             'is_unique' => 'Value is already exists'
    //         ], 
    //         'productDescription'    => [
    //             'required'  => 'Value is required !'
    //         ], 
    //         'productUom'    => [
    //             'required'  => 'Value is required !'
    //         ], 
    //         'productMultipleUom'    => [
    //             'required'  => 'Value is required !'
    //         ], 
    //         'productUomSchema' => [
    //             'required'  => 'Value is required !'
    //         ], 
    //         'productCurrency' => [
    //             'required'  => 'Value is required !'
    //         ], 
    //         'productStatus' => [
    //             'required'  => 'Value is required !'
    //         ], 
    //         'productDefaultLocation'    => [
    //             'required'  => 'Value is required !'
    //         ],
    //         'productUnitPrice'  => [
    //             'required'  => 'Value is required'
    //         ]
    //     ])) {
    //         // set session failed
    //         $validation = \Config\Services::validation();
    //         $this->session->setFlasdata('error_message', '<strong>Failed !</strong> insert new data, please check & try again !');
    //         return redirect()->to('warehouse/products')->withInput()->with('validation', $validation);
    //     } else {
    //         // sukses validasi

    //         $this->models->insertProducts($productCode, $productType, $productName, $productDescription, $productUom, $productMultipleUom, $productUomSchema, $productCurrency,
    //         $productStatus, $productDefaultLocation, $productUnitPrice);

    //         // set session sukses
    //         $this->session->setFlashdata('success_message', '<strong>Successfully !!</strong> insert new data, thanks..');
    //         return redirect()->to('warehouse/products');
    //     }
    // }
}
