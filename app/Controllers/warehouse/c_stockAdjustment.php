<?php

namespace App\Controllers\warehouse;
use App\Controllers\BaseController;
use Config\Services;
use App\Models\warehouse\models;
// use App\Models\models;

class c_stockAdjustment extends BaseController
{

    protected $table = 'stock';
    protected $database = 'warehouse';
    protected $column_order = [];
    protected $column_search = [];
    protected $order = [];
    
    public function __construct() {
        $this->request = Services::request();
        $this->models = new models($this->request, $this->table, $this->column_order, $this->column_search, $this->order, $this->database);
        $this->session = session();
    }

    public function index()
    {  

        if(!$this->session->get('login')) {
            return redirect()->to('/');
        }
        
        $data = [
            'whs'           => $this->models->getAllDataWhs(),
            'validation'    => \Config\Services::validation(),
            // add new main product
            'multipleUom'   => $this->models->getAllDataMultipleUom(),
            'uomSchema'     => $this->models->getAllDataUomSchema(),
            'currency'      => $this->models->getAllDataCurrency(),
            'status'        => $this->models->getAllDataStatus(),
            // add new user management
            'dept'          => $this->models->getAllDataDept(),
            'level'         => $this->models->getAllDataLevel(),
            // add new assembly
            'whs'           => $this->models->getAllDataWhs(),
        ];

        return view('warehouse/v_stockAdjustment.php', $data); 
    }

    public function insertStockAdjustment() {
        $dataDate = $this->request->getVar('date');
        // next jika sudah ada login system
        // $dataBy = $this->session->get('');
        $dataBy = 'Testing Account';
        $dataMemo = $this->request->getVar('memo');
        $dataReason = $this->request->getVar('reason');
        $dataProductCode = $this->request->getVar('productCode[]');
        $dataWhsCode = $this->request->getVar('defLoc');
        $dataAdjType = $this->request->getVar('adjType');
        $dataQty = $this->request->getVar('qty');
        $log = array('success' => 0, 'failed' => 0);
        $filename = 'Manual Insert';

        // auto sa number
        if($this->models->getLastIdDataStockAdjustmentLog() != null) {
            $lastSaLog = $this->models->getLastIdDataStockAdjustmentLog();
            $number = explode('-', $lastSaLog['insert_sa_number']);
            if(substr($number[2],0,4) == date('Y')) {                
                $saNumber = 'SA-PTPF-' . intval($number[2] + 1);
            } else {
                $saNumber = 'SA-PTPF-' . date('Y') . '00001';
            }
        } else {
            $saNumber = 'SA-PTPF-' . date('Y') . '00001';
        }

        // looping untuk memasukan data sesuai panjang data stock adjustment product code
        for ($i = 0; $i < count($dataProductCode);) {
            $date = $dataDate;
            $memo = $dataMemo;
            $reason = $dataReason;
            $productCode = $dataProductCode[$i];
            $whsCode = $dataWhsCode[$i];
            $adjType = $dataAdjType[$i];
            $qty = $dataQty[$i];
            $i++;

            // general validation
            if(!$this->validate([
                'date'      => 'required',
                'reason'    => 'required',
                'memo'      => 'required',
                'productCode'   => 'required',
                'adjType'   => 'required',
                'qty'       => 'required'
            ], [
                'date'  => [
                    'required'  => 'Must be filled !'
                ],
                'reason'    => [
                    'required'  => 'Must bed filled !'
                ],
                'memo'  => [
                    'required'  => 'Must be filled !'
                ],
                'productCode'   => [
                    'required'  => 'Must be filled !'
                ],
                'adjType'   => [
                    'required'  => 'Must be filled !'
                ],
                'qty'   => [
                    'required'  => 'Must be filled !'
                ]
            ])) {
                $validation = \Config\Services::validation();
                $this->session->setFlashdata('error_message', '<strong>Successfully !!</strong> insert stock adjustment !');
                return redirect()->to('warehouse/c_stockAdjustment')->withInput()->with('validation', $validation);
            } else {
                // jika ada yg sama bikin continue
                if($this->models->checkDuplicateInSameTime($saNumber, $productCode) != null) {
                    $log['failed'] += 1;
                    continue;
                }

                $getOldData = $this->models->getOldDataForStockAdjustment($productCode, $whsCode);

                // validasi data - jika sudah ada bernilai true dan tidak ada false atau failed
                if($this->models->getOldDataForStockAdjustment($productCode, $whsCode) != null) {
                    // validasi data stock in or out
                    if($adjType == 1) {
                        $newStock = $getOldData['stock_quantity'] + $qty;
                    } else {
                        // cek stock out kalo lebih banyak return error
                        if(floatval($qty) > floatval($getOldData['stock_quantity'])) {
                            $this->session->setFlashdata('error_message', '<strong>Failed !!</strong> Process Stock Adjustment !, Error on input Quantity !');
                            return redirect()->to('warehouse/c_stockAdjustment');
                        } else {
                            $newStock = $getOldData['stock_quantity'] - $qty;
                        }
                    }

                    // masukan ke database warehouse
                    $this->models->updateStockAdjustment($productCode, $whsCode, $newStock);
    
                    // masukan ke database log
                    $this->models->insertStockAdjustmentLog($saNumber, $dataDate, $dataBy, $dataReason, $dataMemo, $adjType, $productCode, $whsCode, $qty, $filename);
    
                    $log['success'] += 1;
                } else {
                    $log['failed'] += 1;
                    continue;
                }
            }
        }

        // pesan sukses or failed
        $this->session->setFlashdata('success_message', '<strong>Successfully !!</strong> insert for update stock ' . $log['success'] . ' and ' . $log['failed'] . ' <span class="text-danger">failure</span>');
        return redirect()->to('warehouse/stockAdjustment');

    }

    public function importStockAdjustment() {
        $file = $this->request->getFile('importStockAdjustment');
        $filename = $file->getName();
        $ext = $file->getClientExtension();
        
        if($ext == 'xls') {
            $render = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
        } else {
            $render = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        }

        // jika error = 0 maka file ada, jika error = 4 maka file null
        if($file->getError() == 4) {
            $validation = \Config\Services::validation();
            $this->session->setFlashdata('error_message', '<strong>Failed !!</strong> file not found ! Please try again...');
            return redirect()->to('warehouse/stockAdjustment')->withInput()->with('validation', $validation);
        }

        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file);

        $sheet = $spreadsheet->getActiveSheet()->toArray();
        $log = array('success' => 0, 'failed' => 0);

        if($this->models->getLastIdDataStockAdjustmentLog() != null) {
            $lastSaLog = $this->models->getLastIdDataStockAdjustmentLog();
            $number = explode('-', $lastSaLog['insert_sa_number']);
            if(substr($number[2],0,4) == date('Y')) {                
                $saNumber = 'SA-PTPF-' . intval($number[2] + 1);
            } else {
                $saNumber = 'SA-PTPF-' . date('Y') . '00001';
            }
        } else {
            $saNumber = 'SA-PTPF-' . date('Y') . '00001';
        }

        if($spreadsheet != null) {
            foreach($sheet as $baris => $kolom) {
                if($baris == 0) {
                    continue;
                }

                // jika semua ngga diinput langsung continue biar yg note informatiaon ngga kebaca failed
                if($kolom['0'] == null && $kolom['1'] == null && $kolom['2'] == null && $kolom['3'] === null && $kolom['4'] == null && $kolom['5'] == null && $kolom['6'] == null) { 
                    continue;
                };

                // yg wajib harus di isi kalo tidak failed
                if($kolom['0'] != null && $kolom['1'] != null && $kolom['2'] != null && $kolom['3'] !== null && $kolom['4'] != null && $kolom['5'] != null && $kolom['6'] != null) {

                    // format tanggal harus bener YYYY-MM-DD
                    if(!preg_match("/^((((19|[2-9]\d)\d{2})\-(0[13578]|1[02])\-(0[1-9]|[12]\d|3[01]))|(((19|[2-9]\d)\d{2})\-(0[13456789]|1[012])\-(0[1-9]|[12]\d|30))|(((19|[2-9]\d)\d{2})\-02\-(0[1-9]|1\d|2[0-8]))|(((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))\-02\-29))$/", $kolom['4'])) {
                        $log['failed'] += 1;
                        continue;
                    }

                    // jika di dalam sekali import ada data yg sama failed
                    // jika ada yg sama bikin continue
                    if($this->models->checkDuplicateInSameTime($saNumber, $kolom['0']) != null) {
                        $log['failed'] += 1;
                        continue;
                    }

                    // jika data ada maka lanjut else continue
                    if($this->models->getOldDataForStockAdjustment($kolom['0'], $kolom['2']) != null) {
                        $oldData = $this->models->getOldDataForStockAdjustment($kolom['0'], $kolom['2']);
                        // validasi stock in or out
                        if($kolom['1'] == 1) {
                            // kalkulasi stock product
                            $newStock = floatval($oldData['stock_quantity']) + floatval($kolom['3']);
                        } else {
                            // cek stock out kalo lebih banyak return error
                            if(floatval($kolom['3']) > floatval($oldData['stock_quantity'])) {
                                $log['failed'] += 1;
                                continue;
                            } else {
                                $newStock = floatval($oldData['stock_quantity']) - floatval($kolom['3']);
                            }
                        }

                        $this->models->updateStockAdjustment($kolom['0'], $kolom['2'] , floatval($newStock));

                        // masukan ke database log
                        // nanti user diganti session
                        $this->models->insertStockAdjustmentLog($saNumber, $kolom['4'], 'Testing Account', $kolom['5'], $kolom['6'], $kolom['1'], $kolom['0'], $kolom['2'], $kolom['3'], $filename);
                        
                        $log['success'] += 1;
                    } else {
                        $log['failed'] += 1;
                        continue;
                    }
                } else {
                    $log['failed'] += 1;
                    continue;
                }
            }

            $this->session->setFlashdata('success_message', '<strong>Successfully !!</strong> import for update stock ' . $log['success'] . ' and ' . $log['failed'] . ' <span class="text-danger">failure</span>');
            return redirect()->to('warehouse/stockAdjustment');
        }
    }

    public function exportTemplateStockAdjustment() {
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $dataWhs = $this->models->getAllDataWhs();
        $dataReason = $this->models->getAllDataReason();

        $sheet->setCellValue('A1', 'Product/Assembly Code*');
        $sheet->setCellValue('B1', 'Adjustment Type*');
        $sheet->setCellValue('C1', 'Default Location*');
        $sheet->setCellValue('D1', 'Quantity*');
        $sheet->setCellValue('E1', 'Date*');
        $sheet->setCellValue('F1', 'Reason*');
        $sheet->setCellValue('G1', 'Memo*');

        $sheet->setCellValue('A2', "WIP-1909-00344");
        $sheet->setCellValue('B2', "1");
        $sheet->setCellValue('C2', "1");
        $sheet->setCellValue('D2', "100");
        $sheet->setCellValue('E2', "2030-12-30");
        $sheet->setCellValue('F2', "7");
        $sheet->setCellValue('G2', "Inventory November 2021");

        // informasi untuk template 
        $sheet->setCellValue('I4', "Information");
        $sheet->setCellValue('I5', "# Adjustment Type");
        $sheet->setCellValue('I6', "1. Stock In");
        $sheet->setCellValue('I7', "2. Stock Out");

        $sheet->setCellValue('I9', "# Default Location");
        $rowWhs = 10;
        foreach($dataWhs as $dw) {
            $sheet->setCellValue('I'.$rowWhs, $dw['whs_code'] . ". " . $dw['whs_name']);
            $rowWhs++;
        }

        $sheet->setCellValue('K5', "# Reason");
        $rowReason = 6;
        foreach($dataReason as $dr) {
            $sheet->setCellValue('K'.$rowReason, $dr['reason_code'] . ". " . $dr['reason_name']);
            $rowReason++;
        }

        // buat excelnya, fyi inisialisasi spreadsheet itu buat file excel kosong baru dan writer itu mengisi file kosong itu dengan data diatas
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $fileName = 'Template import stock adjustment';
    
        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName.'.xlsx');
        header('Cache-Control: max-age=0');
    
        // auto download disini, gaperlu dibalikin ke redirect lagi
        $writer->save('php://output');

        // return redirect()->to('/warehouse/products');
    }
    
    public function getTerm() {
        $data = $this->request->getVar('name');
        $data = $this->models->getTermStockAdjustment($data);
        echo json_encode($data);
    }
}
