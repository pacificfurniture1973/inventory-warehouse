<?php

namespace App\Controllers\warehouse;
use App\Controllers\BaseController;
use Config\Services;
use App\Models\warehouse\models;
// use App\Models\models;

class c_stockAdjustmentLog extends BaseController
{

    protected $table = 'stock_adjustment_log';
    protected $database = 'log';
    protected $column_order = ['insert_sa_id', null, 'insert_sa_number', 'insert_sa_product_code', 'insert_sa_whs_code', null, 'insert_sa_type', null, null, null];
    protected $column_search = ['insert_sa_number', 'insert_sa_product_code'];
    protected $order = ['insert_sa_id' => 'DESC'];
    
    public function __construct() {
        $this->request = Services::request();
        $this->models = new models($this->request, $this->table, $this->column_order, $this->column_search, $this->order, $this->database, 'stockAdjustmentLog');
        $this->session = session();
    }

    public function index()
    {  

        if(!$this->session->get('login')) {
            return redirect()->to('/');
        }
        
        $data = [
            'validation'    => \Config\Services::validation(),
            // add new main product
            'multipleUom'   => $this->models->getAllDataMultipleUom(),
            'uomSchema'     => $this->models->getAllDataUomSchema(),
            'currency'      => $this->models->getAllDataCurrency(),
            'status'        => $this->models->getAllDataStatus(),
            // add new user management
            'dept'          => $this->models->getAllDataDept(),
            'level'         => $this->models->getAllDataLevel(),
            // add new assembly
            'whs'           => $this->models->getAllDataWhs(),
        ];

        return view('warehouse/v_stockAdjustmentLog.php', $data); 
    }

    public function ajaxList()
    {
        if ($this->request->getMethod(true) === 'POST') {
            $fromDate = $this->request->getVar('fromDate');
            $toDate = $this->request->getVar('toDate');

            $lists = $this->models->getDatatables($fromDate, $toDate, 'adjustmentLog');

            $data = [];
            $no = $this->request->getPost('start');

            foreach ($lists as $list) {
                $no++;
                $row = [];
                $row[] = $no;
                $row[] = "
				<a class='fas fa-file-export text-primary' href='c_stockAdjustmentLog/exportStockAdjustmentByLog/$list->insert_sa_number' style='margin-right: 5px; cursor: pointer;'></a>
                <a class='fas fa-print text-primary' href='c_stockAdjustmentLog/printSaLog/$list->insert_sa_number/single/$list->insert_sa_product_code' target='_blank' style='margin-right: 5px; cursor: pointer;'></a>
                <a class='fas fa-atlas text-primary' href='c_stockAdjustmentLog/printSaLog/$list->insert_sa_number/multiple/$list->insert_sa_product_code' target='_blank' style='margin-right: 5px; cursor: pointer;'></a>";
                $row[] = "<p class='fw-bold text-success'>$list->insert_sa_number</p><small class='text-muted'>$list->insert_sa_date</small>";
                $row[] = "<p class='fw-bold text-warning'>$list->insert_sa_product_code</p>";
                $row[] = $list->whs_name;
                $row[] = $list->insert_sa_quantity;
                $row[] = $list->insert_sa_type == 1 ? "<p class='fw-bold text-success'>In</p>" : "<p class='fw-bold text-danger'>Out</p>";
                $row[] = $list->reason_name;
                $row[] = $list->insert_sa_memo;
                $row[] = $list->insert_sa_by;
                $data[] = $row;
            }

            $output = [
                'draw' => $this->request->getPost('draw'),
                'recordsTotal' => $this->models->countAll($this->database),
                'recordsFiltered' => $this->models->countFiltered($fromDate, $toDate, 'adjustmentLog'),
                'data' => $data
            ];

            echo json_encode($output);
        }
    }

    public function printSaLog($saNumber, $type, $productCode) {
        // dd($saNumber, $type);
        // dd($this->models->getDataForPrintWithdrawal($wdNumber));
        if($type == 'single') {
            $stockAdjustment = $this->models->getDataForPrintSaLogSingle($saNumber, $productCode);
        } elseif ($type == 'multiple') {
            $stockAdjustment = $this->models->getDataForPrintSaLogMultiple($saNumber);
        }
        
        $data = [
            'stockAdjustment'  => $stockAdjustment,
        ];

        $mpdf = new \Mpdf\Mpdf();
        $html = view('/layout/warehouse/printStockAdjustment', $data);

        $mpdf->WriteHTML($html);
        $this->response->setHeader('Content-Type', 'application/pdf');
        $mpdf->Output('test.pdf','I'); // opens in browser
    }

    public function exportStockAdjustment() {
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $dataTemplate = $this->models->getAllDataStockAdjustment();

        $sheet->setCellValue('A1', 'SA Number');
        $sheet->setCellValue('B1', 'SA Date');
        $sheet->setCellValue('C1', 'Product Code');
        $sheet->setCellValue('D1', 'Default Location');
        $sheet->setCellValue('E1', 'Quantity');
        $sheet->setCellValue('F1', 'Type');
        $sheet->setCellValue('G1', 'Reason');
        $sheet->setCellValue('H1', 'Memo');
        $sheet->setCellValue('I1', 'Created By');
        $sheet->setCellValue('J1', 'Created At');
        $rows = 2;

        foreach ($dataTemplate as $dt){
            $sheet->setCellValue('A' . $rows, $dt['insert_sa_number']);
            $sheet->setCellValue('B' . $rows, $dt['insert_sa_date']);
            $sheet->setCellValue('C' . $rows, $dt['insert_sa_product_code']);
            $sheet->setCellValue('D' . $rows, $dt['whs_name']);
            $sheet->setCellValue('E' . $rows, $dt['insert_sa_quantity']);
            if($dt['insert_sa_type'] == 1) {
                $sheet->setCellValue('F' . $rows, 'IN');
            } else {
                $sheet->setCellValue('F' . $rows, 'OUT');
            }
            $sheet->setCellValue('G' . $rows, $dt['reason_name']);
            $sheet->setCellValue('H' . $rows, $dt['insert_sa_memo']);
            $sheet->setCellValue('I' . $rows, $dt['insert_sa_by']);
            $sheet->setCellValue('J' . $rows, $dt['insert_sa_created']);
            $rows++;
        }

        // buat excelnya, fyi inisialisasi spreadsheet itu buat file excel kosong baru dan writer itu mengisi file kosong itu dengan data diatas
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $date = date('j M Y');
        $fileName = 'All data stock adjustment - ' . $date;
    
        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName.'.xlsx');
        header('Cache-Control: max-age=0');
    
        // auto download disini, gaperlu dibalikin ke redirect lagi
        $writer->save('php://output');

        // return redirect()->to('/warehouse/products');
    }
	
	public function exportStockAdjustmentByLog($SaNumber) {
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $dataTemplate = $this->models->getDataStockAdjustmentByLog($SaNumber);

        $sheet->setCellValue('A1', 'SA Number');
        $sheet->setCellValue('B1', 'SA Date');
        $sheet->setCellValue('C1', 'Product Code');
        $sheet->setCellValue('D1', 'Default Location');
        $sheet->setCellValue('E1', 'Quantity');
        $sheet->setCellValue('F1', 'Type');
        $sheet->setCellValue('G1', 'Reason');
        $sheet->setCellValue('H1', 'Memo');
        $sheet->setCellValue('I1', 'Created By');
        $sheet->setCellValue('J1', 'Created At');
        $rows = 2;

        foreach ($dataTemplate as $dt){
            $sheet->setCellValue('A' . $rows, $dt['insert_sa_number']);
            $sheet->setCellValue('B' . $rows, $dt['insert_sa_date']);
            $sheet->setCellValue('C' . $rows, $dt['insert_sa_product_code']);
            $sheet->setCellValue('D' . $rows, $dt['whs_name']);
            $sheet->setCellValue('E' . $rows, $dt['insert_sa_quantity']);
            if($dt['insert_sa_type'] == 1) {
                $sheet->setCellValue('F' . $rows, 'IN');
            } else {
                $sheet->setCellValue('F' . $rows, 'OUT');
            }
            $sheet->setCellValue('G' . $rows, $dt['reason_name']);
            $sheet->setCellValue('H' . $rows, $dt['insert_sa_memo']);
            $sheet->setCellValue('I' . $rows, $dt['insert_sa_by']);
            $sheet->setCellValue('J' . $rows, $dt['insert_sa_created']);
            $rows++;
        }

        // buat excelnya, fyi inisialisasi spreadsheet itu buat file excel kosong baru dan writer itu mengisi file kosong itu dengan data diatas
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $date = date('j M Y');
        $fileName = 'All data stock adjustment by ' . $SaNumber;
    
        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName.'.xlsx');
        header('Cache-Control: max-age=0');
    
        // auto download disini, gaperlu dibalikin ke redirect lagi
        $writer->save('php://output');

        // return redirect()->to('/warehouse/products');
    }
}
