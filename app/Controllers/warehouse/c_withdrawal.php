<?php

namespace App\Controllers\warehouse;
use App\Controllers\BaseController;
use Config\Services;
use App\Models\warehouse\models;
// use App\Models\models;

class c_withdrawal extends BaseController
{
    protected $table = 'stock';
    protected $database = 'warehouse';
    protected $column_order = [];
    protected $column_search = [];
    protected $order = [];

    public function __construct() {
        $this->request = Services::request();
        $this->models = new models($this->request, $this->table, $this->column_order, $this->column_search, $this->order, $this->database);
        $this->session = session();
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index()
    {  

        if(!$this->session->get('login')) {
            return redirect()->to('/');
        }
        
        // ntar diganti session input parameternya
        $handle = $this->models->getUserDeptHandleByUserId($this->session->get('user_id'));
        $handle = explode(',',$handle['user_dept_handle']);
        $handleName = [];

        foreach($handle as $h) {
            $isi = $this->models->getUserDeptHandleName($h);
            $handleName[] = $isi['dept_name'];
        }

        $combineDeptName = array_combine($handle, $handleName);

        $data = [
            'handleName'    => $handleName,
            'handle'        => $handle,
            'combine'       => $combineDeptName,
            'defLoc'        => $this->models->getAllDataDefLocExcept(),
            'validation'     => \Config\Services::validation(),
            // add new main product
            'multipleUom'   => $this->models->getAllDataMultipleUom(),
            'uomSchema'     => $this->models->getAllDataUomSchema(),
            'currency'      => $this->models->getAllDataCurrency(),
            'status'        => $this->models->getAllDataStatus(),
            // add new user management
            'dept'          => $this->models->getAllDataDept(),
            'level'         => $this->models->getAllDataLevel(),
            // add new assembly
            'whs'           => $this->models->getAllDataWhs(),
        ];

        return view('warehouse/v_withdrawal.php', $data);
    }

    public function insertWithdrawal() {
        $dataToWhs      = $this->request->getVar('wsToWhs');
        $dataDept       = $this->request->getVar('wsDept');
        $dataWithJo     = $this->request->getVar('wsWithJo');
        $dataDate       = $this->request->getVar('wsDate');
        $dataProductCode = $this->request->getVar('wsProductCode');
        $dataFromWhs    = $this->request->getVar('wsDefLoc');
        $dataJoNumber   = $this->request->getVar('wsJoNumber');
        $dataAssemblyProduct   = $this->request->getVar('wsAssemblyProduct');
        $dataReqQuantity = $this->request->getVar('wsRequestQty');
        $dataRemark     = $this->request->getVar('wsRemark');
        // nanti di ganti session user id
        $wsBy = $this->session->get('user_id'); 
        $log = array('success' => 0, 'failed' => 0);
        $filename = "Manual Request";

        // dd($dataToWhs, $dataDeptHandle, $dataDate, $dataProductCode, $dataFromWhs, $dataJoNumber, $dataReqQuantity, $dataRemark);

        // auto ws number
        if($this->models->getLastIdDataWithdrawalLog() != null) {
            $lastWsLog = $this->models->getLastIdDataWithdrawalLog();
            $number = explode('-', $lastWsLog['insert_wd_number']);
            if(substr($number[2],0,4) == date('Y')) {                
                $wsNumber = 'WS-PTPF-' . intval($number[2] + 1);
            } else {
                $wsNumber = 'WS-PTPF-' . date('Y') . '00001';
            }
        } else {
            $wsNumber = 'WS-PTPF-' . date('Y') . '00001';
        }

        // specific rule
        if($dataWithJo == 1) {
            $rule = 'required';
        } else {
            $rule = 'permit_empty';
        }


        // looping untuk memasukan data sesuai panjang data withdrawal stock product code
        for ($i = 0; $i < count($dataProductCode);) {
            $date       = $dataDate;
            $toWhs      = $dataToWhs;
            $dept       = $dataDept;
            $productCode = $dataProductCode[$i];
            $whsCode    = $dataFromWhs[$i];
            $joNumber   = $dataJoNumber[$i];
            $assemblyProduct   = $dataAssemblyProduct[$i];
            $reqQty     = $dataReqQuantity[$i];
            $remark     = $dataRemark[$i];
            $status     = 3;
            $i++;

            // general validation
            if(!$this->validate([
                'wsProductCode' => 'required',
                'wsJoNumber'    => $rule,
                'wsRequestQty'  => 'required',
                'wsRemark'      => 'required',
                'wsToWhs'       => 'required',
                'wsDept'        => 'required',
                'wsAssemblyProduct' => 'required'
            ], [
                'wsProductCode' => [
                    'required'  => 'Must be filled !'
                ],
                'wsRequestQty'  => [
                    'required'  => 'Must be filled !'
                ],
                'wsRemark'      => [
                    'required'  => 'Must be filled !'
                ],
                'wsToWhs'       => [
                    'required'  => 'Must be filled !'
                ],
                'wsDept'        => [
                    'required'  => 'Must be filled !'
                ],
                'wsAssemblyProduct'        => [
                    'required'  => 'Must be filled !'
                ]
            ])) {
                $validation = \Config\Services::validation();
                $this->session->setFlashdata('error_message', '<strong>Failed !!</strong> Process Withdrawal Request ! Please try again...');
                return redirect()->to('warehouse/c_withdrawal')->withInput()->with('validation', $validation);
            } else {
                // validasi data - jika sudah ada bernilai true dan tidak ada false atau failed
                if($this->models->getOldDataForWithdrawal($productCode, $whsCode) != null) {
                    $getOldData = $this->models->getOldDataForWithdrawal($productCode, $whsCode);

                    if($joNumber != null) {
                        // cek jika product code ngga ada di susunan data assembly yg di bom return error
                        if($this->models->checkProductAndAssemblyForWithdrawal($productCode, $assemblyProduct) == null) {
                            $log['failed'] += 1;
                            continue;
                        }

                        if($this->models->checkDuplicateWsCodeAssemblyJoProduct($wsNumber, $assemblyProduct, $joNumber, $productCode) == null) {
                            // masukan ke database warehouse withdrawal and insert log withdrawal

                            $this->models->insertWitdrawalRequest($wsNumber, $wsBy, $date, $toWhs, $dept, $productCode, $whsCode, $joNumber, $assemblyProduct, $reqQty, $remark, $status, $filename);

                            $log['success'] += 1;
                        } else {
                            $log['failed'] += 1;
                            continue;
                        }
                    } elseif($joNumber == null) {
                        if($this->models->checkDuplicateWsCodeProduct($wsNumber, $productCode) == null) {
                            // masukan ke database warehouse withdrawal and insert log withdrawal
                            $this->models->insertWitdrawalRequest($wsNumber, $wsBy, $date, $toWhs, $dept, $productCode, $whsCode, $joNumber, $assemblyProduct, $reqQty, $remark, $status, $filename);

                            $log['success'] += 1;
                        } else {
                            $log['failed'] += 1;
                            continue;
                        }
                    } else {
                        // return failed
                        $log['failed'] += 1;
                        continue;
                    }
                } else {
                    $log['failed'] += 1;
                    continue;
                }
            }
        }

        // pesan sukses or failed
        $this->session->setFlashdata('success_message', '<strong>Successfully !!</strong> insert for withdrawal stock ' . $log['success'] . ' and ' . $log['failed'] . ' <span class="text-danger">failure</span>');
        return redirect()->to('warehouse/c_withdrawal');
    }

    public function exportTemplateWithdrawal() {
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $dataWhs = $this->models->getAllDataWhs();
        $dataDept = $this->models->getAllDataDept();

        $sheet->setCellValue('A1', 'Destination Wahehouse*');
        $sheet->setCellValue('B1', 'Department Handle*');
        $sheet->setCellValue('C1', 'Product Code*');
        $sheet->setCellValue('D1', 'JO Number*');
        $sheet->setCellValue('E1', 'Assembly Code*');
        $sheet->setCellValue('F1', 'Request Quantity*');
        $sheet->setCellValue('G1', 'Remark*');

        $sheet->setCellValue('A2', "2");
        $sheet->setCellValue('B2', "2");
        $sheet->setCellValue('C2', "WIP-1909-00344");
        $sheet->setCellValue('D2', "JO-PTPF-21000506");
        $sheet->setCellValue('E2', "PK-00016-00");
        $sheet->setCellValue('F2', "100");
        $sheet->setCellValue('G2', "Remark");

        // informasi untuk template 
        $sheet->setCellValue('I2', "Information");

        $sheet->setCellValue('I3', "# Warehouse");
        $rowWhs = 4;
        foreach($dataWhs as $dw) {
            $sheet->setCellValue('I'.$rowWhs, $dw['whs_code'] . ". " . $dw['whs_name']);
            $rowWhs++;
        }

        $sheet->setCellValue('I'. $rowWhs + 2, "# Department");
        $rowDept = $rowWhs + 3;
        foreach($dataDept as $dd) {
            $sheet->setCellValue('I'.$rowDept, $dd['dept_id'] . ". " . $dd['dept_name']);
            $rowDept++;
        }

        // buat excelnya, fyi inisialisasi spreadsheet itu buat file excel kosong baru dan writer itu mengisi file kosong itu dengan data diatas
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $fileName = 'Template import withdrawal';
    
        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName.'.xlsx');
        header('Cache-Control: max-age=0');
    
        // auto download disini, gaperlu dibalikin ke redirect lagi
        $writer->save('php://output');

        // return redirect()->to('/warehouse/products');
    }

    public function importWithdrawal() {
        $file = $this->request->getFile('importWithdrawal');
        $filename = $file->getName();
        $userId = $this->session->get('user_id');
        $status = 4;
        $date = date("Y-m-d");
        $ext = $file->getClientExtension();
        
        if($ext == 'xls') {
            $render = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
        } else {
            $render = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        }

        // jika error = 0 maka file ada, jika error = 4 maka file null
        if($file->getError() == 4) {
            $validation = \Config\Services::validation();
            $this->session->setFlashdata('error_message', '<strong>Failed !!</strong> file not found ! Please try again...');
            return redirect()->to('warehouse/c_withdrawal')->withInput()->with('validation', $validation);
        }

        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file);

        $sheet = $spreadsheet->getActiveSheet()->toArray();
        $log = array('success' => 0, 'failed' => 0);

        // auto ws number
        if($this->models->getLastIdDataWithdrawalLog() != null) {
            $lastWsLog = $this->models->getLastIdDataWithdrawalLog();
            $number = explode('-', $lastWsLog['insert_wd_number']);
            if(substr($number[2],0,4) == date('Y')) {                
                $iwsNumber = 'IWS-PTPF-' . intval($number[2] + 1);
            } else {
                $iwsNumber = 'IWS-PTPF-' . date('Y') . '00001';
            }
        } else {
            $iwsNumber = 'IWS-PTPF-' . date('Y') . '00001';
        }

        if($spreadsheet != null) {
            foreach($sheet as $baris => $kolom) {
                if($baris == 0) {
                    continue;
                }

                // jika semua ngga diinput langsung continue biar yg note informatiaon ngga kebaca failed
                if($kolom['0'] == null && $kolom['1'] == null && $kolom['2'] == null && $kolom['3'] == null && $kolom['4'] == null && $kolom['5'] === null && $kolom['6'] == null) {
                    continue;
                }

                // yg wajib harus di isi kalo tidak failed
                if($kolom['0'] != null && $kolom['1'] != null && $kolom['2'] != null && $kolom['3'] != null && $kolom['4'] != null && $kolom['5'] !== null && $kolom['6'] != null) {
                    // jika ws code, product code, assembly code, jo number sudah ada maka failed (ketika import itukan satu ws code jadi ada kemungkinan input double di situ)
                    if($this->models->checkDuplicateWithdrawal($iwsNumber, $kolom['2'], $kolom['3'], $kolom['4']) != null) {
                        $log['failed'] += 1;
                        continue;
                    }
                    
                    // jika data ada maka lanjut else continue
                    if($this->models->getOldDataForWithdrawal($kolom['2'], '1') != null) {
                        
                        $oldData = $this->models->getOldDataForWithdrawal($kolom['2'], '1');

                        if($this->models->checkDataBeforeImportWithdrawal($kolom['3'], $kolom['4'], $kolom['2']) != null) {
                            $toWhs = $kolom['0'];
                            $dept = $kolom['1'];
                            $productCode = $kolom['2'];
                            $joNumber = $kolom['3'];
                            $assemblyCode = $kolom['4'];
                            $reqQty = $kolom['5'];
                            $remark = $kolom['6'];

                            // masukan ke database log
                            // nanti user diganti session
                            // masukan ke database warehouse withdrawal and insert log withdrawal
                            $this->models->insertWitdrawalRequest($iwsNumber, $userId, $date, $toWhs, $dept, $productCode, $whsCode, $joNumber, $assemblyCode, $reqQty, $remark, $status, $filename);

                            $log['success'] += 1;
                        } else {
                            $log['failed'] += 1;
                            continue;
                        }
                    } else {
                        $log['failed'] += 1;
                        continue;
                    }
                } else {
                    $log['failed'] += 1;
                    continue;
                }
            }

            $this->session->setFlashdata('success_message', '<strong>Successfully !!</strong> import for withdrawal request ' . $log['success'] . ' and ' . $log['failed'] . ' <span class="text-danger">failure</span>');
            return redirect()->to('warehouse/c_withdrawal');
        }
    }

    public function getTerm() {
        $data = $this->request->getVar('name');
        $data = $this->models->getTermWithdrawal($data);
        echo json_encode($data);
    }

    public function getTermCustomer() {
        $data = $this->request->getVar('nameCustomer');
        $data = $this->models->getTermWithdrawalCustomer($data);
        echo json_encode($data);
    }

    public function getTermJo() {
        $dataJo = $this->request->getVar('nameJo');
        $dataCustomer = $this->request->getVar('nameCustomer');
        $dataProduct = $this->request->getVar('nameProduct');
        $data = $this->models->getTermWithdrawalJo($dataJo, $dataCustomer, $dataProduct);
        echo json_encode($data);
    }
}
