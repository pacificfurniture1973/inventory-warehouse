<?php

namespace App\Controllers\warehouse;
use App\Controllers\BaseController;
use Config\Services;
use App\Models\warehouse\models;
// use App\Models\models;

class c_withdrawalFinish extends BaseController
{
    protected $table = 'withdrawal';
    protected $database = 'warehouse';
    // protected $column_order = ['wd_number', 'wd_by', 'wd_product_code', null, 'wd_jo_number', null, null, null, null, null, null, 'wd_status', null];
    protected $column_order = ['wd_id',null, 'wd_number', null, null, null, 'wd_status'];
    protected $column_search = ['wd_number', 'wd_product_code', 'wd_jo_number', 'wd_remark', 'wd_remark_warehouse', 'wd_issue_date', 'wd_date', 'wd_status'];
    protected $order = ['wd_id' => 'DESC'];

    public function __construct() {
        $this->session = session();
        $this->request = Services::request();
        $this->models = new models($this->request, $this->table, $this->column_order, $this->column_search, $this->order, $this->database, 'withdrawalFinish');
    }

    public function index()
    {  

        if(!$this->session->get('login')) {
            return redirect()->to('/');
        }
        
        $data = [
            'validation'     => \Config\Services::validation(),
            // add new main product
            'multipleUom'   => $this->models->getAllDataMultipleUom(),
            'uomSchema'     => $this->models->getAllDataUomSchema(),
            'currency'      => $this->models->getAllDataCurrency(),
            'status'        => $this->models->getAllDataStatus(),
            // add new user management
            'dept'          => $this->models->getAllDataDept(),
            'level'         => $this->models->getAllDataLevel(),
            // add new assembly
            'whs'           => $this->models->getAllDataWhs(),
        ];

        return view('warehouse/v_withdrawalFinish.php', $data);
    }

    public function ajaxList() {
        if ($this->request->getMethod(true) === 'POST') {
            $lists = $this->models->getDatatables();
            $data = [];
            $no = $this->request->getPost('start');

            foreach ($lists as $list) {
                $no++; 
                $row = [];
                $row[] = $no;
                $row[] = "
                <a class='fas fa-print text-primary".($list->wd_status != 6 ? 'text-muted' : '')."'
                    data-wd_number='$list->wd_number'
                    href='c_withdrawalFinish/printWithdrawal/$list->wd_number' target='_blank' style='margin-right: 5px; cursor: pointer; ".($list->wd_status != 6 ? 'pointer-events: none;' : '')."'></a>";
                $row[] = "<p class='fw-bold text-success'>$list->wd_number</p><small class='text-muted'>$list->wd_date</small>";
                $row[] = "$list->from_whs >> $list->to_whs";
                $row[] = $list->wd_remark;
                $row[] = "<p class='fw-bold'>$list->user_username</p><small class='text-muted'>$list->dept_name - Department</small>";
                $row[] = ($list->wd_status == 6) ? "<p class='fw-bold text-success'>$list->status_name</p>" : "<p class='fw-bold text-danger'>$list->status_name</p>";
                $data[] = $row;
            }

            $output = [
                'draw' => $this->request->getPost('draw'),
                'recordsTotal' => $this->models->countAll('custom', 'withdrawalFinish'),
                'recordsFiltered' => $this->models->countFiltered(),
                'data' => $data
            ];

            echo json_encode($output);
        }
    }

    public function printWithdrawal($wdNumber) {
        $data = [
            'withdrawal'  => $this->models->getDataForPrintWithdrawal($wdNumber),
        ];

        $mpdf = new \Mpdf\Mpdf();
        $html = view('/layout/warehouse/printWithdrawal', $data);

        $mpdf->WriteHTML($html);
		$this->response->setHeader('Content-Type', 'application/pdf');
		$mpdf->Output('test.pdf','I'); // opens in browser
    }
}
