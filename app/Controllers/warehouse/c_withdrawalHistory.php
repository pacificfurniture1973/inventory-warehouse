<?php

namespace App\Controllers\warehouse;
use App\Controllers\BaseController;
use Config\Services;
use App\Models\warehouse\models;
// use App\Models\models;

class c_withdrawalHistory extends BaseController
{
    protected $table = 'withdrawal';
    protected $database = 'warehouse';
    protected $column_order = ['wd_id', 'wd_number', 'wd_product_code', null, null, 'wd_jo_number', null, null, 'wd_status'];  
    protected $column_search = ['wd_number', 'wd_product_code', 'wd_jo_number', 'wd_remark', 'wd_remark_warehouse', 'wd_issue_date', 'wd_date', 'wd_status'];
    protected $order = ['wd_id' => 'DESC'];

    public function __construct() {
        $this->session = session();
        $this->request = Services::request();
        $this->models = new models($this->request, $this->table, $this->column_order, $this->column_search, $this->order, $this->database, 'withdrawalHistory');
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index()
    {  

        if(!$this->session->get('login')) {
            return redirect()->to('/');
        }
        
        $data = [
            'validation'     => \Config\Services::validation(),
            // add new main product
            'multipleUom'   => $this->models->getAllDataMultipleUom(),
            'uomSchema'     => $this->models->getAllDataUomSchema(),
            'currency'      => $this->models->getAllDataCurrency(),
            'status'        => $this->models->getAllDataStatus(),
            // add new user management
            'dept'          => $this->models->getAllDataDept(),
            'level'         => $this->models->getAllDataLevel(),
            // add new assembly
            'whs'           => $this->models->getAllDataWhs(),
        ];

        return view('warehouse/v_withdrawalHistory.php', $data);
    }

    public function ajaxList() {
        if ($this->request->getMethod(true) === 'POST') {

            $fromDate = $this->request->getVar('fromDate');
            $toDate = $this->request->getVar('toDate');

            $lists = $this->models->getDatatables($fromDate, $toDate, 'withdrawalHistory');

            $data = [];
            $no = $this->request->getPost('start');

            foreach ($lists as $list) {
                $no++; 
                $row = [];
                $row[] = $no;
                $row[] = "<p class='fw-bold text-success'>$list->wd_number</p><small class='text-muted'>$list->wd_date</small>";
                $row[] = "<p class='fw-bold text-warning'>$list->wd_product_code</p>";
                $row[] = "<p class='fw-bold text-warning'>$list->product_name</p>";
                $row[] = "<p class='fw-bold text-dark'>Receipt: $list->wd_receipt_quantity_warehouse</p><small class='text-muted'>Request: $list->wd_request_quantity</small>";
                $row[] = "<p class='fw-bold text-primary'>$list->wd_jo_number</p>";
                $row[] = "$list->from_whs >> $list->to_whs";
                $row[] = "<p class='fw-bold'>$list->user_username</p><small class='text-muted'>$list->dept_name - Department</small>";
                $row[] = ($list->wd_status == 3 or $list->wd_status == 4 or $list->wd_status == 5 ) ? "<p class='fw-bold text-warning'>$list->status_name</p>" : "<p class='fw-bold text-success'>$list->status_name</p>";
                $data[] = $row;
            }

            $output = [
                'draw' => $this->request->getPost('draw'),
                'recordsTotal' => $this->models->countAll($this->database),
                'recordsFiltered' => $this->models->countFiltered($fromDate, $toDate, 'withdrawalHistory'),
                'data' => $data
            ];

            echo json_encode($output);
        }
    }

    public function exportWithdrawalHistory() {
        $fromDate = $this->request->getVar('fromDate');
        $toDate = $this->request->getVar('toDate');

        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $dataTemplate = $this->models->getAllWithdrawalHistoryFromTo($fromDate, $toDate);

        $sheet->setCellValue('A1', 'Withdrawal Number');
        $sheet->setCellValue('B1', 'Date');
        $sheet->setCellValue('C1', 'Created By');
        $sheet->setCellValue('D1', 'Department');
        $sheet->setCellValue('E1', 'Product Code');
        $sheet->setCellValue('F1', 'From Warehouse');
        $sheet->setCellValue('G1', 'To Warehouse');
        $sheet->setCellValue('H1', 'JO Number');
        $sheet->setCellValue('I1', 'Assembly Product');
        $sheet->setCellValue('J1', 'Request Quantity');
        $sheet->setCellValue('K1', 'Remark');
        $sheet->setCellValue('L1', 'Receipt Quantity');
        $sheet->setCellValue('M1', 'Remark Warehouse');
        $sheet->setCellValue('N1', 'Issue Date');
        $sheet->setCellValue('O1', 'Status');
        $rows = 2;


        foreach ($dataTemplate as $dt){
            $sheet->setCellValue('A' . $rows, $dt['wd_number']);
            $sheet->setCellValue('B' . $rows, date("F j, Y", strtotime($dt['wd_date'])));
            $sheet->setCellValue('C' . $rows, $dt['user_username']);
            $sheet->setCellValue('D' . $rows, $dt['dept_name']);
            $sheet->setCellValue('E' . $rows, $dt['wd_product_code']);
            $sheet->setCellValue('F' . $rows, $dt['fromWhs']);
            $sheet->setCellValue('G' . $rows, $dt['toWhs']);
            $sheet->setCellValue('H' . $rows, $dt['wd_jo_number']);
            $sheet->setCellValue('I' . $rows, $dt['wd_assembly_product']);
            $sheet->setCellValue('J' . $rows, $dt['wd_request_quantity']);
            $sheet->setCellValue('K' . $rows, $dt['wd_remark']);
            $sheet->setCellValue('L' . $rows, $dt['wd_receipt_quantity_warehouse']);
            $sheet->setCellValue('M' . $rows, $dt['wd_remark_warehouse']);
            $sheet->setCellValue('N' . $rows, $dt['wd_issue_date']);
            $sheet->setCellValue('O' . $rows, $dt['status_name']);
            $rows++;
        }

        // buat excelnya, fyi inisialisasi spreadsheet itu buat file excel kosong baru dan writer itu mengisi file kosong itu dengan data diatas
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $fileName = 'all data withdrawal from ' . $fromDate .' to ' . $toDate;
    
        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName.'.xlsx');
        header('Cache-Control: max-age=0');
    
        // auto download disini, gaperlu dibalikin ke redirect lagi
        $writer->save('php://output');
    }
}
