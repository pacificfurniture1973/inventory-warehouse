<?php

namespace App\Controllers\warehouse;
use App\Controllers\BaseController;
use Config\Services;
use App\Models\warehouse\models;
// use App\Models\models;

class c_withdrawalProcess extends BaseController
{
    protected $table = 'withdrawal';
    protected $database = 'warehouse';
    // protected $column_order = ['wd_number', 'wd_by', 'wd_product_code', null, 'wd_jo_number', null, null, null, null, null, null, 'wd_status', null];
    protected $column_order = ['wd_id', null, 'wd_number', 'wd_product_code', null, 'wd_jo_number', null, null, 'wd_status'];
    protected $column_search = ['wd_number', 'wd_product_code', 'wd_jo_number', 'wd_remark', 'wd_remark_warehouse', 'wd_issue_date', 'wd_date', 'wd_status'];
    protected $order = ['wd_id' => 'DESC'];

    public function __construct() {
        $this->session = session();
        $this->request = Services::request();
        $this->models = new models($this->request, $this->table, $this->column_order, $this->column_search, $this->order, $this->database, 'withdrawalProcess');
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index() {  

        if(!$this->session->get('login')) {
            return redirect()->to('/');
        }
        
        $data = [
            'withdrawal'    => $this->models->getAllDataWithdrawal(),
            'validation'     => \Config\Services::validation(),
            // add new main product
            'multipleUom'   => $this->models->getAllDataMultipleUom(),
            'uomSchema'     => $this->models->getAllDataUomSchema(),
            'currency'      => $this->models->getAllDataCurrency(),
            'status'        => $this->models->getAllDataStatus(),
            // add new user management
            'dept'          => $this->models->getAllDataDept(),
            'level'         => $this->models->getAllDataLevel(),
            // add new assembly
            'whs'           => $this->models->getAllDataWhs(),
        ];  

        return view('warehouse/v_withdrawalProcess.php', $data);
    }

    public function ajaxList() {
        if ($this->request->getMethod(true) === 'POST') {
            $lists = $this->models->getDatatables();
            $data = [];
            $no = $this->request->getPost('start');

            foreach ($lists as $list) {
                $no++; 
                $status = $list->wd_status == 3 || $list->wd_status == 4 || $list->wd_status == 5 ? 'text-warning' : 'text-success';
                // $statusOverbudget = $list->statusOverbudget == 'Overbudget' ? 'text-danger' : 'text-warning';
                $row = [];
                $row[] = $no;
                if(($this->session->get('level') <= 4 && $this->session->get('dept') == 10) || $this->session->get('level') <= 3) {
                    if($this->session->get('level') == 4 && $this->session->get('dept') == 10) {
                        $row[] = "
                        <a class='fas fa-external-link-alt text-primary modalWithdrawalProcess ".($list->wd_status != 6 || $list->wd_status != 7 ? '' : 'text-muted')."'
                            data-id='$list->wd_id' data-wd_number='$list->wd_number' data-p_code='$list->wd_product_code' data-jo_number='$list->wd_jo_number' data-bom_qty='$list->bom_quantity_based_jo' data-req_qty='$list->wd_request_quantity'
                            data-wd_from_wh='$list->wd_from_whs_code' data-wd_to_wh='$list->wd_to_whs_code' data-wd_product_name='$list->product_name' data-wd_stock_available='$list->stock_quantity' data-wd_remark='$list->wd_remark'
                            onclick='modalWithdrawalProcess()' style='margin-right: 5px; cursor: pointer; ".($list->wd_status != 6 || $list->wd_status != 7 ? '' : 'pointer-events: none;')."' data-bs-toggle='modal' data-bs-target='#modalWithdrawalProcess'></a>";
                    } elseif($this->session->get('level') == 3 && $this->session->get('dept') != 10) {
                        $row[] = "
                        <a class='fas fa-external-link-alt text-primary modalWithdrawalProcess ".($list->wd_status != 6 || $list->wd_status != 7 ? '' : 'text-muted')."'
                            data-id='$list->wd_id' data-wd_number='$list->wd_number' data-p_code='$list->wd_product_code' data-jo_number='$list->wd_jo_number' data-bom_qty='$list->bom_quantity_based_jo' data-req_qty='$list->wd_request_quantity'
                            data-wd_from_wh='$list->wd_from_whs_code' data-wd_to_wh='$list->wd_to_whs_code' data-wd_product_name='$list->product_name' data-wd_stock_available='$list->stock_quantity' data-wd_remark='$list->wd_remark' data-wd_status='$list->wd_status'
                            onclick='modalWithdrawalProcess()' style='margin-right: 5px; cursor: pointer; ".($list->wd_status != 6 || $list->wd_status != 7 ? '' : 'pointer-events: none;')."' data-bs-toggle='modal' data-bs-target='#modalWithdrawalProcess'></a>
                        <a href='#' data-bs-toggle='modal' data-bs-target='#modalApproveWithdrawal' id='approveRejectWithdrawal' data-info='Approve' data-status='$list->wd_status' data-id='$list->wd_id' data-p_code='$list->wd_product_code' data-wd_from_wh='$list->wd_from_whs_code' data-wd_to_wh='$list->wd_to_whs_code' class='far fa-check-square text-decoration-none text-success mx-2 ".($list->wd_status == 3 ? '' : 'text-muted')."' style='margin-right: 5px; cursor: pointer; ".($list->wd_status == 3 ? '' : 'pointer-events: none;')."'></a>
                        <a href='#' data-bs-toggle='modal' data-bs-target='#modalApproveWithdrawal' id='approveRejectWithdrawal' data-info='Reject' data-status='$list->wd_status' data-id='$list->wd_id' data-p_code='$list->wd_product_code' data-wd_from_wh='$list->wd_from_whs_code' data-wd_to_wh='$list->wd_to_whs_code' class='fas fa-times text-decoration-none text-danger ".($list->wd_status == 3 ? '' : 'text-muted')."' style='margin-right: 5px; cursor: pointer; ".($list->wd_status == 3 ? '' : 'pointer-events: none;')."'></a>";            
                    } elseif($this->session->get('level') == 3 && $this->session->get('dept') == 10) {
                        if($list->wd_status == 4) {
                            $row[] = "
                            <a class='fas fa-external-link-alt text-primary modalWithdrawalProcess ".($list->wd_status != 6 || $list->wd_status != 7 ? '' : 'text-muted')."'
                                data-id='$list->wd_id' data-wd_number='$list->wd_number' data-p_code='$list->wd_product_code' data-jo_number='$list->wd_jo_number' data-bom_qty='$list->bom_quantity_based_jo' data-req_qty='$list->wd_request_quantity'
                                data-wd_from_wh='$list->wd_from_whs_code' data-wd_to_wh='$list->wd_to_whs_code' data-wd_product_name='$list->product_name' data-wd_stock_available='$list->stock_quantity' data-wd_remark='$list->wd_remark' data-wd_status='$list->wd_status'
                                data-receipt='$list->wd_receipt_quantity_warehouse' data-remark_wh='$list->wd_remark_warehouse'
                                onclick='modalWithdrawalProcess()' style='margin-right: 5px; cursor: pointer; ".($list->wd_status != 6 || $list->wd_status != 7 ? '' : 'pointer-events: none;')."' data-bs-toggle='modal' data-bs-target='#modalWithdrawalProcess'></a>
                            <a href='#' data-bs-toggle='modal' data-bs-target='#modalApproveWithdrawal' id='approveRejectWithdrawal' data-info='Approve' data-status='$list->wd_status' data-id='$list->wd_id' data-p_code='$list->wd_product_code' data-wd_from_wh='$list->wd_from_whs_code' data-wd_to_wh='$list->wd_to_whs_code' data-receipt='$list->wd_receipt_quantity_warehouse' data-remark_wh='$list->wd_remark_warehouse' class='far fa-check-square text-decoration-none text-warning mx-2 ".($list->wd_status == 3 || $list->wd_status == 5 ? '' : 'text-muted')."' style='margin-right: 5px; cursor: pointer; ".($list->wd_status == 3 || $list->wd_status == 5 ? '' : 'pointer-events: none;')."'></a>
                            <a href='#' data-bs-toggle='modal' data-bs-target='#modalApproveWithdrawal' id='approveRejectWithdrawal' data-info='Reject' data-status='$list->wd_status' data-id='$list->wd_id' data-p_code='$list->wd_product_code' data-wd_from_wh='$list->wd_from_whs_code' data-wd_to_wh='$list->wd_to_whs_code' data-receipt='$list->wd_receipt_quantity_warehouse' data-remark_wh='$list->wd_remark_warehouse' class='fas fa-times text-decoration-none text-danger ".($list->wd_status == 3 || $list->wd_status == 5 ? '' : 'text-muted')."' style='margin-right: 5px; cursor: pointer; ".($list->wd_status == 3 || $list->wd_status == 5 ? '' : 'pointer-events: none;')."' ></a>";
                        } else {
                            $row[] = "
                            <a class='fas fa-external-link-alt text-primary modalWithdrawalProcessHeadWh ".($list->wd_status != 6 || $list->wd_status != 7 ? '' : 'text-muted')."'
                                data-id='$list->wd_id' data-wd_number='$list->wd_number' data-p_code='$list->wd_product_code' data-jo_number='$list->wd_jo_number' data-bom_qty='$list->bom_quantity_based_jo' data-req_qty='$list->wd_request_quantity'
                                data-wd_from_wh='$list->wd_from_whs_code' data-wd_to_wh='$list->wd_to_whs_code' data-wd_product_name='$list->product_name' data-wd_stock_available='$list->stock_quantity' data-wd_remark='$list->wd_remark' data-wd_status='$list->wd_status'
                                data-receipt='$list->wd_receipt_quantity_warehouse' data-remark_wh='$list->wd_remark_warehouse'
                                onclick='modalWithdrawalProcessHeadWh()' style='margin-right: 5px; cursor: pointer; ".($list->wd_status != 6 || $list->wd_status != 7 ? '' : 'pointer-events: none;')."' data-bs-toggle='modal' data-bs-target='#modalWithdrawalProcessHeadWh'></a>
                            <a href='#' data-bs-toggle='modal' data-bs-target='#modalApproveWithdrawal' id='approveRejectWithdrawal' data-info='Approve' data-status='$list->wd_status' data-id='$list->wd_id' data-p_code='$list->wd_product_code' data-wd_from_wh='$list->wd_from_whs_code' data-wd_to_wh='$list->wd_to_whs_code' data-receipt='$list->wd_receipt_quantity_warehouse' data-remark_wh='$list->wd_remark_warehouse' class='far fa-check-square text-decoration-none text-warning mx-2 ".($list->wd_status == 3 || $list->wd_status == 5 ? '' : 'text-muted')."' style='margin-right: 5px; cursor: pointer; ".($list->wd_status == 3 || $list->wd_status == 5 ? '' : 'pointer-events: none;')."'></a>
                            <a href='#' data-bs-toggle='modal' data-bs-target='#modalApproveWithdrawal' id='approveRejectWithdrawal' data-info='Reject' data-status='$list->wd_status' data-id='$list->wd_id' data-p_code='$list->wd_product_code' data-wd_from_wh='$list->wd_from_whs_code' data-wd_to_wh='$list->wd_to_whs_code' data-receipt='$list->wd_receipt_quantity_warehouse' data-remark_wh='$list->wd_remark_warehouse' class='fas fa-times text-decoration-none text-danger ".($list->wd_status == 3 || $list->wd_status == 5 ? '' : 'text-muted')."' style='margin-right: 5px; cursor: pointer; ".($list->wd_status == 3 || $list->wd_status == 5 ? '' : 'pointer-events: none;')."' ></a>";
                        }
                    } else {
                        $row[] = "
                        <a class='fas fa-external-link-alt text-primary modalWithdrawalProcess ".($list->wd_status != 6 || $list->wd_status != 7 ? '' : 'text-muted')."'
                            data-id='$list->wd_id' data-wd_number='$list->wd_number' data-p_code='$list->wd_product_code' data-jo_number='$list->wd_jo_number' data-bom_qty='$list->bom_quantity_based_jo' data-req_qty='$list->wd_request_quantity'
                            data-wd_from_wh='$list->wd_from_whs_code' data-wd_to_wh='$list->wd_to_whs_code' data-wd_product_name='$list->product_name' data-wd_stock_available='$list->stock_quantity' data-wd_remark='$list->wd_remark'
                            onclick='modalWithdrawalProcess()' style='margin-right: 5px; cursor: pointer; ".($list->wd_status != 6 || $list->wd_status != 7 ? '' : 'pointer-events: none;')."' data-bs-toggle='modal' data-bs-target='#modalWithdrawalProcess'></a>
                        <a href='#' data-bs-toggle='modal' data-bs-target='#modalApproveWithdrawal' id='approveRejectWithdrawal' data-info='Approve' data-status='$list->wd_status' data-id='$list->wd_id' data-p_code='$list->wd_product_code' data-wd_from_wh='$list->wd_from_whs_code' data-wd_to_wh='$list->wd_to_whs_code' class='far fa-check-square text-decoration-none text-success mx-2 ".($list->wd_status == 3 ? '' : 'text-muted')."' style='cursor: pointer; ".($list->wd_status == 3 ? '' : 'pointer-events: none;')."'></a>
                        <a href='#' data-bs-toggle='modal' data-bs-target='#modalApproveWithdrawal' id='approveRejectWithdrawal' data-info='Reject' data-status='$list->wd_status' data-id='$list->wd_id' data-p_code='$list->wd_product_code' data-wd_from_wh='$list->wd_from_whs_code' data-wd_to_wh='$list->wd_to_whs_code' class='fas fa-times text-decoration-none text-danger ".($list->wd_status == 3 ? '' : 'text-muted')."' style='cursor: pointer; ".($list->wd_status == 3 ? '' : 'pointer-events: none;')."'></a>";    
                    }
                };
                $row[] = "<p class='fw-bold text-success'>$list->wd_number</p><small class='text-muted'>$list->wd_date</small>";
                $row[] = "<p class='fw-bold text-warning'>$list->wd_product_code</p>";
                $row[] = "<p class='fw-bold text-warning'>$list->product_name</p>";
                $row[] = "<p class='fw-bold text-primary'>$list->wd_jo_number</p>";
                $row[] = "$list->from_whs >> $list->to_whs";
                $row[] = "<p class='fw-bold'>$list->user_username</p><small class='text-muted'>$list->dept_name - Department</small>";
                $row[] = "<p class='fw-bold $status'>$list->status_name</p>";
                // $row[] = "<p class='fw-bold $statusOverbudget'>$list->statusOverbudget</p><small class='$status'>$list->status_name</small>";
                $data[] = $row;
            }

            $output = [
                'draw' => $this->request->getPost('draw'),
                'recordsTotal' => $this->models->countAll('custom', 'withdrawalProcess'),
                'recordsFiltered' => $this->models->countFiltered(),
                'data' => $data
            ];

            echo json_encode($output);
        }
    }

    public function processWithdrawal() {
        $info   = $this->request->getVar('infoWd');
        $status = $this->request->getVar('statusWd');
        $wdId   = $this->request->getVar('withdrawalId');
        $productCode = $this->request->getVar('productCodeWithdrawal');
        $fromWhs  = $this->request->getVar('fromWarehouseWithdrawal');
        $toWhs  = $this->request->getVar('toWarehouseWithdrawal');
        $changeStatus = 6;
        $receiptQty = floatval($this->request->getVar('receiptQty'));
        $remarkWh = $this->request->getVar('remarkWh');
        $issueDate = date("F j, Y, g:i a");
        
        // dd($info, $status, $wdId, $productCode, $fromWhs, $toWhs, $changeStatus, $receiptQty, $remarkWh, $issueDate);

        if($info == 'Approve') {
            if($status == 3) {
                // wd id, status, wd_approve_head 
                $this->models->processWithdrawal($wdId, 4, 1, 0);

                $this->session->setFlashdata('success_message', '<strong>Successfully !!</strong> Approve withdrawal step 1 !, thanks...');
                return redirect()->to('warehouse/c_withdrawalProcess');
            } elseif($status == 5) {

                // pengurangan qty di stock warehouse
                // check apakah ada datanya di stock warehouse
                if($this->models->getOldDataForWithdrawal($productCode, $fromWhs) != null) {
                    $getOldDataStock = $this->models->getOldDataForWithdrawal($productCode, $fromWhs);
                    // $getOldDataWithdrawal = $this->models->getDataForWithdrawal($wdId);
                    
                    $newStock = $getOldDataStock['stock_quantity'] - $receiptQty;
                    
                    $this->models->updateStockWithdrawal($productCode, $fromWhs, $newStock);

                    if($this->models->getOldDataForWithdrawal($productCode, $toWhs) != null) {
                        // update stock tujuan nya
                        $stockAwal = $this->models->getOldDataForWithdrawal($productCode, $toWhs);
                        $this->models->updateStockWithdrawal($productCode, $toWhs, floatval($stockAwal['stock_quantity']) + $receiptQty);
                    } else {
                        // insert baru 
                        $this->models->insertNewStockWithdrawal($productCode, $toWhs, $receiptQty);
                    }

                    // update data withdrawal for status and issue date
                    $this->models->updateDataWithdrawal($changeStatus, $issueDate, $toWhs, $wdId);

                    // session 
                    $this->session->setFlashdata('success_message', '<strong>Successfully !!</strong> Your withdrawal is received !');
                    return redirect()->to('warehouse/c_withdrawalProcess');
                } else {
                    $this->session->setFlashdata('error_message', '<strong>Failed !!</strong> Data not found !');
                    return redirect()->to('warehouse/c_withdrawalProcess');
                }
            }
        } elseif($info == 'Reject') {
            if($status == 3) {
                // set status to 7, set approve head to 0 by wd_id
                $issueDate = date("F j, Y, g:i a");
                $this->models->processWithdrawal($wdId, 7, 0, 0, $issueDate);
            } elseif($status == 5) {
                $issueDate = date("F j, Y, g:i a");
                $this->models->processWithdrawal($wdId, 7, 1, 0, $issueDate);
            }

            $this->session->setFlashdata('success_message', '<strong>Successfully !!</strong> Reject withdrawal !, thanks... ');
            return redirect()->to('warehouse/c_withdrawalProcess');
        }

    }

    public function withdrawalProcess() {
        $wdId   = $this->request->getVar('wdId');
        $productCode = $this->request->getVar('wdProductCode');
        $wdNumber = $this->request->getVar('wdNumber');
        $wdJoNumber = $this->request->getVar('wdJoNumber');
        $fromWh = $this->request->getVar('wdFromWh');
        $toWh = $this->request->getVar('wdToWh');
        $receiptQty = $this->request->getVar('wdReceiptQty');
        $remarkWh = $this->request->getVar('wdRemarkWh');
        $changeStatus = 5;
        // dd($productCode, $fromWh, $toWh, $receiptQty, $remarkWh, $changeStatus);

        $getOldDataForCheckQuantity = $this->models->getOldDataForWithdrawal($productCode, $fromWh);

        // validasi stock jika melebihi jumlah stock tersedia return error
        if(!$this->validate([
            'wdReceiptQty'  => 'required',
            'wdRemarkWh'    => 'required',
        ], [
            'wdReceiptQty'  => [
                'required'  => 'Must be filled !'
            ], 
            'wdRemarkWh'    => [
                'required'  => 'Must be filled !'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $this->session->setFlashdata('error_message', '<strong>Failed !!</strong> Process Withdrawal Number '  . $wdNumber . ' Product Code ' . $productCode . '!');
            return redirect()->to('warehouse/c_withdrawalProcess')->withInput()->with('validation', $validation);
        } else {
            if($this->models->getDataSpecificWithdrawalProcess($productCode, 5) != null) {
                $validation = \Config\Services::validation();
                $this->session->setFlashdata('error_message', '<strong>Failed !!</strong> Similar product code found, You need to seek approval from (Head of WH) this product code first !');
                return redirect()->to('warehouse/c_withdrawalProcess')->withInput()->with('validation', $validation);
            }

            $getDataOverbudget = $this->models->getDataOverbudget($productCode);
            if($getDataOverbudget['status'] == 'Overbudget') {
                if(!($this->session->get('level') == 3 && $this->session->get('dept') == 10)) {
                    $validation = \Config\Services::validation();
                    $this->session->setFlashdata('error_message', '<strong>Failed !!</strong> The status of this product code is overbudget, only the Warehouse Manager can issue this product !');
                    return redirect()->to('warehouse/c_withdrawalProcess')->withInput()->with('validation', $validation);
                }
            }

            if(floatval($receiptQty) > floatval($getOldDataForCheckQuantity['stock_quantity'])) {
                $this->session->setFlashdata('error_message', '<strong>Failed !!</strong> Process Withdrawal Number '  . $wdNumber . ' Product Code ' . $productCode . '!, Error on Receipt Quantity !');
                return redirect()->to('warehouse/c_withdrawalProcess');
            } else {

                $getOldData = $this->models->getDataForWithdrawal($wdId);

                if($getOldData['wd_approve_head'] != null && $getOldData['wd_status'] == 4) {
                    $this->models->processWithdrawalIssue($wdId, $changeStatus, 1, $receiptQty, $remarkWh);

                    $this->session->setFlashdata('success_message', '<strong>Successfully !!</strong> Issue withdrawal !, thanks... ');
                    return redirect()->to('warehouse/c_withdrawalProcess');
                }
            }
        }
    }

    public function statusOverbudget() {
        // ambil data apakah overbudget atau tidak
        $productCode = $this->request->getVar('productCodeWithdrawal');
        $statusOverbudget = $this->models->getDataOverbudget($productCode);

        if($statusOverbudget['status'] == 'Overbudget') {
            $msg = [
                'overbudget_message' => '<strong class="text-warning">This Product <span class="text-danger">Overbudget</span></strong><br> Do you want to continue ?',
            ];
        } else {
            $msg = [
                'on_process_message' => '<strong class="text-warning">This Product <span class="text-danger">On Process</span></strong><br> Do you want to continue ?',
            ];
        }

        echo json_encode($msg);

    }
}
