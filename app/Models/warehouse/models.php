<?php 

// namespace App\Models;
namespace App\Models\warehouse;
use CodeIgniter\Model;
use CodeIgniter\HTTP\RequestInterface;

class models extends Model
{
    public function __construct(RequestInterface $request, $table, $column_order, $column_search, $order, $database, $dataFor=null)
    {
        // make sure main database become db, then other like database warehouse become db_warehouse
        $this->db = db_connect();
        $this->db_warehouse = \Config\Database::connect('warehouse');
        $this->db_log = \Config\Database::connect('log');
        $this->db_pde = \Config\Database::connect('pde');
        $this->session = session();

        // dynamic server side
        $this->request = $request;
        $this->column_order = $column_order;
        $this->column_search = $column_search;
        $this->order = $order;
        $this->table = $table;

        // next kita ganti parameter yg sekarang cuma tabel jadi query kyk dibawah biar dynamic yg pake join bisa lancar jaya
        if($database == 'main') {
            if($dataFor == 'mainProduct') {             
                $tabel = $this->table . ' mp';
                $this->dt = $this->db->table($tabel);   
                $this->dt->select("mp.*, ms.status_name, mc.currency_name");
                $this->dt->join('main.status ms', 'mp.product_status = ms.status_id', 'left');
                $this->dt->join('main.currency mc', 'mp.product_currency = mc.currency_id', 'left');
                // $query = $this->dt->get();
                // dd($query->getResultArray());
            } elseif($dataFor == 'mainAssembly') {
                $tabel = $this->table . ' ma';
                $this->dt = $this->db->table($tabel);   
                $this->dt->select("ma.*, ms.status_name");
                $this->dt->join('main.status ms', 'ma.assembly_status = ms.status_id', 'inner');
                // $query = $this->dt->get();
                // dd($query->getResultArray());
            } elseif ($dataFor == 'mainCustomer') {
                $tabel = $this->table . ' mc';
                $this->dt = $this->db->table($tabel);   
                $this->dt->select("mc.*, ms.status_name");
                $this->dt->join('main.status ms', 'mc.customer_status = ms.status_id', 'inner');
                // $query = $this->dt->get();
                // dd($query->getResultArray());
            } elseif ($dataFor == 'mainVendor') {
                $tabel = $this->table . ' mv';
                $this->dt = $this->db->table($tabel);   
                $this->dt->select("mv.*, ms.status_name");
                $this->dt->join('main.status ms', 'mv.vendor_status = ms.status_id', 'inner');
                // $query = $this->dt->get();
                // dd($query->getResultArray());
            } elseif($dataFor == 'mainJobOrder') {
                $tabel = $this->table . ' jo';
                $this->dt = $this->db->table($tabel);   
                $this->dt->select("jo.*, mc.currency_name, mu.user_username, mcu.customer_agency, ma.assembly_uom");
                $this->dt->join('main.currency mc', 'jo.jo_currency = mc.currency_id', 'inner');
                $this->dt->join('main.user mu', 'jo.jo_by = mu.user_id', 'inner');
                $this->dt->join('main.customer mcu', 'jo.jo_customer_code = mcu.customer_code', 'inner');
                $this->dt->join('main.assembly ma', 'jo.jo_assembly_code = ma.assembly_code', 'inner');
                // $query = $this->dt->get();
                // dd($query->getResultArray());
            } elseif($dataFor == 'mainReceivementReport') {
                $tabel = $this->table . ' rr';
                $this->dt = $this->db->table($tabel);   
                $this->dt->select("rr.*, mu.user_username, mp.product_unit_price, mp.product_uom, mc.currency_name, mv.vendor_agency");
                $this->dt->join('main.user mu', 'rr.rr_by = mu.user_id', 'inner');
                $this->dt->join('main.product mp', 'rr.rr_product_code = mp.product_code AND rr.rr_product_name = mp.product_name', 'left');
                $this->dt->join('main.currency mc', 'mp.product_currency = mc.currency_id', 'left');
                $this->dt->join('main.vendor mv', 'mv.vendor_code = rr.rr_vendor_code', 'left');
                // $query = $this->dt->get();
                // dd($query->getResultArray());
            } elseif($dataFor == 'userManagement') {
                $tabel = $this->table . ' um';
                $this->dt = $this->db->table($tabel);   
                $this->dt->select("um.*, ml.level_name, ms.status_name, md.dept_name");
                $this->dt->join('main.level ml', 'um.user_level = ml.level_id', 'inner');
                $this->dt->join('main.status ms', 'um.user_status = ms.status_id', 'inner');
                $this->dt->join('main.department md', 'um.user_dept = md.dept_id', 'inner');
                $this->dt->where('ms.status_id', '1');
                $this->dt->orWhere('ms.status_id', '2');
                // $query = $this->dt->get();
                // dd($query->getResultArray());
            } elseif($dataFor == 'userRequest') {
                $tabel = $this->table . ' ur';
                $this->dt = $this->db->table($tabel);   
                $this->dt->select("ur.*, ml.level_name, ms.status_name, md.dept_name");
                $this->dt->join('main.level ml', 'ur.user_level = ml.level_id', 'inner');
                $this->dt->join('main.status ms', 'ur.user_status = ms.status_id', 'inner');
                $this->dt->join('main.department md', 'ur.user_dept = md.dept_id', 'inner');
                if($this->session->get('level') <= 3 && $this->session->get('dept') != 1) {
                    $this->dt->where('ms.status_id', '3');
                } elseif($this->session->get('level') <= 3 && $this->session->get('dept') == 1) {
                    $this->dt->where('ms.status_id', '8');
                }
                // $query = $this->dt->get();
                // dd($query->getResultArray());
            } elseif($dataFor == 'masterPlan') {
                $tabel = $this->table . ' jo';
                $this->dt = $this->db->table($tabel);   
                $this->dt->select("jo.jo_number, jo.jo_date, jo.jo_assembly_code, bom.bom_product_code,
                jo.jo_quantity, jo.jo_balance_quantity, bom.bom_quantity_needed,
                ww.wd_receipt_quantity_warehouse AS receipt,
                (jo.jo_balance_fixed * bom.bom_quantity_needed) AS totalRequired,
                SUM(ww.wd_receipt_quantity_warehouse) AS totalWithdrawal,
                IF((jo.jo_balance_fixed * bom.bom_quantity_needed) - (SUM(ww.wd_receipt_quantity_warehouse)) < 0 , 0, (jo.jo_balance_fixed * bom.bom_quantity_needed) - (SUM(ww.wd_receipt_quantity_warehouse))) AS needWithdrawal,
                IF(SUM(ww.wd_receipt_quantity_warehouse) > (jo.jo_balance_fixed * bom.bom_quantity_needed), 'Overbudget', 'On Process') AS status,
                mp.product_unit_price, (SUM(ww.wd_receipt_quantity_warehouse) * mp.product_unit_price) AS totalPrice,
                jo.jo_amount, mp.product_name, ma.assembly_name, ws.stock_quantity");
                $this->dt->join('pde.bom bom', 'jo.jo_assembly_code = bom.bom_assembly_code', 'left');
                $this->dt->join('main.product mp', 'bom.bom_product_code = mp.product_code', 'left');
                $this->dt->join('main.assembly ma', 'jo.jo_assembly_code = ma.assembly_code', 'left');
                $this->dt->join('warehouse.stock ws', 'bom.bom_product_code = ws.product_assembly_code AND ws.whs_code = 1', 'inner');
                $this->dt->join('warehouse.withdrawal ww', 'jo.jo_number = ww.wd_jo_number AND jo.jo_assembly_code = ww.wd_assembly_product AND bom.bom_product_code = ww.wd_product_code', 'inner');
                $this->dt->where('jo_balance_quantity !=', '0');
                $this->dt->groupBy('mp.product_code, jo.jo_number');
                // $query = $this->dt->get();
                // dd($query->getResultArray());
            }

        } elseif ($database == 'warehouse') {
            if($dataFor == 'withdrawalProcess') {
                $tabel = $this->table . ' ww';
                $this->dt = $this->db_warehouse->table($tabel);   
                $this->dt->select("ww.*, mu.user_username, md.dept_name, mw1.whs_name as from_whs, mw2.whs_name as to_whs, ms.status_name, (bom.bom_quantity_needed*jo.jo_balance_fixed) as bom_quantity_based_jo, mp.product_name, ws.stock_quantity");
                // , SUM(ww.wd_receipt_quantity_warehouse) as standar, IF(SUM(ww.wd_receipt_quantity_warehouse) > jo.jo_balance_fixed * bom.bom_quantity_needed, 'Overbudget', 'On Process') AS statusOverbudget");
                $this->dt->join('main.product mp', 'ww.wd_product_code = mp.product_code', 'inner');
                $this->dt->join('warehouse.stock ws', 'ww.wd_product_code = ws.product_assembly_code AND ww.wd_from_whs_code = ws.whs_code');
                $this->dt->join('main.user mu', 'ww.wd_by = mu.user_id', 'inner');
                $this->dt->join('main.department md', 'ww.wd_dept = md.dept_id', 'inner');
                $this->dt->join('main.whs mw1', 'ww.wd_from_whs_code = mw1.whs_code', 'inner');
                $this->dt->join('main.whs mw2', 'ww.wd_to_whs_code = mw2.whs_code', 'inner');
                $this->dt->join('main.status ms', 'ww.wd_status = ms.status_id', 'inner');
                // $this->dt->join('pde.bom bom', 'ww.wd_assembly_product = bom.bom_assembly_code AND ww.wd_product_code = bom.bom_product_code', 'left');
                // // custom dibawah
                // $this->dt->join('main.job_order jo', 'ww.wd_jo_number = jo.jo_number AND ww.wd_assembly_product = jo.jo_assembly_code AND bom.bom_product_code = ww.wd_product_code', 'inner');
                // $this->dt->groupBy('mp.product_code', 'jo.jo_number');
                // $this->dt->where('mp.product_code', 'ww.wd_product_code');
                $this->dt->join('main.job_order jo', 'ww.wd_jo_number = jo.jo_number AND ww.wd_assembly_product = jo.jo_assembly_code', 'inner');
                $this->dt->join('pde.bom bom', 'ww.wd_assembly_product = bom.bom_assembly_code AND ww.wd_product_code = bom.bom_product_code', 'left');

                $dataList = explode(',', $this->session->get('deptHandle'));

                if($this->session->get('level') == 4) {
                    if($this->session->get('dept') == 10) {
                        // $this->dt->whereIn('ww.wd_dept', $dataList);
                        $this->dt->whereIn('ww.wd_status', [3,4]);
                    } else {
                        $this->dt->whereIn('ww.wd_dept', $dataList);
                        $this->dt->where('ww.wd_status !=', '6');   
                        $this->dt->where('ww.wd_status !=', '7');
                    }
                } elseif($this->session->get('level') == 3) {
                    if($this->session->get('dept') == 10) {
                        // $this->dt->whereIn('ww.wd_dept', $dataList);
                        $this->dt->whereIn('ww.wd_status', [3,4,5]);
                    } else {
                        $this->dt->whereIn('ww.wd_dept', $dataList);
                        // $this->dt->where('ww.wd_status', '3');
                        $this->dt->where('ww.wd_status !=', '6');   
                        $this->dt->where('ww.wd_status !=', '7');
                    }
                } else {
                    $this->dt->where('ww.wd_status !=', '6');
                    $this->dt->where('ww.wd_status !=', '7');
                }
                // $query = $this->dt->get();
                // dd($query->getResultArray());
            } elseif($dataFor == 'withdrawalHistory') {
                $tabel = $this->table . ' ww';
                $this->dt = $this->db_warehouse->table($tabel);   
                $this->dt->select("ww.*, mu.user_username, md.dept_name, mw1.whs_name as from_whs, mw2.whs_name as to_whs, ms.status_name, mp.product_name");
                $this->dt->join('main.product mp', 'ww.wd_product_code = mp.product_code', 'inner');
                $this->dt->join('main.user mu', 'ww.wd_by = mu.user_id', 'inner');
                $this->dt->join('main.department md', 'ww.wd_dept = md.dept_id', 'inner');
                $this->dt->join('main.whs mw1', 'ww.wd_from_whs_code = mw1.whs_code', 'inner');
                $this->dt->join('main.whs mw2', 'ww.wd_to_whs_code = mw2.whs_code', 'inner');
                $this->dt->join('main.status ms', 'ww.wd_status = ms.status_id', 'inner');
                // $query = $this->dt->get();
                // dd($query->getResultArray());
            } elseif ($dataFor == 'warehouseProduct') {
                $tabel = $this->table . ' ws';
                $this->dt = $this->db_warehouse->table($tabel);   
                $this->dt->select("ws.*, mp.*, mw.whs_name, ms.status_name, (mp.product_unit_price * stock_quantity) as totalPrice");
                $this->dt->join('main.product mp', 'ws.product_assembly_code = mp.product_code', 'inner');
                $this->dt->join('main.status ms', 'mp.product_status = ms.status_id', 'inner');
                $this->dt->join('main.whs mw', 'ws.whs_code = mw.whs_code', 'inner');
                // $query = $this->dt->get();
                // dd($query->getResultArray());
            } elseif($dataFor == 'withdrawalFinish') {
                $tabel = $this->table . ' ww';
                $this->dt = $this->db_warehouse->table($tabel);   
                $this->dt->select("ww.*, mu.user_username, md.dept_name, mw1.whs_name as from_whs, mw2.whs_name as to_whs, ms.status_name");
                $this->dt->join('main.product mp', 'ww.wd_product_code = mp.product_code', 'inner');
                $this->dt->join('main.user mu', 'ww.wd_by = mu.user_id', 'inner');
                $this->dt->join('main.department md', 'ww.wd_dept = md.dept_id', 'inner');
                $this->dt->join('main.whs mw1', 'ww.wd_from_whs_code = mw1.whs_code', 'inner');
                $this->dt->join('main.whs mw2', 'ww.wd_to_whs_code = mw2.whs_code', 'inner');
                $this->dt->join('main.status ms', 'ww.wd_status = ms.status_id', 'inner');
                $this->dt->whereIn('ww.wd_status' , [6,7]);
                $this->dt->groupBy('ww.wd_number');
                // $query = $this->dt->get();
                // dd($query->getResultArray());
            }
        } elseif ($database == 'log') {
            if($dataFor == 'stockAdjustmentLog') {
                $tabel = $this->table . ' sal';
                $this->dt = $this->db_log->table($tabel);   
                $this->dt->select("sal.*, mr.reason_name, mw.whs_name");
                $this->dt->join('main.reason mr', 'sal.insert_sa_reason = mr.reason_code', 'inner');
                $this->dt->join('main.whs mw', 'sal.insert_sa_whs_code = mw.whs_code', 'inner');
                // $query = $this->dt->get();
                // dd($query->getResultArray());
            } elseif($dataFor == 'productsLog') {
                $tabel = $this->table . ' pl';
                $this->dt = $this->db_log->table($tabel);   
                $this->dt->select("pl.*, mp.*, mu.user_fullname, muom.multiple_uom_name, schema.uom_schema_name, mc.currency_name, ms.status_name");
                $this->dt->join('main.product mp', 'pl.product_code = mp.product_code', 'left');
                $this->dt->join('main.user mu', 'pl.user_id = mu.user_id', 'left');
                $this->dt->join('main.multiple_uom muom', 'mp.product_multiple_uom = muom.multiple_uom_id', 'left');
                $this->dt->join('main.uom_schema schema', 'mp.product_uom_schema = schema.uom_schema_id', 'left');
                $this->dt->join('main.currency mc', 'mp.product_currency = mc.currency_id', 'left');
                $this->dt->join('main.status ms', 'mp.product_status = ms.status_id', 'left');
                // speedup query by disabled order by
                // $this->dt->orderBy('import_product_id', 'DESC');
                // $query = $this->dt->get();
                // dd($query->getResultArray());
            } elseif($dataFor == 'assemblyLog') {
                $tabel = $this->table . ' al';
                $this->dt = $this->db_log->table($tabel);   
                $this->dt->select("al.*, ma.*, mc.currency_name, ms.status_name, mu.user_fullname");
                $this->dt->join('main.assembly ma', 'al.assembly_code = ma.assembly_code', 'left');
                $this->dt->join('main.currency mc', 'ma.assembly_currency = mc.currency_id', 'left');
                $this->dt->join('main.status ms', 'ma.assembly_status = ms.status_id', 'left');
                $this->dt->join('main.user mu', 'al.user_id = mu.user_id', 'left');
                $this->dt->orderBy('import_assembly_id', 'DESC');
                // $query = $this->dt->get();
                // dd($query->getResultArray());
            } elseif($dataFor == 'customerLog') {
                $tabel = $this->table . ' cl';
                $this->dt = $this->db_log->table($tabel);   
                $this->dt->select("cl.*, mu.user_fullname as lastUpdateBy, ms.status_name, mc.*");
                $this->dt->join('main.customer mc', 'cl.customer_code = mc.customer_code', 'left');
                $this->dt->join('main.user mu', 'cl.user_id = mu.user_id', 'left');
                $this->dt->join('main.status ms', 'mc.customer_status = ms.status_id', 'left');
                $this->dt->orderBy('import_customer_id', 'DESC');
                // $query = $this->dt->get();
                // dd($query->getResultArray());
            } elseif($dataFor == 'vendorLog') {
                $tabel = $this->table . ' vl';
                $this->dt = $this->db_log->table($tabel);   
                $this->dt->select("vl.*, mu.user_fullname, mv.*, ms.status_name");
                $this->dt->join('main.user mu', 'vl.user_id = mu.user_id', 'left');
                $this->dt->join('main.vendor mv', 'vl.vendor_code = mv.vendor_code', 'left');
                $this->dt->join('main.status ms', 'mv.vendor_status = ms.status_id', 'left');
                $this->dt->orderBy('import_vendor_id', 'DESC');
                // $query = $this->dt->get();
                // dd($query->getResultArray());
            } elseif($dataFor == 'joLog') {
                $tabel = $this->table . ' jl';
                $this->dt = $this->db_log->table($tabel);   
                $this->dt->select("jl.*, mu.user_fullname, jo.*, mc.customer_agency, currency.currency_name");
                $this->dt->join('main.user mu', 'jl.user_id = mu.user_id', 'left');
                $this->dt->join('main.job_order jo', 'jl.jo_number = jo.jo_number AND jl.jo_assembly_code = jo.jo_assembly_code', 'left');
                $this->dt->join('main.customer mc', 'jo.jo_customer_code = mc.customer_code', 'left');
                $this->dt->join('main.currency currency', 'jo.jo_currency = currency.currency_id', 'left');
                $this->dt->orderBy('jl.jo_id', 'DESC');
                // $query = $this->dt->get();
                // dd($query->getResultArray());
            } elseif($dataFor == 'rrLog') {
                $tabel = $this->table . ' rl';
                $this->dt = $this->db_log->table($tabel);   
                $this->dt->select("rl.*, mu.user_fullname, rr.*, mv.vendor_name");
                $this->dt->join('main.user mu', 'rl.user_id = mu.user_id', 'left');
                $this->dt->join('main.vendor mv', 'rl.rr_vendor_code = mv.vendor_code', 'left');
                $this->dt->join('main.receivement_report rr', 'rl.rr_number = rr.rr_number AND rl.rr_product_code = rr.rr_product_code', 'left');
                $this->dt->orderBy('rl.rr_id', 'DESC');
                // $query = $this->dt->get();
                // dd($query->getResultArray());
            } elseif($dataFor == 'withdrawalLog') {
                $tabel = $this->table . ' wl';
                $this->dt = $this->db_log->table($tabel);   
                $this->dt->select("wl.*, mu.user_fullname, whs1.whs_name as fromWhs, whs2.whs_name as toWhs, md.dept_name, ww.*, ms.status_name, mp.product_name");
                $this->dt->join('main.user mu', 'wl.insert_wd_by = mu.user_id', 'left');
                $this->dt->join('main.whs whs1', 'wl.insert_wd_from_whs = whs1.whs_code', 'left');
                $this->dt->join('main.whs whs2', 'wl.insert_wd_to_whs = whs2.whs_code', 'left');
                $this->dt->join('main.department md', 'wl.insert_wd_dept = md.dept_id', 'left');
                $this->dt->join('main.status ms', 'wl.insert_wd_dept = ms.status_id', 'left');
                $this->dt->join('main.product mp', 'wl.insert_wd_product_code = mp.product_code', 'left');
                $this->dt->join('warehouse.withdrawal ww', 'wl.insert_wd_product_code = ww.wd_product_code AND wl.insert_wd_jo_number = ww.wd_jo_number', 'left');
                $this->dt->orderBy('wl.insert_wd_id', 'DESC');
                // $query = $this->dt->get();
                // dd($query->getResultArray());
            } elseif($dataFor == 'bomLog') {
                $tabel = $this->table . ' bl';
                $this->dt = $this->db_log->table($tabel);   
                $this->dt->select("bl.*, mu1.user_fullname as lastUpdateBy, mu2.user_fullname as createBy, bom.*, mp.product_name");
                $this->dt->join('pde.bom bom', 'bl.import_assembly_code = bom.bom_assembly_code AND bl.import_product_code = bom.bom_product_code', 'left');
                $this->dt->join('main.user mu1', 'bl.user_id = mu1.user_id', 'left');
                $this->dt->join('main.user mu2', 'bom.bom_by = mu2.user_id', 'left');
                $this->dt->join('main.product mp', 'bl.import_product_code = mp.product_code', 'left');
                $this->dt->orderBy('bl.import_bom_id', 'DESC');
                // $query = $this->dt->get();
                // dd($query->getResultArray());
            }
        } elseif($database == 'pde') {
            if($dataFor == 'bom') {
                $tabel = $this->table . ' bom';
                $this->dt = $this->db_pde->table($tabel);   
                $this->dt->select("bom.*, mu.user_username, md.dept_name, mp.product_name, mp.product_uom, mp.product_unit_price, log.import_bom_create as lastEdit");
                $this->dt->join('main.product mp', 'bom.bom_product_code = mp.product_code', 'inner');
                $this->dt->join('main.user mu', 'bom.bom_by = mu.user_id', 'inner');
                $this->dt->join('main.department md', 'mu.user_dept = md.dept_id', 'inner');
                $this->dt->join('log.bom_log log', 'bom.bom_assembly_code = log.import_assembly_code AND bom.bom_product_code = log.import_product_code AND bom.bom_code = log.bom_code', 'left');
                $this->dt->orderBy('bom_assembly_code ASC, bom_id DESC');;
                // $query = $this->dt->get();
                // dd($query->getResultArray());
            } elseif($dataFor == 'processAssembly') {
                $tabel = $this->table . ' ba';
                $this->dt = $this->db_pde->table($tabel);   
                $this->dt->select("ba.*, ms.status_name, jo.jo_balance_quantity, mu1.user_username, mu2.user_username as approveBy, ws.stock_quantity, bom.bom_quantity_needed");
                $this->dt->join('main.status ms', 'ba.ba_status = ms.status_id', 'inner');
                $this->dt->join('main.job_order jo', 'ba.ba_jo_number = jo.jo_number', 'inner');
                $this->dt->join('main.user mu1', 'ba.ba_user_id = mu1.user_id', 'inner');
                $this->dt->join('main.user mu2', 'ba.ba_approve_by = mu2.user_id', 'left');
                $this->dt->join('warehouse.stock ws', 'ba.ba_product_code = ws.product_assembly_code AND ws.whs_code = 2', 'inner');
                $this->dt->join('pde.bom bom', 'ba.ba_assembly_code = bom.bom_assembly_code AND ba.ba_product_code = bom.bom_product_code', 'inner');
                $this->dt->where('ba.ba_status' , '9');
                // $query = $this->dt->get();
                // dd($query->getResultArray());
            } elseif($dataFor == 'finishAssembly') {
                $tabel = $this->table . ' ba';
                $this->dt = $this->db_pde->table($tabel);   
                $this->dt->select("ba.*, ms.status_name, jo.jo_balance_quantity, mu1.user_username, mu2.user_username as approveBy, ws.stock_quantity, bom.bom_quantity_needed");
                $this->dt->join('main.status ms', 'ba.ba_status = ms.status_id', 'inner');
                $this->dt->join('main.job_order jo', 'ba.ba_jo_number = jo.jo_number', 'inner');
                $this->dt->join('main.user mu1', 'ba.ba_user_id = mu1.user_id', 'inner');
                $this->dt->join('main.user mu2', 'ba.ba_approve_by = mu2.user_id', 'left');
                $this->dt->join('warehouse.stock ws', 'ba.ba_product_code = ws.product_assembly_code AND ws.whs_code = 2', 'inner');
                $this->dt->join('pde.bom bom', 'ba.ba_assembly_code = bom.bom_assembly_code AND ba.ba_product_code = bom.bom_product_code', 'inner');
                $this->dt->whereIn('ba.ba_status' , [7,10]);
                // $query = $this->dt->get();
                // dd($query->getResultArray());
            } elseif($dataFor == 'overbudget') {
                $tabel = $this->table . ' build';
                $this->dt = $this->db_pde->table($tabel);   
                $this->dt->select("build.ba_process_quantity, bom.bom_product_code, bom.bom_quantity_needed, jo.jo_assembly_code, jo.jo_number, jo.jo_quantity, jo.jo_balance_quantity, ROUND(bom.bom_quantity_needed * jo.jo_quantity) as totalRequired, ww.wd_number, ww.wd_receipt_quantity_warehouse, SUM(ww.wd_receipt_quantity_warehouse) as totalWithdrawal");
                $this->dt->join('main.job_order jo', 'build.ba_assembly_code = jo.jo_assembly_code AND build.ba_jo_number = jo.jo_number', 'inner');
                $this->dt->join('pde.bom bom', 'build.ba_assembly_code = bom.bom_assembly_code AND build.ba_product_code = bom.bom_product_code', 'inner');
                $this->dt->join('warehouse.withdrawal ww', 'build.ba_jo_number = ww.wd_jo_number AND build.ba_assembly_code = ww.wd_assembly_product AND build.ba_product_code = ww.wd_product_code', 'left');
                $this->dt->groupBy("bom.bom_product_code");
                // $query = $this->dt->get();
                // dd($query->getResultArray());
            }
        }
    }
   
    public function getDatatables($filter1 = null, $filter2 = null, $for = null)
    {
        $this->getDatatablesQuery($filter1, $filter2, $for);

        if ($this->request->getPost('length') != -1)
            $this->dt->limit($this->request->getPost('length'), $this->request->getPost('start'));                       
        $query = $this->dt->get();

        return $query->getResult();
    }

    private function getDatatablesQuery($filter1 = null, $filter2 = null, $for = null)
    {        
        
        if($filter2 != null AND $filter1 != null) {
            if($for == 'withdrawalHistory') {
                $this->dt->where("wd_date BETWEEN '$filter1' AND '$filter2'");
            } elseif($for == 'rr') {
                $this->dt->where("rr_date BETWEEN '$filter1' AND '$filter2'");
            } elseif($for == 'jo') {
                $this->dt->where("jo_date BETWEEN '$filter1' AND '$filter2'");
            } elseif($for == 'adjustmentLog') {
                $this->dt->where("insert_sa_date BETWEEN '$filter1' AND '$filter2'");
            }
        }

        // var_dump($this->column_search);

        $i = 0;
        foreach ($this->column_search as $item) {
            if ($this->request->getPost('search')['value']) {
                if ($i === 0) {
                    $this->dt->groupStart();
                    $this->dt->like($item, $this->request->getPost('search')['value']);
                } else {
                    $this->dt->orLike($item, $this->request->getPost('search')['value']);
                }
                if (count($this->column_search) - 1 == $i)
                    $this->dt->groupEnd();
            }
            $i++;
        }

        // urutan
        if ($this->request->getPost('order')) {
         
            $this->dt->orderBy($this->column_order[$this->request->getPost('order')['0']['column']], $this->request->getPost('order')['0']['dir']);
        } else if (!isset($this->order)) {
            $order = $this->order;
            $this->dt->orderBy(key($order), $order[key($order)]);
        }
    }

    public function countFiltered($filter1 = null, $filter2 = null, $for = null)
    {
        $this->getDatatablesQuery($filter1, $filter2, $for);
        return $this->dt->countAllResults();
    }

    public function countAll($database, $dataFor = null)
    {
        if($database == 'main') {
            $tbl_storage = $this->db->table($this->table);
            return $tbl_storage->countAllResults();
        } elseif ($database == 'warehouse') {
            $tbl_storage = $this->db_warehouse->table($this->table);
            return $tbl_storage->countAllResults();
        } elseif($database == 'log') {
            $tbl_storage = $this->db_log->table($this->table);
            return $tbl_storage->countAllResults();
        } elseif($database == 'pde') {
            $tbl_storage = $this->db_pde->table($this->table);
            return $tbl_storage->countAllResults();
        } elseif($database == 'custom') {
            if($dataFor == 'masterPlan') {
                $tabel = $this->table . ' jo';
                $this->dt = $this->db->table($tabel);   
                $this->dt->select("jo.jo_number");
                $this->dt->join('pde.bom bom', 'jo.jo_assembly_code = bom.bom_assembly_code', 'left');
                $this->dt->join('main.product mp', 'bom.bom_product_code = mp.product_code', 'left');
                $this->dt->join('main.assembly ma', 'jo.jo_assembly_code = ma.assembly_code', 'left');
                $this->dt->join('warehouse.withdrawal ww', 'jo.jo_number = ww.wd_jo_number AND jo.jo_assembly_code = ww.wd_assembly_product AND bom.bom_product_code = ww.wd_product_code', 'inner');
                $this->dt->groupBy('mp.product_code, jo.jo_number');
                return $this->countAllResults();
            } elseif($dataFor == 'withdrawalProcess') {
                $tabel = $this->table . ' ww';
                $this->dt = $this->db_warehouse->table($tabel);   
                $this->dt->select("ww.*, mu.user_username, md.dept_name, mw1.whs_name as from_whs, mw2.whs_name as to_whs, ms.status_name, (bom.bom_quantity_needed*jo.jo_quantity) as bom_quantity_based_jo, mp.product_name, ws.stock_quantity");
                $this->dt->join('main.product mp', 'ww.wd_product_code = mp.product_code', 'inner');
                $this->dt->join('warehouse.stock ws', 'ww.wd_product_code = ws.product_assembly_code AND ww.wd_from_whs_code = ws.whs_code');
                $this->dt->join('main.user mu', 'ww.wd_by = mu.user_id', 'inner');
                $this->dt->join('main.department md', 'ww.wd_dept = md.dept_id', 'inner');
                $this->dt->join('main.whs mw1', 'ww.wd_from_whs_code = mw1.whs_code', 'inner');
                $this->dt->join('main.whs mw2', 'ww.wd_to_whs_code = mw2.whs_code', 'inner');
                $this->dt->join('main.status ms', 'ww.wd_status = ms.status_id', 'inner');
                $this->dt->join('main.job_order jo', 'ww.wd_jo_number = jo.jo_number AND ww.wd_assembly_product = jo.jo_assembly_code', 'left');
                $this->dt->join('pde.bom bom', 'ww.wd_assembly_product = bom.bom_assembly_code AND ww.wd_product_code = bom.bom_product_code', 'left');

                $dataList = explode(',', $this->session->get('deptHandle'));

                if($this->session->get('level') == 4) {
                    if($this->session->get('dept') == 10) {
                        // $this->dt->whereIn('ww.wd_dept', $dataList);
                        $this->dt->whereIn('ww.wd_status', [3,4]);
                    } else {
                        $this->dt->whereIn('ww.wd_dept', $dataList);
                        $this->dt->where('ww.wd_status !=', '6');   
                        $this->dt->where('ww.wd_status !=', '7');
                    }
                } elseif($this->session->get('level') == 3) {
                    if($this->session->get('dept') == 10) {
                        // $this->dt->whereIn('ww.wd_dept', $dataList);
                        $this->dt->whereIn('ww.wd_status', [3,4,5]);
                    } else {
                        $this->dt->whereIn('ww.wd_dept', $dataList);
                        // $this->dt->where('ww.wd_status', '3');
                        $this->dt->where('ww.wd_status !=', '6');   
                        $this->dt->where('ww.wd_status !=', '7');
                    }
                } else {
                    $this->dt->where('ww.wd_status !=', '6');
                    $this->dt->where('ww.wd_status !=', '7');
                }
            } elseif($dataFor == 'withdrawalFinish') {
                $tabel = $this->table . ' ww';
                $this->dt = $this->db_warehouse->table($tabel);   
                $this->dt->select("ww.*");
                $this->dt->join('main.product mp', 'ww.wd_product_code = mp.product_code', 'inner');
                $this->dt->join('main.user mu', 'ww.wd_by = mu.user_id', 'inner');
                $this->dt->join('main.department md', 'ww.wd_dept = md.dept_id', 'inner');
                $this->dt->join('main.whs mw1', 'ww.wd_from_whs_code = mw1.whs_code', 'inner');
                $this->dt->join('main.whs mw2', 'ww.wd_to_whs_code = mw2.whs_code', 'inner');
                $this->dt->join('main.status ms', 'ww.wd_status = ms.status_id', 'inner');
                $this->dt->whereIn('ww.wd_status' , [6,7]);
                $this->dt->groupBy('ww.wd_number');
            } elseif($dataFor == 'processAssembly') {
                $tabel = $this->table . ' ba';
                $this->dt = $this->db_pde->table($tabel);   
                $this->dt->select("ba.*");
                $this->dt->join('main.status ms', 'ba.ba_status = ms.status_id', 'inner');
                $this->dt->join('main.job_order jo', 'ba.ba_jo_number = jo.jo_number', 'inner');
                $this->dt->join('main.user mu1', 'ba.ba_user_id = mu1.user_id', 'inner');
                $this->dt->join('main.user mu2', 'ba.ba_approve_by = mu2.user_id', 'left');
                $this->dt->join('warehouse.stock ws', 'ba.ba_product_code = ws.product_assembly_code AND ws.whs_code = 2', 'inner');
                $this->dt->join('pde.bom bom', 'ba.ba_assembly_code = bom.bom_assembly_code AND ba.ba_product_code = bom.bom_product_code', 'inner');
                $this->dt->where('ba.ba_status' , '9');
            } elseif($dataFor == 'finishAssembly') {
                $tabel = $this->table . ' ba';
                $this->dt = $this->db_pde->table($tabel);
                $this->dt->select("ba.*");
                $this->dt->join('main.status ms', 'ba.ba_status = ms.status_id', 'inner');
                $this->dt->join('main.job_order jo', 'ba.ba_jo_number = jo.jo_number AND ba.ba_assembly_code = jo.jo_assembly_code', 'inner');
                $this->dt->join('main.user mu1', 'ba.ba_user_id = mu1.user_id', 'inner');
                $this->dt->join('main.user mu2', 'ba.ba_approve_by = mu2.user_id', 'left');
                $this->dt->join('warehouse.stock ws', 'ba.ba_product_code = ws.product_assembly_code AND ws.whs_code = 2', 'inner');
                $this->dt->join('pde.bom bom', 'ba.ba_assembly_code = bom.bom_assembly_code AND ba.ba_product_code = bom.bom_product_code', 'inner');
                $this->dt->where('ba.ba_status' , '10');
                $this->dt->orWhere('ba.ba_status' , '7');
            }
        }
    }

    public function getUser($username) {
        $query = "SELECT * FROM main.user mu 
            INNER JOIN main.level ml ON mu.user_level = ml.level_id
            INNER JOIN main.status ms ON mu.user_status = ms.status_id
            INNER JOIN main.department md ON mu.user_dept = md.dept_id
            WHERE mu.user_username = '$username' AND mu.user_status = 1";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function getUserDeptHandleByUserId($userId) {
        $query = "SELECT mu.user_dept_handle FROM main.user mu WHERE mu.user_id = '$userId'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function getUserDeptHandleName($data) {
        $query = "SELECT md.dept_name FROM main.department md WHERE dept_id = '$data'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function insertUser($username, $userLevel, $fullname, $password, $userDept, $userPosition, $userEmail, $deptHandle, $userStatus, $userTelp, $userAddress) {
        $query = "INSERT INTO user (user_username, user_fullname, user_position, user_level, user_password, user_telp, user_address, user_email, user_status, user_dept, user_dept_handle)
            VALUES ('$username', '$fullname', '$userPosition', '$userLevel', '$password', '$userTelp', '$userAddress', '$userEmail', '$userStatus', '$userDept', '$deptHandle')";
        $this->db->query($query);
    }

    public function getDataForEditUser($userId) {
        $query = "SELECT * FROM user WHERE user_id = '$userId'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function updateUser($edituserId, $editUsername, $editUserLevel, $editFullname, $editPassword, $editUserDept, $editUserPosition, $editUserEmail, $editDeptHandle, $editUserStatus, $editUserTelp, $editUserAddress) {
        $query = "UPDATE user SET
            user_username = '$editUsername', user_fullname = '$editFullname', user_position = '$editUserPosition', user_level = '$editUserLevel', user_password = '$editPassword', 
            user_telp = '$editUserTelp', user_address = '$editUserAddress', user_email = '$editUserEmail', user_status = '$editUserStatus', user_dept = '$editUserDept', user_dept_handle = '$editDeptHandle' 
            WHERE user_id = '$edituserId'";
        $this->db->query($query);
    }

    public function deleteDataUser($userId) {
        $query = "DELETE FROM user WHERE user_id = '$userId'";
        $this->db->query($query);
    }

    public function getAllDataUser() {
        $query = "SELECT mu.*, ml.level_name, ms.status_name, md.dept_name FROM main.user mu 
            INNER JOIN main.level ml ON mu.user_level = ml.level_id
            INNER JOIN main.status ms ON mu.user_status = ms.status_id
            INNER JOIN main.department md ON mu.user_dept = md.dept_id";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function registerUser($regUsername, $regFullname, $regPassword, $regPosition, $regTelp, $regDept, $regDeptHandle, $regEmail, $regLevel, $regStatus, $regAddress) {
        $query = "INSERT INTO user (user_username, user_fullname, user_position, user_level, user_password, user_telp, user_address, user_email, user_status, user_dept, user_dept_handle)
            VALUES ('$regUsername', '$regFullname', '$regPosition', '$regLevel', '$regPassword', '$regTelp', '$regAddress', '$regEmail', '$regStatus', '$regDept', '$regDeptHandle')";
        $this->db->query($query);
    }

    public function updateProcessUser($userId, $userStatus, $userLevel) {
        $query = "UPDATE user SET 
            user_status = '$userStatus', user_level = '$userLevel' WHERE user_id = '$userId'";
        $this->db->query($query);
    }

    public function getAllProducts() {
        $query = "SELECT * FROM product";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function importProducts($productCode, $productType, $productName, $productDescription, $productUom, $productMultipleUom, $productUomSchema, $productCurrency, $productStatus, $productUnitPrice, $productValuation, $productCategory, $importCode) {
        $query = "INSERT INTO product (product_code, product_type, product_name, product_description, product_uom, product_multiple_uom, product_uom_schema, product_currency, product_status, product_unit_price, product_valuation_method, product_category, product_import_code) 
            VALUES (
            ".$this->db->escape($productCode).", ".$this->db->escape($productType).", ".$this->db->escape($productName).", ".$this->db->escape($productDescription).", 
            ".$this->db->escape($productUom).", ".$this->db->escape($productMultipleUom).", ".$this->db->escape($productUomSchema).", ".$this->db->escape($productCurrency).", 
            ".$this->db->escape($productStatus).", ".$this->db->escape($productUnitPrice).", ".$this->db->escape($productValuation).", ".$this->db->escape($productCategory).", ".$this->db->escape($importCode).")";
        $this->db->query($query);
    }

    public function insertToStockWarehouse($productCode, $whsCode, $qty) {
        $query = "INSERT INTO stock (product_assembly_code, whs_code, stock_quantity) 
            VALUES ('$productCode', '$whsCode', '$qty')";
        $this->db_warehouse->query($query);
    }

    public function importProductsLog($importCode, $productCode, $filename, $userId) {
        $query = "INSERT INTO product_log (import_code, product_code, import_product_filename, user_id)
        VALUES (".$this->db->escape($importCode).", ".$this->db->escape($productCode).", ".$this->db->escape($filename).", ".$this->db->escape($userId).")";
        $this->db_log->query($query);
    }

    public function checkDuplicateProducts($productCode, $productName) {
        // dd($productCode, $productName);
        $query = "SELECT * FROM product WHERE product_code = ".$this->db->escape($productCode)." AND product_name = ".$this->db->escape($productName)."";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function checkDuplicateProductsByProductCode($productCode) {
        $query = "SELECT * FROM product WHERE product_code = ".$this->db->escape($productCode)."";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function getLastIdDataProducts() {
        $query = "SELECT * FROM product ORDER BY product_id DESC";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function getLastIdDataProductsLog() {
        $query = "SELECT * FROM product_log ORDER BY import_product_id DESC";
        $hasil = $this->db_log->query($query);
        return $hasil->getRowArray();
    }

    public function getFirstDataProducts() {
        $query = "SELECT * FROM product LIMIT 1";
        $hasil = $this->db->query($query);
        return $hasil->getresultArray();
    }

    public function getAllDataMainProducts() {
        $query = "SELECT mp.*, mmu.multiple_uom_name, mus.uom_schema_name, mc.currency_name, ms.status_name FROM main.product mp
            LEFT JOIN main.multiple_uom mmu ON mp.product_multiple_uom = mmu.multiple_uom_id
            LEFT JOIN main.uom_schema mus ON mp.product_uom_schema = mus.uom_schema_id
            INNER JOIN main.currency mc ON mp.product_currency = mc.currency_id
            INNER JOIN main.status ms ON mp.product_status = ms.status_id";
        $hasil = $this->db->query($query);
        return $hasil->getresultArray();
    }

    public function getAllDataWarehouseProducts() {
        $query = "SELECT ws.*, mp.product_name, mp.product_description, mw.whs_name FROM warehouse.stock ws 
            INNER JOIN main.product mp ON ws.product_assembly_code = mp.product_code
            INNER JOIN main.whs mw on ws.whs_code = mw.whs_code";
        $hasil = $this->db_warehouse->query($query);
        return $hasil->getresultArray();
    }

    public function insertProducts($productCode, $productType, $productName, $productDescription, $productUom, $productMultipleUom, $productUomSchema, $productCurrency, $productStatus, $productUnitPrice, $productValuation, $productCategory, $addCode) {
        $query = "INSERT INTO main.product mp (product_code, product_type, product_name, product_description, product_uom, product_multiple_uom, product_uom_schema, product_currency, product_status, product_unit_price, product_valuation_method, product_category, product_import_code) 
        VALUES ('$productCode', '$productType', '$productName', '$productDescription', '$productUom', '$productMultipleUom', '$productUomSchema', '$productCurrency', '$productStatus', '$productUnitPrice', '$productValuation', '$productCategory', '$addCode')";
        $this->db->query($query);
    }

    public function insertProductsLog($addCode, $productCode, $filename, $userId) {
        $query = "INSERT INTO log.product_log (import_code, product_code, import_product_filename, user_id) 
            VALUES ('$addCode', '$productCode', '$filename', '$userId')";
        $this->db_log->query($query);
    }

    public function updateDataProducts($productId, $productCode, $productType, $productName, $updateCode, $desc, $uom, $mUom, $uomSchema, $status, $currency, $unitPrice, $valuation, $category) {
        $query = "UPDATE product SET 
            product_code = '$productCode', product_type = '$productType', product_name = '$productName', product_description = '$desc', product_uom = '$uom', product_multiple_uom = '$mUom', product_uom_schema = '$uomSchema', product_currency = '$currency', product_status = '$status', product_unit_price = '$unitPrice', product_valuation_method = '$valuation', product_import_code = '$updateCode', product_category = '$category'
            WHERE product_id = '$productId'";
        $this->db->query($query);
    }

    public function updateDataProductsLog($updateCode, $productCode, $filename, $userId) {
        $query = "INSERT INTO product_log (import_code, product_code, import_product_filename, user_id) VALUES 
            ('$updateCode', '$productCode', '$filename', '$userId')";
        $this->db_log->query($query);
    }

    public function checkDataForDeleteProducts($productId) {
        $query = "SELECT * FROM product WHERE product_id = '$productId'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function deleteDataProducts($productId) {
        $query = "DELETE FROM product WHERE product_id = '$productId'";
        $this->db->query($query);
    }

    public function deleteDataProductsLog($deleteCode, $productCode, $filename, $userId) {
        $query = "INSERT INTO product_log (import_code, product_code, import_product_filename, user_id) VALUES ('$deleteCode', '$productCode', '$filename', '$userId')";
        $this->db_log->query($query);
    }

    public function getDataForEditProduct($id) {
        $query = "SELECT mp.*, mmu.multiple_uom_name, mus.uom_schema_name, mc.currency_name, ms.status_name FROM main.product mp
            INNER JOIN main.multiple_uom mmu ON mp.product_multiple_uom = mmu.multiple_uom_id
            INNER JOIN main.uom_schema mus ON mp.product_uom_schema = mus.uom_schema_id
            INNER JOIN main.currency mc ON mp.product_currency = mc.currency_id
            INNER JOIN main.status ms ON mp.product_status = ms.status_id WHERE mp.product_id = '$id'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function getAllDataWhs() {
        $query = "SELECT * FROM whs";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getAllDataReason() {
        $query = 'SELECT * FROM reason';
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getAllDataMultipleUom() {
        $query = 'SELECT * FROM multiple_uom';
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getAllDataUomSchema() {
        $query = 'SELECT * FROM uom_schema';
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getAllDataCurrency() {
        $query = 'SELECT * FROM currency';
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getAllDataStatus() {
        $query = 'SELECT * FROM status';
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getFirstDataStock() {
        $query = 'SELECT ws.*, mp.product_type, mp.whs_code, mw.whs_name FROM warehouse.stock ws 
            INNER JOIN main.product mp ON mp.product_code = ws.product_assembly_code 
            INNER JOIN main.whs mw ON mp.whs_code = mw.whs_code;';
        $hasil = $this->db_warehouse->query($query);
        return $hasil->getResultArray();
    }

    public function getAllDataStockAdjustment() {
        $query = "SELECT sal.*, mr.reason_name, mw.whs_name FROM stock_adjustment_log sal 
            INNER JOIN main.reason mr ON sal.insert_sa_reason = mr.reason_code
            INNER JOIN main.whs mw ON sal.insert_sa_whs_code = mw.whs_code";
        $hasil = $this->db_log->query($query);
        return $hasil->getResultArray();
    }

    public function getSpecificDataProduct($productCode, $whsCode) {
        $query = "SELECT mp.*, ws.stock_quantity FROM main.product mp 
            INNER JOIN warehouse.stock ws ON mp.product_code = ws.product_assembly_code WHERE mp.product_code = ".$this->db->escape($productCode)." AND mp.whs_code = ".$this->db->escape($whsCode).""; 
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function getOldDataForStockAdjustment($productCode, $whsCode) {
        $query = "SELECT * FROM warehouse.stock ws WHERE product_assembly_code = '$productCode' AND whs_code = '$whsCode'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }
    
    public function updateStockAdjustment($productCode, $whsCode, $newStock) {
        $query = "UPDATE stock SET 
            stock_quantity = ".$this->db->escape(floatval($newStock))." WHERE product_assembly_code = ".$this->db->escape($productCode)." AND whs_code = ".$this->db->escape($whsCode)."";
        $this->db_warehouse->query($query);
    }

    public function insertStockAdjustmentLog($saNumber, $dataDate, $dataBy, $dataReason, $dataMemo, $adjType, $productCode, $whsCode, $qty, $filename) {
        $query = "INSERT INTO log.stock_adjustment_log 
            (insert_sa_number, insert_sa_date, insert_sa_by, insert_sa_reason, insert_sa_memo, insert_sa_type, insert_sa_product_code, insert_sa_whs_code, insert_sa_quantity, insert_sa_filename)
            VALUES ('$saNumber', '$dataDate', '$dataBy', '$dataReason', '$dataMemo', '$adjType', '$productCode', '$whsCode', '$qty', '$filename')";
        $this->db_log->query($query);
    }

    public function getLastIdDataStockAdjustmentLog() {
        $query = "SELECT * FROM stock_adjustment_log ORDER BY insert_sa_id DESC";
        $hasil = $this->db_log->query($query);
        return $hasil->getRowArray();
    }

    public function getDataForPrintSaLogSingle($saNumber, $productCode) {
        $query = "SELECT sal.*, mr.reason_name, mw.whs_name, mp.product_name, mp.product_uom FROM stock_adjustment_log sal 
            INNER JOIN main.reason mr ON sal.insert_sa_reason = mr.reason_code 
            INNER JOIN main.whs mw ON sal.insert_sa_whs_code = mw.whs_code 
            INNER JOIN main.product mp ON sal.insert_sa_product_code = mp.product_code
            WHERE insert_sa_number = '$saNumber' AND insert_sa_product_code = '$productCode'";
        $hasil = $this->db_log->query($query);
        return $hasil->getResultArray();
    }

    public function getDataForPrintSaLogMultiple($saNumber) {
        $query = "SELECT sal.*, mr.reason_name, mw.whs_name, mp.product_name, mp.product_uom FROM stock_adjustment_log sal 
            INNER JOIN main.reason mr ON sal.insert_sa_reason = mr.reason_code 
            INNER JOIN main.whs mw ON sal.insert_sa_whs_code = mw.whs_code
            INNER JOIN main.product mp ON sal.insert_sa_product_code = mp.product_code
            WHERE insert_sa_number = '$saNumber'";
        $hasil = $this->db_log->query($query);
        return $hasil->getResultArray();
    }

    public function getTermStockAdjustment($data){
        $query = "SELECT mp.*, ws.stock_quantity, ws.whs_code, mw.whs_name FROM warehouse.stock ws 
            INNER JOIN main.product mp ON ws.product_assembly_code = mp.product_code
            INNER JOIN main.whs mw ON ws.whs_code = mw.whs_code WHERE mp.product_code LIKE '%$data%' LIMIT 100";
        $hasil = $this->db_warehouse->query($query);
        return $hasil->getResultArray();
    }

    public function getTermWithdrawal($data){
        $query = "SELECT mp.*, ws.stock_quantity, ws.whs_code, mw.whs_name FROM warehouse.stock ws 
            INNER JOIN main.product mp ON ws.product_assembly_code = mp.product_code 
            INNER JOIN main.whs mw ON ws.whs_code = mw.whs_code WHERE mp.product_code LIKE '%$data%' AND ws.whs_code = '1' LIMIT 100";
        $hasil = $this->db_warehouse->query($query);
        return $hasil->getResultArray();
    }

    public function getTermWithdrawalCustomer($data){
        $query = "SELECT * FROM customer WHERE customer_agency LIKE '%$data%' LIMIT 100";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getTermWithdrawalJo($dataJo, $dataCustomer, $dataProduct){
        // $query = "SELECT jo.jo_number, jo.jo_model_code, jo.jo_assembly_code, bom.bom_quantity_needed, bom.bom_quantity_needed*jo.jo_quantity as bom_quantity_based_jo FROM main.job_order jo 
        //     LEFT JOIN pde.bom bom ON jo.jo_assembly_code = bom.bom_assembly_code WHERE mc.customer_agency = '$dataCustomer' AND jo.jo_number LIKE '%$dataJo%'";
        $query = "SELECT jo.jo_number, jo.jo_model_code, jo.jo_assembly_code, bom.bom_quantity_needed, ROUND(bom.bom_quantity_needed*jo.jo_quantity,2) as bom_quantity_based_jo FROM main.job_order jo 
            LEFT JOIN pde.bom bom ON jo.jo_assembly_code = bom.bom_assembly_code AND bom.bom_product_code = '$dataProduct' 
            WHERE jo.jo_customer_code = '$dataCustomer' AND jo.jo_number LIKE '%$dataJo%' LIMIT 100";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getLastIdDataWithdrawalLog() {
        $query = "SELECT * FROM withdrawal_log ORDER BY insert_wd_id DESC";
        $hasil = $this->db_log->query($query);
        return $hasil->getRowArray();
    }

    public function getOldDataForWithdrawal($productCode, $whsCode) {
        $query = "SELECT * FROM warehouse.stock ws WHERE ws.product_assembly_code = '$productCode' AND ws.whs_code = '$whsCode'";
        $hasil = $this->db_warehouse->query($query);
        return $hasil->getRowArray();
    }

    public function updateStockWithdrawal($productCode, $fromWh, $newStock) {
        $query = "UPDATE stock SET
            stock_quantity = '$newStock' WHERE product_assembly_code = '$productCode' AND whs_code = '$fromWh'";
        $this->db_warehouse->query($query);
    }

    public function updateDataWithdrawal($changeStatus, $issueDate, $toWhs, $wdId) {
        $query = "UPDATE withdrawal SET 
            wd_issue_date = '$issueDate', wd_status = '$changeStatus'
            WHERE wd_id = '$wdId' AND wd_to_whs_code = '$toWhs'";
        $this->db_warehouse->query($query);
    }

    public function insertNewStockWithdrawal($productCode, $toWh, $receiptQty) {
        $query = "INSERT INTO stock (product_assembly_code, whs_code, stock_quantity) VALUES (
            '$productCode', '$toWh', '$receiptQty')";
        $this->db_warehouse->query($query);
    }

    public function getAllDataWithdrawal() {
        $query = "SELECT ww.*, mu.user_username, md.dept_name, mw1.whs_name as from_whs, mw2.whs_name as to_whs, ms.status_name, bom.bom_quantity_needed*jo.jo_quantity as bom_quantity_based_jo FROM warehouse.withdrawal ww
            INNER JOIN main.user mu ON ww.wd_by = mu.user_id
            INNER JOIN main.department md ON ww.wd_dept = md.dept_id
            INNER JOIN main.whs mw1 ON ww.wd_from_whs_code = mw1.whs_code
            INNER JOIN main.whs mw2 ON ww.wd_to_whs_code = mw2.whs_code
            INNER JOIN main.status ms ON ww.wd_status = ms.status_id
            INNER JOIN main.job_order jo ON ww.wd_jo_number = jo.jo_number
            INNER JOIN pde.bom bom ON jo.jo_assembly_code = bom.bom_assembly_code";
        $hasil = $this->db_warehouse->query($query);
        return $hasil->getResultArray();
    }

    public function getDataForPrintWithdrawal($wdNumber) {
        $query = "SELECT ww.*, mw1.whs_name as from_wh, mw2.whs_name as to_wh, mu.user_username as username, mp.product_name, mp.product_uom, ms.status_name, md.dept_name FROM withdrawal ww
            INNER JOIN main.whs mw1 ON ww.wd_from_whs_code = mw1.whs_code
            INNER JOIN main.whs mw2 ON ww.wd_to_whs_code = mw2.whs_code 
            INNER JOIN main.user mu ON ww.wd_by = mu.user_id
            INNER JOIN main.product mp ON ww.wd_product_code = mp.product_code 
            INNER JOIN main.status ms ON ww.wd_status = ms.status_id
            INNER JOIN main.department md ON ww.wd_dept = md.dept_id WHERE wd_number = '$wdNumber'";
        $hasil = $this->db_warehouse->query($query);
        return $hasil->getResultArray();
    }

    public function insertWitdrawalRequest($wsNumber, $wsBy, $date, $toWhs, $dept, $productCode, $whsCode, $joNumber, $assemblyCode, $reqQty, $remark, $status, $filename) {
        $query1 = "INSERT INTO warehouse.withdrawal (
                wd_number, wd_by, wd_date, wd_to_whs_code, wd_dept, wd_product_code, wd_from_whs_code, wd_jo_number, wd_assembly_product, wd_request_quantity, wd_remark, wd_status) VALUES (
                '$wsNumber', '$wsBy', '$date', '$toWhs', '$dept', '$productCode', '$whsCode', '$joNumber', '$assemblyCode', '$reqQty', '$remark', '$status')";
        $query2 = "INSERT INTO log.withdrawal_log (
                insert_wd_number, insert_wd_by, insert_wd_to_whs, insert_wd_dept, insert_wd_product_code, insert_wd_from_whs, insert_wd_jo_number, insert_wd_request_quantity, insert_wd_remark, insert_wd_filename)
                VALUES ('$wsNumber', '$wsBy', '$toWhs', '$dept', '$productCode', '$whsCode', '$joNumber', '$reqQty', '$remark', '$filename')";
        $this->db_warehouse->query($query1);
        $this->db_log->query($query2);
    }

    public function getLastIdDataCustomer() {
        $query = "SELECT * FROM customer ORDER BY customer_id DESC";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function getLastIdDataCustomerLog() {
        $query = "SELECT * FROM customer_log ORDER BY import_customer_id DESC";
        $hasil = $this->db_log->query($query);
        return $hasil->getRowArray();
    }

    public function checkDuplicateCustomer($customerCode, $customerAgency) {
        $query = "SELECT * FROM customer WHERE customer_code = ".$this->db->escape($customerCode)." OR customer_agency = ".$this->db->escape($customerAgency)."";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function importCustomer($customerCode, $customerTitle, $customerAgency, $customerAddress, $customerEmail, $customerName, $customerTelp, $customerBankName, $customerBankAccount, $customerBankNumber, $customerCreditTermDefault, $customerStatus, $importCode) {
        $query = "INSERT INTO customer (
            customer_code, customer_title, customer_agency, customer_address, customer_email, customer_name, customer_telp, customer_bank_name, 
            customer_bank_account, customer_bank_number, customer_credit_term_default, customer_status, customer_import_code) VALUES (
            ".$this->db->escape($customerCode).", ".$this->db->escape($customerTitle).", ".$this->db->escape($customerAgency).", ".$this->db->escape($customerAddress).", ".$this->db->escape($customerEmail).",
            ".$this->db->escape($customerName).", ".$this->db->escape($customerTelp).", ".$this->db->escape($customerBankName).", ".$this->db->escape($customerBankAccount).", ".$this->db->escape($customerBankNumber).",
            ".$this->db->escape($customerCreditTermDefault).", ".$this->db->escape($customerStatus).", ".$this->db->escape($importCode).")";
            $this->db->query($query);
    }

    public function insertCustomer($customerCode, $customerTitle , $customerAgency, $customerAddress, $customerEmail, $customerName, $customerTelp, 
        $customerBankName, $customerBankAccount, $customerBankNumber, $customerCreditTerm, $customerStatus, $addCode) {
        $query = "INSERT INTO customer (customer_code, customer_title, customer_agency, customer_address, customer_email, customer_name, customer_telp, customer_bank_name,
            customer_bank_account, customer_bank_number, customer_credit_term_default, customer_status, customer_import_code) VALUES (
            '$customerCode', '$customerTitle', '$customerAgency', '$customerAddress', '$customerEmail', '$customerName', '$customerTelp', '$customerBankName', '$customerBankAccount',
            '$customerBankNumber', '$customerCreditTerm', '$customerStatus', '$addCode')";
        $this->db->query($query);
    }

    public function insertCustomerLog($addCode, $customerCode, $filename, $userId) {
        $query = "INSERT INTO customer_log (import_code, customer_code, import_customer_filename, user_id) VALUES (
            '$addCode', '$customerCode', '$filename', '$userId')";
        $this->db_log->query($query);
    }

    public function updateDataCustomer($customerId, $customerCode, $customerTitle, $customerAgency, $customerAddress, $customerEmail, $customerName,
        $customerBankName, $customerBankAccount, $customerBankNumber, $customerTelp, $customerCreditTerm, $customerStatus, $updateCode) {
        $query = "UPDATE customer SET 
            customer_code = '$customerCode', customer_title = '$customerTitle', customer_agency = '$customerAgency', customer_address = '$customerAddress', customer_email = '$customerEmail',
            customer_name = '$customerName', customer_bank_name = '$customerBankName', customer_bank_account = '$customerBankAccount', customer_bank_number = '$customerBankNumber',
            customer_telp = '$customerTelp', customer_credit_term_default = '$customerCreditTerm', customer_status = '$customerStatus', customer_import_code  = '$updateCode'
            WHERE customer_id = $customerId";
        $this->db->query($query);
    }

    public function updateDataCustomerLog($updateCode, $customerCode, $filename, $userId) {
        $query = "INSERT INTO customer_log (import_code, customer_code, import_customer_filename, user_id)
            VALUES ('$updateCode', '$customerCode', '$filename', '$userId')";
        $this->db_log->query($query);
    }

    public function getDataForEditCustomer($editCustomerId) {
        $query = "SELECT * FROM customer WHERE customer_id = '$editCustomerId'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function getAllDataCustomer() {
        $query = "SELECT mc.*, ms.status_name FROM main.customer mc 
            INNER JOIN main.status ms ON mc.customer_status = ms.status_id";
        $hasil = $this->db->query($query);
        return $hasil->getresultArray();
    }

    public function importCustomerLog($importCode, $customerCode, $filename, $userId) {
        $query = "INSERT INTO customer_log (import_code, customer_code, import_customer_filename, user_id)
            VALUES ('$importCode', '$customerCode', '$filename', '$userId')";
        $this->db_log->query($query);
    }

    public function checkDataForDeleteCustomer($deleteId) {
        $query = "SELECT * FROM customer WHERE customer_id = '$deleteId'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function deleteDataCustomerLog($deleteCode, $productCode, $filename, $userId) {
        $query = "INSERT INTO customer_log (import_code, customer_code, import_customer_filename, user_id)
            VALUES ('$deleteCode', '$productCode', '$filename', '$userId')";
        $this->db_log->query($query);
    }

    public function deleteDataCustomer($deleteId) {
        $query = "DELETE FROM customer WHERE customer_id = '$deleteId'";
        $this->db->query($query);
    }

    public function getLastIdDataVendor() {
        $query = "SELECT * FROM vendor ORDER BY vendor_id DESC";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function getLastIdDataVendorLog() {
        $query = "SELECT * FROM vendor_log ORDER BY import_vendor_id DESC";
        $hasil = $this->db_log->query($query);
        return $hasil->getRowArray();
    }

    public function checkDuplicateVendor($vendorCode, $vendorAgency) {
        $query = "SELECT * FROM vendor WHERE vendor_code = ".$this->db->escape($vendorCode)." OR vendor_agency = ".$this->db->escape($vendorAgency)."";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function importVendor($vendorCode, $vendorTitle, $vendorAgency, $vendorAddress, $vendorEmail, $vendorName, $vendorTelp, $vendorBankName, 
    $vendorBankAccount, $vendorBankNumber, $vendorDebitTermDefault, $vendorStatus, $importCode) {
        $query = "INSERT INTO vendor (vendor_code, vendor_title, vendor_agency, vendor_address, vendor_email, vendor_name, vendor_telp, vendor_bank_name, vendor_bank_account,
            vendor_bank_number, vendor_debit_term_default, vendor_status, vendor_import_code) VALUES (
            ".$this->db->escape($vendorCode).", ".$this->db->escape($vendorTitle).", ".$this->db->escape($vendorAgency).", ".$this->db->escape($vendorAddress).", 
            ".$this->db->escape($vendorEmail).", ".$this->db->escape($vendorName).", ".$this->db->escape($vendorTelp).", ".$this->db->escape($vendorBankName).", 
            ".$this->db->escape($vendorBankAccount).", ".$this->db->escape($vendorBankNumber).", ".$this->escape($vendorDebitTermDefault).", 
            ".$this->db->escape($vendorStatus).", ".$this->db->escape($importCode).")";
        $this->db->query($query);
    }

    public function importVendorLog($importCode, $vendorCode, $filename, $userId) {
        $query = "INSERT INTO vendor_log (import_code, vendor_code, import_vendor_filename, user_id) VALUES
            ('$importCode', '$vendorCode', '$filename', '$userId')";
        $this->db_log->query($query);
    }

    public function insertVendor($vendorCode, $vendorTitle, $vendorAgency, $vendorAddress, $vendorEmail, $vendorName, $vendorImportCode, $vendorBankName, $vendorBankAccount,
    $vendorBankNumber, $vendorTelp, $vendorDebitTerm, $vendorStatus, $addCode) {
        $query = "INSERT INTO vendor (vendor_title, vendor_code, vendor_agency, vendor_address, vendor_email, vendor_name, vendor_telp, vendor_bank_name, vendor_bank_account, 
            vendor_bank_number, vendor_debit_term_default, vendor_status, vendor_import_code) 
            VALUES ('$vendorTitle', '$vendorCode', '$vendorAgency', '$vendorAddress', '$vendorEmail', '$vendorName', '$vendorTelp', '$vendorBankName', '$vendorBankAccount',
            '$vendorBankNumber', '$vendorDebitTerm', '$vendorStatus', '$addCode')";
        $this->db->query($query);
    }

    public function insertVendorLog($addCode, $vendorCode, $filename, $userId) {
        $query = "INSERT INTO vendor_log (import_code, vendor_code, import_vendor_filename, user_id) 
            VALUES ('$addCode', '$vendorCode', '$filename', '$userId')";
        $this->db_log->query($query);
    }

    public function getDataForEditVendor($vendorId) {
        $query = "SELECT * FROM vendor WHERE vendor_id = '$vendorId'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function updateDataVendor($editVendorId, $editVendorCode, $editVendorTitle, $editVendorAgency, $editVendorAddress, $editVendorEmail, $editVendorName,
    $editVendorBankName, $editVendorBankAccount, $editVendorBankNumber, $editVendorTelp, $editVendorDebitTerm, $editVendorStatus, $updateCode) {
        $query = "UPDATE vendor SET
            vendor_title = '$editVendorTitle', vendor_code = '$editVendorCode', vendor_agency = '$editVendorAgency', vendor_address = '$editVendorAddress', vendor_email = '$editVendorAddress',
            vendor_name = '$editVendorName', vendor_telp = '$editVendorTelp', vendor_bank_name = '$editVendorBankName', vendor_bank_account = '$editVendorBankAccount', vendor_bank_number = '$editVendorBankNumber',
            vendor_debit_term_default = '$editVendorDebitTerm', vendor_status = '$editVendorStatus', vendor_import_code = '$updateCode'
            WHERE vendor_id = '$editVendorId'";
        $this->db->query($query);
    }

    public function updateDataVendorLog($updateCode, $vendorCode, $filename, $userId) {
        $query = "INSERT INTO vendor_log (import_code, vendor_code, import_vendor_filename, user_id) 
            VALUES ('$updateCode', '$vendorCode', '$filename', '$userId')";
        $this->db_log->query($query);
    }

    public function getAllDataVendor() {
        $query = "SELECT mv.*, ms.status_name FROM main.vendor mv
            INNER JOIN main.status ms ON mv.vendor_status = ms.status_id";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function checkDataForDeleteVendor($deleteId) {
        $query = "SELECT * FROM vendor WHERE vendor_id = '$deleteId'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function deleteDataVendorLog($deleteCode, $vendorCode, $filename, $userId) {
        $query  = "INSERT INTO vendor_log (import_code, vendor_code, import_vendor_filename, user_id) 
            VALUES ('$deleteCode', '$vendorCode', '$filename', '$userId')";
        $this->db_log->query($query);
    }

    public function deleteDataVendor($deleteId) {
        $query = "DELETE FROM vendor WHERE vendor_id = '$deleteId'";
        $this->db->query($query);
    }

    public function getAllDataDefLoc() {
        $query = "SELECT * FROM main.whs mw";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getAllDataLevel() {
        $query = "SELECT * FROM level";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getAllDataDefLocExcept() {
        $query = "SELECT * FROM main.whs mw WHERE mw.whs_code != 1";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getAllDataDept() {
        $query = "SELECT * FROM department";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getAllDataDeptObject() {
        $query = "SELECT * FROM department";
        $hasil = $this->db->query($query);
        return $hasil->getResult();
    }

    public function getAllDataJobOrder() {
        $query = "SELECT jo.*, mu.user_username, mc.currency_name FROM main.job_order jo 
            INNER JOIN main.user mu ON jo.jo_by = mu.user_id
            INNER JOIN main.currency mc ON jo.jo_currency = mc.currency_id";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function checkDuplicateJobOrder($joNumber, $assemblyCode) {
        $query = "SELECT * FROM job_order WHERE jo_number = '$joNumber' AND jo_assembly_code = '$assemblyCode'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function getLastIdDataJobOrderLog() {
        $query = "SELECT * FROM job_order_log ORDER BY jo_id DESC";
        $hasil = $this->db_log->query($query);
        return $hasil->getRowArray();
    }

    public function importJobOrder($joNumber, $joDate, $joBy, $joCustomerCode, $joCustomerPo, $joDueDate, $joCurrency, $joDiscount, $joMemo, $joCreditTerm, 
    $joAssemblyCode, $joQuantity, $joBalance, $joAmount, $joModelCode, $importCode) {
        $query = "INSERT INTO job_order (jo_number, jo_date, jo_by, jo_customer_code, jo_customer_po, jo_due_date, jo_currency, jo_discount, jo_memo, jo_credit_term, jo_assembly_code,
            jo_quantity, jo_balance_quantity, jo_balance_fixed, jo_amount, jo_model_code, jo_import_code) 
            VALUES ('$joNumber', '$joDate', '$joBy', '$joCustomerCode', '$joCustomerPo', '$joDueDate', '$joCurrency', '$joDiscount', '$joMemo', '$joCreditTerm', '$joAssemblyCode', 
            '$joQuantity', '$joBalance', '$joBalance', '$joAmount', '$joModelCode', ". $this->db->escape($importCode) . ")";
        $this->db->query($query);
    }

    public function importJobOrderLog($importCode, $joNumber, $joAssemblyCode, $filename, $userId) {
        $query = "INSERT INTO job_order_log (import_code, jo_number, jo_assembly_code, jo_filename, user_id) 
            VALUES ('$importCode', '$joNumber', '$joAssemblyCode', '$filename', '$userId')";
        $this->db_log->query($query);
    }

    public function checkDataForDeleteJo($deleteId) {
        $query = "SELECT * FROM job_order WHERE jo_id = '$deleteId'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function deleteDataJoLog($deleteCode, $joNumber, $assemblyCode, $filename, $userId) {
        $query = "INSERT INTO job_order_log (import_code, jo_number, jo_assembly_code, jo_filename, user_id)
            VALUES ('$deleteCode', '$joNumber', '$assemblyCode', '$filename', '$userId')";
        $this->db_log->query($query);
    }

    public function deleteDataJo($deleteId) {
        $query = "DELETE FROM job_order WHERE jo_id = '$deleteId'";
        $this->db->query($query);
    }

    public function getLastIdDataRRLog() {
        $query = "SELECT * FROM receivement_report_log ORDER BY rr_id DESC";
        $hasil = $this->db_log->query($query);
        return $hasil->getRowArray();
    }

    public function checkDuplicateRR($rrNumber, $productCode, $vendorCode) {
        $query = "SELECT * FROM receivement_report WHERE rr_number = '$rrNumber' AND rr_product_code = '$productCode' AND rr_vendor_code = '$vendorCode'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function importRR($rrNumber, $date, $productCode, $productName, $vendorCode, $quantity, $by, $importCode) {
        $query = "INSERT INTO receivement_report (rr_number, rr_date, rr_product_code, rr_product_name, rr_vendor_code, rr_quantity, rr_by, rr_import_code)
            VALUES ('$rrNumber', '$date', '$productCode', '$productName', '$vendorCode', '$quantity', '$by', '$importCode')";
        $this->db->query($query);
    }

    public function importRRLog($importCode, $rrNumber, $productCode, $vendorCode, $filename, $userId) {
        $query = "INSERT INTO receivement_report_log (import_code, rr_number, rr_product_code, rr_vendor_code, rr_filename, user_id)
            VALUES ('$importCode', '$rrNumber', '$productCode', '$vendorCode', '$filename', '$userId')";
        $this->db_log->query($query);
    }

    public function updateStockWarehouse($productCode, $newStock) {
        $query = "UPDATE stock SET stock_quantity = '$newStock' WHERE product_assembly_code = '$productCode' AND whs_code = 1";
        $this->db_warehouse->query($query);
    }

    public function getDataForRR($productCode, $whsCode) {
        $query = "SELECT * FROM stock WHERE product_assembly_code = '$productCode' AND whs_code = '$whsCode'";
        $hasil = $this->db_warehouse->query($query);
        return $hasil->getRowArray();
    }

    public function getAllDataRR() {
        $query = "SELECT rr.*, mu.user_username, mv.vendor_agency FROM main.receivement_report rr
            INNER JOIN main.user mu ON rr.rr_by = mu.user_id
            INNER JOIN main.vendor mv ON rr.rr_vendor_code = mv.vendor_code";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function checkDataForDeleteRR($deleteId) {
        $query  = "SELECT * FROM receivement_report WHERE rr_id = '$deleteId'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function deleteDataRRLog($deleteCode, $rrNumber, $productCode, $vendorCode, $filename, $userId) {
        $query = "INSERT INTO receivement_report_log (import_code, rr_number, rr_product_code, rr_vendor_code, rr_filename, user_id) 
            VALUES ('$deleteCode', '$rrNumber', '$productCode', '$vendorCode', '$filename', '$userId')";
        $this->db_log->query($query);
    }

    public function deleteDataRR($deleteId) {
        $query = "DELETE FROM receivement_report WHERE rr_id = '$deleteId'";
        $this->db->query($query);
    }

    public function processWithdrawal($wdId, $wdStatus, $wdApproveHead = 1, $wdApproveHeadWh = 1, $issueDate = NULL) {
        $query = "UPDATE withdrawal SET 
            wd_approve_head = '$wdApproveHead', wd_approve_head_warehouse = '$wdApproveHeadWh', wd_status = '$wdStatus', wd_issue_date = '$issueDate' WHERE wd_id = '$wdId'";
        $this->db_warehouse->query($query);
    }

    public function processWithdrawalIssue($wdId, $wdStatus, $wdApproveHeadWh, $receiptQty, $remarkWh) {
        $query = "UPDATE withdrawal SET 
            wd_approve_head_warehouse = '$wdApproveHeadWh', wd_status = '$wdStatus', wd_receipt_quantity_warehouse = '$receiptQty', wd_remark_warehouse = '$remarkWh' WHERE wd_id = '$wdId'";
        $this->db_warehouse->query($query);
    }

    public function getDataForWithdrawal($wdId) {
        $query = "SELECT * FROM withdrawal WHERE wd_id = '$wdId'";
        $hasil = $this->db_warehouse->query($query);
        return $hasil->getRowArray();
    }

    public function getLastIdDataAssemblyLog() {
        $query = "SELECT * FROM assembly_log ORDER BY import_assembly_id DESC";
        $hasil = $this->db_log->query($query);
        return $hasil->getRowArray();
    }

    public function getDataForEditAssembly($assemblyId) {
        $query = "SELECT * FROM assembly WHERE assembly_id = '$assemblyId'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function getAllDataAssembly() {
        $query = "SELECT ma.*, ms.status_name, mc.currency_name FROM main.assembly ma 
            INNER JOIN main.status ms ON ma.assembly_status = ms.status_id
            INNER JOIN main.currency mc ON ma.assembly_currency = mc.currency_id";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function updateDataAssembly($assemblyId, $assemblyCode, $assemblyType, $assemblyName, $importCode, $description, $uom, $currency, $status, $unitPrice,
        $category, $package, $itemSpec, $updateCode) {
        $query = "UPDATE assembly SET 
            assembly_code = '$assemblyCode', assembly_type = '$assemblyType', assembly_name = '$assemblyName', assembly_description = '$description', assembly_uom = '$uom',
            assembly_currency = '$currency', assembly_status = '$status', assembly_whs_code = '$whs', assembly_unit_price = '$unitPrice', assembly_item_spec = '$itemSpec',
            assembly_packaging = '$package', assembly_category = '$category', assembly_import_code = '$updateCode' WHERE assembly_id = '$assemblyId'";
        $this->db->query($query);
    }

    public function updateDataAssemblyLog($updateCode, $assemblyCode, $filename, $userId) {
        $query = "INSERT INTO assembly_log (import_code, assembly_code, import_assembly_filename, user_id)
            VALUES ('$updateCode', '$assemblyCode', '$filename', '$userId')";
        $this->db_log->query($query);
    }

    public function insertAssembly($assemblyCode, $assemblyType, $assemblyName, $description, $uom, $currency, $status, $unitPrice, $category,
        $package, $itemSpec, $addCode) {
        $query = "INSERT INTO assembly (assembly_code, assembly_type, assembly_name, assembly_description, assembly_uom, assembly_currency, assembly_status, 
            assembly_unit_price, assembly_item_spec, assembly_packaging, assembly_category, assembly_import_code) 
            VALUES ('$assemblyCode', '$assemblyType', '$assemblyName', '$description', '$uom', '$currency', '$status', '$unitPrice', '$category', ". $this->db->escape($package) .", ". $this->db->escape($itemSpec) .", '$addCode')";
        $this->db->query($query);
    }

    public function insertAssemblyLog($addCode, $assemblyCode, $filename, $userId) {
        $query = "INSERT INTO assembly_log (import_code, assembly_code, import_assembly_filename, user_id) 
            VALUES ('$addCode', '$assemblyCode', '$filename', '$userId')";
        $this->db_log->query($query);
    }

    public function checkDataForDeleteAssembly($assemblyId) {
        $query = "SELECT * FROM assembly WHERE assembly_id = '$assemblyId'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function deleteDataAssemblyLog($deleteCode, $assemblyCode, $filename, $userId) {
        $query = "INSERT INTO assembly_log (import_code, assembly_code, import_assembly_filename, user_id) 
            VALUES ('$deleteCode', '$assemblyCode', '$filename', '$userId')";
        $this->db_log->query($query);
    }

    public function deleteDataAssembly($deleteId) {
        $query = "DELETE FROM assembly WHERE assembly_id = '$deleteId'";
        $this->db->query($query);
    }

    public function checkDuplicateAssembly($assemblyCode, $assemblyName) {
        $query = "SELECT * FROM assembly WHERE assembly_code = '$assemblyCode' AND assembly_name = '$assemblyName'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function importAssembly($assemblyCode, $assemblyType, $assemblyName, $assemblyDescription, $assemblyUom, $assemblyCurrency, $assemblyStatus, 
    $assemblyUnitPrice, $assemblyCategory, $assemblyPackage, $assemblyItemSpec, $importCode) {
        $query = "INSERT INTO assembly (assembly_code, assembly_type, assembly_name, assembly_description, assembly_uom, assembly_currency, assembly_status, 
            assembly_unit_price, assembly_category, assembly_packaging, assembly_item_spec, assembly_import_code)
            VALUES ('$assemblyCode', '$assemblyType', '$assemblyName', '$assemblyDescription', '$assemblyUom', '$assemblyCurrency', '$assemblyStatus', '$assemblyUnitPrice',
            '$assemblyCategory', ".$this->db->escape($assemblyPackage).", ".$this->db->escape($assemblyItemSpec).", '$importCode')";
        $this->db->query($query);
    }

    public function importAssemblyLog($importCode, $assemblyCode, $filename, $userId) {
        $query = "INSERT INTO assembly_log (import_code, assembly_code, import_assembly_filename, user_id) 
            VALUES ('$importCode', '$assemblyCode', '$filename', '$userId')";
        $this->db_log->query($query);
    }

    public function getAllWithdrawalHistoryFromTo($fromDate, $toDate) {
        $query = "SELECT ww.*, mu.user_username, md.dept_name, mw1.whs_name as fromWhs, mw2.whs_name as toWhs, ms.status_name FROM warehouse.withdrawal ww
            INNER JOIN main.user mu ON ww.wd_by = mu.user_id
            INNER JOIN main.department md ON ww.wd_dept = md.dept_id
            INNER JOIN main.whs mw1 ON ww.wd_from_whs_code = mw1.whs_code
            INNER JOIN main.whs mw2 ON ww.wd_to_whs_code = mw2.whs_code
            INNER JOIN main.status ms ON ww.wd_status = ms.status_id
            WHERE ww.wd_date BETWEEN '$fromDate' AND '$toDate'";
        $hasil = $this->db_warehouse->query($query);
        return $hasil->getResultArray();
    }

    public function checkDuplicateBom($assemblyCode, $productCode) {
        $query = "SELECT * FROM bom WHERE bom_assembly_code = '$assemblyCode' AND bom_product_code = '$productCode'";
        $hasil = $this->db_pde->query($query);
        return $hasil->getRowArray();
    }

    public function getLastIdDataBomLog() {
        $query = "SELECT * FROM bom_log ORDER BY import_bom_id DESC";
        $hasil = $this->db_log->query($query);
        return $hasil->getRowArray();
    }

    public function importBom($importCode, $bomType, $bomAssembly, $bomProduct, $bomQuantity, $userId) {
        $query = "INSERT INTO bom (bom_code, bom_type, bom_assembly_code, bom_product_code, bom_quantity_needed, bom_by)
            VALUES ('$importCode', '$bomType', '$bomAssembly', '$bomProduct', '$bomQuantity', '$userId')";
        $this->db_pde->query($query);
    }

    public function importBomLog($importCode, $bomAssembly, $bomProduct, $filename, $userId) {
        $query = "INSERT INTO bom_log (bom_code, import_assembly_code, import_product_code, import_bom_filename, user_id)
            VALUES ('$importCode', '$bomAssembly', '$bomProduct', '$filename', '$userId')";
        $this->db_log->query($query);
    }

    public function checkAssemblyAndProduct($assemblyCode, $productCode) {
        $query1 = "SELECT ma.assembly_code FROM main.assembly ma WHERE ma.assembly_code = '$assemblyCode'";
        $query2 = "SELECT mp.product_code FROM main.product mp WHERE mp.product_code = '$productCode'";
        $hasil1 = $this->db->query($query1);
        $hasil2 = $this->db->query($query2);

        if($hasil1->getRowArray() != null && $hasil2->getRowArray() != null) {
            // yang penting tidak null biar tidak dibaca failed
            return $hasil1->getRowArray();
        } else {
            return null;
        }
    }

    public function getAllBom() {
        $query = "SELECT bom.*, mu.user_username, md.dept_name, mp.product_name, mp.product_uom, mp.product_unit_price FROM pde.bom bom
            INNER JOIN main.product mp ON bom.bom_product_code = mp.product_code
            INNER JOIN main.user mu ON bom.bom_by = mu.user_id
            INNER JOIN main.department md ON mu.user_dept = md.dept_id
            ORDER BY bom_id DESC;";
        $hasil = $this->db_pde->query($query);
        return $hasil->getResultArray();
    }

    public function updateDataBom($updateCode, $bomId, $bomType, $bomAssembly, $bomProduct, $bomQty) {
        $query = "UPDATE bom SET
            bom_code = '$updateCode', bom_type = '$bomType', bom_assembly_code = '$bomAssembly', bom_product_code = '$bomProduct', bom_quantity_needed = '$bomQty'
            WHERE bom_id = '$bomId'";
        $this->db_pde->query($query);
    }

    public function updateDataBomLog($updateCode, $bomAssembly, $bomProduct, $filename, $userId) {
        $query = "INSERT INTO bom_log (bom_code, import_assembly_code, import_product_code, import_bom_filename, user_id)
            VALUES ('$updateCode', '$bomAssembly', '$bomProduct', '$filename', '$userId')";
        $this->db_log->query($query);
    }

    public function checkDataForDeleteBom($bomId) {
        $query = "SELECT * FROM bom WHERE bom_id = '$bomId'";
        $hasil = $this->db_pde->query($query);
        return $hasil->getRowArray();
    }

    public function deleteDataBomLog($deleteCode, $assemblyCode, $productCode, $filename, $userId) {
        $query = "INSERT INTO bom_log (bom_code, import_assembly_code, import_product_code, import_bom_filename, user_id)
            VALUES ('$deleteCode', '$assemblyCode', '$productCode', '$filename', '$userId')";
        $this->db_log->query($query);
    }

    public function deleteDataBom($bomId) {
        $query = "DELETE FROM bom WHERE bom_id = '$bomId'";
        $this->db_pde->query($query);
    }

    public function insertBom($addCode, $bomType, $bomQty, $bomAssembly, $bomProduct, $userId) {
        $query = "INSERT INTO bom (bom_code, bom_type, bom_assembly_code, bom_product_code, bom_quantity_needed, bom_by)
            VALUES ('$addCode', '$bomType', '$bomAssembly', '$bomProduct', '$bomQty', '$userId')";
        $this->db_pde->query($query);
    }

    public function insertBomLog($addCode, $bomAssembly, $bomProduct, $filename, $userId) {
        $query = "INSERT INTO bom_log (bom_code, import_assembly_code, import_product_code, import_bom_filename, user_id) 
            VALUES ('$addCode', '$bomAssembly', '$bomProduct', '$filename', '$userId')";
        $this->db_log->query($query);
    }

    public function checkCustomerAndAssembly($customerCode, $assemblyCode) {
        $query1 = "SELECT * FROM customer WHERE customer_code = '$customerCode'";
        $query2 = "SELECT * FROM assembly WHERE assembly_code = '$assemblyCode'";

        $hasil1 = $this->db->query($query1);
        $hasil2 = $this->db->query($query2);

        if($hasil1->getRowArray() != null && $hasil2->getRowArray() != null) {
            // yang penting tidak null biar tidak dibaca failed
            return $hasil1->getRowArray();
        } else {
            return null;
        }
    }

    public function checkProductForRR($productCode) {
        $query = "SELECT * FROM product WHERE product_code = '$productCode'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function getTermAssembly($dataAssembly, $dataJo){
        // $query = "SELECT ma.* FROM main.assembly ma 
        //     WHERE ma.assembly_code LIKE '%$data%'";
        $query = "SELECT jo.*, ma.assembly_name FROM main.job_order jo
            INNER JOIN main.assembly ma ON jo.jo_assembly_code = ma.assembly_code
            WHERE jo.jo_assembly_code LIKE '%$dataAssembly%' AND jo.jo_number = '$dataJo'";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getTermJo($data) {
        $query = "SELECT jo.* FROM main.job_order jo 
            WHERE jo.jo_number LIKE '%$data%' GROUP BY jo.jo_number";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getDataForBuildAssembly($assemblyCode, $joNumber) {
        $query = "SELECT jo.jo_number, jo.jo_assembly_code, jo.jo_quantity, jo.jo_balance_quantity, mp.product_code, mp.product_name, mp.product_description, mp.product_unit_price, mp.product_type, mp.product_uom, bom.bom_quantity_needed, stock1.stock_quantity as quantity_main, stock2.stock_quantity as quantity_production, ROUND(bom.bom_quantity_needed * jo.jo_quantity,2) as quantity_for_pr FROM main.job_order jo 
            INNER JOIN pde.bom bom ON jo.jo_assembly_code = bom.bom_assembly_code 
            INNER JOIN main.product mp ON bom.bom_product_code = mp.product_code 
            INNER JOIN warehouse.stock stock1 ON bom.bom_product_code = stock1.product_assembly_code AND stock1.whs_code = 1 
            LEFT JOIN warehouse.stock stock2 ON bom.bom_product_code = stock2.product_assembly_code AND stock2.whs_code = 2 
            WHERE bom.bom_assembly_code = '$assemblyCode' AND jo.jo_number = '$joNumber'";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getLastIdDataBuildAssembly() {
        $query = "SELECT * FROM build_assembly ORDER BY ba_id DESC";
        $hasil = $this->db_pde->query($query);
        return $hasil->getRowArray();
    }

    public function checkJoAndAssembly($joNumber, $assemblyCode) {
        $query1 = "SELECT * FROM job_order WHERE jo_number = '$joNumber'";
        $query2 = "SELECT * FROM assembly WHERE assembly_code = '$assemblyCode'";

        $hasil1 = $this->db->query($query1);
        $hasil2 = $this->db->query($query2);

        if($hasil1->getRowArray() != null && $hasil2->getRowArray() != null) {
            // yang penting tidak null biar tidak dibaca failed
            return $hasil1->getRowArray();
        } else {
            return null;
        }
    }

    public function checkJoAndAssemblyAndProductAndCode($joNumber, $assemblyCode, $productCode, $baCode) {
        $query1 = "SELECT * FROM job_order WHERE jo_number = '$joNumber' AND jo_assembly_code = '$assemblyCode'";
        $query2 = "SELECT * FROM product WHERE product_code = '$productCode'";

        $hasil1 = $this->db->query($query1);
        $hasil2 = $this->db->query($query2);

        if($hasil1->getRowArray() != null && $hasil2->getRowArray() != null) {
            // yang penting tidak null biar tidak dibaca failed
            return $hasil1->getRowArray();
        } else {
            return null;
        }
    }

    public function checkJoAndAssemblyAndProduct($joNumber, $assemblyCode, $productCode) {
        $query1 = "SELECT * FROM job_order WHERE jo_number = '$joNumber' AND jo_assembly_code = '$assemblyCode'";
        $query2 = "SELECT * FROM product WHERE product_code = '$productCode'";

        $hasil1 = $this->db->query($query1);
        $hasil2 = $this->db->query($query2);

        if($hasil1->getRowArray() != null && $hasil2->getRowArray() != null) {
            // yang penting tidak null biar tidak dibaca failed
            return $hasil1->getRowArray();
        } else {
            return null;
        }
    }

    public function getStockProduction($productCode) {
        $query = "SELECT * FROM stock WHERE product_assembly_code = '$productCode' AND whs_code = '2'";
        $hasil = $this->db_warehouse->query($query);
        return $hasil->getRowArray();
    }

    public function getQtyNeedBom($assemblyCode, $productCode) {
        $query = "SELECT * FROM bom WHERE bom_assembly_code = '$assemblyCode' AND bom_product_code = '$productCode'";
        $hasil = $this->db_pde->query($query);
        return $hasil->getRowArray();
    }

    public function insertDataBuildAssembly($baCode, $userId, $assemblyCode, $productCode, $joNumber, $processQty, $productQuantity, $status) {
        $query = "INSERT INTO build_assembly (ba_code, ba_user_id, ba_assembly_code, ba_product_code, ba_jo_number, ba_process_quantity, ba_balance_process_quantity, ba_product_quantity, ba_status) 
            VALUES ('$baCode', '$userId', '$assemblyCode', '$productCode', '$joNumber', '$processQty', '$processQty', '$productQuantity', '$status')";
        $this->db_pde->query($query);
    }

    public function checkBuildAssemblyId($baId) {
        $query = "SELECT * FROM build_assembly WHERE ba_id = '$baId'";
        $hasil = $this->db_pde->query($query);
        return $hasil->getRowArray();
    }

    public function checkApproveAndTimeApprove($baId) {
        $query = "SELECT * FROM build_assembly WHERE ba_id = '$baId'";
        $hasil = $this->db_pde->query($query);
        return $hasil->getRowArray();
    }

    public function updateDataProductionWarehouse($productCode, $whsCode, $newStockProduction) {
        $query = "UPDATE stock SET 
            stock_quantity = '$newStockProduction'
            WHERE product_assembly_code = '$productCode' AND whs_code = '$whsCode'";
        $this->db_warehouse->query($query);
    }

    public function getDataStockFinishGood($productCode, $whsCode) {
        $query = "SELECT * FROM stock WHERE product_assembly_code = '$productCode' AND whs_code = '$whsCode'";
        $hasil = $this->db_warehouse->query($query);
        return $hasil->getRowArray();
    }

    public function insertDataFinishGoodWarehouse($productCode, $whsCode, $newStockFinishGood) {
        $query = "INSERT INTO stock (product_assembly_code, whs_code, stock_quantity) 
            VALUES ('$productCode', '$whsCode', '$newStockFinishGood')";
        $this->db_warehouse->query($query);
    }

    public function updateDataFinishGoodWarehouse($productCode, $whsCode, $newStockFinishGood) {
        $query = "UPDATE stock SET 
            stock_quantity = '$newStockFinishGood' WHERE product_assembly_code = '$productCode' AND whs_code = '$whsCode'";
        $this->db_warehouse->query($query);
    }

    public function updateDataBuildAssembly($baId, $status, $approveBy, $approveAt) {
        $query = "UPDATE build_assembly SET
            ba_status = '$status', ba_approve_by = '$approveBy', ba_approve_at = '$approveAt'
            WHERE ba_id = '$baId'";
        $this->db_pde->query($query);
    }

    public function updateDataJoBalance($joNumber, $assemblyCode, $newStockJoBalance) {
        $query = "UPDATE job_order SET
            jo_balance_quantity = '$newStockJoBalance'
            WHERE jo_number = '$joNumber' AND jo_assembly_code = '$assemblyCode'";
        $this->db->query($query);
    }

    public function getDataJoBalance($joNumber, $assemblyCode) {
        $query = "SELECT * FROM job_order WHERE jo_number = '$joNumber' AND jo_assembly_code = '$assemblyCode'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function updateDataBalanceProcessQuantity($newStockBalanceQuantity, $joNumber, $assemblyCode) {
        $query = "UPDATE build_assembly SET
            ba_balance_process_quantity = '$newStockBalanceQuantity'
            WHERE ba_jo_number = '$joNumber' AND ba_assembly_code = '$assemblyCode'";
        $this->db_pde->query($query);
    }

    public function checkDuplicateJoAssemblyProductBaCode($baCode, $joNumber, $assemblyCode, $productCode) {
        $query = "SELECT * FROM build_assembly WHERE ba_code = '$baCode' AND ba_jo_number = '$joNumber' AND ba_assembly_code = '$assemblyCode' AND ba_product_code = '$productCode'";
        $hasil = $this->db_pde->query($query);
        return $hasil->getRowArray();
    }

    public function checkDuplicateWsCodeAssemblyJoProduct($wsNumber, $assemblyProduct, $joNumber, $productCode) {
        $query = "SELECT * FROM withdrawal
            WHERE wd_number = '$wsNumber' AND wd_assembly_product = '$assemblyProduct' AND wd_jo_number = '$joNumber' AND wd_product_code = '$productCode'";
        $hasil = $this->db_warehouse->query($query);
        return $hasil->getRowArray();
    }

    public function checkDuplicateWsCodeProduct($wsNumber, $productCode) {
        $query = "SELECT * FROM withdrawal 
            WHERE wd_number = '$wsNumber' AND wd_product_code = '$productCode'";
        $hasil = $this->db_warehouse->query($query);
        return $hasil->getRowArray();
    }

    public function getDataSpecificWithdrawalProcess($productCode, $status) {
        $query = "SELECT * FROM withdrawal WHERE wd_product_code = '$productCode' AND wd_status = '$status'";
        $hasil = $this->db_warehouse->query($query);
        return $hasil->getRowArray();
    }

    public function getAllBuildAssembly() {
        $query = "SELECT ba.*, ms.status_name, jo.jo_balance_quantity, mu1.user_username, mu2.user_username as approveBy, ws.stock_quantity, bom.bom_quantity_needed FROM pde.build_assembly ba
            INNER JOIN main.status ms ON ba.ba_status = ms.status_id
            INNER JOIN main.job_order jo ON ba.ba_jo_number = jo.jo_number
            INNER JOIN main.user mu1 ON ba.ba_user_id = mu1.user_id
            LEFT JOIN main.user mu2 ON ba.ba_approve_by = mu2.user_id
            INNER JOIN warehouse.stock ws ON ba.ba_product_code = ws.product_assembly_code AND ws.whs_code = 2
            INNER JOIN pde.bom bom ON ba.ba_assembly_code = bom.bom_assembly_code AND ba.ba_product_code = bom.bom_product_code
            WHERE ba.ba_status = '7' OR ba.ba_status = '10'";
        $hasil = $this->db_pde->query($query);
        return $hasil->getResultArray();
    }

    public function updatePassword($userId, $newPassword) {
        $query = "UPDATE user SET
            user_password = '$newPassword' WHERE user_id = '$userId'";
        $this->db->query($query);
    }

    public function getUserData($userId) {
        $query = "SELECT * FROM user WHERE user_id = '$userId'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function getTotalProducts($seq) {
        if($seq == 'total') {
            $query = "SELECT COUNT(*) as totalProducts FROM product";
            $hasil = $this->db->query($query);
            return $hasil->getRowArray();
        } else {            
        $query = "SELECT DISTINCT mp.product_code FROM main.product mp
            INNER JOIN log.product_log pl ON mp.product_code = pl.product_code
            WHERE pl.import_product_create >= CURDATE() - INTERVAL DAY(CURDATE())-1 DAY";
            $hasil = $this->db->query($query);
            return $hasil->getResultArray();
        }
    }

    public function getTotalCustomer($seq) {
        if($seq == 'total') {
            $query = "SELECT COUNT(*) as totalCustomer FROM customer";
            $hasil = $this->db->query($query);
            return $hasil->getRowArray();
        } else {
            $query = "SELECT DISTINCT mc.customer_code FROM main.customer mc
            INNER JOIN log.customer_log cl ON mc.customer_code = cl.customer_code
            WHERE cl.import_customer_create >= CURDATE() - INTERVAL DAY(CURDATE())-1 DAY";
            $hasil = $this->db->query($query);
            return $hasil->getResultArray();
        }
    }

    public function getTotalVendor($seq) {
        if($seq == 'total') {
            $query = "SELECT COUNT(*) as totalVendor FROM vendor";
            $hasil = $this->db->query($query);
            return $hasil->getRowArray();
        } else {
            $query = "SELECT DISTINCT mv.vendor_code FROM main.vendor mv
                INNER JOIN log.vendor_log lv ON mv.vendor_code = lv.vendor_code
                WHERE lv.import_vendor_create >= CURDATE() - INTERVAL DAY(CURDATE())-1 DAY";
            $hasil = $this->db->query($query);
            return $hasil->getResultArray();
        }
    }

    public function getTotalAssembly($seq) {
        if($seq == 'total') {
            $query = "SELECT COUNT(*) as totalAssembly FROM assembly";
            $hasil = $this->db->query($query);
            return $hasil->getRowArray();
        } else {
            $query = "SELECT DISTINCT ma.assembly_code FROM main.assembly ma
                INNER JOIN log.assembly_log la ON ma.assembly_code = la.assembly_code
                WHERE la.import_assembly_create >= CURDATE() - INTERVAL DAY(CURDATE())-1 DAY";
            $hasil = $this->db->query($query);
            return $hasil->getResultArray();
        }
    }

    public function getTotalJo($seq) { 
        if($seq == 'total') {
            $query = "SELECT COUNT(*) as totalJo FROM job_order";
            $hasil = $this->db->query($query);
            return $hasil->getRowArray();
        } else {
            $query = "SELECT DISTINCT jo.jo_number FROM main.job_order jo
                INNER JOIN log.job_order_log jol ON jo.jo_number = jol.jo_number AND jo.jo_assembly_code = jol.jo_assembly_code
                WHERE jol.jo_create >= CURDATE() - INTERVAL DAY(CURDATE())-1 DAY";
            $hasil = $this->db->query($query);
            return $hasil->getResultArray();
        }
    }

    public function getTotalRR($seq) {
        if($seq == 'total') {
            $query = "SELECT COUNT(*) as totalRR FROM receivement_report";
            $hasil = $this->db->query($query);
            return $hasil->getRowArray();
        } else {
            $query = "SELECT DISTINCT rr.rr_number FROM main.receivement_report rr
                INNER JOIN log.receivement_report_log rrl ON rr.rr_number = rrl.rr_number AND rr.rr_product_code = rrl.rr_product_code
                WHERE rrl.rr_created >= CURDATE() - INTERVAL DAY(CURDATE())-1 DAY";
            $hasil = $this->db->query($query);
            return $hasil->getResultArray();
        }
    }

    public function getDataJoAndAssembly($joNumber, $assemblyCode) {
        $query = "SELECT * FROM job_order WHERE jo_number = '$joNumber' AND jo_assembly_code = '$assemblyCode'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function getTotalPendingJo($seq) { 
        if($seq == 'pending') {
            $query = "SELECT DISTINCT jo.jo_number FROM main.job_order jo
                INNER JOIN log.job_order_log jol ON jo.jo_number = jol.jo_number AND jo.jo_assembly_code = jol.jo_assembly_code
                WHERE jo_balance_quantity > 0";
            $hasil = $this->db->query($query);
            return $hasil->getResultArray();
        } else {
            $query = "SELECT DISTINCT jo.jo_number FROM main.job_order jo
                INNER JOIN log.job_order_log jol ON jo.jo_number = jol.jo_number AND jo.jo_assembly_code = jol.jo_assembly_code
                WHERE jol.jo_create >= CURDATE() - INTERVAL DAY(CURDATE())-1 DAY";
            $hasil = $this->db->query($query);
            return $hasil->getResultArray();
        }
    }

    public function getTotalWithdrawal($seq) { 
        if($seq == 'day') {
            $query = "SELECT DISTINCT ww.wd_number FROM warehouse.withdrawal ww
                INNER JOIN log.withdrawal_log wl ON ww.wd_number = wl.insert_wd_number
                WHERE wl.insert_wd_date > DATE_SUB(NOW(), INTERVAL 1 DAY)";
            $hasil = $this->db_warehouse->query($query);
            return $hasil->getResultArray();
        } else {
            $query = "SELECT DISTINCT ww.wd_number FROM warehouse.withdrawal ww
                INNER JOIN log.withdrawal_log wl ON ww.wd_number = wl.insert_wd_number
                WHERE wl.insert_wd_date >= CURDATE() - INTERVAL DAY(CURDATE())-1 DAY";
            $hasil = $this->db_warehouse->query($query);
            return $hasil->getResultArray();
        }
    }

    public function getWithdrawalForDashboard($seq) {
        if($seq == 'pending') {
            $query = "SELECT ww.*, mu.user_fullname FROM warehouse.withdrawal ww
                INNER JOIN main.user mu ON ww.wd_by = mu.user_id
                INNER JOIN main.product mp on ww.wd_product_code = mp.product_code
                WHERE wd_status != 6 AND wd_status != 7";
            $hasil = $this->db_warehouse->query($query);
            return $hasil->getResultArray();
        } else {
            $query = "SELECT ww.*, mu.user_fullname, ms.status_name, mp.product_name FROM warehouse.withdrawal ww
                INNER JOIN main.user mu ON ww.wd_by = mu.user_id
                INNER JOIN main.product mp ON ww.wd_product_code = mp.product_code
                INNER JOIN main.status ms ON ww.wd_status = ms.status_id
                WHERE wd_status != 6 AND wd_status != 7 ORDER BY wd_date DESC LIMIT 5";
            $hasil = $this->db_warehouse->query($query);
            return $hasil->getResultArray();
        }
    }

    public function checkProductAndAssemblyForWithdrawal($productCode, $assemblyCode) {
        $query = "SELECT * FROM bom
            WHERE bom_assembly_code = '$assemblyCode' AND bom_product_code = '$productCode'";
        $hasil = $this->db_pde->query($query);
        return $hasil->getRowArray();
    }

    public function getDataFeature($feature) {
        $query = "SELECT * FROM " . $feature;
        $hasil = $this->db->query($query);

        return $hasil->getResultArray();
    }

    public function getLastDataFeature($feature) {
        if($feature == 'reason' || $feature == 'whs') {
            $featureId = $feature . '_code';
        } elseif($feature == 'department') {
            $featureId = 'dept_id';
        } else {
            $featureId = $feature . '_id';
        }
        $query = "SELECT * FROM " . $feature . " ORDER BY " . $featureId . " DESC";
        $hasil = $this->db->query($query);

        return $hasil->getRowArray();
    }

    public function insertNewFeature($feature, $featureId, $featureName) {
        if($feature == 'reason' || $feature == 'whs') {
            $featureIdColumn = $feature . '_code';
            $featureNameColumn = $feature . '_name';
        } elseif($feature == 'department') {
            $featureIdColumn = 'dept_id';
            $featureNameColumn = 'dept_name';
        } else {
            $featureIdColumn = $feature . '_id';
            $featureNameColumn = $feature . '_name';
        }

        $query = "INSERT INTO $feature ($featureIdColumn, $featureNameColumn) VALUES ('$featureId', '$featureName')";
        $this->db->query($query);
    }

    public function checkDeleteFeature($dataBase, $deleteId) {
        if($dataBase == 'reason' || $dataBase == 'whs') {
            $featureIdColumn = $dataBase . '_code';
        } elseif($dataBase == 'department') {
            $featureIdColumn = 'dept_id';
        } else {
            $featureIdColumn = $dataBase . '_id';
        }

        $query = "SELECT * FROM $dataBase WHERE $featureIdColumn = '$deleteId'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function deleteFeature($dataBase, $deleteId) {
        if($dataBase == 'reason' || $dataBase == 'whs') {
            $featureIdColumn = $dataBase . '_code';
        } elseif($dataBase == 'department') {
            $featureIdColumn = 'dept_id';
        } else {
            $featureIdColumn = $dataBase . '_id';
        }

        $query = "DELETE FROM $dataBase WHERE $featureIdColumn = '$deleteId'";
        $this->db->query($query);
    }

    public function getSpecificDataFeature($dataBase, $featureId, $featureName) {
        if($dataBase == 'reason' || $dataBase == 'whs') {
            $featureIdColumn = $dataBase . '_code';
            $featureNameColumn = $dataBase . '_name';
        } elseif($dataBase == 'department') {
            $featureIdColumn = 'dept_id';
            $featureNameColumn = 'dept_name';
        } else {
            $featureIdColumn = $dataBase . '_id';
            $featureNameColumn = $dataBase . '_name';
        }
        
        $query = "SELECT * FROM $dataBase WHERE $featureIdColumn = '$featureId'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function updateDataFeature($dataBase, $featureId, $featureName, $oldDataName) {
        if($dataBase == 'reason' || $dataBase == 'whs') {
            $featureIdColumn = $dataBase . '_code';
            $featureNameColumn = $dataBase . '_name';
        } elseif($dataBase == 'department') {
            $featureIdColumn = 'dept_id';
            $featureNameColumn = 'dept_name';
        } else {
            $featureIdColumn = $dataBase . '_id';
            $featureNameColumn = $dataBase . '_name';
        }

        $query = "UPDATE $dataBase SET $featureIdColumn = '$featureId', $featureNameColumn = '$featureName' WHERE $featureIdColumn = '$featureId' OR $featureNameColumn = '$oldDataName'";
        $this->db->query($query);
    }

    public function checkDataBeforeImportWithdrawal($joNumber, $assemblyCode, $productCode) {
        $query = "SELECT jo.*, bom.* FROM main.job_order jo 
            INNER JOIN pde.bom bom ON jo.jo_assembly_code = bom.bom_assembly_code
            WHERE jo.jo_number = 'JO-PTPF-21000507' AND bom.bom_product_code = 'RWM-1909-01476' AND bom.bom_assembly_code = 'PK-1394-00'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function checkDuplicateInSameTime($saNumber, $productCode) {
        $query = "SELECT * FROM stock_adjustment_log
            WHERE insert_sa_number = '$saNumber' AND insert_sa_product_code = '$productCode'";
        $hasil = $this->db_log->query($query);
        return $hasil->getRowArray();
    }

    public function checkDuplicateProductsUnitPrice($productCode, $importCode) {
        $query = "SELECT * FROM product WHERE product_code = '$productCode' AND product_import_code = '$importCode'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function getDataForUnitPrice($productCode) {
        $query = "SELECT * FROM product WHERE product_code = '$productCode'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function importProductsUnitPrice($productCode, $unitPrice, $importCode) {
        $query = "UPDATE product SET product_unit_price = '$unitPrice', product_import_code = '$importCode' WHERE product_code = '$productCode'";
        $this->db->query($query);
    }

    public function importProductsUnitPriceLog($importCode, $productCode, $filename, $userId) {
        $query = "INSERT INTO product_log (import_code, product_code, import_product_filename, user_id)
            VALUES ('$importCode', '$productCode', '$filename', '$userId')";
        $this->db_log->query($query);
    }

    public function getDataProductAvailable($productCode) {
        $query = "SELECT * FROM stock WHERE product_assembly_code = '$productCode' AND whs_code = 1";
        $hasil = $this->db_warehouse->query($query);
        return $hasil->getRowArray();
    }

    public function getDataRRLog($importCode) {
        $query = "SELECT rl.*, mu.user_fullname, rr.*, mv.vendor_name FROM log.receivement_report_log rl
            LEFT JOIN main.user mu ON rl.user_id = mu.user_id
            LEFT JOIN main.vendor mv ON rl.rr_vendor_code = mv.vendor_code
            LEFT JOIN main.receivement_report rr ON rl.rr_number = rr.rr_number AND rl.rr_product_code = rr.rr_product_code
            WHERE import_code = '$importCode'";
        $hasil = $this->db_log->query($query);
        return $hasil->getResultArray();
    }

    public function getDataProductsLog($importCode) {
        $query = "SELECT pl.*, mp.*, mu.user_fullname, muom.multiple_uom_name, sc.uom_schema_name, mc.currency_name, ms.status_name FROM log.product_log pl
            LEFT JOIN main.product mp ON pl.product_code = mp.product_code
            LEFT JOIN main.user mu ON pl.user_id = mu.user_id
            LEFT JOIN main.multiple_uom muom ON mp.product_multiple_uom = muom.multiple_uom_id
            LEFT JOIN main.uom_schema sc ON mp.product_uom_schema = sc.uom_schema_id
            LEFT JOIN main.currency mc ON mp.product_currency = mc.currency_id
            LEFT JOIN main.status ms ON mp.product_status = ms.status_id
            WHERE import_code = '$importCode'";
        $hasil = $this->db_log->query($query);
        return $hasil->getResultArray();
    }

    public function getDataAssemblyLog($importCode) {
        $query = "SELECT al.*, ma.*, mc.currency_name, ms.status_name, mu.user_fullname FROM log.assembly_log al
            LEFT JOIN main.assembly ma ON al.assembly_code = ma.assembly_code
            LEFT JOIN main.currency mc ON ma.assembly_currency = mc.currency_id
            LEFT JOIN main.status ms ON ma.assembly_status = ms.status_id
            LEFT JOIN main.user mu ON al.user_id = mu.user_id
            WHERE import_code = '$importCode'";
        $hasil = $this->db_log->query($query);
        return $hasil->getResultArray();
    }

    public function getDataCustomerLog($importCode) {
        $query = "SELECT cl.*, mu.user_fullname as lastUpdateBy, ms.status_name, mc.* FROM log.customer_log cl
            LEFT JOIN main.customer mc ON cl.customer_code = mc.customer_code
            LEFT JOIN main.user mu ON cl.user_id = mu.user_id
            LEFT JOIN main.status ms ON mc.customer_status = ms.status_id
            WHERE import_code = '$importCode'";
        $hasil = $this->db_log->query($query);
        return $hasil->getResultArray();
    }

    public function getDataVendorLog($importCode) {
        $query = "SELECT vl.*, mu.user_fullname, mv.*, ms.status_name FROM log.vendor_log vl
            LEFT JOIN main.user mu ON vl.user_id = mu.user_id
            LEFT JOIN main.vendor mv ON vl.vendor_code = mv.vendor_code
            LEFT JOIN main.status ms ON mv.vendor_status = ms.status_id
            WHERE import_code = '$importCode'";
        $hasil = $this->db_log->query($query);
        return $hasil->getResultArray();
    }

    public function getDataJOLog($importCode) {
        $query = "SELECT jl.*, mu.user_fullname, jo.*, mc.customer_agency, currency.currency_name FROM log.job_order_log jl
            LEFT JOIN main.user mu ON jl.user_id = mu.user_id
            LEFT JOIN main.job_order jo ON jl.jo_number = jo.jo_number AND jl.jo_assembly_code = jo.jo_assembly_code
            LEFT JOIN main.customer mc ON jo.jo_customer_code = mc.customer_code
            LEFT JOIN main.currency currency ON jo.jo_currency = currency.currency_id
            WHERE import_code = '$importCode'";
        $hasil = $this->db_log->query($query);
        return $hasil->getResultArray();
    }

    public function getDataWithdrawalLog($wdNumber) {
        $query = "SELECT wl.*, mu.user_fullname, whs1.whs_name as fromWhs, whs2.whs_name as toWhs, md.dept_name, ww.*, ms.status_name, mp.product_name FROM log.withdrawal_log wl
            LEFT JOIN main.user mu ON wl.insert_wd_by = mu.user_id
            LEFT JOIN main.whs whs1 ON wl.insert_wd_from_whs = whs1.whs_code
            LEFT JOIN main.whs whs2 ON wl.insert_wd_to_whs = whs2.whs_code
            LEFT JOIN main.department md ON wl.insert_wd_dept = md.dept_id
            LEFT JOIN main.status ms ON wl.insert_wd_dept = ms.status_id
            LEFT JOIN main.product mp ON wl.insert_wd_product_code = mp.product_code
            LEFT JOIN warehouse.withdrawal ww ON wl.insert_wd_product_code = ww.wd_product_code AND wl.insert_wd_jo_number = ww.wd_jo_number
            WHERE insert_wd_number = '$wdNumber'";
        $hasil = $this->db_log->query($query);
        return $hasil->getResultArray();
    }

    public function getDataBomLog($bomCode) {
        $query = "SELECT bl.*, mu1.user_fullname as lastUpdateBy, mu2.user_fullname as createBy, bom.*, mp.product_name FROM log.bom_log bl
            LEFT JOIN pde.bom bom ON bl.import_assembly_code = bom.bom_assembly_code AND bl.import_product_code = bom.bom_product_code
            LEFT JOIN main.user mu1 ON bl.user_id = mu1.user_id
            LEFT JOIN main.user mu2 ON bom.bom_by = mu2.user_id
            LEFT JOIN main.product mp ON bl.import_product_code = mp.product_code
            WHERE bl.bom_code = '$bomCode'";
        $hasil = $this->db_log->query($query);
        return $hasil->getResultArray();
    }

    public function updateProductPriceCurrencyFromRR($productCode, $price, $currency) {
        $query = "UPDATE product SET product_unit_price = '$price', product_currency = '$currency' WHERE product_code = '$productCode'";
        $this->db->query($query);
    }

    public function updateProductUnitPriceFromRR($productCode, $price) {
        $query = "UPDATE product SET product_unit_price = '$price' WHERE product_code = '$productCode'";
        $this->db->query($query);
    }

    public function updateProductCurrencyFromRR($productCode, $currency) {
        $query = "UPDATE product SET product_currency = '$currency' WHERE product_code = '$productCode'";
        $this->db->query($query);
    }

    public function getDataOverbudget($productCode) {
        $query = "SELECT 
            bom.bom_product_code, jo.jo_number,
            (jo.jo_balance_fixed * bom.bom_quantity_needed) AS totalRequired,
            SUM(ww.wd_receipt_quantity_warehouse) AS totalWithdrawal,
            IF(SUM(ww.wd_receipt_quantity_warehouse) > jo.jo_balance_fixed * bom.bom_quantity_needed, 'Overbudget', 'On Process') AS status FROM main.job_order jo 
                LEFT JOIN pde.bom bom ON jo.jo_assembly_code = bom.bom_assembly_code
                LEFT JOIN main.product mp ON bom.bom_product_code = mp.product_code
                INNER JOIN warehouse.withdrawal ww ON jo.jo_number = ww.wd_jo_number AND jo.jo_assembly_code = ww.wd_assembly_product AND bom.bom_product_code = ww.wd_product_code
                WHERE mp.product_code = '$productCode'
                GROUP BY mp.product_code, jo.jo_number";
        $hasil = $this->db->query($query);
        // dd($hasil->getRowArray());
        return $hasil->getRowArray();
    }
	
	public function getDataStockAdjustmentByLog($SaNumber) {
		$query = "SELECT sal.*, mr.reason_name, mw.whs_name FROM stock_adjustment_log sal 
            INNER JOIN main.reason mr ON sal.insert_sa_reason = mr.reason_code
            INNER JOIN main.whs mw ON sal.insert_sa_whs_code = mw.whs_code
			WHERE sal.insert_sa_number = '$SaNumber'";
        $hasil = $this->db_log->query($query);
        return $hasil->getResultArray();
	}
}  