<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Inventory Warehouse</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="<?= base_url('vendors//feather/feather.css') ?>">
  <link rel="stylesheet" href="<?= base_url('vendors/mdi/css/materialdesignicons.min.css') ?>">
  <link rel="stylesheet" href="<?= base_url('vendors/ti-icons/css/themify-icons.css') ?>">
  <link rel="stylesheet" href="<?= base_url('vendors/typicons/typicons.css') ?>">
  <link rel="stylesheet" href="<?= base_url('vendors/simple-line-icons/css/simple-line-icons.css') ?>">
  <link rel="stylesheet" href="<?= base_url('vendors/css/vendor.bundle.base.css') ?>">
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
  <link rel="stylesheet" href="<?= base_url('vendors/font-awesome/css/fontawesome5/css/all.css') ?>">
  <link rel="stylesheet" href="<?= base_url('vendors/sweetalert2/sweetalert2.min.css') ?>">
  <script src="<?= base_url('vendors/sweetalert2/sweetalert2.min.js') ?>"></script>

  <!-- Datatables 5 -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.1/css/bootstrap.min.css">
  <!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap5.min.css"> -->
  <link rel="stylesheet" href="<?= base_url('dataTables/DataTables/css/dataTables.bootstrap5.min.css') ?>">

  <link rel="stylesheet" href="<?= base_url('js/jquery-ui/jquery-ui.css') ?>">

  <!-- endinject -->
  <!-- Plugin css for this page -->
  <link rel="stylesheet" href="<?= base_url('js/select.dataTables.min.css') ?>">
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?= base_url('css/vertical-layout-light/style.css') ?>">
  <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous"> -->
  <!-- endinject -->
  <link rel="shortcut icon" href="<?= base_url('images/favicon.png') ?>" />
</head>
<body>
  <div class="container-scroller"> 
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex align-items-top flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-start">
        <div class="me-3">
          <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-bs-toggle="minimize">
            <span class="icon-menu"></span>
          </button>
        </div>
        <div>
          <a class="navbar-brand brand-logo" href="<?= base_url('/') ?>">
            <img src="<?= base_url('images/logoManrope.png') ?>" alt="logo" />
          </a>
          <!-- <a class="navbar-brand brand-logo-mini" href="<?= base_url('/') ?>">
            <img src="<?= base_url('images/logo.png') ?>" alt="logo" />
          </a> -->
        </div>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-top"> 
        <ul class="navbar-nav">
          <li class="nav-item font-weight-semibold d-none d-lg-block ms-0">
            <h1 class="welcome-text"><?= session()->get('pesan') ?>, <span class="text-black fw-bold"><?= session()->get('fullname') ?></span></h1>
            <h3 class="welcome-sub-text">Inventory Management System </h3>
          </li>
        </ul>
        <ul class="navbar-nav ms-auto">
          <li class="nav-item d-none d-lg-block">
            <div id="datepicker-popup" class="input-group date datepicker navbar-date-picker">
              <span class="input-group-addon input-group-prepend border-right">
                <span class="icon-calendar input-group-text calendar-icon"></span>
              </span>
              <input type="text" class="form-control">
            </div>
          </li>
          <li class="nav-item dropdown"> 
            <a class="nav-link count-indicator" id="countDropdown" href="#" data-bs-toggle="dropdown" aria-expanded="false">
              <i class="icon-bell"></i>
              <span class="count"></span>
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list pb-0" aria-labelledby="countDropdown" style="overflow: auto; height: 300px">
              <div class="dropdown-divider"></div>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <img src="<?= base_url('/images/faces/face10.jpg') ?>" alt="image" class="img-sm profile-pic">
                </div>
                <div class="preview-item-content flex-grow py-2">
                  <p class="preview-subject ellipsis font-weight-medium text-dark">Marian Garner </p>
                  <p class="fw-light small-text mb-0"> The meeting is cancelled </p>
                </div>
              </a>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <img src="<?= base_url('/images/faces/face12.jpg') ?>" alt="image" class="img-sm profile-pic">
                </div>
                <div class="preview-item-content flex-grow py-2">
                  <p class="preview-subject ellipsis font-weight-medium text-dark">David Grey </p>
                  <p class="fw-light small-text mb-0"> The meeting is cancelled </p>
                </div>
              </a>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <img src="<?= base_url('/images/faces/face1.jpg') ?>" alt="image" class="img-sm profile-pic">
                </div>
                <div class="preview-item-content flex-grow py-2">
                  <p class="preview-subject ellipsis font-weight-medium text-dark">Travis Jenkins </p>
                  <p class="fw-light small-text mb-0"> The meeting is cancelled </p>
                </div>
              </a>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <img src="<?= base_url('/images/faces/face1.jpg') ?>" alt="image" class="img-sm profile-pic">
                </div>
                <div class="preview-item-content flex-grow py-2">
                  <p class="preview-subject ellipsis font-weight-medium text-dark">Travis Jenkins </p>
                  <p class="fw-light small-text mb-0"> The meeting is cancelled </p>
                </div>
              </a>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <img src="<?= base_url('/images/faces/face1.jpg') ?>" alt="image" class="img-sm profile-pic">
                </div>
                <div class="preview-item-content flex-grow py-2">
                  <p class="preview-subject ellipsis font-weight-medium text-dark">Travis Jenkins </p>
                  <p class="fw-light small-text mb-0"> The meeting is cancelled </p>
                </div>
              </a>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <img src="<?= base_url('/images/faces/face1.jpg') ?>" alt="image" class="img-sm profile-pic">
                </div>
                <div class="preview-item-content flex-grow py-2">
                  <p class="preview-subject ellipsis font-weight-medium text-dark">Travis Jenkins </p>
                  <p class="fw-light small-text mb-0"> The meeting is cancelled </p>
                </div>
              </a>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <img src="<?= base_url('/images/faces/face1.jpg') ?>" alt="image" class="img-sm profile-pic">
                </div>
                <div class="preview-item-content flex-grow py-2">
                  <p class="preview-subject ellipsis font-weight-medium text-dark">Travis Jenkins </p>
                  <p class="fw-light small-text mb-0"> The meeting is cancelled </p>
                </div>
              </a>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <img src="<?= base_url('/images/faces/face1.jpg') ?>" alt="image" class="img-sm profile-pic">
                </div>
                <div class="preview-item-content flex-grow py-2">
                  <p class="preview-subject ellipsis font-weight-medium text-dark">Travis Jenkins </p>
                  <p class="fw-light small-text mb-0"> The meeting is cancelled </p>
                </div>
              </a>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <img src="<?= base_url('/images/faces/face1.jpg') ?>" alt="image" class="img-sm profile-pic">
                </div>
                <div class="preview-item-content flex-grow py-2">
                  <p class="preview-subject ellipsis font-weight-medium text-dark">Travis Jenkins </p>
                  <p class="fw-light small-text mb-0"> The meeting is cancelled </p>
                </div>
              </a>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <img src="<?= base_url('/images/faces/face1.jpg') ?>" alt="image" class="img-sm profile-pic">
                </div>
                <div class="preview-item-content flex-grow py-2">
                  <p class="preview-subject ellipsis font-weight-medium text-dark">Travis Jenkins </p>
                  <p class="fw-light small-text mb-0"> The meeting is cancelled </p>
                </div>
              </a>
            </div>
          </li>
          <li class="nav-item dropdown d-none d-lg-block user-dropdown">
            <a class="nav-link" id="UserDropdown" href="#" data-bs-toggle="dropdown" aria-expanded="false">
              <?php if(session()->get('level') == 1) : ?>
                <img class="img-xs rounded-circle" src="<?= base_url('images/superadmin.jpg') ?>" alt="Profile image"> </a>
              <?php else : ?>
                <img class="img-xs rounded-circle" src="<?= base_url('images/user.jpg') ?>" alt="Profile image"> </a>
              <?php endif; ?>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
              <div class="dropdown-header text-center">
                <?php if(session()->get('level') == 1) : ?>
                  <img class="img-xs rounded-circle" src="<?= base_url('images/superadmin.jpg') ?>" alt="Profile image" style="width: 100px; height: 100px"> </a>
                <?php else : ?>
                  <img class="img-xs rounded-circle" src="<?= base_url('images/user.jpg') ?>" alt="Profile image"> </a>
                <?php endif; ?>
                <p class="mb-1 mt-3 font-weight-semibold"><?= session()->get('fullname') ?></p>
                <p class="fw-light text-muted mb-0"><?= session()->get('email') ?></p>
              </div>
              <!-- <a class="dropdown-item"><i class="dropdown-item-icon mdi mdi-account-outline text-primary me-2"></i> My Profile <span class="badge badge-pill badge-danger">1</span></a> -->
              <a class="dropdown-item" data-toggle="modal" data-target="#modalChangePassword"><i class="dropdown-item-icon mdi mdi-message-text-outline text-primary me-2"></i> Change Password</a>
              <a class="dropdown-item" href="<?= base_url('/signOut') ?>"><i class="dropdown-item-icon mdi mdi-power text-primary me-2"></i>Sign Out</a>
            </div>
          </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-bs-toggle="offcanvas">
          <span class="mdi mdi-menu"></span>
        </button>
      </div>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_settings-panel.html -->
      <div class="theme-setting-wrapper">
        <div id="settings-trigger"><i class="ti-settings"></i></div>
        <div id="theme-settings" class="settings-panel">
          <i class="settings-close ti-close"></i>
          <p class="settings-heading">SIDEBAR SKINS</p>
          <div class="sidebar-bg-options selected" id="sidebar-light-theme"><div class="img-ss rounded-circle bg-light border me-3"></div>Light</div>
          <div class="sidebar-bg-options" id="sidebar-dark-theme"><div class="img-ss rounded-circle bg-dark border me-3"></div>Dark</div>
          <p class="settings-heading mt-2">HEADER SKINS</p>
          <div class="color-tiles mx-0 px-4">
            <div class="tiles success"></div>
            <div class="tiles warning"></div>
            <div class="tiles danger"></div>
            <div class="tiles info"></div>
            <div class="tiles dark"></div>
            <div class="tiles default"></div>
          </div>
        </div>
      </div>
      <!-- partial -->
      <!-- partial:partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" style="margin-top: 20px" id="sidebar">
        <ul class="nav">
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url('/warehouse/dashboard') ?>">
              <i class="mdi mdi-home menu-icon"></i>
              <span class="menu-title">Dashboard</span>
            </a>
          </li>
          <li class="nav-item nav-category">Master</li>
          <li class="nav-item">
            <a class="nav-link" data-bs-toggle="collapse" href="#master" aria-expanded="false" aria-controls="master">
              <i class="menu-icon mdi mdi-floor-plan"></i>
              <span class="menu-title">Inventory Master</span>
              <i class="menu-arrow"></i> 
            </a>
            <div class="collapse" id="master">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="<?= base_url('main/products') ?>">Products</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?= base_url('main/assembly') ?>">Assembly Products</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?= base_url('main/customer') ?>">Customer</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?= base_url('main/vendor') ?>">Vendor</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?= base_url('main/jobOrder') ?>">Job Order</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?= base_url('main/receivementReport') ?>">Receivement Report</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?= base_url('main/masterPlan') ?>">Master Plan</a></li>
              </ul>
            </div>
          </li>
          <li class="nav-item nav-category">Warehouse</li>
          <li class="nav-item">
            <a class="nav-link" data-bs-toggle="collapse" href="#available" aria-expanded="false" aria-controls="ui-basic">
              <i class="menu-icon mdi mdi-floor-plan"></i>
              <span class="menu-title">Stock Available</span>
              <i class="menu-arrow"></i> 
            </a>
            <div class="collapse" id="available">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="<?= base_url('warehouse/products') ?>">Products Availability</a></li>
              </ul>
            </div>
          </li>
          <?php if(session()->get('level') <= 2) : ?>
          <li class="nav-item">
            <a class="nav-link" data-bs-toggle="collapse" href="#adjustment" aria-expanded="false" aria-controls="ui-basic">
              <i class="menu-icon mdi mdi-floor-plan"></i>
              <span class="menu-title">Adjustment</span>
              <i class="menu-arrow"></i> 
            </a>
            <div class="collapse" id="adjustment">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="<?= base_url('warehouse/stockAdjustment') ?>">Stock Adjustment</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?= base_url('warehouse/stockAdjustmentLog') ?>">Stock Adjustment Log</a></li>
              </ul>
            </div>
          </li>
          <?php endif; ?>
          <li class="nav-item">
            <a class="nav-link" data-bs-toggle="collapse" href="#withdrawal" aria-expanded="false" aria-controls="ui-basic">
              <i class="menu-icon mdi mdi-floor-plan"></i>
              <span class="menu-title">Withdrawal</span>
              <i class="menu-arrow"></i> 
            </a>
            <div class="collapse" id="withdrawal">
              <ul class="nav flex-column sub-menu">
                <?php if(session()->get('level') <= 4 && session()->get('level') != 3) : ?>
                <li class="nav-item"> <a class="nav-link" href="<?= base_url('warehouse/withdrawal') ?>">Withdrawal Request</a></li>
                <?php endif; ?>
                <?php if(session()->get('level') <= 4) : ?>
                <li class="nav-item"> <a class="nav-link" href="<?= base_url('warehouse/withdrawalProcess') ?>">Withdrawal Process</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?= base_url('warehouse/withdrawalHistory') ?>">Withdrawal History</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?= base_url('warehouse/withdrawalFinish') ?>">Withdrawal Finish</a></li>
                <?php endif; ?>
              </ul>
            </div>
          </li>
          <?php if(session()->get('level') <= 3) : ?>
          <li class="nav-item nav-category">Management</li>
          <li class="nav-item">
            <a class="nav-link" data-bs-toggle="collapse" href="#management" aria-expanded="false" aria-controls="ui-basic">
              <i class="menu-icon mdi mdi-floor-plan"></i>
              <span class="menu-title">Administrator</span>
              <i class="menu-arrow"></i> 
            </a>
            <div class="collapse" id="management">
              <ul class="nav flex-column sub-menu">
                <?php if(session()->get('level') == 1 ) : ?>
                <li class="nav-item"> <a class="nav-link" href="<?= base_url('management/userManagement') ?>">User Management</a></li>
                <?php endif; ?>
                <?php if(session()->get('level') <= 3 ) : ?>
                  <li class="nav-item"> <a class="nav-link" href="<?= base_url('management/userRequest') ?>">User Request</a></li>
                <?php endif; ?>
                <?php if(session()->get('level') <= 1 ) : ?>
                  <li class="nav-item"> <a class="nav-link" href="<?= base_url('management/otherManagement') ?>">Other Management</a></li>
                <?php endif; ?>
              </ul>
            </div>
          </li>
          <?php endif; ?>
          <?php if(session()->get('level') <= 2 || (session()->get('level') <= 5 && session()->get('dept') == 9) || (session()->get('level') <= 5 && session()->get('dept') == 7)) : ?>
          <li class="nav-item nav-category">Manufacture</li>
          <li class="nav-item">
            <a class="nav-link" data-bs-toggle="collapse" href="#pde" aria-expanded="false" aria-controls="ui-basic">
              <i class="menu-icon mdi mdi-floor-plan"></i>
              <span class="menu-title">Assembly</span>
              <i class="menu-arrow"></i> 
            </a>
            <div class="collapse" id="pde">
              <ul class="nav flex-column sub-menu">
                <?php if(session()->get('level') <= 2 || (session()->get('level') <= 5 && session()->get('dept') == 9)) : ?>
                  <li class="nav-item"> <a class="nav-link" href="<?= base_url('pde/bom') ?>">Build of Materials</a></li>
                <?php endif; ?>
                <?php if(session()->get('level') <= 2 || session()->get('level') <= 5 && session()->get('dept') == 7) : ?>
                  <li class="nav-item"> <a class="nav-link" href="<?= base_url('pde/buildAssembly') ?>">Build Assembly</a></li>
                  <li class="nav-item"> <a class="nav-link" href="<?= base_url('pde/processAssembly') ?>">Process Assembly</a></li>
                  <li class="nav-item"> <a class="nav-link" href="<?= base_url('pde/finishAssembly') ?>">Finish Assembly</a></li>
                <?php endif; ?>
              </ul>
            </div>
          </li>
          <?php endif; ?>
          <?php if(session()->get('level') <= 2 ) : ?>
          <li class="nav-item nav-category">Logistics</li>
          <li class="nav-item">
            <a class="nav-link" data-bs-toggle="collapse" href="#log" aria-expanded="false" aria-controls="ui-basic">
              <i class="menu-icon mdi mdi-floor-plan"></i>
              <span class="menu-title">History</span>
              <i class="menu-arrow"></i> 
            </a>
            <div class="collapse" id="log">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="<?= base_url('log/productsLog') ?>">Products Log</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?= base_url('log/assemblyLog') ?>">Assembly Log</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?= base_url('log/customerLog') ?>">Customer Log</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?= base_url('log/vendorLog') ?>">Vendor Log</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?= base_url('log/joLog') ?>">Job Order Log</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?= base_url('log/rrLog') ?>">Receivement Report Log</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?= base_url('log/withdrawalLog') ?>">Withdrawal Log</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?= base_url('log/bomLog') ?>">Build of Material Log</a></li>
              </ul>
            </div>
          </li>
          <?php endif; ?>
        </ul>
      </nav>
      <!-- partial -->
      <div class="main-panel" id="mainPanel">
        <div class="content-wrapper">
          <div class="row">
            <div class="col-sm-12">
              <div class="card">
                  <div class="card-body">

                    <?= $this->renderSection('content'); ?>

                  </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Premium <a href="https://www.bootstrapdash.com/" target="_blank">Bootstrap admin template</a> from BootstrapDash.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Copyright © 2021. All rights reserved.</span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  <script src="<?= base_url('vendors/js/vendor.bundle.base.js') ?>"></script>
  <!-- endinject -->
  <!-- Plugin js for this page -->
  <script src="<?= base_url('vendors/chart.js/Chart.min.js') ?>"></script>
  <script src="<?= base_url('vendors/bootstrap-datepicker/bootstrap-datepicker.min.js') ?>"></script>
  <script src="<?= base_url('vendors/progressbar.js/progressbar.min.js') ?>"></script>

  <!-- End plugin js for this page -->
  <!-- inject:js -->
  <script src="<?= base_url('js/off-canvas.js') ?>"></script>
  <script src="<?= base_url('js/hoverable-collapse.js') ?>"></script>
  <script src="<?= base_url('js/template.js') ?>"></script>
  <script src="<?= base_url('js/settings.js') ?>"></script>
  <script src="<?= base_url('js/todolist.js') ?>"></script>
  <!-- tambhan ini kalo mau add file upload input -->
  <script src="<?= base_url('js/file-upload.js') ?>"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="<?= base_url('js/dashboard.js') ?>"></script>
  <script src="<?= base_url('js/Chart.roundedBarCharts.js') ?>"></script>
  <!-- End custom js for this page-->
  
  <!-- Datatables 5 -->
  <!-- kalo pakai jquery 3.5.1 error toUpperCase jadi aku tambah di bawahnya yg versi 1.12.4 -->
  <!-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script> -->
  <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
  <!-- <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script> -->
  <!-- <script src="https://code.jquery.com/jquery-1.12.4.js" integrity="sha256-Qw82+bXyGq6MydymqBxNPYTaUXXq7c8v3CwiYwLLNXU=" crossorigin="anonymous"></script> -->
  <script src="<?= base_url('js/jquery-ui/jquery-ui.min.js') ?>"></script>
  <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap5.min.js"></script>
  <!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script> -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script> 
  
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
  
  <script>
    function triggerSelect2() {
      $('#addUserDeptHandle').select2({
        dropdownParent: $('#modalAddUser .modal-content'),
        width: '100%',
      });
      // jika ada validasi error maka return deptHandler di multiple select
      <?php if($validation->listErrors()) : ?>
        $('#addUserDeptHandle').val([<?= session()->getFlashdata('deptHandle') ?>]);
        $('#addUserDeptHandle').trigger('change');
      <?php endif; ?>
      $('#editUserDeptHandle').select2({
        dropdownParent: $('#modalEditUser .modal-content'),
        width: '100%',
      });
      $('#approveUserDeptHandle').select2({
        dropdownParent: $('#modalApproveUser .modal-content'),
        width: '100%',
      });
    }

    // activate input for update password
    function updatePass() {
      $('#editPassword').removeAttr('readonly');
      $('#editPasswordConfirmation').removeAttr('readonly');
    }

    // stock adjustment
    // pakai onfocus data tidak dinamic, yg masuk cuma yg pertama doang
    $(document).on('focus','.autocomplete_text-sa', handleAutocompleteSa);
    $(document).on('focus','.autocomplete_text-ws', handleAutocompleteWs);
    $(document).on('focus','.autocomplete_text-ws-customer', handleAutocompleteWsCustomer);
    $(document).on('focus','.autocomplete_text-ws-jo', handleAutocompleteWsJo);

    $(document).on('focus','.buildAssemblyCode', handleBuildAssembly);
    $(document).on('focus','.buildJoNumber', handleBuildJo);
    $(document).on('change','#addFeature', addFeature);

    // $(document).on('submit','#searchBom', testAjax);

    function getId(element){
      let id, idArr;
      id = element.attr('id');
      idArr = id.split("_");
      return idArr[idArr.length - 1];
    }

    function handleAutocompleteSa() {

      var currentEle, idArr, rowNo;
      currentEle = $(this);
	  console.log(currentEle);

      $(".autocomplete_text-sa").autocomplete({
        // currentEle.autocomplete({
          source: function(request, cb){
              console.log(request);
              
              $.ajax({
                  url: '<?= base_url('/warehouse/c_stockAdjustment/getTerm') ?>',
                  dataType: 'json',
                  method: 'POST',
                  data: {
                    name: request.term
                  },
                  success: function(res){
                      var result;
                      result = [
                          {
                              label: 'There is no matching record found for '+request.term,
                              value: ''
                          }
                      ];

                      console.log("Before format", res);
                      

                      if (res.length) {
                          result = $.map(res, function(obj){
                              return {
                                  label: obj.product_code + ' - ' + obj.whs_name,
                                  value: obj.product_code,
                                  data : obj
                              };
                          });
                      }

                      console.log("formatted response", result);
                      cb(result);
                  }
              });
          },
          minLength: 1,
          select: function( event, selectedData ) {
              console.log(selectedData);
              let resArr, rowNo;
            
              console.log('currentEle' + currentEle);
              rowNo = getId(currentEle);
              // resArr = selectedData.item.data.split("|");

              if (selectedData && selectedData.item && selectedData.item.data){
                  var data = selectedData.item.data;

                  $('#productName_'+rowNo).val(data.product_name);
                  $('#description_'+rowNo).val(data.product_description);
                  $('#uom_'+rowNo).val(data.product_uom);
                  $('#defLocHidden_'+rowNo).val(data.whs_code);
                  $('#defLoc_'+rowNo).val(data.whs_name);
                  $('#stock_'+rowNo).val(data.stock_quantity);
              }
              
          }  
      });  
    };

    function handleAutocompleteWs() {

      var currentEle, idArr, rowNo;
      currentEle = $(this);

      $(".autocomplete_text-ws").autocomplete({
        // currentEle.autocomplete({
          source: function(request, cb){
              console.log(request);
              
              $.ajax({
                  url: '<?= base_url('/warehouse/c_withdrawal/getTerm') ?>',
                  dataType: 'json',
                  method: 'POST',
                  data: {
                    name: request.term
                  },
                  success: function(res){
                      var result;
                      result = [
                          {
                              label: 'There is no matching record found for '+request.term,
                              value: ''
                          }
                      ];

                      console.log("Before format", res);  

                      if (res.length) {
                          result = $.map(res, function(obj){
                              return {
                                  label: obj.product_code + ' - ' + obj.whs_name,
                                  value: obj.product_code,
                                  data : obj,
                              };
                          });
                      }

                      console.log("formatted response", result);
                      cb(result);
                  }
              });
          },
          minLength: 1,
          select: function( event, selectedData ) {
              console.log(selectedData);
              let resArr, rowNo;
            
              
              rowNo = getId(currentEle);
              // resArr = selectedData.item.data.split("|");

              if (selectedData && selectedData.item && selectedData.item.data){
                  var data = selectedData.item.data;

                  $('#wsProductName_'+rowNo).val(data.product_name);
                  $('#wsDescription_'+rowNo).val(data.product_description);
                  $('#wsUom_'+rowNo).val(data.product_uom);
                  $('#wsStockAvailable_'+rowNo).val(data.stock_quantity);
                  $('#wsDefLocHidden_'+rowNo).val(data.whs_code);
                  $('#wsDefLoc_'+rowNo).val(data.whs_name);
              }
              
          }  
      });  
    };

    function handleAutocompleteWsCustomer() {

      var currentEle, idArr, rowNo;
      currentEle = $(this);

      $(".autocomplete_text-ws-customer").autocomplete({
        // currentEle.autocomplete({
          source: function(request, cb){
              console.log(request);
              
              $.ajax({
                  url: '<?= base_url('/warehouse/c_withdrawal/getTermCustomer') ?>',
                  dataType: 'json',
                  method: 'POST',
                  data: {
                    nameCustomer: request.term
                  },
                  success: function(res){
                      var result;
                      result = [
                          {
                              label: 'There is no matching record found for '+request.term,
                              value: ''
                          }
                      ];

                      console.log("Before format", res);  

                      if (res.length) {
                          result = $.map(res, function(obj){
                              return {
                                  label: obj.customer_agency,
                                  value: obj.customer_agency,
                                  data : obj,
                              };
                          });
                      }

                      console.log("formatted response", result);
                      cb(result);
                  }
              });
          },
          minLength: 1,
          select: function( event, selectedData ) {
              console.log(selectedData);
              let resArr, rowNo;
            
              
              rowNo = getId(currentEle);
              // resArr = selectedData.item.data.split("|");

              if (selectedData && selectedData.item && selectedData.item.data){
                  var data = selectedData.item.data;

                  $('#hiddenCustomerCode_'+rowNo).val(data.customer_code);
              }
              
          }  
      });  
    };

    function handleAutocompleteWsJo() {

      var currentEle, idArr, rowNo;
      currentEle = $(this);

      $(".autocomplete_text-ws-jo").autocomplete({
        // currentEle.autocomplete({
          source: function(request, cb){
              console.log(request);

              let customer = $('#hiddenCustomerCode_'+getId(currentEle)).val();
              let productCode = $('#wsProductCode_'+getId(currentEle)).val();
              
              $.ajax({
                  url: '<?= base_url('/warehouse/c_withdrawal/getTermJo') ?>',
                  dataType: 'json',
                  method: 'POST',
                  data: {
                    nameJo: request.term,
                    nameCustomer: customer,
                    nameProduct: productCode
                  },
                  success: function(res){
                      var result;
                      result = [
                          {
                              label: 'There is no matching record found for '+request.term,
                              value: ''
                          }
                      ];

                      console.log("Before format", res);  

                      if (res.length) {
                          result = $.map(res, function(obj){
                              return {
                                  label: obj.jo_number + ' - ' + obj.jo_assembly_code,
                                  value: obj.jo_number,
                                  data : obj,
                              };
                          });
                      }

                      console.log("formatted response", result);
                      cb(result);
                  }
              });
          },
          minLength: 1,
          select: function( event, selectedData ) {
              console.log(selectedData);
              let resArr, rowNo;
            
              
              rowNo = getId(currentEle);
              // resArr = selectedData.item.data.split("|");

              if (selectedData && selectedData.item && selectedData.item.data){
                  var data = selectedData.item.data;

                  $('#wsAssemblyProduct_'+rowNo).val(data.jo_assembly_code);
                  $('#wsBomQuantity_'+rowNo).val(data.bom_quantity_based_jo);
              }
              
          }  
      });  
      };

    function handleBuildAssembly() {

      var currentEle, idArr, rowNo;
      currentEle = $(this);

      $(".buildAssemblyCode").autocomplete({
        // currentEle.autocomplete({
          source: function(request, cb){
              console.log(request);
              let dataJo = $('#buildJoNumber').val();
              $.ajax({
                  url: '<?= base_url('/pde/c_buildAssembly/getTermAssembly') ?>',
                  dataType: 'json',
                  method: 'POST',
                  data: {
                    assemblyCode: request.term,
                    joNumber: dataJo
                  },
                  success: function(res){
                      var result;
                      result = [
                          {
                              label: 'There is no matching record found for '+request.term,
                              value: ''
                          }
                      ];

                      console.log("Before format", res);  

                      if (res.length) {
                          result = $.map(res, function(obj){
                              return {
                                  label: obj.jo_assembly_code + ' - ' + obj.assembly_name,
                                  value: obj.jo_assembly_code,
                                  data : obj,
                              };
                          });
                      }

                      console.log("formatted response", result);
                      cb(result);
                  }
              });
          }
      });  
    };

    function handleBuildJo() {

      var currentEle, idArr, rowNo;
      currentEle = $(this);

      $(".buildJoNumber").autocomplete({
        // currentEle.autocomplete({
          source: function(request, cb){
              console.log(request);
              
              $.ajax({
                  url: '<?= base_url('/pde/c_buildAssembly/getTermJo') ?>',
                  dataType: 'json',
                  method: 'POST',
                  data: {
                    joNumber: request.term
                  },
                  success: function(res){
                      var result;
                      result = [
                          {
                              label: 'There is no matching record found for '+request.term,
                              value: ''
                          }
                      ];

                      console.log("Before format", res);  

                      if (res.length) {
                          result = $.map(res, function(obj){
                              return {
                                  label: obj.jo_number,
                                  value: obj.jo_number,
                                  data : obj,
                              };
                          });
                      }

                      console.log("formatted response", result);
                      cb(result);
                  }
              });
          } 
      });  
    };

    // saerch build assembly
    $("#searchBom").submit(function(e){
      $.ajax({
        type:"POST",
        dataType: 'json',
        data: $('#searchBom').serialize(),
        url:'<?= base_url('/pde/c_buildAssembly/getDataForAssembly') ?>',
        success: function(data){
            let totalCost = 0;

            // jika masih ada class first "serach first" maka kalo data kosong rewrite pake html ke data not found
            if($("#tabel_build_assembly").hasClass("first")) {
              if(data.length > 0) {

                $('#joQty').val(data[0]['jo_quantity']);
                $('#joBalanceQty').val(data[0]['jo_balance_quantity']);
                $("#buildAssemblyCodeHidden").val(data[0].jo_assembly_code);
                $("#buildJoNumberHidden").val(data[0].jo_number);
                $('#processQty').attr('required');

                for(let i=0; i<data.length; i++) {
                  let row = $('<tr><td>' + (i + 1) + '</td><td>' + data[i].product_code + '</td><td style="display: none;"><input class="form-control" id="buildProductCode_'+i+'" name="buildProductCode[]" type="hidden"></td><td>' + data[i].product_name + '</td><td>' + data[i].product_description + '</td><td>' + data[i].product_type + '</td><td>' + data[i].product_uom + '</td><td>' + data[i].product_unit_price + '</td><td>' + data[i].quantity_for_pr + '</td><td>' + data[i].quantity_main + '</td><td>' + data[i].quantity_production + '</td><td>' + data[i].bom_quantity_needed + '</td></tr>');              
                  $('#tabel_build_assembly').append(row);
                  totalCost = parseFloat(totalCost) + parseFloat(data[i].product_unit_price);
                  $('#tabel_build_assembly .first').remove()
                  // isi value yg input
                  $("#buildProductCode_" + i).val(data[i].product_code);
                }
              } else {
                $('#joQty').val('0');
                $('#tabel_build_assembly .first').html('Data not found...');
              }
              $('#totalCost').val(totalCost);
            } else {
              // clear dulu
              $('#tabel_build_assembly tbody tr').remove()

              if(data.length > 0) {

                $('#joQty').val(data[0]['jo_quantity']);
                $('#joBalanceQty').val(data[0]['jo_balance_quantity']);
                $("#buildAssemblyCodeHidden").val(data[0].jo_assembly_code);
                $("#buildJoNumberHidden").val(data[0].jo_number);
                $('#processQty').removeAttr('readonly', true);
                $('#processQty').attr('required', true);
                $("#processQty").val('');

                for(let i=0; i<data.length; i++) {
                  let row = $('<tr><td>' + (i + 1) + '</td><td>' + data[i].product_code + '</td><td style="display: none;"><input class="form-control" id="buildProductCode_'+i+'" name="buildProductCode[]" type="hidden"></td><td>' + data[i].product_name + '</td><td>' + data[i].product_description + '</td><td>' + data[i].product_type + '</td><td>' + data[i].product_uom + '</td><td>' + data[i].product_unit_price + '</td><td>' + data[i].quantity_for_pr + '</td><td>' + data[i].quantity_main + '</td><td>' + data[i].quantity_production + '</td><td>' + data[i].bom_quantity_needed + '</td></tr>');              
                  $('#tabel_build_assembly').append(row);
                  totalCost = parseFloat(totalCost) + parseFloat(data[i].product_unit_price);
                  $('#tabel_build_assembly .first').remove()
                  // isi value yg input
                  $("#buildProductCode_" + i).val(data[i].product_code);
                }
              } else {
                $('#joQty').val('0');
                $("#buildAssemblyCodeHidden").val('');
                $("#buildJoNumberHidden").val('');
                $('#processQty').removeAttr('required', true);
                $('#processQty').attr('readonly', true);
                $("#processQty").val('');
                $("#tabel_build_assembly").append("<tr id='row'><td colspan='11' class='text-center mt-3 first'>Data not found...</td></tr>");
              }
              $('#totalCost').val(totalCost);
            }
          }
      });
      e.preventDefault();
      // // or
      return false;
    });
    
    function addFeature() {
      // other management add feature
        var feature = $('#addFeature').val();
        $.ajax({
          dataType: 'json',
          url:'<?= base_url('/management/c_otherManagement/getDataFeature') ?>',
          method: 'POST',
          data:  "feature="+feature,
          success: function(data){
            if(feature == 'currency') {
              // clear dulu
              $('#tabel_otherManagement tbody tr').remove();
              for(let i=0; i<data.length; i++) {
                let row = $('<tr><td class="text-center">' + (i + 1) + '</td><td class="text-center">' + data[i].currency_id + '</td><td class="text-center">' + data[i].currency_name + '</td><td class="text-center"><a href="#" data-bs-toggle="modal" data-bs-target="#modalEditFeature" id="editCurrencyFeature" data-id="'+ data[i].currency_id +'" data-name="'+ data[i].currency_name +'" data-judul="Currency" class="text-decoration-none text-warning far fa-edit fa-1x fa-fw"></a><a href="#" data-bs-toggle="modal" data-bs-target="#modalDelete" data-id="'+ data[i].currency_id +'" id="deleteItemCurrencyFeature" class="text-decoration-none text-danger fas fa-trash-alt fa-1x fa-fw"></a></td></tr>');              
                $('#tabel_otherManagement').append(row);
              }
            } else if(feature == 'multiple_uom') {
              // clear dulu
              $('#tabel_otherManagement tbody tr').remove();
              for(let i=0; i<data.length; i++) {
                let row = $('<tr><td class="text-center">' + (i + 1) + '</td><td class="text-center">' + data[i].multiple_uom_id + '</td><td class="text-center">' + data[i].multiple_uom_name + '</td><td class="text-center"><a href="#" data-bs-toggle="modal" data-bs-target="#modalEditFeature" id="editMultipleUomFeature" data-id="'+ data[i].multiple_uom_id +'" data-name="'+ data[i].multiple_uom_name +'" data-judul="Multiple Uom" class="text-decoration-none text-warning far fa-edit fa-1x fa-fw"></a><a href="#" data-bs-toggle="modal" data-bs-target="#modalDelete" data-id="' + data[i].multiple_uom_id + '" id="deleteItemMultipleUomFeature" class="text-decoration-none text-danger fas fa-trash-alt fa-1x fa-fw"></a></td></tr>');              
                $('#tabel_otherManagement').append(row);
              }
            } else if(feature == 'uom_schema') {
              // clear dulu
              $('#tabel_otherManagement tbody tr').remove();
              for(let i=0; i<data.length; i++) {
                let row = $('<tr><td class="text-center">' + (i + 1) + '</td><td class="text-center">' + data[i].uom_schema_id + '</td><td class="text-center">' + data[i].uom_schema_name + '</td><td class="text-center"><a href="#" data-bs-toggle="modal" data-bs-target="#modalEditFeature" id="editUomSchemaFeature" data-id="'+ data[i].uom_schema_id +'" data-name="'+ data[i].uom_schema_name +'" data-judul="Uom Schema" class="text-decoration-none text-warning far fa-edit fa-1x fa-fw"></a><a href="#" data-bs-toggle="modal" data-bs-target="#modalDelete" data-id="' + data[i].uom_schema_id + '" id="deleteItemUomSchemaFeature" class="text-decoration-none text-danger fas fa-trash-alt fa-1x fa-fw"></a></td></tr>');              
                $('#tabel_otherManagement').append(row);
              }
            } else if(feature == 'reason') {
              // clear dulu
              $('#tabel_otherManagement tbody tr').remove();
              for(let i=0; i<data.length; i++) {
                let row = $('<tr><td class="text-center">' + (i + 1) + '</td><td class="text-center">' + data[i].reason_code + '</td><td class="text-center">' + data[i].reason_name + '</td><td class="text-center"><a href="#" data-bs-toggle="modal" data-bs-target="#modalEditFeature" id="editReasonFeature" data-id="'+ data[i].reason_code +'" data-name="'+ data[i].reason_name +'" data-judul="Reason" class="text-decoration-none text-warning far fa-edit fa-1x fa-fw"></a><a href="#" data-bs-toggle="modal" data-bs-target="#modalDelete" data-id="' + data[i].reason_code + '" id="deleteItemReasonFeature" class="text-decoration-none text-danger fas fa-trash-alt fa-1x fa-fw"></a></td></tr>');              
                $('#tabel_otherManagement').append(row);
              }
            } else if(feature == 'department') {
              // clear dulu
              $('#tabel_otherManagement tbody tr').remove();
              for(let i=0; i<data.length; i++) {
                let row = $('<tr><td class="text-center">' + (i + 1) + '</td><td class="text-center">' + data[i].dept_id + '</td><td class="text-center">' + data[i].dept_name + '</td><td style="' + (data[i].dept_id <= 12 ? "pointer-events: none" : "") + '" class="text-center"><a href="#" data-bs-toggle="modal" data-bs-target="#modalEditFeature" id="editDeptFeature" data-id="'+ data[i].dept_id +'" data-name="'+ data[i].dept_name +'" data-judul="Department" class="' + (data[i].dept_id <= 12 ? "text-muted" : "") + ' text-decoration-none text-warning far fa-edit fa-1x fa-fw"></a><a href="#" data-bs-toggle="modal" data-bs-target="#modalDelete" data-id="' + data[i].dept_id + '" id="deleteItemDeptFeature" class="' + (data[i].dept_id <= 12 ? "text-muted" : "") + ' text-decoration-none text-danger fas fa-trash-alt fa-1x fa-fw"></a></td></tr>');              
                $('#tabel_otherManagement').append(row);
              }
            } else if(feature == 'whs') {
              // clear dulu
              $('#tabel_otherManagement tbody tr').remove();
              for(let i=0; i<data.length; i++) {
                let row = $('<tr><td class="text-center">' + (i + 1) + '</td><td class="text-center">' + data[i].whs_code + '</td><td class="text-center">' + data[i].whs_name + '</td><td style="' + (data[i].whs_code <= 4 ? "pointer-events: none" : "") + '" class="text-center"><a href="#" data-bs-toggle="modal" data-bs-target="#modalEditFeature" id="editWhsFeature" data-id="'+ data[i].whs_code +'" data-name="'+ data[i].whs_name +'" data-judul="Warehouse" class="' + (data[i].whs_code <= 4 ? "text-muted" : "") + ' text-decoration-none text-warning far fa-edit fa-1x fa-fw"></a><a href="#" data-bs-toggle="modal" data-bs-target="#modalDelete" data-id="' + data[i].whs_code + '" id="deleteItemWhsFeature" class="' + (data[i].whs_code <= 4 ? "text-muted" : "") + ' text-decoration-none text-danger fas fa-trash-alt fa-1x fa-fw"></a></td></tr>');              
                $('#tabel_otherManagement').append(row);
              }
            } else if(feature == 'status') {
              // clear dulu
              $('#tabel_otherManagement tbody tr').remove();
              for(let i=0; i<data.length; i++) {
                let row = $('<tr><td class="text-center">' + (i + 1) + '</td><td class="text-center">' + data[i].status_id + '</td><td class="text-center">' + data[i].status_name + '</td><td style="' + (data[i].status_id <= 10 ? "pointer-events: none" : "") + '" class="text-center"><a href="#" data-bs-toggle="modal" data-bs-target="#modalEditFeature" id="editStatusFeature" data-id="'+ data[i].status_id +'" data-name="'+ data[i].status_name +'" data-judul="Status" class="' + (data[i].status_id <= 10 ? "text-muted" : "") + ' text-decoration-none text-warning far fa-edit fa-1x fa-fw"></a><a href="#" data-bs-toggle="modal" data-bs-target="#modalDelete" data-id="' + data[i].status_id + '" id="deleteItemStatusFeature" class="' + (data[i].status_id <= 10 ? "text-muted" : "") + ' text-decoration-none text-danger fas fa-trash-alt fa-1x fa-fw"></a></td></tr>');              
                $('#tabel_otherManagement').append(row);
              }
            }
          }
        });
        // // or
        return false;
    };

    // tabel dataTable warehouse product
    $(document).ready(function() {  
      let table = $('#tabel_product').DataTable({
          "processing": true,
          "serverSide": true,
          'scrollX': true,
          "order": [],
          "ajax": {
              "url": "<?php echo base_url('/warehouse/c_products/ajaxList') ?>",
              "type": "POST",
          },
          "columnDefs": [{
              "targets": [2,3,4],
              "orderable": false,
          }, ],
      });
    });

    // tabel dataTable main products
    $(document).ready(function() {
      let table = $('#tabel_main_product').DataTable({
          "processing": true,
          "serverSide": true,
          'scrollX': true,
          "order": [],
          "ajax": {
              "url": "<?php echo base_url('/main/c_products/ajaxList') ?>",
              "type": "POST",
          },
          "columnDefs": [{
              "targets": [2,3,4,5],
              "orderable": false,
          }, ],
      });
    });

    // tabel dataTable dashboard overbudget
    $(document).ready(function() {  
      let table = $('#tabel_overbudget').DataTable({
          "processing": true,
          "serverSide": true,
          'scrollX': true,
          "order": [],
          "ajax": {
              "url": "<?php echo base_url('/main/c_masterPlan/ajaxList') ?>",
              "type": "POST",
          },
          "columnDefs": [{
              "targets": [0,3,4,5,6],
              "orderable": false,
          }, ],
      });
    });

    // tabel dataTable main products Log
    $(document).ready(function() {  
      let table = $('#tabel_main_productLog').DataTable({
          "processing": true,
          "serverSide": true,
          'scrollX': true,
          "order": [],
          "ajax": {
              "url": "<?php echo base_url('/main/c_productsLog/ajaxList') ?>",
              "type": "POST",
          },
          "columnDefs": [{
              "targets": [2,3,4,5],
              "orderable": false,
          }, ],
      });
    });

    // tabel dataTable main assembly
    $(document).ready(function() {  
      let table = $('#tabel_main_assembly').DataTable({
          "processing": true,
          "serverSide": true,
          'scrollX': true,
          "order": [],
          "ajax": {
              "url": "<?php echo base_url('/main/c_assembly/ajaxList') ?>",
              "type": "POST",
          },
          "columnDefs": [{
              "targets": [2,3,4,5],
              "orderable": false,
          }, ],
      });
    });

    // tabel dataTable main customer
    $(document).ready(function() {  
      let table = $('#tabel_main_customer').DataTable({
          "processing": true,
          "serverSide": true,
          'scrollX': true,
          "order": [],
          "ajax": {
              "url": "<?php echo base_url('/main/c_customer/ajaxList') ?>",
              "type": "POST",
          },
          "columnDefs": [{
              "targets": [2,4,5,7,8,9,10],
              "orderable": false,
          }, ],
      });
    });

    // tabel dataTable main vendor
    $(document).ready(function() {  
      let table = $('#tabel_main_vendor').DataTable({
          "processing": true,
          "serverSide": true,
          'scrollX': true,
          "order": [],
          "ajax": {
              "url": "<?php echo base_url('/main/c_vendor/ajaxList') ?>",
              "type": "POST",
          },
          "columnDefs": [{
              "targets": [2,4,5,7,8,9,10],
              "orderable": false,
          }, ],
      });
    });

    // tabel dataTable main job order
    $(document).ready(function() {  
      let table = $('#tabel_main_jo').DataTable({
          "processing": true,
          "serverSide": true,
          'scrollX': true,
          "order": [],
          "ajax": {
              "url": "<?php echo base_url('/main/c_jobOrder/ajaxList') ?>",
              "type": "POST",
              'data': function(data){
                data.fromDate = $('#fromDateJO').val();
                data.toDate = $('#toDateJO').val();
              }
          },
          "columnDefs": [{
              "targets": [2],
              "orderable": false,
          }, ],
      });

      $('#toDateJO').change(function(){
        table.draw();
      });

    });

    // tabel dataTable main master plan
    $(document).ready(function() {  
      let table = $('#tabel_master_plan').DataTable({
          "processing": true,
          "serverSide": true,
          'scrollX': true,
          // agar filtered from ... total entris tetep kosong soalnya ngga work disini (khusus)
          "language": {
            "infoFiltered": '',
          },
          "order": [],
          "ajax": {
            "url": "<?php echo base_url('/main/c_masterPlan/ajaxList') ?>",
            "type": "POST",
          },
          "columnDefs": [{
              "targets": [2,3,4,5,6,7],
              "orderable": false,
          }, ],
      });
    });

    // tabel dataTable main rr
    $(document).ready(function() {  
      let table = $('#tabel_main_rr').DataTable({
          "processing": true,
          "serverSide": true,
          'scrollX': true,
          "order": [],
          "ajax": {
              "url": "<?php echo base_url('/main/c_receivementReport/ajaxList') ?>",
              "type": "POST",
              'data': function(data){
                data.fromDate = $('#fromDateRR').val();
                data.toDate = $('#toDateRR').val();
              }
          },
          "columnDefs": [{
              "targets": [3,5],
              "orderable": false,
          }, ],
      });
      
      $('#toDateRR').change(function(){
        table.draw();
      });
    });

    // tabel dataTable user management
    $(document).ready(function() {  
      let table = $('#tabel_user_management').DataTable({
          "processing": true,
          "serverSide": true,
          'scrollX': true,
          "order": [],
          "ajax": {
              "url": "<?php echo base_url('/management/c_userManagement/ajaxList') ?>",
              "type": "POST",
          },
          "columnDefs": [{
              "targets": [1,4,5],
              "orderable": false,
          }, ],
      });
    });

    // tabel dataTable user request
    $(document).ready(function() {  
      let table = $('#tabel_user_request').DataTable({
          "processing": true,
          "serverSide": true,
          'scrollX': true,
          "order": [],
          "ajax": {
              "url": "<?php echo base_url('/management/c_userRequest/ajaxList') ?>",
              "type": "POST",
          },
          "columnDefs": [{
              "targets": [1,4,5],
              "orderable": false,
          }, ],
      });
    });

    // tabel dataTable products log
    $(document).ready(function() {  
      let table = $('#tabel_productsLog').DataTable({
          "processing": true,
          "serverSide": true,
          'scrollX': true,
          "order": [],
          "ajax": {
              "url": "<?php echo base_url('/log/c_productsLog/ajaxList') ?>",
              "type": "POST",
          },
          "columnDefs": [{
              "targets": [0,1,2,3,4,5,6,7,8,9,10,11,12,13],
              "orderable": false,
          }, ],
      });
    });

    // tabel dataTable assembly log
    $(document).ready(function() {  
      let table = $('#tabel_assemblyLog').DataTable({
          "processing": true,
          "serverSide": true,
          'scrollX': true,
          "order": [],
          "ajax": {
              "url": "<?php echo base_url('/log/c_assemblyLog/ajaxList') ?>",
              "type": "POST",
          },
          "columnDefs": [{
              "targets": [0,1,2,3,4,5,6,7,8,9,10,11,12,13],
              "orderable": false,
          }, ],
      });
    });

    // tabel dataTable customer log
    $(document).ready(function() {  
      let table = $('#tabel_customerLog').DataTable({
          "processing": true,
          "serverSide": true,
          'scrollX': true,
          "order": [],
          "ajax": {
              "url": "<?php echo base_url('/log/c_customerLog/ajaxList') ?>",
              "type": "POST",
          },
          "columnDefs": [{
              "targets": [0,1,2,3,4,5,6,7,8,9,10,11,12],
              "orderable": false,
          }, ],
      });
    });

    // tabel dataTable vendor log
    $(document).ready(function() {  
      let table = $('#tabel_vendorLog').DataTable({
          "processing": true,
          "serverSide": true,
          'scrollX': true,
          "order": [],
          "ajax": {
              "url": "<?php echo base_url('/log/c_vendorLog/ajaxList') ?>",
              "type": "POST",
          },
          "columnDefs": [{
              "targets": [0,1,2,3,4,5,6,7,8,9,10,11,12],
              "orderable": false,
          }, ],
      });
    });

    // tabel dataTable job order log
    $(document).ready(function() {  
      let table = $('#tabel_joLog').DataTable({
          "processing": true,
          "serverSide": true,
          'scrollX': true,
          "order": [],
          "ajax": {
              "url": "<?php echo base_url('/log/c_joLog/ajaxList') ?>",
              "type": "POST",
          },
          "columnDefs": [{
              "targets": [0,1,2,3,4,5,6,7,8,9,10,11,12,13],
              "orderable": false,
          }, ],
      });
    });

    // tabel dataTable rr log
    $(document).ready(function() {  
      let table = $('#tabel_rrLog').DataTable({
          "processing": true,
          "serverSide": true,
          'scrollX': true,
          "order": [],
          "ajax": {
              "url": "<?php echo base_url('/log/c_rrLog/ajaxList') ?>",
              "type": "POST",
          },
          "columnDefs": [{
              "targets": [0,1,2,3,4,5,6,7,8],
              "orderable": false,
          }, ],
      });
    });

    // tabel dataTable withdrawal log
    $(document).ready(function() {  
      let table = $('#tabel_withdrawalLog').DataTable({
          "processing": true,
          "serverSide": true,
          'scrollX': true,
          "order": [],
          "ajax": {
              "url": "<?php echo base_url('/log/c_withdrawalLog/ajaxList') ?>",
              "type": "POST",
          },
          "columnDefs": [{
              "targets": [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17],
              "orderable": false,
          }, ],
      });
    });

    // tabel dataTable bom log
    $(document).ready(function() {  
      let table = $('#tabel_bomLog').DataTable({
          "processing": true,
          "serverSide": true,
          'scrollX': true,
          "order": [],
          "ajax": {
              "url": "<?php echo base_url('/log/c_bomLog/ajaxList') ?>",
              "type": "POST",
          },
          "columnDefs": [{
              "targets": [0,1,2,3,4,5,6,7,8,9],
              "orderable": false,
          }, ],
      });
    });

    // tabel dataTable withdrawal process server side
    $(document).ready(function() {  
      let table = $('#tabel_withdrawalProcess').DataTable({
          "processing": true,
          "serverSide": true,
          'scrollX': true,
          // agar filtered from ... total entris tetep kosong soalnya ngga work disini (khusus)
          "language": {
            "infoFiltered": '',
          },
          "order": [],
          "ajax": {
              "url": "<?php echo base_url('/warehouse/c_withdrawalProcess/ajaxList') ?>",
              "type": "POST"
          },
          "columnDefs": [{
              "targets": [1,4,6,7],
              "orderable": false,
          }, ],
      });
    });

    // tabel dataTable withdrawal history server side
    $(document).ready(function() {  

      let table = $('#tabel_withdrawalHistory').DataTable({
          "processing": true,
          "serverSide": true,
          'scrollX': true,
          "order": [],
          "ajax": {
              "url": "<?php echo base_url('/warehouse/c_withdrawalHistory/ajaxList') ?>",
              "type": "POST",
              'data': function(data){
                data.fromDate = $('#fromDateWithdrawalHistory').val();
                data.toDate = $('#toDateWithdrawalHistory').val();
              }
          },
          "columnDefs": [{
              "targets": [3,4,6,7],
              "orderable": false,
          }, ],
      });

      $('#toDateWithdrawalHistory').change(function(){
        table.draw();
      });

    });

    // tabel dataTable withdrawal finish server side
    $(document).ready(function() {  
      // $('#tabel_withdrawalFinish thead.1 th').each(function () {
      //     var title = $(this).text();
      //     $(this).html(title+' <input type="text" class="col-search-input" placeholder="Search ' + title + '" />');
      // });

      let table = $('#tabel_withdrawalFinish').DataTable({
          "processing": true,
          "serverSide": true,
          // 'scrollX': true,
          // agar filtered from ... total entris tetep kosong soalnya ngga work disini (khusus)
          "language": {
            "infoFiltered": '',
          },
          "order": [],
          "ajax": {
              "url": "<?php echo base_url('/warehouse/c_withdrawalFinish/ajaxList') ?>",
              "type": "POST"
          },
          "columnDefs": [{
              "targets": [1,3,4,5],
              "orderable": false,
          }, ],
      });
         
      // table.columns().every(function () {
      //       var table = this;
      //       $('input', this.header()).on('keyup change', function () {
      //           if (table.search() !== this.value) {
      //           	   table.search(this.value).draw();
      //           }
      //       });
      //   });
    });

    // tabel dataTable stock adjustment log server side
    $(document).ready(function() {  
      let table = $('#tabel_stockAdjustmentLog').DataTable({
          "processing": true,
          "serverSide": true,
          'scrollX': true,
          "order": [],
          "ajax": {
              "url": "<?php echo base_url('/warehouse/c_stockAdjustmentLog/ajaxList') ?>",
              "type": "POST",
              'data': function(data){
                data.fromDate = $('#fromDateAdjustmentLog').val();
                data.toDate = $('#toDateAdjustmentLog').val();
              }
          },
          "columnDefs": [{
              "targets": [1,5,7,8,9],
              "orderable": false,
          }, ],
      });
      
      $('#toDateAdjustmentLog').change(function(){
        table.draw();
      });

    });

    // tabel dataTable bom
    $(document).ready(function() {  
      let table = $('#tabel_bom').DataTable({
          "processing": true,
          "serverSide": true,
          'scrollX': true,
          "order": [],
          "ajax": {
              "url": "<?php echo base_url('/pde/c_bom/ajaxList') ?>",
              "type": "POST"
          },
          "columnDefs": [{
              "targets": [1,4,5,6,7,8,9],
              "orderable": false,
          }, ],
      });
    });

    // tabel dataTable process assembly
    $(document).ready(function() {  
      let table = $('#tabel_process_assembly').DataTable({
          "processing": true,
          "serverSide": true,
          'scrollX': true,
          // agar filtered from ... total entris tetep kosong soalnya ngga work disini (khusus)
          "language": {
            "infoFiltered": '',
          },
          "order": [],
          "ajax": {
              "url": "<?php echo base_url('/pde/c_processAssembly/ajaxList') ?>",
              "type": "POST"
          },
          "columnDefs": [{
              "targets": [1,6,7,8,9],
              "orderable": false,
          }, ],
      });
    });

    // tabel dataTable finish assembly
    $(document).ready(function() {  
      let table = $('#tabel_finish_assembly').DataTable({
          "processing": true,
          "serverSide": true,
          'scrollX': true,
          // agar filtered from ... total entris tetep kosong soalnya ngga work disini (khusus)
          "language": {
            "infoFiltered": '',
          },
          "order": [],
          "ajax": {
              "url": "<?php echo base_url('/pde/c_finishAssembly/ajaxList') ?>",
              "type": "POST"
          },
          "columnDefs": [{
              "targets": [5,6,7],
              "orderable": false,
          }, ],
      });
    });

    // modals withdrawal process biasa
    function modalWithdrawalProcess() {
      $('#tabel_withdrawalProcess tbody').on('click','.modalWithdrawalProcess', function() {
        var wdId = $(this).data('id');
        var wdNumber = $(this).data('wd_number');
        var wdProductCode = $(this).data('p_code');
        var wdJoNumber = $(this).data('jo_number');
        var wdReqQty = $(this).data('req_qty');
        var wdBomQty = $(this).data('bom_qty');
        var wdFromWh = $(this).data('wd_from_wh');
        var wdToWh = $(this).data('wd_to_wh');
        var wdProductName = $(this).data('wd_product_name');
        var wdStockAvailable = $(this).data('wd_stock_available');
        let wdRemark = $(this).data('wd_remark');
        var wdReceiptQuantity = $(this).data('receipt');
        var wdRemarkWh = $(this).data('remark_wh');

        // var link = '/Asset/deleteData';
        $('#wdId').val(wdId);
        $('#wdNumber').val(wdNumber);
        $('#wdProductCode').val(wdProductCode);
        $('#wdJoNumber').val(wdJoNumber);
        $('#wdReqQty').val(wdReqQty);
        $('#wdBomQty').val(wdBomQty);
        $('#wdFromWh').val(wdFromWh);
        $('#wdToWh').val(wdToWh);
        $('#wdProductName').val(wdProductName);
        $('#wdStockAvailable').val(wdStockAvailable);
        $('#wdRemark').val(wdRemark);
        $('#wdReceiptQty').val(wdReceiptQuantity);
        $('#wdRemarkWh').val(wdRemarkWh);
        // $('#modalDelete form').attr('action', link);
      });
    }

    // modals withdrawal process biasa buat head wh soalnya kalo biasa (yg atas dipake) ntar bisa receipt lagi dan issue lagi
    function modalWithdrawalProcessHeadWh() {
      $('#tabel_withdrawalProcess tbody').on('click','.modalWithdrawalProcessHeadWh', function() {
        var wdId = $(this).data('id');
        var wdNumber = $(this).data('wd_number');
        var wdProductCode = $(this).data('p_code');
        var wdJoNumber = $(this).data('jo_number');
        var wdReqQty = $(this).data('req_qty');
        var wdBomQty = $(this).data('bom_qty');
        var wdFromWh = $(this).data('wd_from_wh');
        var wdToWh = $(this).data('wd_to_wh');
        var wdProductName = $(this).data('wd_product_name');
        var wdStockAvailable = $(this).data('wd_stock_available');
        let wdRemark = $(this).data('wd_remark');
        var wdReceiptQuantity = $(this).data('receipt');
        var wdRemarkWh = $(this).data('remark_wh');

        // var link = '/Asset/deleteData';
        $('#wdIdHeadWh').val(wdId);
        $('#wdNumberHeadWh').val(wdNumber);
        $('#wdProductCodeHeadWh').val(wdProductCode);
        $('#wdJoNumberHeadWh').val(wdJoNumber);
        $('#wdReqQtyHeadWh').val(wdReqQty);
        $('#wdBomQtyHeadWh').val(wdBomQty);
        $('#wdFromWhHeadWh').val(wdFromWh);
        $('#wdToWhHeadWh').val(wdToWh);
        $('#wdProductNameHeadWh').val(wdProductName);
        $('#wdStockAvailableHeadWh').val(wdStockAvailable);
        $('#wdRemarkHeadWh').val(wdRemark);
        $('#wdReceiptQtyHeadWh').val(wdReceiptQuantity);
        $('#wdRemarkWhHeadWh').val(wdRemarkWh);
        // $('#modalDelete form').attr('action', link);
      });
    }

    // add withdrawal and stock adjustment
    $(document).ready(function(){      
      let i=1;  

      $('#add-sa').click(function(){  
           i++;             
           $('tbody').append('<tr id="row'+i+'"><td>'+i+'</td><td><input style="padding-left: 12px" type="text" class="form-control autocomplete_text-sa <?= $validation->hasError('productCode') ? 'is-invalid' : '' ?>" name="productCode[]" id="productCode_'+i+'" placeholder="Product Code..." required><div class="invalid-feedback"><?= $validation->getError('productCode') ?></div></td><td><input style="padding-left: 12px" type="text" class="form-control productName_'+i+'" name="productName[]" id="productName_'+i+'" placeholder="Product Name..." readonly></td><td><input style="padding-left: 12px" type="text" class="form-control description_'+i+'" name="description[]" id="description_'+i+'" placeholder="Description..." readonly></td><td><input style="padding-left: 12px" type="text" class="form-control uom_'+i+'" id="uom_'+i+'" name="uom[]" placeholder="UoM..." readonly></td><td><select class="form-control form-select text-dark <?= $validation->hasError('adjType') ? 'is-invalid' : '' ?>" name="adjType[]" required><div class="invalid-feedback"><?= $validation->getError('adjType') ?></div><option disabled selected value="">Choose...</option><option value="1">Stock In</option><option value="2">Stock Out</option></select></td><td><input style="padding-left: 12px; width: 200px" type="hidden" class="form-control defLocHidden_'+i+'" name="defLoc[]" id="defLocHidden_'+i+'" placeholder="Warehouse Loc..." readonly><input style="padding-left: 12px" type="text" class="form-control defLoc_'+i+'" id="defLoc_'+i+'" placeholder="Warehouse Loc..." readonly></td><td><input style="padding-left: 12px; width: 100px" type="text" class="form-control stock_'+i+'" name="stock[]" id="stock_'+i+'" placeholder="Available..." readonly></td><td><input style="padding-left: 12px" type="text" class="form-control qty_'+i+'  <?= $validation->hasError('qty') ? 'is-invalid' : '' ?>" name="qty[]" id="qty_'+i+'" placeholder="Quantity..." required><div class="invalid-feedback"><?= $validation->getError('qty') ?></div></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove mt-2 text-white">Del</button></td></tr>');
      });

      

      $('#add-ws').click(function(){  
           i++;             
           $('tbody').append('<tr id="row'+i+'"><td>'+i+'</td><td><input style="padding-left: 12px; width: 250px" type="text" class="form-control autocomplete_text-ws <?= $validation->hasError('wsProductCode') ? 'is-invalid' : '' ?>" name="wsProductCode[]" id="wsProductCode_'+i+'" placeholder="Product/Assembly Code..." autocomplete="off" required><div class="invalid-feedback mt-2"><?= $validation->getError('wsProductCode') ?></div></td><td><input style="padding-left: 12px; width: 300px" type="text" class="form-control" name="wsProductName[]" id="wsProductName_'+i+'" placeholder="Product Name..." autocomplete="off" readonly></td><td><input style="padding-left: 12px; width: 300px" type="text" class="form-control" name="wsDescription[]" id="wsDescription_'+i+'" placeholder="Description..." autocomplete="off" readonly></td><td><input style="padding-left: 12px; width: 70px" type="text" class="form-control" name="wsUom[]" id="wsUom_'+i+'" placeholder="UoM..." autocomplete="off" readonly></td><td><input style="padding-left: 12px; width: 120px" type="text" class="form-control" name="wsStockAvailable[]" id="wsStockAvailable_'+i+'" placeholder="Stock Available..." autocomplete="off" readonly></td><td><input style="padding-left: 12px; width: 200px" type="hidden" class="form-control wsDefLocHidden_'+i+'" name="wsDefLoc[]" id="wsDefLocHidden_'+i+'" placeholder="Warehouse Loc..." readonly><input style="padding-left: 12px; width: 200px" type="text" class="form-control" id="wsDefLoc_'+i+'" placeholder="Warehouse Loc..." autocomplete="off" readonly></td><td><input style="padding-left: 12px; width: 200px" type="text" class="form-control autocomplete_text-ws-customer <?= $validation->hasError('wsCustomer') ? 'is-invalid' : '' ?>" name="wsCustomer[]" id="wsCustomer_'+i+'" placeholder="Customer..." autocomplete="off"><div class="invalid-feedback mt-2"><?= $validation->getError('wsCustomer') ?></div></td><td style="display: none"><input style="padding-left: 12px; width: 200px" type="hidden" class="form-control hiddenCustomerCode" id="hiddenCustomerCode_'+i+'" placeholder="Customer Code..." autocomplete="off"></td><td><input style="padding-left: 12px; width: 200px" type="text" class="form-control autocomplete_text-ws-jo <?= $validation->hasError('wsJoNumber') ? 'is-invalid' : '' ?>" name="wsJoNumber[]" id="wsJoNumber_'+i+'" placeholder="Search JO..." autocomplete="off"><div class="invalid-feedback mt-2"><?= $validation->getError('wsJoNumber') ?></div></td><td><input style="padding-left: 12px; width: 200px" type="text" class="form-control" name="wsAssemblyProduct[]" id="wsAssemblyProduct_'+i+'" placeholder="Assembly Product..." autocomplete="off" readonly></td><td><input style="padding-left: 12px; width: 150px" type="text" class="form-control" name="wsBomQuantity[]" id="wsBomQuantity_'+i+'" placeholder="BOM Quantity..." autocomplete="off" readonly></td><td><input style="padding-left: 12px; width: 150px" type="number" step="any" class="form-control <?= $validation->hasError('wsRequestQty') ? 'is-invalid' : '' ?>" name="wsRequestQty[]" id="wsRequestQty_'+i+'" placeholder="Request Quantity..." autocomplete="off" required><div class="invalid-feedback mt-2"><?= $validation->getError('wsRequestQty') ?></div></td><td><input style="padding-left: 12px; width: 300px" type="text" class="form-control <?= $validation->hasError('wsRemark') ? 'is-invalid' : '' ?>" name="wsRemark[]" id="wsRemark'+i+'" placeholder="Remark..." autocomplete="off" required><div class="invalid-feedback mt-2"><?= $validation->getError('wsRemark') ?></div></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove mt-2 text-white">Del</button></td></tr>');
      });
     
      $(document).on('click', '.btn_remove', function(){  
        let button_id = $(this).attr("id"); 
        let res = confirm('Are You Sure You Want To Delete This?');
        if(res==true){
        $('#row'+button_id+'').remove();  
        $('#'+button_id+'').remove();  
        }
      }); 
    });

    // edit data product on modals
    $('#tabel_main_product tbody').on('click', '#editProducts', function() {
      let productId   = $(this).data('id');
      let productCode = $(this).data('code');
      let productType = $(this).data('type');
      let productName = $(this).data('name');
      let decsription = $(this).data('desc');
      let uom         = $(this).data('uom');
      let mUom        = $(this).data('muom');
      let uomSchema   = $(this).data('uoms');
      let currency    = $(this).data('currency');
      let status      = $(this).data('status');
      let unitPrice   = $(this).data('uprice');
      let valuation   = $(this).data('valuation');
      let category   = $(this).data('category');
      let importCode  = $(this).data('import');

      $('#editProductId').val(productId);
      $('#editProductCode').val(productCode);
      $('#editProductType').val(productType);
      $('#editProductName').val(productName);
      $('#editProductDescription').val(decsription);
      $('#editProductUom').val(uom);
      $('#editProductMultipleUom').val(mUom);
      $('#editProductUomSchema').val(uomSchema);
      $('#editProductStatus').val(status);
      $('#editProductCurrency').val(currency);
      $('#editProductUnitPrice').val(unitPrice);
      $('#editProductValuation').val(valuation);
      $('#editProductCategory').val(category);
      $('#editImportCode').val(importCode);
    });

    // edit data assembly product on modals
    $('#tabel_main_assembly tbody').on('click', '#editAssembly', function() {
      let assemblyId    = $(this).data('id');
      let assemblyCode  = $(this).data('code');
      let assemblyType  = $(this).data('type');
      let assemblyName  = $(this).data('name');
      let decsription   = $(this).data('desc');
      let uom           = $(this).data('uom');
      let currency      = $(this).data('currency');
      let whs           = $(this).data('whs');
      let status        = $(this).data('status');
      let unitPrice     = $(this).data('uprice');
      let itemSpec      = $(this).data('item_spec');
      let package       = $(this).data('package');
      let category      = $(this).data('category');
      let importCode    = $(this).data('import');

      $('#editAssemblyId').val(assemblyId);
      $('#editAssemblyCode').val(assemblyCode);
      $('#editAssemblyType').val(assemblyType);
      $('#editAssemblyName').val(assemblyName);
      $('#editAssemblyDescription').val(decsription);
      $('#editAssemblyUom').val(uom);
      $('#editAssemblyStatus').val(status);
      $('#editAssemblyCurrency').val(currency);
      $('#editAssemblyUnitPrice').val(unitPrice);
      $('#editAssemblyWhs').val(whs);
      $('#editAssemblyCategory').val(category);
      $('#editAssemblyPackage').val(package);
      $('#editAssemblyItemSpec').val(itemSpec);
      $('#editAssemblyImportCode').val(importCode);
    });

    // edit data customer on modals
    $('#tabel_main_customer tbody').on('click', '#editCustomer', function() {

      let customerId    = $(this).data('id');
      let customerCode  = $(this).data('code');
      let title         = $(this).data('title');
      let agency        = $(this).data('agency');
      let address       = $(this).data('address');
      let email         = $(this).data('email');
      let name          = $(this).data('name');
      let importCode    = $(this).data('test');
      let telp          = $(this).data('telp');
      let bankName      = $(this).data('bank_name');
      let bankAccount   = $(this).data('bank_account');
      let bankNumber    = $(this).data('bank_number');
      let creditTerm    = $(this).data('credit_term');
      let status        = $(this).data('status');

      $('#editCustomerId').val(customerId);
      $('#editCustomerCode').val(customerCode);
      $('#editCustomerTitle').val(title);
      $('#editCustomerAgency').val(agency);
      $('#editCustomerAddress').val(address);
      $('#editCustomerEmail').val(email);
      $('#editCustomerName').val(name);
      $('#editCustomerBankName').val(bankName);
      $('#editCustomerBankAccount').val(bankAccount);
      $('#editCustomerBankNumber').val(bankNumber);
      $('#editCustomerTelp').val(telp);
      $('#editCustomerCreditTerm').val(creditTerm);
      $('#editCustomerStatus').val(status);
      $('#editCustomerImportCode').val(importCode);

    });

    // edit data vendor on modals
    $('#tabel_main_vendor tbody').on('click', '#editVendor', function() {

      let vendorId      = $(this).data('id');
      let vendorCode    = $(this).data('code');
      let title         = $(this).data('title');
      let agency        = $(this).data('agency');
      let address       = $(this).data('address');
      let email         = $(this).data('email');
      let name          = $(this).data('name');
      let importCode    = $(this).data('import');
      let telp          = $(this).data('telp');
      let bankName      = $(this).data('bank_name');
      let bankAccount   = $(this).data('bank_account');
      let bankNumber    = $(this).data('bank_number');
      let debitTerm     = $(this).data('debit_term');
      let status        = $(this).data('status');

      $('#editVendorId').val(vendorId);
      $('#editVendorCode').val(vendorCode);
      $('#editVendorTitle').val(title);
      $('#editVendorAgency').val(agency);
      $('#editVendorAddress').val(address);
      $('#editVendorEmail').val(email);
      $('#editVendorName').val(name);
      $('#editVendorBankName').val(bankName);
      $('#editVendorBankAccount').val(bankAccount);
      $('#editVendorBankNumber').val(bankNumber);
      $('#editVendorTelp').val(telp);
      $('#editVendorDebitTerm').val(debitTerm);
      $('#editVendorStatus').val(status);
      $('#editVendorImportCode').val(importCode);

    });

    // edit data bom on modals
    $('#tabel_bom tbody').on('click', '#editBom', function() {

      let bomId      = $(this).data('id');
      let bomType    = $(this).data('type');
      let bomAssembly = $(this).data('assembly');
      let bomProduct  = $(this).data('product');
      let bomQty      = $(this).data('qty');

      $('#editBomId').val(bomId);
      $('#editBomType').val(bomType);
      $('#editBomQuantityNeeded').val(bomQty);
      $('#editBomAssemblyCode').val(bomAssembly);
      $('#editBomProductCode').val(bomProduct);

    });

    // edit data user on modals
    $('#tabel_user_management tbody').on('click', '#editUser', function() {

      let userId      = $(this).data('id');
      let username    = $(this).data('user');
      let fullname    = $(this).data('fullname');
      let position    = $(this).data('position');
      let level       = $(this).data('level');
      let password    = $(this).data('password');
      let telp        = $(this).data('telp');
      let address     = $(this).data('address');
      let email       = $(this).data('email');
      let status      = $(this).data('status');
      let dept        = $(this).data('dept');
      let deptHandle  = $(this).data('dept_handle');


      // jika data dept handle lebih dari satu maka lakukan split biar datanya bisa masuk lebih dari satu dna ngga error
      if(deptHandle.length > 1) {
        deptHandle = deptHandle.split(",")
        depHandle = deptHandle.map(Number);
      }

      $('#editUserDeptHandle').val(deptHandle);
      $('#editUserDeptHandle').trigger('change');
      
      $('#editUserId').val(userId);
      $('#editUsername').val(username);
      $('#editUserLevel').val(level);
      $('#editFullname').val(fullname);
      // $('#editPasswordReal').val(password);
      $('#editUserDept').val(dept);
      $('#editUserPosition').val(position);
      $('#editUserEmail').val(email);
      $('#editUserDeptHandle').val(deptHandle);
      $('#editUserStatus').val(status);
      $('#editUserTelp').val(telp);
      $('#editUserAddress').val(address);
    });

    // edit data currency feature on modals
    $('#tabel_otherManagement tbody').on('click', '#editCurrencyFeature', function() {

      let id      = $(this).data('id');
      let name    = $(this).data('name');
      let judul   = $(this).data('judul');
      
      $('#editFeature').text('Edit ' + judul + ' here !');
      $('#editFeatureId').val(id);
      $('#editRealFeatureId').val(id);
      $('#editFeatureName').val(name);
      $('#editRealFeatureName').val(name);
      $('#editDataBaseFeature').val('currency');
    });

    // edit data multiple uom feature on modals
    $('#tabel_otherManagement tbody').on('click', '#editMultipleUomFeature', function() {

      let id      = $(this).data('id');
      let name    = $(this).data('name');
      let judul   = $(this).data('judul');
      
      $('#editFeature').text('Edit ' + judul + ' here !');
      $('#editFeatureId').val(id);
      $('#editRealFeatureId').val(id);
      $('#editFeatureName').val(name);
      $('#editRealFeatureName').val(name);
      $('#editDataBaseFeature').val('multiple_uom');
    });

    // edit data uom schema feature on modals
    $('#tabel_otherManagement tbody').on('click', '#editUomSchemaFeature', function() {

      let id      = $(this).data('id');
      let name    = $(this).data('name');
      let judul   = $(this).data('judul');
      
      $('#editFeature').text('Edit ' + judul + ' here !');
      $('#editFeatureId').val(id);
      $('#editRealFeatureId').val(id);
      $('#editFeatureName').val(name);
      $('#editRealFeatureName').val(name);
      $('#editDataBaseFeature').val('uom_schema');
    });

    // edit data reason feature on modals
    $('#tabel_otherManagement tbody').on('click', '#editReasonFeature', function() {

      let id      = $(this).data('id');
      let name    = $(this).data('name');
      let judul   = $(this).data('judul');
      
      $('#editFeature').text('Edit ' + judul + ' here !');
      $('#editFeatureId').val(id);
      $('#editRealFeatureId').val(id);
      $('#editFeatureName').val(name);
      $('#editRealFeatureName').val(name);
      $('#editDataBaseFeature').val('reason');
    });

    // edit data department feature on modals
    $('#tabel_otherManagement tbody').on('click', '#editDeptFeature', function() {

      let id      = $(this).data('id');
      let name    = $(this).data('name');
      let judul   = $(this).data('judul');
      
      $('#editFeature').text('Edit ' + judul + ' here !');
      $('#editFeatureId').val(id);
      $('#editRealFeatureId').val(id);
      $('#editFeatureName').val(name);
      $('#editRealFeatureName').val(name);
      $('#editDataBaseFeature').val('department');
    });

    // edit data whs feature on modals
    $('#tabel_otherManagement tbody').on('click', '#editWhsFeature', function() {

      let id      = $(this).data('id');
      let name    = $(this).data('name');
      let judul   = $(this).data('judul');
      
      $('#editFeature').text('Edit ' + judul + ' here !');
      $('#editFeatureId').val(id);
      $('#editRealFeatureId').val(id);
      $('#editFeatureName').val(name);
      $('#editRealFeatureName').val(name);
      $('#editDataBaseFeature').val('whs');
    });

    // edit data status feature on modals
    $('#tabel_otherManagement tbody').on('click', '#editStatusFeature', function() {

      let id      = $(this).data('id');
      let name    = $(this).data('name');
      let judul   = $(this).data('judul');
      
      $('#editFeature').text('Edit ' + judul + ' here !');
      $('#editFeatureId').val(id);
      $('#editRealFeatureId').val(id);
      $('#editFeatureName').val(name);
      $('#editRealFeatureName').val(name);
      $('#editDataBaseFeature').val('status');
    });

    // preveiw request user on modals
    $('#tabel_user_request tbody').on('click', '#approveUser', function() {

      let userId      = $(this).data('id');
      let username    = $(this).data('user');
      let fullname    = $(this).data('fullname');
      let position    = $(this).data('position');
      let level       = $(this).data('level');
      let telp        = $(this).data('telp');
      let address     = $(this).data('address');
      let email       = $(this).data('email');
      let status      = $(this).data('status');
      let dept        = $(this).data('dept');
      let deptHandle  = $(this).data('dept_handle');

      // jika data dept handle lebih dari satu maka lakukan split biar datanya bisa masuk lebih dari satu dan ngga error
      if(deptHandle.length > 1) {
        deptHandle = deptHandle.split(",")
        depHandle = deptHandle.map(Number);
      }

      $('#approveUserDeptHandle').val(deptHandle);
      $('#approveUserDeptHandle').trigger('change');
      
      $('#approveUserId').val(userId);
      $('#approveUsername').val(username);
      $('#approveUserLevel').val(level);
      $('#approveFullname').val(fullname);
      // $('#approvePasswordReal').val(password);
      $('#approveUserDept').val(dept);
      $('#approveUserPosition').val(position);
      $('#approveUserEmail').val(email);
      $('#approveUserDeptHandle').val(deptHandle);
      $('#approveUserStatus').val(status);
      $('#approveUserTelp').val(telp);
      $('#approveUserAddress').val(address);
    });

    // modals delete products
    $('#tabel_main_product tbody').on('click', '#deleteItemProducts', function() {
        let productId   = $(this).data('id');
        let importCode  = $(this).data('import');
        let link        = 'c_products/deleteProducts';
        $('#deleteId').val(productId);
        $('#importId').val(importCode);
        $('#modalDelete form').attr('action', link);
    });

    // modals delete customer
    $('#tabel_main_customer tbody').on('click', '#deleteItemCustomer', function() {
        let customerId   = $(this).data('id');
        let importCode  = $(this).data('import');
        let link        = 'c_customer/deleteCustomer';
        $('#deleteId').val(customerId);
        $('#importId').val(importCode);
        $('#modalDelete form').attr('action', link);
    });

    // modals delete vendor
    $('#tabel_main_vendor tbody').on('click', '#deleteItemVendor', function() {
        let vendorId   = $(this).data('id');
        let importCode  = $(this).data('import');
        let link        = 'c_vendor/deleteVendor';
        $('#deleteId').val(vendorId);
        $('#importId').val(importCode);
        $('#modalDelete form').attr('action', link);
    });

    // modals delete RR
    $('#tabel_main_rr tbody').on('click', '#deleteItemRR', function() {
        let rrId   = $(this).data('id');
        let importCode  = $(this).data('import');
        let link        = 'c_receivementReport/deleteRR';
        $('#deleteId').val(rrId);
        $('#importId').val(importCode);
        $('#modalDelete form').attr('action', link);
    });

    // modals delete JO
    $('#tabel_main_jo tbody').on('click', '#deleteItemJo', function() {
        let joId   = $(this).data('id');
        let importCode  = $(this).data('import');
        let link        = 'c_jobOrder/deleteJobOrder';
        $('#deleteId').val(joId);
        $('#importId').val(importCode);
        $('#modalDelete form').attr('action', link);
    });

    // modals delete User
    $('#tabel_user_management tbody').on('click', '#deleteUser', function() {
        let userId   = $(this).data('id');
        let link        = 'c_userManagement/deleteUser';
        $('#deleteId').val(userId);
        $('#modalDelete form').attr('action', link);
    });

    // modals delete bom
    $('#tabel_bom tbody').on('click', '#deleteBom', function() {
        let bomId   = $(this).data('id');
        let link    = 'c_bom/deleteBom';
        $('#deleteId').val(bomId);
        $('#modalDelete form').attr('action', link);
    });

    // modals delete assembly
    $('#tabel_main_assembly tbody').on('click', '#deleteItemAssembly', function() {
        let assemblyId   = $(this).data('id');
        let link         = 'c_assembly/deleteAssembly';
        $('#deleteId').val(assemblyId);
        $('#modalDelete form').attr('action', link);
    });

    // modals delete currency feature
    $('#tabel_otherManagement tbody').on('click', '#deleteItemCurrencyFeature', function() {
        let currencyId   = $(this).data('id');
        let database     = 'currency';
        let link         = 'c_otherManagement/deleteFeature';
        $('#deleteId').val(currencyId);
        $('#importId').val(database);
        $('#modalDelete form').attr('action', link);
    });

    // modals delete multiple uom feature
    $('#tabel_otherManagement tbody').on('click', '#deleteItemMultipleUomFeature', function() {
        let multipleUomId   = $(this).data('id');
        let database     = 'multiple_uom';
        let link         = 'c_otherManagement/deleteFeature';
        $('#deleteId').val(multipleUomId);
        $('#importId').val(database);
        $('#modalDelete form').attr('action', link);
    });

    // modals delete uom schema feature
    $('#tabel_otherManagement tbody').on('click', '#deleteItemUomSchemaFeature', function() {
        let uomSchemaId   = $(this).data('id');
        let database     = 'uom_schema';
        let link         = 'c_otherManagement/deleteFeature';
        $('#deleteId').val(uomSchemaId);
        $('#importId').val(database);
        $('#modalDelete form').attr('action', link);
    });

    // modals delete reason adjustment feature
    $('#tabel_otherManagement tbody').on('click', '#deleteItemReasonFeature', function() {
        let reasonId   = $(this).data('id');
        let database     = 'reason';
        let link         = 'c_otherManagement/deleteFeature';
        $('#deleteId').val(reasonId);
        $('#importId').val(database);
        $('#modalDelete form').attr('action', link);
    });

    // modals delete department feature
    $('#tabel_otherManagement tbody').on('click', '#deleteItemDeptFeature', function() {
        let deptId   = $(this).data('id');
        let database     = 'department';
        let link         = 'c_otherManagement/deleteFeature';
        $('#deleteId').val(deptId);
        $('#importId').val(database);
        $('#modalDelete form').attr('action', link);
    });

    // modals delete whs feature
    $('#tabel_otherManagement tbody').on('click', '#deleteItemWhsFeature', function() {
        let whsId   = $(this).data('id');
        let database     = 'whs';
        let link         = 'c_otherManagement/deleteFeature';
        $('#deleteId').val(whsId);
        $('#importId').val(database);
        $('#modalDelete form').attr('action', link);
    });

    // modals delete status feature
    $('#tabel_otherManagement tbody').on('click', '#deleteItemStatusFeature', function() {
        let statusId   = $(this).data('id');
        let database     = 'status';
        let link         = 'c_otherManagement/deleteFeature';
        $('#deleteId').val(statusId);
        $('#importId').val(database);
        $('#modalDelete form').attr('action', link);
    });

    // modals process User
    $('#tabel_user_request tbody').on('click', '#ApproveRejectUser', function() {
        let info   = $(this).data('info');
        let status = $(this).data('status');
        let userId = $(this).data('id');
        let level  = $(this).data('level');
        let link   = 'c_userRequest/processUser';
        $('#infoProcessUser').text('Are you sure ' + info + ' this user ?');
        $('#infoUser').val(info);
        $('#statusUser').val(status);
        $('#idUser').val(userId);
        $('#levelUser').val(level);
        $('#modalApproveRejectUser form').attr('action', link);
    });

    // modals process withdrawal 
    $('#tabel_withdrawalProcess tbody').on('click', '#approveRejectWithdrawal', function() {
        let info   = $(this).data('info');
        let status = $(this).data('status');
        let wdId = $(this).data('id');
        let productCode = $(this).data('p_code');
        let fromWhs = $(this).data('wd_from_wh');
        let toWhs = $(this).data('wd_to_wh');
        let receipt = $(this).data('receipt');
        let remarkWh = $(this).data('remark_wh');
        let link = 'c_withdrawalProcess/processWithdrawal';
        $('#infoProcessWithdrawal').text('Sure ' + info + ' this withdrawal ?');

        $('#infoWd').val(info);
        $('#statusWd').val(status);
        $('#withdrawalId').val(wdId);
        $('#productCodeWithdrawal').val(productCode);
        $('#fromWarehouseWithdrawal').val(fromWhs);
        $('#toWarehouseWithdrawal').val(toWhs);
        $('#receiptQty').val(receipt);
        $('#remarkWh').val(remarkWh);
        $('#modalApproveWithdrawal form').attr('action', link);
    });

    // jika overbudget return confirmation again
    $(document).ready(function() {
      $('#confirmApproveReject').submit(function(e) {
        e.preventDefault();

        $.ajax({
          type  : 'POST',
          url   : "<?= base_url('/warehouse/c_withdrawalProcess/statusOverbudget') ?>",
          data  : $(this).serialize(),
          dataType : 'json',
          success : function(response) {
            if(response.overbudget_message) {
              Swal.fire({
                showClass: {
                    popup: 'animate__animated animate__fadeInDown animate__faster'
                  },
                  hideClass: {
                    popup: 'animate__animated animate__fadeOutUp animate__faster'
                  },
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Approve !',
                title: 'Confirmation !',
                html: response.overbudget_message,
              }).then((result) => {
                if (result.isConfirmed) {
                  $.ajax({
                    type  : 'POST',
                    url   : "<?= base_url('/warehouse/c_withdrawalProcess/processWithdrawal') ?>",
                    data  : $('#confirmApproveReject').serialize(),
                    success: function(response) {
                      $('#modalApproveWithdrawal').modal('hide');
                      window.location.reload();
                    }
                  });
                }
              })
            } else {
              $.ajax({
                type  : 'POST',
                url   : "<?= base_url('/warehouse/c_withdrawalProcess/processWithdrawal') ?>",
                data  : $('#confirmApproveReject').serialize(),
                success: function(response) {
                  $('#modalApproveWithdrawal').modal('hide');
                  window.location.reload();
                }
              });             
            }
            
          },
          error : function(xhr, thrownError) {
            alert(xhr.status + "\n" + xhr.rensponseText + "\n" + thrownError);
          }
        })

        return false;
      })
    })

    // modals process assembly 
    $('#tabel_process_assembly tbody').on('click', '#approveRejectProcessAssembly', function() {
        let info   = $(this).data('info');
        let id     = $(this).data('id');
        let joNumber          = $(this).data('jo_number');
        let assemblyCode      = $(this).data('assembly_code');
        let productCode       = $(this).data('product_code');
        let balanceProcessQuantity   = $(this).data('balance_process_quantity');
        let productQuantity   = $(this).data('product_quantity');
        let link = 'c_processAssembly/processAssembly';
        $('#notifProcessAssembly').text('Sure ' + info + ' this item ?');

        $('#infoProcessAssembly').val(info);
        $('#idProcessAssembly').val(id);
        $('#joNumberProcessAssembly').val(joNumber);
        $('#assemblyCodeProcessAssembly').val(assemblyCode);
        $('#productCodeProcessAssembly').val(productCode);
        $('#balanceProcessQty').val(balanceProcessQuantity);
        $('#productQty').val(productQuantity);
        $('#modalProcessAssembly form').attr('action', link);
    });

    function disableClick(){
        document.onmousedown =function(event){
          if (event.button == 2) {
            alert("Nope, You can't right click on this page !");
            return false;
          }
        }
      }

    // ini useless setelah sudah ada function JORequiredBaseLoc (ini)
    function JORequired() {
      let jo, index;
      jo = document.getElementsByClassName("autocomplete_text-ws-jo");
      customer = document.getElementsByClassName("autocomplete_text-ws-customer");
      for (index = 0; index < jo.length; ++index) {
        if(document.getElementById('wsWithJo').value == 1) {
          jo[index].setAttribute("required","");
          customer[index].setAttribute("required","");
        } else {
          jo[index].removeAttribute("required","");
          customer[index].removeAttribute("required","");
        }
      }
    }

    function JORequiredBaseLoc() {
      if($('.wsToWhs').val() != 3) {
        $('#wsWithJo').val(1);
        $('#wsWithJo').removeAttr("required", '');
        $('#wsWithJo').attr("disabled", true);
      } else {
        $('#wsWithJo').val(2);
        $('#wsWithJo').removeAttr("disabled", '');
        $('#wsWithJo').attr("required", true);
      }
    }

  </script>


<?= $this->include("layout/warehouse/modals"); ?>

</body>

</html>





