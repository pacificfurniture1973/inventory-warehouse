function triggerSelect2() {
      $('#addUserDeptHandle').select2({
        dropdownParent: $('#modalAddUser .modal-content'),
        width: '100%',
      });
      // jika ada validasi error maka return deptHandler di multiple select
      <?php if($validation->listErrors()) : ?>
        $('#addUserDeptHandle').val([<?= session()->getFlashdata('deptHandle') ?>]);
        $('#addUserDeptHandle').trigger('change');
      <?php endif; ?>
      $('#editUserDeptHandle').select2({
        dropdownParent: $('#modalEditUser .modal-content'),
        width: '100%',
      });
      $('#approveUserDeptHandle').select2({
        dropdownParent: $('#modalApproveUser .modal-content'),
        width: '100%',
      });
    }

    // activate input for update password
    function updatePass() {
      $('#editPassword').removeAttr('readonly');
      $('#editPasswordConfirmation').removeAttr('readonly');
    }

    // stock adjustment
    // pakai onfocus data tidak dinamic, yg masuk cuma yg pertama doang
    $(document).on('focus','.autocomplete_text-sa', handleAutocompleteSa);
    $(document).on('focus','.autocomplete_text-ws', handleAutocompleteWs);
    $(document).on('focus','.autocomplete_text-ws-jo', handleAutocompleteWsJo);

    function getId(element){
      let id, idArr;
      id = element.attr('id');
      idArr = id.split("_");
      return idArr[idArr.length - 1];
    }

    function handleAutocompleteSa() {

      var currentEle, idArr, rowNo;
      currentEle = $(this);

      $(".autocomplete_text-sa").autocomplete({
        // currentEle.autocomplete({
          source: function(request, cb){
              console.log(request);
              
              $.ajax({
                  url: '<?= base_url('/warehouse/c_stockAdjustment/getTerm') ?>',
                  dataType: 'json',
                  method: 'POST',
                  data: {
                    name: request.term
                  },
                  success: function(res){
                      var result;
                      result = [
                          {
                              label: 'There is no matching record found for '+request.term,
                              value: ''
                          }
                      ];

                      console.log("Before format", res);
                      

                      if (res.length) {
                          result = $.map(res, function(obj){
                              return {
                                  label: obj.product_code + ' - ' + obj.whs_name,
                                  value: obj.product_code,
                                  data : obj
                              };
                          });
                      }

                      console.log("formatted response", result);
                      cb(result);
                  }
              });
          },
          minLength: 1,
          select: function( event, selectedData ) {
              console.log(selectedData);
              let resArr, rowNo;
            
              
              rowNo = getId(currentEle);
              // resArr = selectedData.item.data.split("|");

              if (selectedData && selectedData.item && selectedData.item.data){
                  var data = selectedData.item.data;

                  $('#productName_'+rowNo).val(data.product_name);
                  $('#description_'+rowNo).val(data.product_description);
                  $('#uom_'+rowNo).val(data.uom_name);
                  $('#defLocHidden_'+rowNo).val(data.whs_code);
                  $('#defLoc_'+rowNo).val(data.whs_name);
                  $('#stock_'+rowNo).val(data.stock_quantity);
              }
              
          }  
      });  
    };

    function handleAutocompleteWs() {

      var currentEle, idArr, rowNo;
      currentEle = $(this);

      $(".autocomplete_text-ws").autocomplete({
        // currentEle.autocomplete({
          source: function(request, cb){
              console.log(request);
              
              $.ajax({
                  url: '<?= base_url('/warehouse/c_withdrawal/getTerm') ?>',
                  dataType: 'json',
                  method: 'POST',
                  data: {
                    name: request.term
                  },
                  success: function(res){
                      var result;
                      result = [
                          {
                              label: 'There is no matching record found for '+request.term,
                              value: ''
                          }
                      ];

                      console.log("Before format", res);  

                      if (res.length) {
                          result = $.map(res, function(obj){
                              return {
                                  label: obj.product_code + ' - ' + obj.whs_name,
                                  value: obj.product_code,
                                  data : obj,
                              };
                          });
                      }

                      console.log("formatted response", result);
                      cb(result);
                  }
              });
          },
          minLength: 1,
          select: function( event, selectedData ) {
              console.log(selectedData);
              let resArr, rowNo;
            
              
              rowNo = getId(currentEle);
              // resArr = selectedData.item.data.split("|");

              if (selectedData && selectedData.item && selectedData.item.data){
                  var data = selectedData.item.data;

                  $('#wsProductName_'+rowNo).val(data.product_name);
                  $('#wsDescription_'+rowNo).val(data.product_description);
                  $('#wsUom_'+rowNo).val(data.uom_name);
                  $('#wsStockAvailable_'+rowNo).val(data.stock_quantity);
                  $('#wsDefLocHidden_'+rowNo).val(data.whs_code);
                  $('#wsDefLoc_'+rowNo).val(data.whs_name);
              }
              
          }  
      });  
    };

    function handleAutocompleteWsJo() {

      var currentEle, idArr, rowNo;
      currentEle = $(this);

      $(".autocomplete_text-ws-jo").autocomplete({
        // currentEle.autocomplete({
          source: function(request, cb){
              console.log(request);
              
              $.ajax({
                  url: '<?= base_url('/warehouse/c_withdrawal/getTermJo') ?>',
                  dataType: 'json',
                  method: 'POST',
                  data: {
                    nameJo: request.term
                  },
                  success: function(res){
                      var result;
                      result = [
                          {
                              label: 'There is no matching record found for '+request.term,
                              value: ''
                          }
                      ];

                      console.log("Before format", res);  

                      if (res.length) {
                          result = $.map(res, function(obj){
                              return {
                                  label: obj.jo_number,
                                  value: obj.jo_number,
                                  data : obj,
                              };
                          });
                      }

                      console.log("formatted response", result);
                      cb(result);
                  }
              });
          },
          minLength: 1,
          select: function( event, selectedData ) {
              console.log(selectedData);
              let resArr, rowNo;
            
              
              rowNo = getId(currentEle);
              // resArr = selectedData.item.data.split("|");

              if (selectedData && selectedData.item && selectedData.item.data){
                  var data = selectedData.item.data;

                  $('#wsCustomer_'+rowNo).val(data.customer_agency);
                  $('#wsModelCode_'+rowNo).val(data.jo_model_code);
                  $('#wsBomQuantity_'+rowNo).val(data.bom_quantity_based_jo);
              }
              
          }  
      });  
    };

    // tabel dataTable warehouse product
    $(document).ready(function() {  
      let table = $('#tabel_product').DataTable({
          "processing": true,
          "serverSide": true,
          'scrollX': true,
          "order": [],
          "ajax": {
              "url": "<?php echo base_url('/warehouse/c_products/ajaxList') ?>",
              "type": "POST",
          },
          "columnDefs": [{
              "targets": [2,3,4],
              "orderable": false,
          }, ],
      });
    });

    // tabel dataTable main products
    $(document).ready(function() {  
      let table = $('#tabel_main_product').DataTable({
          "processing": true,
          "serverSide": true,
          'scrollX': true,
          "order": [],
          "ajax": {
              "url": "<?php echo base_url('/main/c_products/ajaxList') ?>",
              "type": "POST",
          },
          "columnDefs": [{
              "targets": [2,3,4,5],
              "orderable": false,
          }, ],
      });
    });

    // tabel dataTable main customer
    $(document).ready(function() {  
      let table = $('#tabel_main_customer').DataTable({
          "processing": true,
          "serverSide": true,
          'scrollX': true,
          "order": [],
          "ajax": {
              "url": "<?php echo base_url('/main/c_customer/ajaxList') ?>",
              "type": "POST",
          },
          "columnDefs": [{
              "targets": [2,4,5,7,8,9,10],
              "orderable": false,
          }, ],
      });
    });

    // tabel dataTable main vendor
    $(document).ready(function() {  
      let table = $('#tabel_main_vendor').DataTable({
          "processing": true,
          "serverSide": true,
          'scrollX': true,
          "order": [],
          "ajax": {
              "url": "<?php echo base_url('/main/c_vendor/ajaxList') ?>",
              "type": "POST",
          },
          "columnDefs": [{
              "targets": [2,4,5,7,8,9,10],
              "orderable": false,
          }, ],
      });
    });

    // tabel dataTable main job order
    $(document).ready(function() {  
      let table = $('#tabel_main_jo').DataTable({
          "processing": true,
          "serverSide": true,
          'scrollX': true,
          "order": [],
          "ajax": {
              "url": "<?php echo base_url('/main/c_jobOrder/ajaxList') ?>",
              "type": "POST",
          },
          "columnDefs": [{
              "targets": [2],
              "orderable": false,
          }, ],
      });
    });

    // tabel dataTable main rr
    $(document).ready(function() {  
      let table = $('#tabel_main_rr').DataTable({
          "processing": true,
          "serverSide": true,
          'scrollX': true,
          "order": [],
          "ajax": {
              "url": "<?php echo base_url('/main/c_receivementReport/ajaxList') ?>",
              "type": "POST",
          },
          "columnDefs": [{
              "targets": [3,4],
              "orderable": false,
          }, ],
      });
    });

    // tabel dataTable user management
    $(document).ready(function() {  
      let table = $('#tabel_user_management').DataTable({
          "processing": true,
          "serverSide": true,
          'scrollX': true,
          "order": [],
          "ajax": {
              "url": "<?php echo base_url('/management/c_userManagement/ajaxList') ?>",
              "type": "POST",
          },
          "columnDefs": [{
              "targets": [1,4,5],
              "orderable": false,
          }, ],
      });
    });

    // tabel dataTable user request
    $(document).ready(function() {  
      let table = $('#tabel_user_request').DataTable({
          "processing": true,
          "serverSide": true,
          'scrollX': true,
          "order": [],
          "ajax": {
              "url": "<?php echo base_url('/management/c_userRequest/ajaxList') ?>",
              "type": "POST",
          },
          "columnDefs": [{
              "targets": [1,4,5],
              "orderable": false,
          }, ],
      });
    });

    // tabel dataTable withdrawal history server side
    $(document).ready(function() {  
      let table = $('#tabel_withdrawalHistory').DataTable({
          "processing": true,
          "serverSide": true,
          'scrollX': true,
          "order": [],
          "ajax": {
              "url": "<?php echo base_url('/warehouse/c_withdrawalHistory/ajaxList') ?>",
              "type": "POST"
          },
          "columnDefs": [{
              "targets": [1,4,6,7],
              "orderable": false,
          }, ],
      });
    });

    // tabel dataTable withdrawal finish server side
    $(document).ready(function() {  
      let table = $('#tabel_withdrawalFinish').DataTable({
          "processing": true,
          "serverSide": true,
          // 'scrollX': true,
          "order": [],
          "ajax": {
              "url": "<?php echo base_url('/warehouse/c_withdrawalFinish/ajaxList') ?>",
              "type": "POST"
          },
          "columnDefs": [{
              "targets": [1,3,4,5],
              "orderable": false,
          }, ],
      });
    });

    // tabel dataTable stock adjustment log server side
    $(document).ready(function() {  
      let table = $('#tabel_stockAdjustmentLog').DataTable({
          "processing": true,
          "serverSide": true,
          'scrollX': true,
          "order": [],
          "ajax": {
              "url": "<?php echo base_url('/warehouse/c_stockAdjustmentLog/ajaxList') ?>",
              "type": "POST"
          },
          "columnDefs": [{
              "targets": [1,5,7,8,9],
              "orderable": false,
          }, ],
      });
    });

    // modals withdrawal
    function modalWithdrawalHistory() {
      $('#tabel_withdrawalHistory tbody').on('click','.modalWithdrawalHistory', function() {
        var wdId = $(this).data('id');
        var wdNumber = $(this).data('wd_number');
        var wdProductCode = $(this).data('p_code');
        var wdJoNumber = $(this).data('jo_number');
        var wdReqQty = $(this).data('req_qty');
        var wdBomQty = $(this).data('bom_qty');
        var wdFromWh = $(this).data('wd_from_wh');
        var wdToWh = $(this).data('wd_to_wh');
        var wdProductName = $(this).data('wd_product_name');
        var wdStockAvailable = $(this).data('wd_stock_available');
        var wdReceiptQuantity = $(this).data('receipt');
        var wdRemarkWh = $(this).data('remark_wh');

        // var link = '/Asset/deleteData';
        $('#wdId').val(wdId);
        $('#wdNumber').val(wdNumber);
        $('#wdProductCode').val(wdProductCode);
        $('#wdJoNumber').val(wdJoNumber);
        $('#wdReqQty').val(wdReqQty);
        $('#wdBomQty').val(wdBomQty);
        $('#wdFromWh').val(wdFromWh);
        $('#wdToWh').val(wdToWh);
        $('#wdProductName').val(wdProductName);
        $('#wdStockAvailable').val(wdStockAvailable);
        $('#wdReceiptQty').val(wdReceiptQuantity);
        $('#wdRemarkWh').val(wdRemarkWh);
        // $('#modalDelete form').attr('action', link);
      });
    }

    // add withdrawal and stock adjustment
    $(document).ready(function(){      
      let i=1;  

      $('#add-sa').click(function(){  
           i++;             
           $('tbody').append('<tr id="row'+i+'"><td>'+i+'</td><td><input style="padding-left: 12px" type="text" class="form-control autocomplete_text-sa <?= $validation->hasError('productCode') ? 'is-invalid' : '' ?>" name="productCode[]" id="productCode_'+i+'" placeholder="Product Code..." required><div class="invalid-feedback"><?= $validation->getError('productCode') ?></div></td><td><input style="padding-left: 12px" type="text" class="form-control productName_'+i+'" name="productName[]" id="productName_'+i+'" placeholder="Product Name..." readonly></td><td><input style="padding-left: 12px" type="text" class="form-control description_'+i+'" name="description[]" id="description_'+i+'" placeholder="Description..." readonly></td><td><input style="padding-left: 12px" type="text" class="form-control uom_'+i+'" id="uom_'+i+'" name="uom[]" placeholder="UoM..." readonly></td><td><select class="form-control form-select text-dark <?= $validation->hasError('adjType') ? 'is-invalid' : '' ?>" name="adjType[]" required><div class="invalid-feedback"><?= $validation->getError('adjType') ?></div><option disabled selected value="">Choose...</option><option value="1">Stock In</option><option value="2">Stock Out</option></select></td><td><input style="padding-left: 12px; width: 200px" type="hidden" class="form-control defLocHidden_'+i+'" name="defLoc[]" id="defLocHidden_'+i+'" placeholder="Warehouse Loc..." readonly><input style="padding-left: 12px" type="text" class="form-control defLoc_'+i+'" id="defLoc_'+i+'" placeholder="Warehouse Loc..." readonly></td><td><input style="padding-left: 12px; width: 100px" type="text" class="form-control stock_'+i+'" name="stock[]" id="stock_'+i+'" placeholder="Available..." readonly></td><td><input style="padding-left: 12px" type="text" class="form-control qty_'+i+'  <?= $validation->hasError('qty') ? 'is-invalid' : '' ?>" name="qty[]" id="qty_'+i+'" placeholder="Quantity..." required><div class="invalid-feedback"><?= $validation->getError('qty') ?></div></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove mt-2 text-white">Del</button></td></tr>');
      });

      $('#add-ws').click(function(){  
           i++;             
           $('tbody').append('<tr id="row'+i+'"><td>'+i+'</td><td><input style="padding-left: 12px; width: 250px" type="text" class="form-control autocomplete_text-ws <?= $validation->hasError('wsProductCode') ? 'is-invalid' : '' ?>" name="wsProductCode[]" id="wsProductCode_'+i+'" placeholder="Product/Assembly Code..." autocomplete="off" required><div class="invalid-feedback mt-2"><?= $validation->getError('wsProductCode') ?></div></td><td><input style="padding-left: 12px; width: 300px" type="text" class="form-control" name="wsProductName[]" id="wsProductName_'+i+'" placeholder="Product Name..." autocomplete="off" readonly></td><td><input style="padding-left: 12px; width: 300px" type="text" class="form-control" name="wsDescription[]" id="wsDescription_'+i+'" placeholder="Description..." autocomplete="off" readonly></td><td><input style="padding-left: 12px; width: 70px" type="text" class="form-control" name="wsUom[]" id="wsUom_'+i+'" placeholder="UoM..." autocomplete="off" readonly></td><td><input style="padding-left: 12px; width: 120px" type="text" class="form-control" name="wsStockAvailable[]" id="wsStockAvailable_'+i+'" placeholder="Stock Available..." autocomplete="off" readonly></td><td><input style="padding-left: 12px; width: 200px" type="hidden" class="form-control wsDefLocHidden_'+i+'" name="wsDefLoc[]" id="wsDefLocHidden_'+i+'" placeholder="Warehouse Loc..." readonly><input style="padding-left: 12px; width: 200px" type="text" class="form-control" id="wsDefLoc_'+i+'" placeholder="Warehouse Loc..." autocomplete="off" readonly></td><td><input style="padding-left: 12px; width: 200px" type="text" class="form-control autocomplete_text-ws-jo <?= $validation->hasError('wsJoNumber') ? 'is-invalid' : '' ?>" name="wsJoNumber[]" id="wsJoNumber_'+i+'" placeholder="Search customer agency..." autocomplete="off" required><div class="invalid-feedback mt-2"><?= $validation->getError('wsJoNumber') ?></div></td><td><input style="padding-left: 12px; width: 200px" type="text" class="form-control" name="wsCustomer[]" id="wsCustomer_'+i+'" placeholder="Customer..." autocomplete="off" readonly></td><td><input style="padding-left: 12px; width: 200px" type="text" class="form-control" name="wsModelCode[]" id="wsModelCode_'+i+'" placeholder="Model Code..." autocomplete="off" readonly></td><td><input style="padding-left: 12px; width: 150px" type="text" class="form-control" name="wsBomQuantity[]" id="wsBomQuantity_'+i+'" placeholder="BOM Quantity..." autocomplete="off" readonly></td><td><input style="padding-left: 12px; width: 150px" type="text" class="form-control <?= $validation->hasError('wsRequestQty') ? 'is-invalid' : '' ?>" name="wsRequestQty[]" id="wsRequestQty_'+i+'" placeholder="Request Quantity..." autocomplete="off" required><div class="invalid-feedback mt-2"><?= $validation->getError('wsRequestQty') ?></div></td><td><input style="padding-left: 12px; width: 300px" type="text" class="form-control <?= $validation->hasError('wsRemark') ? 'is-invalid' : '' ?>" name="wsRemark[]" id="wsRemark'+i+'" placeholder="Remark..." autocomplete="off" required><div class="invalid-feedback mt-2"><?= $validation->getError('wsRemark') ?></div></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove mt-2 text-white">Del</button></td></tr>');
      });
     
      $(document).on('click', '.btn_remove', function(){  
        let button_id = $(this).attr("id"); 
        let res = confirm('Are You Sure You Want To Delete This?');
        if(res==true){
        $('#row'+button_id+'').remove();  
        $('#'+button_id+'').remove();  
        }
      }); 
    });

    // edit data product on modals
    $('#tabel_main_product tbody').on('click', '#editProducts', function() {
      let productId   = $(this).data('id');
      let productCode = $(this).data('code');
      let productType = $(this).data('type');
      let productName = $(this).data('name');
      let decsription = $(this).data('desc');
      let uom         = $(this).data('uom');
      let mUom        = $(this).data('muom');
      let uomSchema   = $(this).data('uoms');
      let currency    = $(this).data('currency');
      let status      = $(this).data('status');
      let unitPrice   = $(this).data('uprice');
      let importCode  = $(this).data('import');

      $('#editProductId').val(productId);
      $('#editProductCode').val(productCode);
      $('#editProductType').val(productType);
      $('#editProductName').val(productName);
      $('#editProductDescription').val(decsription);
      $('#editProductUom').val(uom);
      $('#editProductMultipleUom').val(mUom);
      $('#editProductUomSchema').val(uomSchema);
      $('#editProductStatus').val(status);
      $('#editProductCurrency').val(currency);
      $('#editProductUnitPrice').val(unitPrice);
      $('#editImportCode').val(importCode);
    });

    // edit data customer on modals
    $('#tabel_main_customer tbody').on('click', '#editCustomer', function() {

      let customerId    = $(this).data('id');
      let customerCode  = $(this).data('code');
      let title         = $(this).data('title');
      let agency        = $(this).data('agency');
      let address       = $(this).data('address');
      let email         = $(this).data('email');
      let name          = $(this).data('name');
      let importCode    = $(this).data('test');
      let telp          = $(this).data('telp');
      let bankName      = $(this).data('bank_name');
      let bankAccount   = $(this).data('bank_account');
      let bankNumber    = $(this).data('bank_number');
      let creditTerm    = $(this).data('credit_term');
      let status        = $(this).data('status');

      $('#editCustomerId').val(customerId);
      $('#editCustomerCode').val(customerCode);
      $('#editCustomerTitle').val(title);
      $('#editCustomerAgency').val(agency);
      $('#editCustomerAddress').val(address);
      $('#editCustomerEmail').val(email);
      $('#editCustomerName').val(name);
      $('#editCustomerBankName').val(bankName);
      $('#editCustomerBankAccount').val(bankAccount);
      $('#editCustomerBankNumber').val(bankNumber);
      $('#editCustomerTelp').val(telp);
      $('#editCustomerCreditTerm').val(creditTerm);
      $('#editCustomerStatus').val(status);
      $('#editCustomerImportCode').val(importCode);

    });

    // edit data vendor on modals
    $('#tabel_main_vendor tbody').on('click', '#editVendor', function() {

      let vendorId      = $(this).data('id');
      let vendorCode    = $(this).data('code');
      let title         = $(this).data('title');
      let agency        = $(this).data('agency');
      let address       = $(this).data('address');
      let email         = $(this).data('email');
      let name          = $(this).data('name');
      let importCode    = $(this).data('import');
      let telp          = $(this).data('telp');
      let bankName      = $(this).data('bank_name');
      let bankAccount   = $(this).data('bank_account');
      let bankNumber    = $(this).data('bank_number');
      let debitTerm     = $(this).data('debit_term');
      let status        = $(this).data('status');

      $('#editVendorId').val(vendorId);
      $('#editVendorCode').val(vendorCode);
      $('#editVendorTitle').val(title);
      $('#editVendorAgency').val(agency);
      $('#editVendorAddress').val(address);
      $('#editVendorEmail').val(email);
      $('#editVendorName').val(name);
      $('#editVendorBankName').val(bankName);
      $('#editVendorBankAccount').val(bankAccount);
      $('#editVendorBankNumber').val(bankNumber);
      $('#editVendorTelp').val(telp);
      $('#editVendorDebitTerm').val(debitTerm);
      $('#editVendorStatus').val(status);
      $('#editVendorImportCode').val(importCode);

    });

    // edit data user on modals
    $('#tabel_user_management tbody').on('click', '#editUser', function() {

      let userId      = $(this).data('id');
      let username    = $(this).data('user');
      let fullname    = $(this).data('fullname');
      let position    = $(this).data('position');
      let level       = $(this).data('level');
      let password    = $(this).data('password');
      let telp        = $(this).data('telp');
      let address     = $(this).data('address');
      let email       = $(this).data('email');
      let status      = $(this).data('status');
      let dept        = $(this).data('dept');
      let deptHandle  = $(this).data('dept_handle');


      // jika data dept handle lebih dari satu maka lakukan split biar datanya bisa masuk lebih dari satu dna ngga error
      if(deptHandle.length > 1) {
        deptHandle = deptHandle.split(",")
        depHandle = deptHandle.map(Number);
      }

      $('#editUserDeptHandle').val(deptHandle);
      $('#editUserDeptHandle').trigger('change');
      
      $('#editUserId').val(userId);
      $('#editUsername').val(username);
      $('#editUserLevel').val(level);
      $('#editFullname').val(fullname);
      // $('#editPasswordReal').val(password);
      $('#editUserDept').val(dept);
      $('#editUserPosition').val(position);
      $('#editUserEmail').val(email);
      $('#editUserDeptHandle').val(deptHandle);
      $('#editUserStatus').val(status);
      $('#editUserTelp').val(telp);
      $('#editUserAddress').val(address);
    });

    // preveiw request user on modals
    $('#tabel_user_request tbody').on('click', '#approveUser', function() {

      let userId      = $(this).data('id');
      let username    = $(this).data('user');
      let fullname    = $(this).data('fullname');
      let position    = $(this).data('position');
      let level       = $(this).data('level');
      let telp        = $(this).data('telp');
      let address     = $(this).data('address');
      let email       = $(this).data('email');
      let status      = $(this).data('status');
      let dept        = $(this).data('dept');
      let deptHandle  = $(this).data('dept_handle');

      // jika data dept handle lebih dari satu maka lakukan split biar datanya bisa masuk lebih dari satu dan ngga error
      if(deptHandle.length > 1) {
        deptHandle = deptHandle.split(",")
        depHandle = deptHandle.map(Number);
      }

      $('#approveUserDeptHandle').val(deptHandle);
      $('#approveUserDeptHandle').trigger('change');
      
      $('#approveUserId').val(userId);
      $('#approveUsername').val(username);
      $('#approveUserLevel').val(level);
      $('#approveFullname').val(fullname);
      // $('#approvePasswordReal').val(password);
      $('#approveUserDept').val(dept);
      $('#approveUserPosition').val(position);
      $('#approveUserEmail').val(email);
      $('#approveUserDeptHandle').val(deptHandle);
      $('#approveUserStatus').val(status);
      $('#approveUserTelp').val(telp);
      $('#approveUserAddress').val(address);
    });

    // modals delete products
    $('#tabel_main_product tbody').on('click', '#deleteItemProducts', function() {
        let productId   = $(this).data('id');
        let importCode  = $(this).data('import');
        let link        = 'c_products/deleteProducts';
        $('#deleteId').val(productId);
        $('#importId').val(importCode);
        $('#modalDelete form').attr('action', link);
    });

    // modals delete customer
    $('#tabel_main_customer tbody').on('click', '#deleteItemCustomer', function() {
        let customerId   = $(this).data('id');
        let importCode  = $(this).data('import');
        let link        = 'c_customer/deleteCustomer';
        $('#deleteId').val(customerId);
        $('#importId').val(importCode);
        $('#modalDelete form').attr('action', link);
    });

    // modals delete vendor
    $('#tabel_main_vendor tbody').on('click', '#deleteItemVendor', function() {
        let vendorId   = $(this).data('id');
        let importCode  = $(this).data('import');
        let link        = 'c_vendor/deleteVendor';
        $('#deleteId').val(vendorId);
        $('#importId').val(importCode);
        $('#modalDelete form').attr('action', link);
    });

    // modals delete RR
    $('#tabel_main_rr tbody').on('click', '#deleteItemRR', function() {
        let rrId   = $(this).data('id');
        let importCode  = $(this).data('import');
        let link        = 'c_receivementReport/deleteRR';
        $('#deleteId').val(rrId);
        $('#importId').val(importCode);
        $('#modalDelete form').attr('action', link);
    });

    // modals delete JO
    $('#tabel_main_jo tbody').on('click', '#deleteItemJo', function() {
        let joId   = $(this).data('id');
        let importCode  = $(this).data('import');
        let link        = 'c_jobOrder/deleteJobOrder';
        $('#deleteId').val(joId);
        $('#importId').val(importCode);
        $('#modalDelete form').attr('action', link);
    });

    // modals delete User
    $('#tabel_user_management tbody').on('click', '#deleteUser', function() {
        let userId   = $(this).data('id');
        let link        = 'c_userManagement/deleteUser';
        $('#deleteId').val(userId);
        $('#modalDelete form').attr('action', link);
    });

    // modals process User
    $('#tabel_user_request tbody').on('click', '#ApproveRejectUser', function() {
        let info   = $(this).data('info');
        let status = $(this).data('status');
        let userId = $(this).data('id');
        let level  = $(this).data('level');
        let link   = 'c_userRequest/processUser';
        $('#infoProcessUser').text('Are you sure ' + info + ' this user ?');
        $('#infoUser').val(info);
        $('#statusUser').val(status);
        $('#idUser').val(userId);
        $('#levelUser').val(level);
        $('#modalApproveRejectUser form').attr('action', link);
    });

    // modals process withdrawal 
    $('#tabel_withdrawalHistory tbody').on('click', '#approveRejectWithdrawal', function() {
        let info   = $(this).data('info');
        let status = $(this).data('status');
        let wdId = $(this).data('id');
        let productCode = $(this).data('p_code');
        let fromWhs = $(this).data('wd_from_wh');
        let toWhs = $(this).data('wd_to_wh');
        let receipt = $(this).data('receipt');
        let remarkWh = $(this).data('remark_wh');
        let link = 'c_withdrawalHistory/processWithdrawal';
        $('#infoProcessWithdrawal').text('Sure ' + info + ' this withdrawal ?');

        $('#infoWd').val(info);
        $('#statusWd').val(status);
        $('#withdrawalId').val(wdId);
        $('#productCodeWithdrawal').val(productCode);
        $('#fromWarehouseWithdrawal').val(fromWhs);
        $('#toWarehouseWithdrawal').val(toWhs);
        $('#receiptQty').val(receipt);
        $('#remarkWh').val(remarkWh);
        $('#modalApproveWithdrawal form').attr('action', link);
    });