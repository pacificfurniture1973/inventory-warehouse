<!-- modals import master products -->
<div class="modal fade" id="modalImportProducts" tabindex="-1" aria-labelledby="modalImportProducts" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Import Products</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="<?= base_url('main/c_products/importProducts') ?>" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="form-group">
                        <label>File upload</label>
                        <input type="file" required name="importProducts" class="file-upload-default" accept=".xlsx,.xls">
                        <div class="input-group col-xs-12">
                        <input type="text" class="form-control file-upload-info" disabled placeholder="Upload Product">
                        <span class="input-group-append">
                            <button class="file-upload-browse btn btn-primary" type="button">Browse</button>
                        </span>
                        </div>
                    </div>
                </div>
                <div class="px-4 pb-3">
                    <button type="submit" class="btn btn-success">Upload</button>
                    <a href="<?= base_url('/main/c_products/exportTemplateProducts') ?>" class="text-decoration-none text-white"><button type="button" class="btn btn-warning">Download Template</button></a>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modals -->

<!-- modals import master products unit price -->
<div class="modal fade" id="modalImportUnitPrice" tabindex="-1" aria-labelledby="modalImportUnitPrice" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Import Products Unit Price</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="<?= base_url('main/c_products/importUnitPrice') ?>" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="form-group">
                        <label>File upload</label>
                        <input type="file" required name="importUnitPrice" class="file-upload-default" accept=".xlsx,.xls">
                        <div class="input-group col-xs-12">
                        <input type="text" class="form-control file-upload-info" disabled placeholder="Upload Product Unit Price">
                        <span class="input-group-append">
                            <button class="file-upload-browse btn btn-primary" type="button">Browse</button>
                        </span>
                        </div>
                    </div>
                </div>
                <div class="px-4 pb-3">
                    <button type="submit" class="btn btn-success">Upload</button>
                    <a href="<?= base_url('/main/c_products/exportTemplateUnitPrice') ?>" class="text-decoration-none text-white"><button type="button" class="btn btn-warning">Download Template</button></a>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modals -->

<!-- modals import master assembly products -->
<div class="modal fade" id="modalImportAssembly" tabindex="-1" aria-labelledby="modalImportAssembly" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Import Assembly</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="<?= base_url('main/c_assembly/importAssembly') ?>" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="form-group">
                        <label>File upload</label>
                        <input type="file" required name="importAssembly" class="file-upload-default" accept=".xlsx,.xls">
                        <div class="input-group col-xs-12">
                        <input type="text" class="form-control file-upload-info" disabled placeholder="Upload Assembly Product">
                        <span class="input-group-append">
                            <button class="file-upload-browse btn btn-primary" type="button">Browse</button>
                        </span>
                        </div>
                    </div>
                </div>
                <div class="px-4 pb-3">
                    <button type="submit" class="btn btn-success">Upload</button>
                    <a href="<?= base_url('/main/c_assembly/exportTemplateAssembly') ?>" class="text-decoration-none text-white"><button type="button" class="btn btn-warning">Download Template</button></a>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modals -->

<!-- modals import rr -->
<div class="modal fade" id="modalImportRR" tabindex="-1" aria-labelledby="modalImportRR" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Import Receiving Report</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="<?= base_url('main/c_receivementReport/importRR') ?>" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="form-group">
                        <label>File upload</label>
                        <input type="file" required name="importRR" class="file-upload-default" accept=".xlsx,.xls">
                        <div class="input-group col-xs-12">
                        <input type="text" class="form-control file-upload-info" disabled placeholder="Upload RR">
                        <span class="input-group-append">
                            <button class="file-upload-browse btn btn-primary" type="button">Browse</button>
                        </span>
                        </div>
                    </div>
                </div>
                <div class="px-4 pb-3">
                    <button type="submit" class="btn btn-success">Upload</button>
                    <a href="<?= base_url('/main/c_receivementReport/exportTemplateRR') ?>" class="text-decoration-none text-white"><button type="button" class="btn btn-warning">Download Template</button></a>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modals -->

<!-- modals add new data master products -->
<div class="modal fade" id="modalAddProducts" tabindex="-1" aria-labelledby="modalAddProducts" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="border-radius: 20px">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Insert new master products</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="<?= base_url('main/c_products/insertProducts') ?>" method="POST">
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="productCode">Product Code/Id*</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('productCode') ? 'is-invalid' : '' ?>" name="productCode" id="productCode" placeholder="Product Code..." value="<?= old("productCode"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError('productCode') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="productType">Product Type</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('productType') ? 'is-invalid' : '' ?>" name="productType" id="productType" placeholder="Product Type..." value="<?= old("productType"); ?>" autocomplete="off">
                            <div class="invalid-feedback">
                                <?= $validation->getError('productType') ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="productName">Product Name*</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('productName') ? 'is-invalid' : '' ?>" name="productName" id="productName" placeholder="Product Name..." value="<?= old("productName"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError('productName') ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="productDescription">Product Description*</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('productDescription') ? 'is-invalid' : '' ?>" name="productDescription" id="productDescription" placeholder="Description..." value="<?= old("productDescription"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError('productDescription') ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                    <div class="form-group col-md-6">
                            <label for="productCategory">Inventory Account</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('productCategory') ? 'is-invalid' : '' ?>" name="productCategory" id="productCategory" placeholder="Product Name..." value="<?= old("productCategory"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError('productCategory') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="productName">Valuation*</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('productValuation') ? 'is-invalid' : '' ?>" name="productValuation" id="productValuation" placeholder="Valuation Method..." value="<?= old("productValuation"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError('productValuation') ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="productUom">Product Uom*</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control" name="productUom" id="productUom" placeholder="Uom..." value="<?= old("productUom"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError('productUom') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="productMultipleUom">Product Multiple Uom</label>
                            <!-- <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control" name="productMultipleUom" id="productMultipleUom" placeholder="Multiple Uom..." value="<?= old("productMultipleUom"); ?>" autocomplete="off" required> -->
                            <select class="form-control form-select text-dark <?= ($validation->hasError('productMultipleUom') ? 'is-invalid' : '') ?>" name="productMultipleUom">
                                <option disabled selected value="">Choose...</option>
                                <?php foreach($multipleUom as $mu) : ?>
                                    <option value="<?= $mu['multiple_uom_id'] ?>" <?= (old('productMultipleUom') == $mu['multiple_uom_id'] ? 'selected' : '') ?>><?= $mu['multiple_uom_id'] ?> - <?= $mu['multiple_uom_name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                            <div class="invalid-feedback">
                                <?= $validation->getError('productMultipleUom') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="productUomSchema">Product Schema</label>
                            <!-- <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control" name="productUomSchema" id="productUomSchema" placeholder="Uom Schema..." value="<?= old("productUomSchema"); ?>" autocomplete="off" required> -->
                            <select class="form-control form-select text-dark <?= ($validation->hasError('productUomSchema') ? 'is-invalid' : '') ?>" name="productUomSchema">
                                <option disabled selected value="">Choose...</option>
                                <?php foreach($uomSchema as $us) : ?>
                                    <option value="<?= $us['uom_schema_id'] ?>" <?= (old('productUomSchema') == $us['uom_schema_id'] ? 'selected' : '') ?>><?= $us['uom_schema_id'] ?> - <?= $us['uom_schema_name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                            <div class="invalid-feedback">
                                <?= $validation->getError('productUomSchema') ?>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="productStatus">Product Status*</label>
                            <!-- <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control" name="productStatus" id="productStatus" placeholder="Status..." value="<?= old("productStatus"); ?>" autocomplete="off" required> -->
                            <select class="form-control form-select text-dark <?= ($validation->hasError('productStatus') ? 'is-invalid' : '') ?>" name="productStatus" required>
                                <option disabled selected value="">Choose...</option>
                                <?php foreach($status as $s) : ?>
                                    <option value="<?= $s['status_id'] ?>" <?= (old('productStatus') == $s['status_id'] ? 'selected' : '') ?>><?= $s['status_id'] ?> - <?= $s['status_name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                            <div class="invalid-feedback">
                                <?= $validation->getError('productStatus') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="productCurrency">Product Currency*</label>
                            <!-- <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control" name="productCurrency" id="productCurrency" placeholder="Currency..." value="<?= old("productCurrency"); ?>" autocomplete="off" required> -->
                            <select class="form-control form-select text-dark <?= ($validation->hasError('productCurrency') ? 'is-invalid' : '') ?>" name="productCurrency" required>
                                <option disabled selected value="">Choose...</option>
                                <?php foreach($currency as $c) : ?>
                                    <option value="<?= $c['currency_id'] ?>" <?= (old('productCurrency') == $c['currency_id'] ? 'selected' : '') ?>><?= $c['currency_id'] ?> - <?= $c['currency_name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                            <div class="invalid-feedback">
                                <?= $validation->getError('productCurrency') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="productUnitPrice">Product Unit Price*</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="number" step="any" class="form-control <?= ($validation->hasError('productUnitPrice') ? 'is-invalid' : '') ?>" name="productUnitPrice" id="productUnitPrice" placeholder="Unit Price..." value="<?= old("productUnitPrice"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError('productUnitPrice') ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="px-3 pb-3">
                    <button type="submit" class="btn btn-success">Submit</button>
                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modals -->

<!-- modals add new data master assembly products -->
<div class="modal fade" id="modalAddAssembly" tabindex="-1" aria-labelledby="modalAddAssembly" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="border-radius: 20px">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Insert new master assembly products</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="<?= base_url('main/c_assembly/insertAssembly') ?>" method="POST">
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="productCode">Assembly Code/Id*</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('assemblyCode') ? 'is-invalid' : '' ?>" name="assemblyCode" id="assemblyCode" placeholder="Assembly Code..." value="<?= old("assemblyCode"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError('assemblyCode') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="productType">Assembly Type</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('assemblyType') ? 'is-invalid' : '' ?>" name="assemblyType" id="assemblyType" placeholder="Assembly Type..." value="<?= old("assemblyType"); ?>" autocomplete="off">
                            <div class="invalid-feedback">
                                <?= $validation->getError('assemblyType') ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="productName">Assembly Name*</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('assemblyName') ? 'is-invalid' : '' ?>" name="assemblyName" id="assemblyName" placeholder="Assembly Name..." value="<?= old("assemblyName"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError('assemblyName') ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="productDescription">Product Description*</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('assemblyDescription') ? 'is-invalid' : '' ?>" name="assemblyDescription" id="assemblyDescription" placeholder="Description..." value="<?= old("assemblyDescription"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError('assemblyDescription') ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="productUom">Assembly Uom*</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('assemblyUom') ? 'is-invalid' : '' ?>" name="assemblyUom" id="assemblyUom" placeholder="Uom..." value="<?= old("assemblyUom"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError('assemblyUom') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="productCurrency">Assembly Currency*</label>
                            <select class="form-control form-select text-dark <?= ($validation->hasError('assemblyCurrency') ? 'is-invalid' : '') ?>" name="assemblyCurrency" id="assemblyCurrency" required>
                                <option disabled selected value="">Choose...</option>
                                <?php foreach($currency as $c) : ?>
                                    <option value="<?= $c['currency_id'] ?>" <?= (old('assemblyCurrency') == $c['currency_id'] ? 'selected' : '') ?>><?= $c['currency_id'] ?> - <?= $c['currency_name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                            <div class="invalid-feedback">
                                <?= $validation->getError('assemblyCurrency') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="productStatus">Assembly Status*</label>
                            <select class="form-control form-select text-dark <?= ($validation->hasError('assemblyStatus') ? 'is-invalid' : '') ?>" name="assemblyStatus" required>
                                <option disabled selected value="">Choose...</option>
                                <?php foreach($status as $s) : ?>
                                    <option value="<?= $s['status_id'] ?>" <?= (old('assemblyStatus') == $s['status_id'] ? 'selected' : '') ?>><?= $s['status_id'] ?> - <?= $s['status_name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                            <div class="invalid-feedback">
                                <?= $validation->getError('assemblyStatus') ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="productUnitPrice">Unit Price</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="number" step="any" class="form-control <?= ($validation->hasError('assemblyUnitPrice') ? 'is-invalid' : '') ?>" name="assemblyUnitPrice" id="assemblyUnitPrice" placeholder="Unit Price..." value="<?= old("assemblyUnitPrice"); ?>" autocomplete="off">
                            <div class="invalid-feedback">
                                <?= $validation->getError('assemblyUnitPrice') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="productUnitPrice">Category</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= ($validation->hasError('assemblyCategory') ? 'is-invalid' : '') ?>" name="assemblyCategory" id="assemblyCategory" placeholder="Category..." value="<?= old("assemblyCategory"); ?>" autocomplete="off">
                            <div class="invalid-feedback">
                                <?= $validation->getError('assemblyCategory') ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="productDescription">Packaging</label>
                            <textarea name="assemblyPackage" id="assemblyPackage" class="form-control form-control-lg <?= $validation->hasError('assemblyPackage') ? 'is-invalid' : '' ?>"><?= old('assemblyPackage') ?></textarea>
                            <div class="invalid-feedback">
                                <?= $validation->getError('assemblyPackage') ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="productDescription">Item Spec</label>
                            <textarea name="assemblyItemSpec" id="assemblyItemSpec" class="form-control form-control-lg <?= $validation->hasError('assemblyItemSpec') ? 'is-invalid' : '' ?>"><?= old('assemblyItemSpec') ?></textarea>
                            <div class="invalid-feedback">
                                <?= $validation->getError('assemblyItemSpec') ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="px-3 pb-3">
                    <button type="submit" class="btn btn-success">Submit</button>
                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modals -->

<!-- modals edit data master products -->
<div class="modal fade" id="modalEditProducts" tabindex="-1" aria-labelledby="modalEditProducts" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="border-radius: 20px">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update data master products</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="<?= base_url('main/c_products/editProducts') ?>" method="POST">
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="productCode">Product Code/Id*</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="hidden" class="form-control" name="editProductId" id="editProductId" value="" autocomplete="off" required>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('editProductCode') ? 'is-invalid' : '' ?>" name="editProductCode" id="editProductCode" placeholder="Product Code..." value="<?= old("productCode"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError('editProductCode') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="productType">Product Type</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('editProductType') ? 'is-invalid' : '' ?>" name="editProductType" id="editProductType" placeholder="Product Type..." value="<?= old("productType"); ?>" autocomplete="off">
                            <div class="invalid-feedback">
                                <?= $validation->getError('editProductType') ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="productName">Product Name*</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('editProductName') ? 'is-invalid' : '' ?>" name="editProductName" id="editProductName" placeholder="Product Name..." value="<?= old("productName"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError('editProductName') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="productName">Import Code</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('editImportCode') ? 'is-invalid' : '' ?>" name="editImportCode" id="editImportCode" placeholder="Product Name..." value="<?= old("productName"); ?>" autocomplete="off" readonly>
                            <div class="invalid-feedback">
                                <?= $validation->getError('editImportCode') ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="productDescription">Product Description*</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('editProductDescription') ? 'is-invalid' : '' ?>" name="editProductDescription" id="editProductDescription" placeholder="Description..." value="<?= old("productDescription"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError('editProductDescription') ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="editProductCategory">Inventory Account</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('editProductCategory') ? 'is-invalid' : '' ?>" name="editProductCategory" id="editProductCategory" placeholder="Inventory Account..." value="<?= old("editProductCategory"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError('editProductCategory') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="productDescription">Valuation Method*</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('editProductValuation') ? 'is-invalid' : '' ?>" name="editProductValuation" id="editProductValuation" placeholder="Valuation..." value="<?= old("editProductValuation"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError('editProductValuation') ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="productUom">Product Uom*</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control" name="editProductUom" id="editProductUom" placeholder="Uom..." value="<?= old("editProductUom"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError('editProductUom') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="productMultipleUom">Product Multiple Uom</label>
                            <!-- <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control" name="productMultipleUom" id="productMultipleUom" placeholder="Multiple Uom..." value="<?= old("productMultipleUom"); ?>" autocomplete="off" required> -->
                            <select class="form-control form-select text-dark <?= ($validation->hasError('editProductMultipleUom') ? 'is-invalid' : '') ?>" name="editProductMultipleUom" id="editProductMultipleUom">
                                <option disabled selected value="">Choose...</option>
                                <?php foreach($multipleUom as $mu) : ?>
                                    <option value="<?= $mu['multiple_uom_id'] ?>" <?= (old('editProductMultipleUom') == $mu['multiple_uom_id'] ? 'selected' : '') ?>><?= $mu['multiple_uom_id'] ?> - <?= $mu['multiple_uom_name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                            <div class="invalid-feedback">
                                <?= $validation->getError('editProductMultipleUom') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="productUomSchema">Product Schema</label>
                            <!-- <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control" name="productUomSchema" id="productUomSchema" placeholder="Uom Schema..." value="<?= old("productUomSchema"); ?>" autocomplete="off" required> -->
                            <select class="form-control form-select text-dark <?= ($validation->hasError('editProductUomSchema') ? 'is-invalid' : '') ?>" name="editProductUomSchema" id="editProductUomSchema">
                                <option disabled selected value="">Choose...</option>
                                <?php foreach($uomSchema as $us) : ?>
                                    <option value="<?= $us['uom_schema_id'] ?>" <?= (old('editProductUomSchema') == $us['uom_schema_id'] ? 'selected' : '') ?>><?= $us['uom_schema_id'] ?> - <?= $us['uom_schema_name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                            <div class="invalid-feedback">
                                <?= $validation->getError('editProductUomSchema') ?>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="productStatus">Product Status*</label>
                            <!-- <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control" name="productStatus" id="productStatus" placeholder="Status..." value="<?= old("productStatus"); ?>" autocomplete="off" required> -->
                            <select class="form-control form-select text-dark <?= ($validation->hasError('editProductStatus') ? 'is-invalid' : '') ?>" name="editProductStatus" id="editProductStatus" required>
                                <option disabled selected value="">Choose...</option>
                                <?php foreach($status as $s) : ?>
                                    <option value="<?= $s['status_id'] ?>" <?= (old('editProductStatus') == $s['status_id'] ? 'selected' : '') ?>><?= $s['status_id'] ?> - <?= $s['status_name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                            <div class="invalid-feedback">
                                <?= $validation->getError('editProductStatus') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="productCurrency">Product Currency*</label>
                            <!-- <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control" name="productCurrency" id="productCurrency" placeholder="Currency..." value="<?= old("productCurrency"); ?>" autocomplete="off" required> -->
                            <select class="form-control form-select text-dark <?= ($validation->hasError('editProductCurrency') ? 'is-invalid' : '') ?>" name="editProductCurrency" id="editProductCurrency" required>
                                <option disabled selected value="">Choose...</option>
                                <?php foreach($currency as $c) : ?>
                                    <option value="<?= $c['currency_id'] ?>" <?= (old('editProductCurrency') == $c['currency_id'] ? 'selected' : '') ?>><?= $c['currency_id'] ?> - <?= $c['currency_name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                            <div class="invalid-feedback">
                                <?= $validation->getError('editProductCurrency') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="productUnitPrice">Product Unit Price*</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="number" step="any" class="form-control <?= ($validation->hasError('editProductUnitPrice') ? 'is-invalid' : '') ?>" name="editProductUnitPrice" id="editProductUnitPrice" placeholder="Unit Price..." value="<?= old("editProductUnitPrice"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError('editProductUnitPrice') ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="px-3 pb-3">
                    <button type="submit" class="btn btn-success">Submit</button>
                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modals -->

<!-- modals edit data master assembly products -->
<div class="modal fade" id="modalEditAssembly" tabindex="-1" aria-labelledby="modalEditAssembly" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="border-radius: 20px">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update data master assembly product</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="<?= base_url('main/c_assembly/editAssembly') ?>" method="POST">
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="productCode">Assembly Code/Id*</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="hidden" class="form-control" name="editAssemblyId" id="editAssemblyId" value="" autocomplete="off" required>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('editAssemblyCode') ? 'is-invalid' : '' ?>" name="editAssemblyCode" id="editAssemblyCode" placeholder="Product Code..." value="<?= old("editAssemblyCode"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError('editAssemblyCode') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="productType">Assembly Type</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('editAssemblyType') ? 'is-invalid' : '' ?>" name="editAssemblyType" id="editAssemblyType" placeholder="Product Type..." value="<?= old("editAssemblyType"); ?>" autocomplete="off">
                            <div class="invalid-feedback">
                                <?= $validation->getError('editAssemblyType') ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="productName">Assembly Name*</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('editAssemblyName') ? 'is-invalid' : '' ?>" name="editAssemblyName" id="editAssemblyName" placeholder="Product Name..." value="<?= old("editAssemblyName"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError('editAssemblyName') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="productName">Import Code</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('editAssemblyImportCode') ? 'is-invalid' : '' ?>" name="editAssemblyImportCode" id="editAssemblyImportCode" placeholder="Product Name..." value="<?= old("editAssemblyImportCode"); ?>" autocomplete="off" readonly>
                            <div class="invalid-feedback">
                                <?= $validation->getError('editAssemblyImportCode') ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="productDescription">Assembly Description*</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('editAssemblyDescription') ? 'is-invalid' : '' ?>" name="editAssemblyDescription" id="editAssemblyDescription" placeholder="Description..." value="<?= old("editAssemblyDescription"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError('editAssemblyDescription') ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="productUom">Assembly Uom*</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('editAssemblyUom') ? 'is-invalid' : '' ?>" name="editAssemblyUom" id="editAssemblyUom" placeholder="Uom..." value="<?= old("editAssemblyUom"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError('editAssemblyUom') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="productCurrency">Assembly Currency*</label>
                            <!-- <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control" name="productCurrency" id="productCurrency" placeholder="Currency..." value="<?= old("productCurrency"); ?>" autocomplete="off" required> -->
                            <select class="form-control form-select text-dark <?= ($validation->hasError('editAssemblyCurrency') ? 'is-invalid' : '') ?>" name="editAssemblyCurrency" id="editAssemblyCurrency" required>
                                <option disabled selected value="">Choose...</option>
                                <?php foreach($currency as $c) : ?>
                                    <option value="<?= $c['currency_id'] ?>" <?= (old('editAssemblyCurrency') == $c['currency_id'] ? 'selected' : '') ?>><?= $c['currency_id'] ?> - <?= $c['currency_name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                            <div class="invalid-feedback">
                                <?= $validation->getError('editAssemblyCurrency') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="productStatus">Assembly Status*</label>
                            <!-- <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control" name="productStatus" id="productStatus" placeholder="Status..." value="<?= old("editAssemblyStatus"); ?>" autocomplete="off" required> -->
                            <select class="form-control form-select text-dark <?= ($validation->hasError('editAssemblyStatus') ? 'is-invalid' : '') ?>" name="editAssemblyStatus" id="editAssemblyStatus" required>
                                <option disabled selected value="">Choose...</option>
                                <?php foreach($status as $s) : ?>
                                    <option value="<?= $s['status_id'] ?>" <?= (old('editAssemblyStatus') == $s['status_id'] ? 'selected' : '') ?>><?= $s['status_id'] ?> - <?= $s['status_name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                            <div class="invalid-feedback">
                                <?= $validation->getError('editAssemblyStatus') ?>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="productUnitPrice">Assembly Unit Price</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="number" step="any" class="form-control <?= ($validation->hasError('editAssemblyUnitPrice') ? 'is-invalid' : '') ?>" name="editAssemblyUnitPrice" id="editAssemblyUnitPrice" placeholder="Unit Price..." value="<?= old("editAssemblyUnitPrice"); ?>" autocomplete="off">
                            <div class="invalid-feedback">
                                <?= $validation->getError('editAssemblyUnitPrice') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="productUnitPrice">Category</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= ($validation->hasError('editAssemblyCategory') ? 'is-invalid' : '') ?>" name="editAssemblyCategory" id="editAssemblyCategory" placeholder="Category..." value="<?= old("editAssemblyCategory"); ?>" autocomplete="off">
                            <div class="invalid-feedback">
                                <?= $validation->getError('editAssemblyCategory') ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="productDescription">Packaging</label>
                                <textarea name="editAssemblyPackage" id="editAssemblyPackage" class="form-control form-control-lg <?= $validation->hasError('editAssemblyPackage') ? 'is-invalid' : '' ?>"><?= old("editAssemblyPackage"); ?></textarea>
                                <div class="invalid-feedback">
                                    <?= $validation->getError('editAssemblyPackage') ?>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="productDescription">Item Spec</label>
                                <textarea name="editAssemblyItemSpec" id="editAssemblyItemSpec" class="form-control form-control-lg <?= $validation->hasError('editAssemblyItemSpec') ? 'is-invalid' : '' ?>"><?= old('editAssemblyItemSpec') ?></textarea>
                                <div class="invalid-feedback">
                                    <?= $validation->getError('editAssemblyItemSpec') ?>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="px-3 pb-3">
                    <button type="submit" class="btn btn-success">Submit</button>
                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modals -->

<!-- modals import inventory stock adjustment -->
<div class="modal fade" id="modalImportStockAdjustment" tabindex="-1" aria-labelledby="modalImportStockAdjustment" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Import Stock Adjustment</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="<?= base_url('warehouse/c_stockAdjustment/importStockAdjustment') ?>" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="form-group">
                        <label>File upload</label>
                        <input type="file" required name="importStockAdjustment" class="file-upload-default" accept=".xlsx,.xls">
                        <div class="input-group col-xs-12">
                        <input type="text" class="form-control file-upload-info" disabled placeholder="Upload Here...">
                        <span class="input-group-append">
                            <button class="file-upload-browse btn btn-primary" type="button">Browse</button>
                        </span>
                        </div>
                    </div>
                </div>
                <div class="px-4 pb-3">
                    <button type="submit" class="btn btn-success">Upload</button>
                    <a href="<?= base_url('/warehouse/c_stockAdjustment/exportTemplateStockAdjustment') ?>" class="text-decoration-none text-white"><button type="button" class="btn btn-warning">Download Template</button></a>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modals -->

<!-- modals import withdrawal -->
<div class="modal fade" id="modalImportWithdrawal" tabindex="-1" aria-labelledby="modalImportWithdrawal" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Import Withdrawal</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="<?= base_url('warehouse/c_withdrawal/importWithdrawal') ?>" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="form-group">
                        <label>File upload</label>
                        <input type="file" required name="importStockAdjustment" class="file-upload-default" accept=".xlsx,.xls">
                        <div class="input-group col-xs-12">
                        <input type="text" class="form-control file-upload-info" disabled placeholder="Upload Here...">
                        <span class="input-group-append">
                            <button class="file-upload-browse btn btn-primary" type="button">Browse</button>
                        </span>
                        </div>
                    </div>
                </div>
                <div class="px-4 pb-3">
                    <button type="submit" class="btn btn-success">Upload</button>
                    <a href="<?= base_url('/warehouse/c_withdrawal/exportTemplateWithdrawal') ?>" class="text-decoration-none text-white"><button type="button" class="btn btn-warning">Download Template</button></a>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modals -->

<!-- modals import customer -->
<div class="modal fade" id="modalImportCustomer" tabindex="-1" aria-labelledby="modalImportCustomer" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Import Customer</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="<?= base_url('main/c_customer/importCustomer') ?>" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="form-group">
                        <label>File upload</label>
                        <input type="file" required name="importCustomer" class="file-upload-default" accept=".xlsx,.xls">
                        <div class="input-group col-xs-12">
                        <input type="text" class="form-control file-upload-info" disabled placeholder="Upload Customer...">
                        <span class="input-group-append">
                            <button class="file-upload-browse btn btn-primary" type="button">Browse</button>
                        </span>
                        </div>
                    </div>
                </div>
                <div class="px-4 pb-3">
                    <button type="submit" class="btn btn-success">Upload</button>
                    <a href="<?= base_url('/main/c_customer/exportTemplateCustomer') ?>" class="text-decoration-none text-white"><button type="button" class="btn btn-warning">Download Template</button></a>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modals -->

<!-- modals add new data master customer -->
<div class="modal fade" id="modalAddCustomer" tabindex="-1" aria-labelledby="modalAddCustomer" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="border-radius: 20px">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Insert new customer</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="<?= base_url('main/c_customer/insertCustomer') ?>" method="POST">
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-md-3">
                            <label for="productCode">Customer Code/Id*</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('customerCode') ? 'is-invalid' : '' ?>" name="customerCode" id="customerCode" placeholder="Customer Code..." value="<?= old("customerCode"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError('customerCode') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="productType">Title</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('customerTitle') ? 'is-invalid' : '' ?>" name="customerTitle" id="customerTitle" placeholder="Customer Title..." value="<?= old("customerTitle"); ?>" autocomplete="off">
                            <div class="invalid-feedback">
                                <?= $validation->getError('customerTitle') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="productType">Customer Agency*</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('customerAgency') ? 'is-invalid' : '' ?>" name="customerAgency" id="customerAgency" placeholder="Customer Agency..." value="<?= old("customerAgency"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError('customerAgency') ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="productName">Address</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('customerAddress') ? 'is-invalid' : '' ?>" name="customerAddress" id="customerAddress" placeholder="Customer Address..." value="<?= old("customerAddress"); ?>" autocomplete="off">
                            <div class="invalid-feedback">
                                <?= $validation->getError('customerAddress') ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="productDescription">Email</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('customerEmail') ? 'is-invalid' : '' ?>" name="customerEmail" id="customerEmail" placeholder="Customer Email..." value="<?= old("customerEmail"); ?>" autocomplete="off">
                            <div class="invalid-feedback">
                                <?= $validation->getError('customerEmail') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="productDescription">Customer Name</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('customerName') ? 'is-invalid' : '' ?>" name="customerName" id="customerName" placeholder="Customer Name..." value="<?= old("customerName"); ?>" autocomplete="off">
                            <div class="invalid-feedback">
                                <?= $validation->getError('customerName') ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="productDescription">Bank Name</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('customerBankName') ? 'is-invalid' : '' ?>" name="customerBankName" id="customerBankName" placeholder="Bank Name..." value="<?= old("customerBankName"); ?>" autocomplete="off">
                            <div class="invalid-feedback">
                                <?= $validation->getError('customerBankName') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="productDescription">Bank Account</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('customerBankAccount') ? 'is-invalid' : '' ?>" name="customerBankAccount" id="customerBankAccount" placeholder="Bank Account..." value="<?= old("customerBankAccount"); ?>" autocomplete="off">
                            <div class="invalid-feedback">
                                <?= $validation->getError('customerBankAccount') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="productDescription">Bank Number</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('customerBankNumber') ? 'is-invalid' : '' ?>" name="customerBankNumber" id="customerBankNumber" placeholder="Bank Number..." value="<?= old("customerBankNumber"); ?>" autocomplete="off">
                            <div class="invalid-feedback">
                                <?= $validation->getError('customerBankNumber') ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="productDescription">Customer Telp</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('customerTelp') ? 'is-invalid' : '' ?>" name="customerTelp" id="customerTelp" placeholder="Customer Telp..." value="<?= old("customerTelp"); ?>" autocomplete="off">
                            <div class="invalid-feedback">
                                <?= $validation->getError('customerTelp') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="productDescription">Credit Term</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('customerCreditTerm') ? 'is-invalid' : '' ?>" name="customerCreditTerm" id="customerCreditTerm" placeholder="Credit Term..." value="<?= old("customerCreditTerm"); ?>" autocomplete="off">
                            <div class="invalid-feedback">
                                <?= $validation->getError('customerCreditTerm') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="productDescription">Status*</label>
                            <select class="form-control form-select text-dark <?= ($validation->hasError('customerStatus') ? 'is-invalid' : '') ?>" name="customerStatus" required>
                                <option disabled selected value="">Choose...</option>
                                <?php foreach($status as $s) : ?>
                                    <option value="<?= $s['status_id'] ?>" <?= (old('customerStatus') == $s['status_id'] ? 'selected' : '') ?>><?= $s['status_id'] ?> - <?= $s['status_name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                            <div class="invalid-feedback">
                                <?= $validation->getError('customerStatus') ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="px-3 pb-3">
                    <button type="submit" class="btn btn-success">Submit</button>
                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modals -->

<!-- modals edit data master customer -->
<div class="modal fade" id="modalEditCustomer" tabindex="-1" aria-labelledby="modalEditCustomer" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="border-radius: 20px">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update customer</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="<?= base_url('main/c_customer/editCustomer') ?>" method="POST">
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-md-3">
                            <label for="productCode">Customer Code/Id*</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="hidden" class="form-control <?= $validation->hasError('editCustomerId') ? 'is-invalid' : '' ?>" name="editCustomerId" id="editCustomerId" placeholder="Customer Id..." value="<?= old("editCustomerId"); ?>" autocomplete="off" required>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('editCustomerCode') ? 'is-invalid' : '' ?>" name="editCustomerCode" id="editCustomerCode" placeholder="Customer Code..." value="<?= old("editCustomerCode"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError('editCustomerCode') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="productType">Title</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('editCustomerTitle') ? 'is-invalid' : '' ?>" name="editCustomerTitle" id="editCustomerTitle" placeholder="Customer Title..." value="<?= old("editCustomerTitle"); ?>" autocomplete="off">
                            <div class="invalid-feedback">
                                <?= $validation->getError('editCustomerTitle') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="productType">Customer Agency*</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('editCustomerAgency') ? 'is-invalid' : '' ?>" name="editCustomerAgency" id="editCustomerAgency" placeholder="Customer Agency..." value="<?= old("editCustomerAgency"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError('editCustomerAgency') ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="productName">Address</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('editCustomerAddress') ? 'is-invalid' : '' ?>" name="editCustomerAddress" id="editCustomerAddress" placeholder="Customer Address..." value="<?= old("editCustomerAddress"); ?>" autocomplete="off">
                            <div class="invalid-feedback">
                                <?= $validation->getError('editCustomerAddress') ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="productDescription">Email</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="email" class="form-control <?= $validation->hasError('editCustomerEmail') ? 'is-invalid' : '' ?>" name="editCustomerEmail" id="editCustomerEmail" placeholder="Customer Email..." value="<?= old("editCustomerEmail"); ?>" autocomplete="off">
                            <div class="invalid-feedback">
                                <?= $validation->getError('editCustomerEmail') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="productDescription">Customer Name</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('editCustomerName') ? 'is-invalid' : '' ?>" name="editCustomerName" id="editCustomerName" placeholder="Customer Name..." value="<?= old("editCustomerName"); ?>" autocomplete="off">
                            <div class="invalid-feedback">
                                <?= $validation->getError('editCustomerName') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="productDescription">Import Code</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('editCustomerImportCode') ? 'is-invalid' : '' ?>" name="editCustomerImportCode" id="editCustomerImportCode" placeholder="Import Code..." value="<?= old("editCustomerImportCode"); ?>" autocomplete="off" readonly>
                            <div class="invalid-feedback">
                                <?= $validation->getError('editCustomerImportCode') ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="productDescription">Bank Name</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('editCustomerBankName') ? 'is-invalid' : '' ?>" name="editCustomerBankName" id="editCustomerBankName" placeholder="Bank Name..." value="<?= old("editCustomerBankName"); ?>" autocomplete="off">
                            <div class="invalid-feedback">
                                <?= $validation->getError('editCustomerBankName') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="productDescription">Bank Account</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('editCustomerBankAccount') ? 'is-invalid' : '' ?>" name="editCustomerBankAccount" id="editCustomerBankAccount" placeholder="Bank Account..." value="<?= old("editCustomerBankAccount"); ?>" autocomplete="off">
                            <div class="invalid-feedback">
                                <?= $validation->getError('editCustomerBankAccount') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="productDescription">Bank Number</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('editCustomerBankNumber') ? 'is-invalid' : '' ?>" name="editCustomerBankNumber" id="editCustomerBankNumber" placeholder="Bank Number..." value="<?= old("editCustomerBankNumber"); ?>" autocomplete="off">
                            <div class="invalid-feedback">
                                <?= $validation->getError('editCustomerBankNumber') ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="productDescription">Customer Telp</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('editCustomerTelp') ? 'is-invalid' : '' ?>" name="editCustomerTelp" id="editCustomerTelp" placeholder="Customer Telp..." value="<?= old("editCustomerTelp"); ?>" autocomplete="off">
                            <div class="invalid-feedback">
                                <?= $validation->getError('editCustomerTelp') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="productDescription">Credit Term</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('editCustomerCreditTerm') ? 'is-invalid' : '' ?>" name="editCustomerCreditTerm" id="editCustomerCreditTerm" placeholder="Credit Term..." value="<?= old("editCustomerCreditTerm"); ?>" autocomplete="off">
                            <div class="invalid-feedback">
                                <?= $validation->getError('editCustomerCreditTerm') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="productDescription">Status*</label>
                            <select class="form-control form-select text-dark <?= ($validation->hasError('editCustomerStatus') ? 'is-invalid' : '') ?>" name="editCustomerStatus" id="editCustomerStatus" required>
                                <option disabled selected value="">Choose...</option>
                                <?php foreach($status as $s) : ?>
                                    <option value="<?= $s['status_id'] ?>" <?= (old('editCustomerStatus') == $s['status_id'] ? 'selected' : '') ?>><?= $s['status_id'] ?> - <?= $s['status_name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                            <div class="invalid-feedback">
                                <?= $validation->getError('editCustomerStatus') ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="px-3 pb-3">
                    <button type="submit" class="btn btn-success">Submit</button>
                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modals -->

<!-- modals add new data master vendor -->
<div class="modal fade" id="modalAddVendor" tabindex="-1" aria-labelledby="modalAddVendor" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="border-radius: 20px">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Insert new vendor</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="<?= base_url('main/c_vendor/insertVendor') ?>" method="POST">
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-md-3">
                            <label for="productCode">Vendor Code/Id*</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('vendorCode') ? 'is-invalid' : '' ?>" name="vendorCode" id="vendorCode" placeholder="Vendor Code..." value="<?= old("vendorCode"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError('vendorCode') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="productType">Title</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('vendorTitle') ? 'is-invalid' : '' ?>" name="vendorTitle" id="vendorTitle" placeholder="Vendor Title..." value="<?= old("vendorTitle"); ?>" autocomplete="off">
                            <div class="invalid-feedback">
                                <?= $validation->getError('vendorTitle') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="productType">Vendor Agency*</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('vendorAgency') ? 'is-invalid' : '' ?>" name="vendorAgency" id="vendorAgency" placeholder="Vendor Agency..." value="<?= old("vendorAgency"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError('vendorAgency') ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="productName">Address</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('vendorAddress') ? 'is-invalid' : '' ?>" name="vendorAddress" id="vendorAddress" placeholder="Vendor Address..." value="<?= old("vendorAddress"); ?>" autocomplete="off">
                            <div class="invalid-feedback">
                                <?= $validation->getError('vendorAddress') ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="productDescription">Email</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('vendorEmail') ? 'is-invalid' : '' ?>" name="vendorEmail" id="vendorEmail" placeholder="Vendor Email..." value="<?= old("vendorEmail"); ?>" autocomplete="off">
                            <div class="invalid-feedback">
                                <?= $validation->getError('vendorEmail') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="productDescription">Vendor Name</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('vendorName') ? 'is-invalid' : '' ?>" name="vendorName" id="vendorName" placeholder="Vendor Name..." value="<?= old("vendorName"); ?>" autocomplete="off">
                            <div class="invalid-feedback">
                                <?= $validation->getError('regCustomerName') ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="productDescription">Bank Name</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('vendorBankName') ? 'is-invalid' : '' ?>" name="vendorBankName" id="vendorBankName" placeholder="Bank Name..." value="<?= old("vendorBankName"); ?>" autocomplete="off">
                            <div class="invalid-feedback">
                                <?= $validation->getError('vendorBankName') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="productDescription">Bank Account</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('vendorBankAccount') ? 'is-invalid' : '' ?>" name="vendorBankAccount" id="vendorBankAccount" placeholder="Bank Account..." value="<?= old("vendorBankAccount"); ?>" autocomplete="off">
                            <div class="invalid-feedback">
                                <?= $validation->getError('vendorBankAccount') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="productDescription">Bank Number</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('vendorBankNumber') ? 'is-invalid' : '' ?>" name="vendorBankNumber" id="vendorBankNumber" placeholder="Bank Number..." value="<?= old("vendorBankNumber"); ?>" autocomplete="off">
                            <div class="invalid-feedback">
                                <?= $validation->getError('vendorBankNumber') ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="productDescription">Vendor Telp</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('vendorTelp') ? 'is-invalid' : '' ?>" name="vendorTelp" id="vendorTelp" placeholder="Vendor Telp..." value="<?= old("vendorTelp"); ?>" autocomplete="off">
                            <div class="invalid-feedback">
                                <?= $validation->getError('vendorTelp') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="productDescription">Debit Term</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('vendorDebitTerm') ? 'is-invalid' : '' ?>" name="vendorDebitTerm" id="vendorDebitTerm" placeholder="Debit Term..." value="<?= old("vendorDebitTerm"); ?>" autocomplete="off">
                            <div class="invalid-feedback">
                                <?= $validation->getError('vendorDebitTerm') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="productDescription">Status*</label>
                            <select class="form-control form-select text-dark <?= ($validation->hasError('vendorStatus') ? 'is-invalid' : '') ?>" name="vendorStatus" id="vendorStatus" required>
                                <option disabled selected value="">Choose...</option>
                                <?php foreach($status as $s) : ?>
                                    <option value="<?= $s['status_id'] ?>" <?= (old('vendorStatus') == $s['status_id'] ? 'selected' : '') ?>><?= $s['status_id'] ?> - <?= $s['status_name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                            <div class="invalid-feedback">
                                <?= $validation->getError('vendorStatus') ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="px-3 pb-3">
                    <button type="submit" class="btn btn-success">Submit</button>
                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modals -->

<!-- modals edit data master vendor -->
<div class="modal fade" id="modalEditVendor" tabindex="-1" aria-labelledby="modalEditVendor" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="border-radius: 20px">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update vendor</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="<?= base_url('main/c_vendor/editVendor') ?>" method="POST">
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-md-3">
                            <label for="productCode">Vendor Code/Id*</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="hidden" class="form-control <?= $validation->hasError('editVendorId') ? 'is-invalid' : '' ?>" name="editVendorId" id="editVendorId" placeholder="Vendor Id..." value="<?= old("editVendorId"); ?>" autocomplete="off" required>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('editVendorCode') ? 'is-invalid' : '' ?>" name="editVendorCode" id="editVendorCode" placeholder="Vendor Code..." value="<?= old("editVendorCode"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError('editVendorCode') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="productType">Title</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('editVendorTitle') ? 'is-invalid' : '' ?>" name="editVendorTitle" id="editVendorTitle" placeholder="Vendor Title..." value="<?= old("editVendorTitle"); ?>" autocomplete="off">
                            <div class="invalid-feedback">
                                <?= $validation->getError('editVendorTitle') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="productType">Vendor Agency*</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('editVendorAgency') ? 'is-invalid' : '' ?>" name="editVendorAgency" id="editVendorAgency" placeholder="Vendor Agency..." value="<?= old("editVendorAgency"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError('editVendorAgency') ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="productName">Address</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('editVendorAddress') ? 'is-invalid' : '' ?>" name="editVendorAddress" id="editVendorAddress" placeholder="Vendor Address..." value="<?= old("editVendorAddress"); ?>" autocomplete="off">
                            <div class="invalid-feedback">
                                <?= $validation->getError('editVendorAddress') ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="productDescription">Email</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="email" class="form-control <?= $validation->hasError('editVendorEmail') ? 'is-invalid' : '' ?>" name="editVendorEmail" id="editVendorEmail" placeholder="Vendor Email..." value="<?= old("editVendorEmail"); ?>" autocomplete="off">
                            <div class="invalid-feedback">
                                <?= $validation->getError('editVendorEmail') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="productDescription">Vendor Name</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('editVendorName') ? 'is-invalid' : '' ?>" name="editVendorName" id="editVendorName" placeholder="Vendor Name..." value="<?= old("editVendorName"); ?>" autocomplete="off">
                            <div class="invalid-feedback">
                                <?= $validation->getError('editVendorName') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="productDescription">Import Code</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('editVendorImportCode') ? 'is-invalid' : '' ?>" name="editVendorImportCode" id="editVendorImportCode" placeholder="Import Code..." value="<?= old("editVendorImportCode"); ?>" autocomplete="off" readonly>
                            <div class="invalid-feedback">
                                <?= $validation->getError('editVendorImportCode') ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="productDescription">Bank Name</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('editVendorBankName') ? 'is-invalid' : '' ?>" name="editVendorBankName" id="editVendorBankName" placeholder="Bank Name..." value="<?= old("editVendorBankName"); ?>" autocomplete="off">
                            <div class="invalid-feedback">
                                <?= $validation->getError('editVendorBankName') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="productDescription">Bank Account</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('editVendorBankAccount') ? 'is-invalid' : '' ?>" name="editVendorBankAccount" id="editVendorBankAccount" placeholder="Bank Account..." value="<?= old("editVendorBankAccount"); ?>" autocomplete="off">
                            <div class="invalid-feedback">
                                <?= $validation->getError('editVendorBankAccount') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="productDescription">Bank Number</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('editVendorBankNumber') ? 'is-invalid' : '' ?>" name="editVendorBankNumber" id="editVendorBankNumber" placeholder="Bank Number..." value="<?= old("editVendorBankNumber"); ?>" autocomplete="off">
                            <div class="invalid-feedback">
                                <?= $validation->getError('editVendorBankNumber') ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="productDescription">Vendor Telp</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('editVendorTelp') ? 'is-invalid' : '' ?>" name="editVendorTelp" id="editVendorTelp" placeholder="Vendor Telp..." value="<?= old("editVendorTelp"); ?>" autocomplete="off">
                            <div class="invalid-feedback">
                                <?= $validation->getError('editVendorTelp') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="productDescription">Credit Term</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('editVendorDebitTerm') ? 'is-invalid' : '' ?>" name="editVendorDebitTerm" id="editVendorDebitTerm" placeholder="Debit Term..." value="<?= old("editVendorDebitTerm"); ?>" autocomplete="off">
                            <div class="invalid-feedback">
                                <?= $validation->getError('editVendorDebitTerm') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="productDescription">Status*</label>
                            <select class="form-control form-select text-dark <?= ($validation->hasError('editVendorStatus') ? 'is-invalid' : '') ?>" name="editVendorStatus" id="editVendorStatus" required>
                                <option disabled selected value="">Choose...</option>
                                <?php foreach($status as $s) : ?>
                                    <option value="<?= $s['status_id'] ?>" <?= (old('editVendorStatus') == $s['status_id'] ? 'selected' : '') ?>><?= $s['status_id'] ?> - <?= $s['status_name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                            <div class="invalid-feedback">
                                <?= $validation->getError('editVendorStatus') ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="px-3 pb-3">
                    <button type="submit" class="btn btn-success">Submit</button>
                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modals -->

<!-- modals import vendor -->
<div class="modal fade" id="modalImportVendor" tabindex="-1" aria-labelledby="modalImportVendor" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Import Vendor</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="<?= base_url('main/c_vendor/importVendor') ?>" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="form-group">
                        <label>File upload</label>
                        <input type="file" required name="importVendor" class="file-upload-default" accept=".xlsx,.xls">
                        <div class="input-group col-xs-12">
                        <input type="text" class="form-control file-upload-info" disabled placeholder="Upload Image">
                        <span class="input-group-append">
                            <button class="file-upload-browse btn btn-primary" type="button">Browse</button>
                        </span>
                        </div>
                    </div>
                </div>
                <div class="px-4 pb-3">
                    <button type="submit" class="btn btn-success">Upload</button>
                    <a href="<?= base_url('/main/c_vendor/exportTemplateVendor') ?>" class="text-decoration-none text-white"><button type="button" class="btn btn-warning">Download Template</button></a>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modals -->

<!-- modals import job order -->
<div class="modal fade" id="modalImportJo" tabindex="-1" aria-labelledby="modalImportJo" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Import Job Order</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="<?= base_url('main/c_jobOrder/importJobOrder') ?>" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="form-group">
                        <label>File upload</label>
                        <input type="file" required name="importJobOrder" class="file-upload-default" accept=".xlsx,.xls">
                        <div class="input-group col-xs-12">
                        <input type="text" class="form-control file-upload-info" disabled placeholder="Upload Here...">
                        <span class="input-group-append">
                            <button class="file-upload-browse btn btn-primary" type="button">Browse</button>
                        </span>
                        </div>
                    </div>
                </div>
                <div class="px-4 pb-3">
                    <button type="submit" class="btn btn-success">Upload</button>
                    <a href="<?= base_url('/main/c_jobOrder/exportTemplateJobOrder') ?>" class="text-decoration-none text-white"><button type="button" class="btn btn-warning">Download Template</button></a>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modals -->

<!-- modals process withdrawal -->
<div class="modal fade" id="modalWithdrawalProcess" tabindex="-1" aria-labelledby="modalWithdrawalProcess" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="border-radius: 20px">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Process Withdrawal</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="<?= base_url('/warehouse/c_withdrawalProcess/withdrawalProcess') ?>" method="POST">
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="wdNumber">Withdrawal Number</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="hidden" class="form-control" name="wdId" id="wdId" placeholder="Withdrawal Number..." value="" autocomplete="off" readonly>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control" name="wdNumber" id="wdNumber" placeholder="Withdrawal Number..." value="" autocomplete="off" readonly>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="wdJoNumber">JO Number</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control" name="wdJoNumber" id="wdJoNumber" placeholder="JO Number..." value="" autocomplete="off" readonly>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="wdProductCode">Product Code</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control" name="wdProductCode" id="wdProductCode" placeholder="Product Code..." value="" autocomplete="off" readonly>
                        </div>
                        <div class="form-group col-md-8">
                            <label for="wdProductCode">Product Name</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control" name="wdProductName" id="wdProductName" placeholder="Product Name..." value="" autocomplete="off" readonly>
                        </div>
                        <input type="hidden" class="form-control" name="wdFromWh" id="wdFromWh" value="" autocomplete="off" readonly>
                        <input type="hidden" class="form-control" name="wdToWh" id="wdToWh" value="" autocomplete="off" readonly>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="wdReqQty">Request Qty</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control" name="wdReqQty" id="wdReqQty" placeholder="Req Qty..." value="" autocomplete="off" readonly>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="wdReqQty">Stock Available</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control" name="wdStockAvailable" id="wdStockAvailable" placeholder="Stock Available..." value="" autocomplete="off" readonly>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="wdBomQty">BOM Qty</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control" name="wdBomQty" id="wdBomQty" placeholder="BOM Qty..." value="" autocomplete="off" readonly>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="wdRemark">Remark</label>
                            <textarea style="border-radius: 15px; padding-left: 12px" name="wdRemark" id="wdRemark" class="form-control form-control-lg" readonly></textarea>
                        </div>
                    </div>

                    <?php if(session()->get('level') <= 4 && session()->get('dept') == 10) : ?>
                        <div class="row mt-4">
                            <div class="form-group col-md-6">
                                <label for="wdReceiptQty">Receipt Qty</label>
                                <input style="border-radius: 15px; padding-left: 12px" type="number" step="any" class="form-control <?= (session()->getFlashdata('error_message') || $validation->hasError('wdReceiptQty') ? 'is-invalid' : '') ?>" name="wdReceiptQty" id="wdReceiptQty" placeholder="Receipt Qty..." value="" autocomplete="off" required>
                                <div class="invalid-feedback"> 
                                    <?= ($validation->hasError('wdReceiptQty') ? $validation->getError('wdReceiptQty') : 'Must be same or less than available stock !') ?>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="wdRemarkWh">Remark Warehouse</label>
                                <textarea style="border-radius: 15px; padding-left: 12px" name="wdRemarkWh" id="wdRemarkWh" id="memo" class="form-control form-control-lg <?= $validation->hasError('wdRemarkWh') ? 'is-invalid' : '' ?>" required></textarea>
                                <div class="invalid-feedback">
                                    <?= $validation->getError('wdRemarkWh') ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>

                <?php if(session()->get('level') <= 4 && session()->get('dept') == 10) : ?>
                    <div class="px-3 pb-3">
                        <button type="submit" class="btn btn-success">Issue</button>
                        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                    </div>
                <?php endif; ?>
            </form>
        </div>
    </div>
</div>
<!-- end modals -->

<!-- modals process withdrawal for wdStatus 5 (pending wd head) -->
<div class="modal fade" id="modalWithdrawalProcessHeadWh" tabindex="-1" aria-labelledby="modalWithdrawalProcessHeadWh" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="border-radius: 20px">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Process Withdrawal</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="<?= base_url('/warehouse/c_withdrawalProcess/withdrawalProcess') ?>" method="POST">
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="wdNumber">Withdrawal Number</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="hidden" class="form-control" name="wdId" id="wdIdHeadWh" placeholder="Withdrawal Number..." value="" autocomplete="off" readonly>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control" name="wdNumber" id="wdNumberHeadWh" placeholder="Withdrawal Number..." value="" autocomplete="off" readonly>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="wdJoNumber">JO Number</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control" name="wdJoNumber" id="wdJoNumberHeadWh" placeholder="JO Number..." value="" autocomplete="off" readonly>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="wdProductCode">Product Code</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control" name="wdProductCode" id="wdProductCodeHeadWh" placeholder="Product Code..." value="" autocomplete="off" readonly>
                        </div>
                        <div class="form-group col-md-8">
                            <label for="wdProductCode">Product Name</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control" name="wdProductName" id="wdProductNameHeadWh" placeholder="Product Name..." value="" autocomplete="off" readonly>
                        </div>
                        <input type="hidden" class="form-control" name="wdFromWh" id="wdFromWh" value="" autocomplete="off" readonly>
                        <input type="hidden" class="form-control" name="wdToWh" id="wdToWh" value="" autocomplete="off" readonly>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="wdReqQty">Request Qty</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control" name="wdReqQty" id="wdReqQtyHeadWh" placeholder="Req Qty..." value="" autocomplete="off" readonly>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="wdReqQty">Stock Available</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control" name="wdStockAvailable" id="wdStockAvailableHeadWh" placeholder="Stock Available..." value="" autocomplete="off" readonly>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="wdBomQty">BOM Qty</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control" name="wdBomQty" id="wdBomQtyHeadWh" placeholder="BOM Qty..." value="" autocomplete="off" readonly>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="wdRemark">Remark</label>
                            <textarea style="border-radius: 15px; padding-left: 12px" name="wdRemark" id="wdRemarkHeadWh" class="form-control form-control-lg" readonly></textarea>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="wdReceiptQty">Receipt Qty</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control" name="wdReceiptQty" id="wdReceiptQtyHeadWh" placeholder="Receipt Qty..." value="" autocomplete="off" readonly>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="wdRemarkWh">Remark Warehouse</label>
                            <textarea style="border-radius: 15px; padding-left: 12px" name="wdRemarkWh" id="wdRemarkWhHeadWh" class="form-control form-control-lg" readonly></textarea>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modals -->

<!-- modals add new data user -->
<div class="modal fade" id="modalAddUser" aria-labelledby="modalAddUser" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="border-radius: 20px">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Insert new user</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="<?= base_url('management/c_userManagement/insertUser') ?>" method="POST">
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-md-3">
                            <label for="productCode">Username</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('username') ? 'is-invalid' : '' ?>" name="username" id="username" placeholder="Username..." value="<?= old("username"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError('username') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="productType">Level</label>
                            <select class="form-control form-select text-dark <?= ($validation->hasError('userLevel') ? 'is-invalid' : '') ?>" name="userLevel" id="userLevel" required>
                                <option disabled selected value="">Choose...</option>
                                <?php foreach($level as $l) : ?>
                                    <option value="<?= $l['level_id'] ?>" <?= (old('userLevel') == $l['level_id'] ? 'selected' : '') ?>><?= $l['level_id'] ?> - <?= $l['level_name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                            <div class="invalid-feedback">
                                <?= $validation->getError('userLevel') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="productType">Fullname</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('fullname') ? 'is-invalid' : '' ?>" name="fullname" id="fullname" placeholder="Fullname..." value="<?= old("fullname"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError('fullname') ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="productName">Password</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="password" class="form-control <?= $validation->hasError('password') ? 'is-invalid' : '' ?>" name="password" id="password" placeholder="Password..." value="<?= old("password"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError('password') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="productName">Confirmation Password</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="password" class="form-control <?= $validation->hasError('passwordConfirmation') ? 'is-invalid' : '' ?>" name="passwordConfirmation" id="passwordConfirmation" placeholder="Confirmation Password..." value="<?= old("passwordConfirmation"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError('passwordConfirmation') ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-3">
                            <label for="productDescription">Department</label>
                            <select class="form-control form-select text-dark <?= ($validation->hasError('userDept') ? 'is-invalid' : '') ?>" name="userDept" id="userDept" required>
                                <option disabled selected value="">Choose...</option>
                                <?php foreach($dept as $d) : ?>
                                    <option value="<?= $d['dept_id'] ?>" <?= (old('userDept') == $d['dept_id'] ? 'selected' : '') ?>><?= $d['dept_id'] ?> - <?= $d['dept_name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                            <div class="invalid-feedback">
                                <?= $validation->getError('userDept') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="productDescription">Position</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('userPosition') ? 'is-invalid' : '' ?>" name="userPosition" id="userPosition" placeholder="Position..." value="<?= old("userPosition"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError('userPosition') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="productDescription">Email</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="email" class="form-control <?= $validation->hasError('userEmail') ? 'is-invalid' : '' ?>" name="userEmail" id="userEmail" placeholder="Email..." value="<?= old("userEmail"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError('userEmail') ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="productDescription">Department Handle</label>
                            <select class="js-example-basic-multiple w-100 <?= $validation->hasError('deptHandle') ? 'is-invalid' : '' ?>" id="addUserDeptHandle" name="deptHandle[]" multiple="multiple">
                                <?php foreach($dept as $row) : ?>
                                    <option value="<?= $row['dept_id'] ?>" ><?= $row['dept_id'] ?> - <?= $row['dept_name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                            <div class="invalid-feedback">
                                <?= $validation->getError('deptHandle') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-5">
                            <label for="productDescription">Status</label>
                            <select class="form-control form-select text-dark <?= ($validation->hasError('userStatus') ? 'is-invalid' : '') ?>" name="userStatus" id="userStatus" required>
                                <option disabled selected value="">Choose...</option>
                                <?php foreach($status as $s) : ?>
                                    <option value="<?= $s['status_id'] ?>" <?= (old('userStatus') == $s['status_id'] ? 'selected' : '') ?>><?= $s['status_id'] ?> - <?= $s['status_name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                            <div class="invalid-feedback">
                                <?= $validation->getError('userStatus') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="productDescription">Telp</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="number" class="form-control <?= $validation->hasError('userTelp') ? 'is-invalid' : '' ?>" name="userTelp" id="userTelp" placeholder="Telp..." value="<?= old("userTelp"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError('userTelp') ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="productDescription">Address</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('userAddress') ? 'is-invalid' : '' ?>" name="userAddress" id="userAddress" placeholder="Address..." value="<?= old("userAddress"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError('userAddress') ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="px-3 pb-3">
                    <button type="submit" class="btn btn-success">Submit</button>
                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modals -->

<!-- modals reg data user -->
<div class="modal fade" id="modalEditUser" aria-labelledby="modalEditUser" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="border-radius: 20px">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update data user</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="<?= base_url('management/c_userManagement/editUser') ?>" method="POST">
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-md-3">
                            <label for="productCode">Username</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="hidden" class="form-control" name="editUserId" id="editUserId" autocomplete="off" required>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('editUsername') ? 'is-invalid' : '' ?>" name="editUsername" id="editUsername" placeholder="Username..." value="<?= old("editUsername"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">  
                                <?= $validation->getError('editUsername') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="productType">Level</label>
                            <select class="form-control form-select text-dark <?= ($validation->hasError('editUserLevel') ? 'is-invalid' : '') ?>" name="editUserLevel" id="editUserLevel" required>
                                <option disabled selected value="">Choose...</option>
                                <?php foreach($level as $l) : ?>
                                    <option value="<?= $l['level_id'] ?>" <?= (old('editUserLevel') == $l['level_id'] ? 'selected' : '') ?>><?= $l['level_id'] ?> - <?= $l['level_name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                            <div class="invalid-feedback">
                                <?= $validation->getError('editUserLevel') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="productType">Fullname</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('editFullname') ? 'is-invalid' : '' ?>" name="editFullname" id="editFullname" placeholder="Fullname..." value="<?= old("editFullname"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError('editFullname') ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="productName">Password</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="password" class="form-control <?= $validation->hasError('editPassword') ? 'is-invalid' : '' ?>" name="editPassword" id="editPassword" placeholder="Password..." value="<?= old("editPassword"); ?>" autocomplete="off" readonly>
                            <div class="form-check mx-sm-2">
                            <label class="form-check-label">
                                <input type="checkbox" class="form-check-input" onchange="updatePass()">
                                Check before update password !
                                <i class="input-helper"></i></label>
                            </div>
                            <div class="invalid-feedback">
                                <?= $validation->getError('editPassword') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="productName">Confirmation Password</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="password" class="form-control <?= $validation->hasError('editPasswordConfirmation') ? 'is-invalid' : '' ?>" name="editPasswordConfirmation" id="editPasswordConfirmation" placeholder="Confirmation Password..." value="<?= old("editPasswordConfirmation"); ?>" autocomplete="off" readonly>
                            <div class="invalid-feedback">
                                <?= $validation->getError('editPasswordConfirmation') ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-3">
                            <label for="productDescription">Department</label>
                            <select class="form-control form-select text-dark <?= ($validation->hasError('editUserDept') ? 'is-invalid' : '') ?>" name="editUserDept" id="editUserDept" required>
                                <option disabled selected value="">Choose...</option>
                                <?php foreach($dept as $d) : ?>
                                    <option value="<?= $d['dept_id'] ?>" <?= (old('editUserDept') == $d['dept_id'] ? 'selected' : '') ?>><?= $d['dept_id'] ?> - <?= $d['dept_name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                            <div class="invalid-feedback">
                                <?= $validation->getError('editUserDept') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="productDescription">Position</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('editUserPosition') ? 'is-invalid' : '' ?>" name="editUserPosition" id="editUserPosition" placeholder="Position..." value="<?= old("editUserPosition"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError('editUserPosition') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="productDescription">Email</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="email" class="form-control <?= $validation->hasError('editUserEmail') ? 'is-invalid' : '' ?>" name="editUserEmail" id="editUserEmail" placeholder="Email..." value="<?= old("editUserEmail"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError('editUserEmail') ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="productDescription">Department Handle</label>
                            <select class="js-example-basic-multiple w-100 <?= $validation->hasError('editDeptHandle') ? 'is-invalid' : '' ?>" id="editUserDeptHandle" name="editDeptHandle[]" multiple="multiple">
                                <?php foreach($dept as $d) : ?>
                                    <option value="<?= $d['dept_id'] ?>"><?= $d['dept_id'] ?> - <?= $d['dept_name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                            <div class="invalid-feedback">
                                <?= $validation->getError('editDeptHandle') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-5">
                            <label for="productDescription">Status</label>
                            <select class="form-control form-select text-dark <?= ($validation->hasError('editUserStatus') ? 'is-invalid' : '') ?>" name="editUserStatus" id="editUserStatus" required>
                                <option disabled selected value="">Choose...</option>
                                <?php foreach($status as $s) : ?>
                                    <option value="<?= $s['status_id'] ?>" <?= (old('editUserStatus') == $s['status_id'] ? 'selected' : '') ?>><?= $s['status_id'] ?> - <?= $s['status_name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                            <div class="invalid-feedback">
                                <?= $validation->getError('editUserStatus') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="productDescription">Telp</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="number" class="form-control <?= $validation->hasError('editUserTelp') ? 'is-invalid' : '' ?>" name="editUserTelp" id="editUserTelp" placeholder="Telp..." value="<?= old("editUserTelp"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError('editUserTelp') ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="productDescription">Address</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('editUserAddress') ? 'is-invalid' : '' ?>" name="editUserAddress" id="editUserAddress" placeholder="Address..." value="<?= old("editUserAddress"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError('editUserAddress') ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="px-3 pb-3">
                    <button type="submit" class="btn btn-success">Submit</button>
                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modals -->

<!-- modals preview request data user -->
<div class="modal fade" id="modalApproveUser" aria-labelledby="modalApproveUser" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="border-radius: 20px">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Approve data user</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-md-3">
                        <label for="productCode">Username</label>
                        <input style="border-radius: 15px; padding-left: 12px" type="hidden" class="form-control" name="approveUserId" id="approveUserId" autocomplete="off" readonly>
                        <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('approveUsername') ? 'is-invalid' : '' ?>" name="approveUsername" id="approveUsername" placeholder="Username..." value="<?= old("approveUsername"); ?>" autocomplete="off" readonly>
                        <div class="invalid-feedback">  
                            <?= $validation->getError('approveUsername') ?>
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="productType">Level</label>
                        <select class="form-control form-select text-dark <?= ($validation->hasError('approveUserLevel') ? 'is-invalid' : '') ?>" name="approveUserLevel" id="approveUserLevel" readonly disabled>
                            <option disabled selected value="">Choose...</option>
                            <?php foreach($level as $l) : ?>
                                <option value="<?= $l['level_id'] ?>" <?= (old('approveUserLevel') == $l['level_id'] ? 'selected' : '') ?>><?= $l['level_id'] ?> - <?= $l['level_name'] ?></option>
                            <?php endforeach; ?>
                        </select>
                        <div class="invalid-feedback">
                            <?= $validation->getError('approveUserLevel') ?>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="productType">Fullname</label>
                        <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('approveFullname') ? 'is-invalid' : '' ?>" name="approveFullname" id="approveFullname" placeholder="Fullname..." value="<?= old("approveFullname"); ?>" autocomplete="off" readonly>
                        <div class="invalid-feedback">
                            <?= $validation->getError('approveFullname') ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-3">
                        <label for="productDescription">Department</label>
                        <select class="form-control form-select text-dark <?= ($validation->hasError('approveUserDept') ? 'is-invalid' : '') ?>" name="approveUserDept" id="approveUserDept" disabled>
                            <option disabled selected value="">Choose...</option>
                            <?php foreach($dept as $d) : ?>
                                <option value="<?= $d['dept_id'] ?>" <?= (old('approveUserDept') == $d['dept_id'] ? 'selected' : '') ?>><?= $d['dept_id'] ?> - <?= $d['dept_name'] ?></option>
                            <?php endforeach; ?>
                        </select>
                        <div class="invalid-feedback">
                            <?= $validation->getError('approveUserDept') ?>
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="productDescription">Position</label>
                        <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('approveUserPosition') ? 'is-invalid' : '' ?>" name="approveUserPosition" id="approveUserPosition" placeholder="Position..." value="<?= old("approveUserPosition"); ?>" autocomplete="off" readonly>
                        <div class="invalid-feedback">
                            <?= $validation->getError('approveUserPosition') ?>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="productDescription">Email</label>
                        <input style="border-radius: 15px; padding-left: 12px" type="email" class="form-control <?= $validation->hasError('approveUserEmail') ? 'is-invalid' : '' ?>" name="approveUserEmail" id="approveUserEmail" placeholder="Email..." value="<?= old("approveUserEmail"); ?>" autocomplete="off" readonly>
                        <div class="invalid-feedback">
                            <?= $validation->getError('approveUserEmail') ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-4">
                        <label for="productDescription">Department Handle</label>
                        <select class="js-example-basic-multiple w-100 <?= $validation->hasError('approveDeptHandle') ? 'is-invalid' : '' ?>" id="approveUserDeptHandle" name="approveDeptHandle[]" multiple="multiple" disabled>
                            <?php foreach($dept as $d) : ?>
                                <option value="<?= $d['dept_id'] ?>"><?= $d['dept_id'] ?> - <?= $d['dept_name'] ?></option>
                            <?php endforeach; ?>
                        </select>
                        <div class="invalid-feedback">
                            <?= $validation->getError('approveDeptHandle') ?>
                        </div>
                    </div>
                    <div class="form-group col-md-5">
                        <label for="productDescription">Status</label>
                        <select class="form-control form-select text-dark <?= ($validation->hasError('approveUserStatus') ? 'is-invalid' : '') ?>" name="approveUserStatus" id="approveUserStatus" disabled>
                            <option disabled selected value="">Choose...</option>
                            <?php foreach($status as $s) : ?>
                                <option value="<?= $s['status_id'] ?>" <?= (old('approveUserStatus') == $s['status_id'] ? 'selected' : '') ?>><?= $s['status_id'] ?> - <?= $s['status_name'] ?></option>
                            <?php endforeach; ?>
                        </select>
                        <div class="invalid-feedback">
                            <?= $validation->getError('approveUserStatus') ?>
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="productDescription">Telp</label>
                        <input style="border-radius: 15px; padding-left: 12px" type="number" class="form-control <?= $validation->hasError('approveUserTelp') ? 'is-invalid' : '' ?>" name="approveUserTelp" id="approveUserTelp" placeholder="Telp..." value="<?= old("approveUserTelp"); ?>" autocomplete="off" readonly>
                        <div class="invalid-feedback">
                            <?= $validation->getError('approveUserTelp') ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-12">
                        <label for="productDescription">Address</label>
                        <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('approveUserAddress') ? 'is-invalid' : '' ?>" name="approveUserAddress" id="approveUserAddress" placeholder="Address..." value="<?= old("approveUserAddress"); ?>" autocomplete="off" readonly>
                        <div class="invalid-feedback">
                            <?= $validation->getError('approveUserAddress') ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end modals -->

<!-- modals delete -->
<div class="modal fade" id="modalDelete" tabindex="-1" aria-labelledby="modalDelete" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content" style="border-radius: 20px">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Delete item here !</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="" method="post">
                <input type="hidden" name="deleteId" class="deleteId" id="deleteId">
                <input type="hidden" name="importId" class="importId" id="importId">
                <h5 class="text-center py-5">Are you sure ?</h5>
                <div class="text-center px-3 pb-3">
                    <button type="submit" name="delete" class="btn btn-success text-white">Confirm</button>
                    <button type="button" class="btn btn-danger text-white" data-bs-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modals -->

<!-- modals approve reject user -->
<div class="modal fade" id="modalApproveRejectUser" tabindex="-1" aria-labelledby="modalApproveRejectUser" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content" style="border-radius: 20px">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Process user here !</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="" method="post">
                <input type="hidden" name="statusUser" class="statusUser" id="statusUser">
                <input type="hidden" name="infoUser" class="infoUser" id="infoUser">
                <input type="hidden" name="idUser" class="idUser" id="idUser">
                <h5 class="text-center pt-5 pb-3" id='infoProcessUser'>Are you sure ?</h5>
                <div class="row justify-content-center pb-4">
                    <div class="form-group col-10">
                        <label for="">Level User</label>
                        <select class="form-control form-select text-dark" name="levelUser" id="levelUser">
                            <option disabled selected value="">Choose...</option>
                            <?php foreach($level as $l) : ?>
                                <option value="<?= $l['level_id'] ?>"?><?= $l['level_id'] ?> - <?= $l['level_name'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="text-center px-3 pb-3">
                    <button type="submit" name="delete" class="btn btn-success text-white">Confirm</button>
                    <button type="button" class="btn btn-danger text-white" data-bs-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modals -->

<!-- modals approve withdrawal -->
<div class="modal fade" id="modalApproveWithdrawal" tabindex="-1" aria-labelledby="modalApproveWithdrawal" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content" style="border-radius: 20px">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Process withdrawal here !</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="" method="post" id="confirmApproveReject">
                <input type="hidden" name="statusWd" class="statusWd" id="statusWd">
                <input type="hidden" name="infoWd" class="infoWd" id="infoWd">
                <input type="hidden" name="withdrawalId" class="withdrawalId" id="withdrawalId">
                <input type="hidden" name="productCodeWithdrawal" class="productCodeWithdrawal" id="productCodeWithdrawal">
                <input type="hidden" name="fromWarehouseWithdrawal" class="fromWarehouseWithdrawal" id="fromWarehouseWithdrawal">
                <input type="hidden" name="toWarehouseWithdrawal" class="toWarehouseWithdrawal" id="toWarehouseWithdrawal">
                <input type="hidden" name="receiptQty" class="receiptQty" id="receiptQty">
                <input type="hidden" name="remarkWh" class="remarkWh" id="remarkWh">
                <h5 class="text-center py-5" id='infoProcessWithdrawal'>Are you sure ?</h5>
                <div class="text-center px-3 pb-3">
                    <button type="submit" name="delete" class="btn btn-success text-white">Confirm</button>
                    <button type="button" class="btn btn-danger text-white" data-bs-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modals -->

<!-- modals Export Withdrawal History -->
<div class="modal fade" id="modalExportWithdrawalHistory" tabindex="-1" aria-labelledby="modalExportWithdrawalHistory" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content" style="border-radius: 20px">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Export Withdrawal History !</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="<?= base_url('warehouse/c_withdrawalHistory/exportWithdrawalHistory') ?>" method="post">
                <div class="row justify-content-center py-4">
                    <div class="form-group col-10">
                        <label for="fromDate">From Date ?</label>
                        <input type="date" name="fromDate" class="fromDate form-control" id="fromDate">
                        <label for="toDate" class="pt-3">To Date ?</label>
                        <input type="date" name="toDate" class="toDate form-control" id="toDate">
                    </div>
                </div>
                <div class="text-center px-3 pb-3">
                    <button type="submit" class="btn btn-success text-white">Confirm</button>
                    <button type="button" class="btn btn-danger text-white" data-bs-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modals -->

<!-- modals import bom -->
<div class="modal fade" id="modalImportBom" tabindex="-1" aria-labelledby="modalImportBom" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Import Build of Materials</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="<?= base_url('pde/c_bom/importBom') ?>" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="form-group">
                        <label>File upload</label>
                        <input type="file" required name="importBom" class="file-upload-default" accept=".xlsx,.xls">
                        <div class="input-group col-xs-12">
                        <input type="text" class="form-control file-upload-info" disabled placeholder="Upload Bom...">
                        <span class="input-group-append">
                            <button class="file-upload-browse btn btn-primary" type="button">Browse</button>
                        </span>
                        </div>
                    </div>
                </div>
                <div class="px-4 pb-3">
                    <button type="submit" class="btn btn-success">Upload</button>
                    <a href="<?= base_url('/pde/c_bom/exportTemplateBom') ?>" class="text-decoration-none text-white"><button type="button" class="btn btn-warning">Download Template</button></a>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modals -->

<!-- modals insert data master bom -->
<div class="modal fade" id="modalAddBom" tabindex="-1" aria-labelledby="modalAddBom" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="border-radius: 20px">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Insert data build of materials</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="<?= base_url('pde/c_bom/insertBom') ?>" method="POST">
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="productCode">Bom Type*</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('addBomType') ? 'is-invalid' : '' ?>" name="addBomType" id="addBomType" placeholder="Bom Type..." value="<?= old("addBomType"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError('addBomType') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="productType">Quantity Required*</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="number" step="any" class="form-control <?= $validation->hasError('addBomQty') ? 'is-invalid' : '' ?>" name="addBomQty" id="addBomQty" placeholder="Quantity Required..." value="<?= old("addBomQty"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError('addBomQty') ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="productName">Assembly Code*</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('addBomAssembly') ? 'is-invalid' : '' ?>" name="addBomAssembly" id="addBomAssembly" placeholder="Assembly Code..." value="<?= old("addBomAssembly"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError('addBomAssembly') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="productName">Product Code*</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('addBomProduct') ? 'is-invalid' : '' ?>" name="addBomProduct" id="addBomProduct" placeholder="Product Code..." value="<?= old("addBomProduct"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError('addBomProduct') ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="px-3 pb-3">
                    <button type="submit" class="btn btn-success">Submit</button>
                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modals -->

<!-- modals edit data master bom -->
<div class="modal fade" id="modalEditBom" tabindex="-1" aria-labelledby="modalEditBom" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="border-radius: 20px">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update data build of materials</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="<?= base_url('pde/c_bom/editBom') ?>" method="POST">
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="productCode">Bom Type*</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="hidden" class="form-control" name="editBomId" id="editBomId" value="" autocomplete="off" required>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('editBomType') ? 'is-invalid' : '' ?>" name="editBomType" id="editBomType" placeholder="Bom Type..." value="<?= old("editBomType"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError('editBomType') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="productType">Quantity Required*</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="number" step="any" class="form-control <?= $validation->hasError('editBomQuantityNeeded') ? 'is-invalid' : '' ?>" name="editBomQuantityNeeded" id="editBomQuantityNeeded" placeholder="Quantity Required..." value="<?= old("editBomQuantityNeeded"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError('editBomQuantityNeeded') ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="productName">Assembly Code*</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('editBomAssemblyCode') ? 'is-invalid' : '' ?>" name="editBomAssemblyCode" id="editBomAssemblyCode" placeholder="Assembly Code..." value="<?= old("editBomAssemblyCode"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError('editBomAssemblyCode') ?>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="productName">Product Code*</label>
                            <input style="border-radius: 15px; padding-left: 12px" type="text" class="form-control <?= $validation->hasError('editBomProductCode') ? 'is-invalid' : '' ?>" name="editBomProductCode" id="editBomProductCode" placeholder="Product Code..." value="<?= old("editBomProductCode"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError('editBomProductCode') ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="px-3 pb-3">
                    <button type="submit" class="btn btn-success">Submit</button>
                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modals -->

<!-- modals approve withdrawal -->
<div class="modal fade" id="modalProcessAssembly" tabindex="-1" aria-labelledby="modalProcessAssembly" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content" style="border-radius: 20px">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Process assembly here !</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="" method="post">
                <input type="hidden" name="infoProcessAssembly" class="infoProcessAssembly" id="infoProcessAssembly">
                <input type="hidden" name="joNumberProcessAssembly" class="joNumberProcessAssembly" id="joNumberProcessAssembly">
                <input type="hidden" name="assemblyCodeProcessAssembly" class="assemblyCodeProcessAssembly" id="assemblyCodeProcessAssembly">
                <input type="hidden" name="idProcessAssembly" class="idProcessAssembly" id="idProcessAssembly">
                <input type="hidden" name="productCodeProcessAssembly" class="productCodeProcessAssembly" id="productCodeProcessAssembly">
                <input type="hidden" name="balanceProcessQty" class="balanceProcessQty" id="balanceProcessQty">
                <input type="hidden" name="productQty" class="productQty" id="productQty">
                <h5 class="text-center py-5" id='notifProcessAssembly'>Are you sure ?</h5>
                <div class="text-center px-3 pb-3">
                    <button type="submit" name="delete" class="btn btn-success text-white">Confirm</button>
                    <button type="button" class="btn btn-danger text-white" data-bs-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modals -->

<!-- modals change password -->
<div class="modal fade" id="modalChangePassword" tabindex="-1" aria-labelledby="modalChangePassword" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content" style="border-radius: 20px">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Change password here !</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="<?= base_url('auth/c_auth/changePassword') ?>" method="post">
                <div class="row justify-content-center">
                    <div class="col-10 mt-4">
                        <label for="">Old Password</label>
                        <input type="hidden" name="changePasswordId" class="changePasswordId form-control mt-2 <?= $validation->hasError('changePasswordOld') ? 'is-invalid' : '' ?>" id="changePasswordId" value="<?= session()->get('user_id') ?>">
                        <input type="password" name="changePasswordOld" class="changePasswordOld form-control mt-2" id="changePasswordOld" value="<?= old('changePasswordOld') ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError('changePasswordOld') ?>
                        </div> 
                    </div>
                    <div class="col-10 my-3">
                        <label for="">New Password</label>
                        <input type="password" name="changePasswordNew" class="changePasswordNew form-control mt-2 <?= $validation->hasError('changePasswordNew') ? 'is-invalid' : '' ?>" id="changePasswordNew" value="<?= old('changePasswordNew') ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError('changePasswordNew') ?>
                        </div> 
                    </div>
                    <div class="col-10 mb-5">
                        <label for="">Confirmation Password</label>
                        <input type="password" name="changePasswordConfirmation" class="changePasswordConfirmation form-control mt-2 <?= $validation->hasError('changePasswordConfirmation') ? 'is-invalid' : '' ?>" id="changePasswordConfirmation" value="<?= old('changePasswordConfirmation') ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError('changePasswordConfirmation') ?>
                        </div> 
                    </div>
                </div>
                <div class="text-center px-3 pb-3">
                    <button type="submit" name="changePassword" class="btn btn-success text-white">Confirm</button>
                    <button type="button" class="btn btn-danger text-white" data-bs-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modals -->

<!-- modals edit feature -->
<div class="modal fade" id="modalEditFeature" tabindex="-1" aria-labelledby="modalEditFeature" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content" style="border-radius: 20px">
            <div class="modal-header">
                <h5 class="modal-title" id="editFeature">Edit feature here !</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="<?= base_url('management/c_otherManagement/editFeature') ?>" method="post">
                <div class="row justify-content-center">
                    <div class="col-10 my-3">
                        <label for="">Feature ID</label>
                        <input type="hidden" required name="editDataBaseFeature" class="editDataBaseFeature form-control mt-2 <?= $validation->hasError('editDataBaseFeature') ? 'is-invalid' : '' ?>" id="editDataBaseFeature" value="<?= old('editDataBaseFeature') ?>">
                        <input type="hidden" required name="editRealFeatureId" class="editRealFeatureId form-control mt-2 <?= $validation->hasError('editRealFeatureId') ? 'is-invalid' : '' ?>" id="editRealFeatureId" value="<?= old('editRealFeatureId') ?>">
                        <input type="hidden" required name="editRealFeatureName" class="editRealFeatureName form-control mt-2 <?= $validation->hasError('editRealFeatureName') ? 'is-invalid' : '' ?>" id="editRealFeatureName" value="<?= old('editRealFeatureName') ?>">
                        <input type="number" required name="editFeatureId" class="editFeatureId form-control mt-2 <?= $validation->hasError('editFeatureId') ? 'is-invalid' : '' ?>" id="editFeatureId" value="<?= old('editFeatureId') ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError('editFeatureId') ?>
                        </div> 
                    </div>
                    <div class="col-10 mb-5">
                        <label for="">Feature Name</label>
                        <input type="text" required name="editFeatureName" class="editFeatureName form-control mt-2 <?= $validation->hasError('editFeatureName') ? 'is-invalid' : '' ?>" id="editFeatureName" value="<?= old('editFeatureName') ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError('editFeatureName') ?>
                        </div> 
                    </div>
                </div>
                <div class="text-center px-3 pb-3">
                    <button type="submit" name="editFeature" class="btn btn-success text-white">Confirm</button>
                    <button type="button" class="btn btn-danger text-white" data-bs-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modals -->

