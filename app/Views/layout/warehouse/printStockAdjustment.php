<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PT Pacific Furniture - Stock Adjustment</title>
    <!-- Custom CSS -->
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap');

        * {
            font-family: 'Poppins', sans-serif;
        }

        .topright { position: absolute; top: 0px; right: 10px; text-align: right; }
        .topleft { position: absolute; top: 0px; left: 10px; text-align: left; }
    </style>
</head>

<body>
    <div class="topright"><p style="font-size: 10px">http://192.168.0.202/warehouse/c_stockAdjustmentLog/printSaLog/<?= $stockAdjustment[0]['insert_sa_number'] ?>/multiple</p></div>
    <div class="topleft"><p style="font-size: 10px"><?php date_default_timezone_set('Asia/Kuala_Lumpur'); echo date("F j, Y, g:i a") ?></p></div>
    <div class="container" style="width: 100%; height: 100%;"> 

        <div class="head" style="text-align: center; border-bottom: 2px solid black">
            <h4>PT Pacific Furniture</h4>
            <p style="font-size: 13px; padding-top: -15px">Jl. Tugu Wijaya III No.12 Kawasan Industri Wijayakusuma, Semarang, Jawa Tengah</p>
        </div>

        <div class="info" style="margin-top: 0px;">
            <h4 style="text-align: center;">Stock Adjustment Report</h4>
            <p style="font-size: 13px">Stock Adjustment Number : <?= $stockAdjustment[0]['insert_sa_number'] ?></p>
            <p style="font-size: 13px; padding-top: -10px">Business Date : <?php $tgl = $stockAdjustment[0]['insert_sa_date']; echo date('d F Y', strtotime($tgl)); ?></p>
            <p style="font-size: 13px; padding-top: -10px">Memo : <?= $stockAdjustment[0]['insert_sa_memo'] ?> Department </p>
        </div>

        <div class="tabel" style="margin-top: 0px">
            <table border='1' style="border-collapse: collapse;" cellpadding='8' cellspacing='0' width="100%">
                <thead>
                    <tr>
                        <th><p style="font-size: 12px">No</p></th>
                        <th><p style="font-size: 12px">Product Code</p></th>
                        <th><p style="font-size: 12px">Product Name</p></th>
                        <th><p style="font-size: 12px">Qty</p></th>
                        <th><p style="font-size: 12px">Uom</p></th>
                        <th><p style="font-size: 12px">Type</p></th>
                        <th><p style="font-size: 12px">Reason</p></th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no = 1; ?>
                    <?php foreach($stockAdjustment as $sa) : ?>
                        <?php $color = $sa['insert_sa_type'] == 1 ? 'color: #198754' : 'color: #DC3545'; ?>
                        <tr>
                            <td style="width: 20px"><p style="font-size: 12px;"><?= $no++ ?></p></td>
                            <td style="width: 120px"><p style="font-size: 12px;"><?= $sa['insert_sa_product_code'] ?></p></td>
                            <td style="width: 200px"><p style="font-size: 12px;"><?= $sa['product_name'] ?></p></td>
                            <td style="width: 50px"><p style="font-size: 12px;"><?= $sa['insert_sa_quantity'] ?></p></td>
                            <td style="width: 70px"><p style="font-size: 12px;"><?= $sa['product_uom'] ?></p></td>
                            <td style="width: 50px"><p style="font-size: 12px; <?= $color ?>"><?= ($sa['insert_sa_type'] == 1 ? 'IN' : 'OUT') ?></p></td>
                            <td style="width: 100px"><p style="font-size: 12px;"><?= $sa['reason_name'] ?></p></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
       
    </div>
</body>

</html>

