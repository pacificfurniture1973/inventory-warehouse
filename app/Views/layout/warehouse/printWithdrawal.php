<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PT Pacific Furniture - Withdrawal</title>
    <!-- Custom CSS -->
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap');

        * {
            font-family: 'Poppins', sans-serif;
        }

        /* table {
            border: 1px solid black;
        } */

        /* #content { width: 300px; border: 1px solid black; position: relative; } */
        .topright { position: absolute; top: 0px; right: 10px; text-align: right; }
        .topleft { position: absolute; top: 0px; left: 10px; text-align: left; }
    </style>
</head>

<body>
    <div class="topright"><p style="font-size: 10px">System generated document, manual signature is not needed.</p></div>
    <div class="topleft"><p style="font-size: 10px"><?php date_default_timezone_set('Asia/Kuala_Lumpur'); echo date("F j, Y, g:i a") ?></p></div>
    <div class="container" style="width: 100%; height: 100%;"> 

        <div class="head" style="text-align: center; border-bottom: 2px solid black">
            <h4>PT Pacific Furniture</h4>
            <p style="font-size: 13px; padding-top: -15px">Jl. Tugu Wijaya III No.12 Kawasan Industri Wijayakusuma, Semarang, Jawa Tengah</p>
        </div>

        <div class="info" style="margin-top: 0px;">
            <h4 style="text-align: center;">Withdrawal Slip Note</h4>
            <p style="font-size: 13px">Withdrawal Number : <?= $withdrawal[0]['wd_number'] ?></p>
            <p style="font-size: 13px; padding-top: -10px">Department : <?= $withdrawal[0]['dept_name'] ?> Department </p>
        </div>

        <div class="tabel" style="margin-top: 0px">
            <table border='1' style="border-collapse: collapse;" cellpadding='8' cellspacing='0' width="100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Product Code</th>
                        <th>Product Name</th>
                        <th>Request</th>
                        <th>Receipt</th>
                        <th>Uom</th>
                        <th>Job Order</th>
                        <th>Issue</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no = 1; ?>
                    <?php foreach($withdrawal as $dw) : ?>
                        <?php $color = $dw['wd_status'] == 6 ? 'color: #198754' : ($dw['wd_status'] == 7 ? 'color: #DC3545' : 'color: #FFC107'); ?>
                        <tr>
                            <td><?= $no++ ?></td>
                            <td><?= $dw['wd_product_code'] ?></td>
                            <td style="width: 150px"><?= $dw['product_name'] ?></td>
                            <td><?= $dw['wd_request_quantity'] ?></td>
                            <td><?= $dw['wd_receipt_quantity_warehouse'] ?></td>
                            <td><?= $dw['product_uom'] ?></td>
                            <td><?= $dw['wd_jo_number'] ?></td>
                            <td style="width: 120px"><?= $dw['wd_issue_date'] ?></td>
                            <td><p style="<?= $color ?>"><?= $dw['status_name'] ?></p></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>

        <div class="signature" style="margin-top: 10px">
            <table width="100%">
                <tbody>
                    <tr>
                        <td width: 50% style="text-align: center">
                            <p style="font-size: 14px">Released By</p>
                        </td>
                        <td width: 50% style="text-align: center">
                            <p style="font-size: 14px">Review</p>
                        </td>
                        <td width: 50% style="text-align: center">
                            <p style="font-size: 14px">Received By</p>
                        </td>                
                    </tr>
                    <tr>
                        <td width: 50% style="text-align: center; padding-top: 10px">
                            <p style="font-size: 12px">Warehouse</p>
                        </td>
                        <td width: 50% style="text-align: center; padding-top: 10px">
                            <p style="font-size: 12px">Warehouse Head</p>
                        </td>
                        <td width: 50% style="text-align: center; padding-top: 10px;">
                            <p style="font-size: 12px"><?= $withdrawal[0]['username'] ?></p>
                        </td>                
                    </tr>
                </tbody>
            </table>
        </div>
       
    </div>
</body>

</html>

