<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Login Template</title>
  <link href="https://fonts.googleapis.com/css?family=Karla:400,700&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://cdn.materialdesignicons.com/4.8.95/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?= base_url('loginPage/assets/css/login.css') ?>">
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
</head>
<body>
  <main class="d-flex align-items-center min-vh-100 my-4 py-md-0">
    <div class="container">
      <div class="card login-card">
        <div class="row no-gutters">
          <div class="col-md-5">
            <img src="<?= base_url('loginPage/assets/images/login.jpg') ?>" alt="login" class="login-card-img">
          </div>
          <div class="col-md-7">
            <div class="card-body">
              <div class="brand-wrapper">
                <img src="<?= base_url('loginPage/assets/images/logo.svg') ?>" alt="logo" class="logo">
              </div>
              <p class="login-card-description">Sign Up your account</p> 
              <form action="<?= base_url('auth/c_auth/doSignUp') ?>" method="POST" style="max-width: 100%;">
                <!-- show alert -->
                <?php if(session()->getFlashdata('success_message')) : ?>
                  <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>
                    <?= session()->getFlashdata('success_message'); ?>
                  </div>
                <?php elseif(session()->getFlashdata('error_message')) : ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
                    <?= session()->getFlashdata('error_message'); ?>
                  </div>
                <?php endif; ?>
                <div class="form-row">
                  <div class="form-group col-6">
                    <label for="username" class="font-weight-bold">Username</label>
                    <input type="text" name="regUsername" id="regUsername" class="form-control <?= ($validation->hasError('regUsername') ? 'is-invalid' : '') ?>" placeholder="Username here..." value="<?= old('regUsername') ?>" required>
                    <div class="invalid-feedback">
                        <?= $validation->getError('regUsername') ?>
                    </div>
                  </div>
                  <div class="form-group col-6">
                    <label for="password" class="font-weight-bold">Fullname</label>
                    <input type="text" name="regFullname" id="regFullname" class="form-control <?= ($validation->hasError('regFullname') ? 'is-invalid' : '') ?>" placeholder="Fullname here..." value="<?= old('regFullname') ?>" required>
                    <div class="invalid-feedback">
                        <?= $validation->getError('regFullname') ?>
                    </div>
                  </div>
                </div>
                <div class="form-row">
                  <div class="form-group col-6">
                    <label for="password" class="font-weight-bold">Password</label>
                    <input type="password" name="regPassword" id="regPassword" class="form-control <?= ($validation->hasError('regPassword') ? 'is-invalid' : '') ?>" placeholder="Password..." value="<?= old('regPassword') ?>" required>
                    <div class="invalid-feedback">
                        <?= $validation->getError('regPassword') ?>
                    </div>
                  </div>
                  <div class="form-group col-6">
                    <label for="password" class="font-weight-bold">Confirmation</label>
                    <input type="password" name="regConfirmPassword" id="regConfirmPassword" class="form-control <?= ($validation->hasError('regConfirmPassword') ? 'is-invalid' : '') ?>" placeholder="Confirmation..." value="<?= old('regConfirmPassword') ?>" required>
                    <div class="invalid-feedback">
                        <?= $validation->getError('regConfirmPassword') ?>
                    </div>
                  </div>
                </div>
                <div class="form-row">
                  <div class="form-group col-6">
                    <label for="password" class="font-weight-bold">Position</label>
                    <input type="text" name="regPosition" id="regPosition" class="form-control <?= ($validation->hasError('regPosition') ? 'is-invalid' : '') ?>" placeholder="Position here..." value="<?= old('regPosition') ?>" required>
                    <div class="invalid-feedback">
                        <?= $validation->getError('regPosition') ?>
                    </div>
                  </div>
                  <div class="form-group col-6">
                    <label for="password" class="font-weight-bold">Telp</label>
                    <input type="number" name="regTelp" id="regTelp" class="form-control <?= ($validation->hasError('regTelp') ? 'is-invalid' : '') ?>" placeholder="Telp here..." value="<?= old('regTelp') ?>" required>
                    <div class="invalid-feedback">
                        <?= $validation->getError('regTelp') ?>
                    </div>
                  </div>
                </div>
                <div class="form-row">                    
                  <div class="form-group col-6">
                    <label for="password" class="font-weight-bold">Dept</label>
                    <select class="form-control form-select text-dark <?= ($validation->hasError('regDept') ? 'is-invalid' : '') ?>" name="regDept" id="regDept" required>
                        <option disabled selected value="">Choose...</option>
                        <?php foreach($dept as $d) : ?>
                            <option value="<?= $d['dept_id'] ?>" <?= (old('regDept') == $d['dept_id'] ? 'selected' : '') ?>><?= $d['dept_id'] ?> - <?= $d['dept_name'] ?></option>
                        <?php endforeach; ?>
                    </select>
                    <div class="invalid-feedback">
                        <?= $validation->getError('regDept') ?>
                    </div>
                  </div>
                  <div class="form-group col-6">
                    <label for="password" class="font-weight-bold">Dept Handle</label>
                    <select class="js-example-basic-multiple w-100 <?= $validation->hasError('regDeptHandle') ? 'is-invalid' : '' ?>" id="regDeptHandle" name="regDeptHandle[]" multiple="multiple">
                        <?php foreach($dept as $row) : ?>
                            <option value="<?= $row['dept_id'] ?>" ><?= $row['dept_id'] ?> - <?= $row['dept_name'] ?></option>
                        <?php endforeach; ?>
                    </select>
                    <div class="invalid-feedback">
                        <?= $validation->getError('regDeptHandle') ?>
                    </div>
                  </div>
                </div>
                <div class="form-group mb-4">
                  <label for="password" class="font-weight-bold">Email</label>
                  <input type="email" name="regEmail" id="regEmail" class="form-control <?= ($validation->hasError('regEmail') ? 'is-invalid' : '') ?>" placeholder="Email here..." value="<?= old('regEmail') ?>" required>
                  <div class="invalid-feedback">
                        <?= $validation->getError('regEmail') ?>
                    </div>
                </div>
                <div class="form-group mb-4">
                  <label for="password" class="font-weight-bold">Address</label>
                  <input type="text" name="regAddress" id="regAddress" class="form-control <?= ($validation->hasError('regAddress') ? 'is-invalid' : '') ?>" placeholder="Address here..." value="<?= old('regAddress') ?>" required>
                  <div class="invalid-feedback">
                      <?= $validation->getError('regAddress') ?>
                  </div>
                </div>
                <input name="login" id="login" class="btn btn-block login-btn mb-4" type="submit" value="Sign Up">
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
  <!-- <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script> -->
  <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>

  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

  <script type="text/javascript">

    $('#regDeptHandle').select2({
      // dropdownParent: $('#modalEditUser .modal-content'),
      width: '100%',
    });

    // jika ada validasi error maka return deptHandler di multiple select
    <?php if($validation->listErrors()) : ?>
      $('#regDeptHandle').val([<?= session()->getFlashdata('regDeptHandle') ?>]);
      $('#regDeptHandle').trigger('change');
    <?php endif; ?>

  </script>

</body>
</html>
