<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Inventory Warehouse</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="<?= base_url('vendors//feather/feather.css') ?>">
  <link rel="stylesheet" href="<?= base_url('vendors/mdi/css/materialdesignicons.min.css') ?>">
  <link rel="stylesheet" href="<?= base_url('vendors/ti-icons/css/themify-icons.css') ?>">
  <link rel="stylesheet" href="<?= base_url('vendors/typicons/typicons.css') ?>">
  <link rel="stylesheet" href="<?= base_url('vendors/simple-line-icons/css/simple-line-icons.css') ?>">
  <link rel="stylesheet" href="<?= base_url('vendors/css/vendor.bundle.base.css') ?>">
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
  <link rel="stylesheet" href="<?= base_url('vendors/font-awesome/css/fontawesome5/css/all.css') ?>">

  <!-- Datatables 5 -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.1/css/bootstrap.min.css">
  <!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap5.min.css"> -->
  <link rel="stylesheet" href="<?= base_url('dataTables/DataTables/css/dataTables.bootstrap5.min.css') ?>">

  ;<link rel="stylesheet" href="<?= base_url('js/jquery-ui/jquery-ui.css') ?>">

  <!-- endinject -->
  <!-- Plugin css for this page -->
  <link rel="stylesheet" href="<?= base_url('js/select.dataTables.min.css') ?>">
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?= base_url('css/vertical-layout-light/style.css') ?>">
  <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous"> -->
  <!-- endinject -->
  <link rel="shortcut icon" href="<?= base_url('images/favicon.png') ?>" />
</head>
<body>
  <!-- <div class="main-panel">
    <div class="content-wrapper"> -->
      <!-- <div class="row">
        <div class="col-sm-12"> -->
          <div class="card">
              <div class="card-body">

                <div class="home-tab">  
                  <div class="d-sm-flex align-items-center justify-content-between border-bottom">
                    <div>
                      <h4>Master Products</h4>
                    </div>
                    <div>
                      <div class="btn-wrapper">
                        <?php if(session()->get('level') <= 2) : ?>
                        <a href="#" class="btn btn-info text-white" data-bs-toggle="modal" data-bs-target="#modalAddProducts"><i class="icon-plus"></i> Add</a>
                        <a href="#" class="btn btn-success text-white" data-bs-toggle="modal" data-bs-target="#modalImportProducts"><i class="icon-upload"></i> Import</a>
                        <?php endif; ?>
                        <a href="<?= site_url('main/c_products/exportDataProducts') ?>" class="btn btn-primary text-white me-0"><i class="icon-download"></i> Export</a>
                      </div>
                    </div>
                  </div>  

                  <div class="table-responsive mt-4">
                    <table id="tabel_main_productLog" class="table table-hover" width="100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Action</th>
                                <th>Product Code</th>
                                <th>Product Type</th>
                                <th>Product Name</th>
                                <th>Description</th>
                                <th>UOM</th>
                                <th>Status</th>
                                <?php if(session()->get('level') <= 2) : ?>
                                <th class="text-center">Action</th>
                                <?php endif; ?>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                  </div>
                </div>
              </div>
        <!-- </div>
      </div> -->
    <!-- </div>
  </div> -->

  </body>
  </html>
