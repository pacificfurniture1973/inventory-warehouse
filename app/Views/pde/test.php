<div class="home-tab">  
  <div class="d-sm-flex align-items-center justify-content-between border-bottom">
    <div>
      <h4>Assembly Finish</h4>
    </div>
    <div>
      <div class="btn-wrapper">
        <!-- <a href="#" class="btn btn-otline-dark align-items-center"><i class="icon-share"></i> Share</a> -->
        <a href="<?= base_url('pde/c_finishAssembly/exportDataBuildAssembly') ?>" class="btn btn-primary text-white"><i class="icon-export"></i> Export</a>
      </div>
    </div>
  </div>  

  <div class="table-responsive mt-4 text-nowrap">
    <table id="tabel_finish_assembly" class="table">
      <thead>
        <tr>
          <th>No</th>
          <th>Build Code</th>
          <th>Assembly Code</th>
          <th>Product Code</th>
          <th>JO Number</th>
          <th>Process Quantity</th>
          <th>Total Quantity</th>
          <th>Need Withdrawal</th>
          <th>Created By</th>
          <th>Approved By</th>
          <th>Status</th>
        </tr>
      </thead>
      <tbody>

      </tbody>
    </table>

  </div>