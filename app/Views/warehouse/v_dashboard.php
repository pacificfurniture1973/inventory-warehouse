<?php $this->extend('layout/template') ?>
<?php $this->section('content') ?>

<!-- show alert -->
<?php if(session()->getFlashdata('success_message')) : ?>
  <div class="alert alert-success alert-dismissible fade show" role="alert">
    <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>
    <?= session()->getFlashdata('success_message'); ?>
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
  </div>
<?php elseif(session()->getFlashdata('error_message')) : ?>
  <div class="alert alert-danger alert-dismissible fade show" role="alert">
    <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
    <?= session()->getFlashdata('error_message'); ?>
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
  </div>
<?php endif; ?>

<div class="home-tab">
  <!-- <div class="d-sm-flex align-items-center justify-content-between border-bottom">
    <ul class="nav nav-tabs" role="tablist">
      <li class="nav-item">
        <a class="nav-link active ps-0" id="home-tab" data-bs-toggle="tab" href="<?= base_url('/Test') ?>" role="tab" aria-controls="overview" aria-selected="true">Overview</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#" id="productsLog" data-bs-toggle="tab" role="tab" aria-selected="false">Products Log</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="contact-tab" data-bs-toggle="tab" href="#demographics" role="tab" aria-selected="false">Customer Log</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="contact-tab" data-bs-toggle="tab" href="#demographics" role="tab" aria-selected="false">Vendor Log</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="contact-tab" data-bs-toggle="tab" href="#demographics" role="tab" aria-selected="false">JO Log</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="contact-tab" data-bs-toggle="tab" href="#demographics" role="tab" aria-selected="false">RR Log</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="contact-tab" data-bs-toggle="tab" href="#demographics" role="tab" aria-selected="false">Adjustment Log</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="contact-tab" data-bs-toggle="tab" href="#demographics" role="tab" aria-selected="false">Withdrawal Log</a>
      </li>
    </ul>
    <div>
      <div class="btn-wrapper">
        <a href="#" class="btn btn-otline-dark align-items-center"><i class="icon-share"></i> Share</a>
        <a href="#" class="btn btn-otline-dark"><i class="icon-printer"></i> Print</a>
        <a href="#" class="btn btn-primary text-white me-0"><i class="icon-download"></i> Export</a>
      </div>
    </div>
  </div> -->
  <div class="tab-content tab-content-basic" id="mainContent">
    <div class="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview"> 
      <div class="row">
        <div class="col-sm-12" style="margin-bottom: -50px">
          <div class="statistics-details d-flex align-items-center justify-content-between px-4">
            <div> 
              <p class="statistics-title">Total Products</p>
              <h3 class="rate-percentage"><?= $totalProducts['totalProducts'] ?></h3>
              <p class="text-success d-flex"><i class="mdi mdi-menu-up"></i><span>+<?= count($totalProductsThisMonth) ?></span></p>
            </div>
            <div>
              <p class="statistics-title">Total Customer</p>
              <h3 class="rate-percentage"><?= $totalCustomer['totalCustomer'] ?></h3>
              <p class="text-success d-flex"><i class="mdi mdi-menu-up"></i><span>+<?= count($totalCustomerThisMonth) ?></span></p>
            </div>
            <div>
              <p class="statistics-title">Total Vendor</p>
              <h3 class="rate-percentage"><?= $totalVendor['totalVendor'] ?></h3>
              <p class="text-success d-flex"><i class="mdi mdi-menu-up"></i><span>+<?= count($totalVendorThisMonth); ?></span></p>
            </div>
            <div class="d-none d-md-block">
              <p class="statistics-title">Total Assembly Products</p>
              <h3 class="rate-percentage"><?= $totalAssembly['totalAssembly'] ?></h3>
              <p class="text-success d-flex"><i class="mdi mdi-menu-up"></i><span>+<?= count($totalAssemblyThisMonth) ?></span></p>
            </div>
            <div class="">
              <p class="statistics-title">Total Withdrawal</p>
              <h3 class="rate-percentage"><?= count($totalWithdrawal); ?> <small style="font-size: 11px">/ day</small></h3>
              <p class="text-success d-flex"><i class="mdi mdi-menu-up"></i><span>+<?= count($totalWithdrawalMonth) ?></span></p>
            </div>
            <div class="">
              <p class="statistics-title ml-5">Total Pending JO</p>
              <h3 class="rate-percentage"><?= count($totalPendingJo) ?></h3>
              <p class="text-success d-flex"><i class="mdi mdi-menu-up"></i><span>+<?= count($totalTotalJoMonth) ?></span></p>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12 d-flex flex-column">
          <div class="row flex-grow">
            <div class="col-12 grid-margin stretch-card">
              <div class="card card-rounded">
                <div class="card-body">
                  <div class="d-sm-flex justify-content-between align-items-start">
                    <div>
                      <h4 class="card-title card-title-dash">Pending Requests</h4>
                      <p class="card-subtitle card-subtitle-dash">Pending : <span class="text-danger"><?= count($totalPendingWithdrawal); ?></span> Withdrawal</p>
                    </div>
                  </div>
                  <div class="table-responsive  mt-1">
                    <table class="table select-table">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Withdrawal Number</th>
                          <th>Created By</th>
                          <th>Product Code</th>
                          <th>Status</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $no = 1; ?>
                        <?php foreach($totalPendingWithdrawalList as $pw) : ?>
                        <tr>
                          <td style="width: 30px"><?= $no++; ?></td>
                          <td style="width: 200px">
                            <div class="d-flex ">
                              <div>
                                <h6 class='fw-bold text-success'><?= $pw['wd_number'] ?></h6>
                                <p><?= $pw['wd_date'] ?></p>
                              </div>
                            </div>
                          </td>
                          <td style="width: 150px">
                            <h6 class='fw-bold text-info'><?= $pw['user_fullname'] ?></h6>
                          </td>
                          <td style="width: 400px">
                            <h6 class='fw-bold text-primary'><?= $pw['wd_product_code'] ?></h6>
                            <p><?= $pw['product_name'] ?></p>
                          </td>
                          <td><div class="badge badge-warning"><?= $pw['status_name'] ?></div></td>
                        </tr>
                        <?php endforeach; ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <?php if((session()->get('level') <= 4 && session()->get('dept') == 7) || (session()->get('level') <= 4 && session()->get('dept') == 10) || (session()->get('level') <= 3)) : ?>
      <div class="row" style="margin-top: -40px">
        <div class="col-lg-12 d-flex flex-column">
          <div class="row flex-grow">
            <div class="col-12 grid-margin stretch-card">
              <div class="card card-rounded">
                <div class="card-body">
                  <div class="d-sm-flex justify-content-between align-items-start">
                    <div>
                      <h4 class="card-title card-title-dash mb-3">Overbudget Withdrawal</h4>
                    </div>
                  </div>
                  <div class="table-responsive  mt-1">
                    <table class="table tabel_overbudget" id="tabel_overbudget">
                      <thead>
                          <tr>
                              <th>No</th>
                              <th>JO Number</th>
                              <th>Assembly</th>
                              <th>Product</th>
                              <th>Stock Available</th>
                              <th>JO Quantity</th>
                              <th>Withdrawal</th>
                              <th>Status</th>
                              <th>Price</th>
                          </tr>
                      </thead>
                      <tbody>
                      
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php endif; ?>

    </div>
  </div>
</div>

<?php $this->endSection('content'); ?>