
<?php $this->extend('/layout/template'); ?>
<?php $this->section('content'); ?>

  <!-- Logo alert -->
  <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
    <symbol id="check-circle-fill" fill="currentColor" viewBox="0 0 16 16">
      <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
    </symbol>
    <symbol id="info-fill" fill="currentColor" viewBox="0 0 16 16">
      <path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412-1 4.705c-.07.34.029.533.304.533.194 0 .487-.07.686-.246l-.088.416c-.287.346-.92.598-1.465.598-.703 0-1.002-.422-.808-1.319l.738-3.468c.064-.293.006-.399-.287-.47l-.451-.081.082-.381 2.29-.287zM8 5.5a1 1 0 1 1 0-2 1 1 0 0 1 0 2z"/>
    </symbol>
    <symbol id="exclamation-triangle-fill" fill="currentColor" viewBox="0 0 16 16">
      <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
    </symbol>
  </svg>

  <!-- show alert -->
  <?php if(session()->getFlashdata('success_message')) : ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>
      <?= session()->getFlashdata('success_message'); ?>
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
  <?php elseif(session()->getFlashdata('error_message')) : ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
      <?= session()->getFlashdata('error_message'); ?>
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
  <?php endif; ?>

<div class="home-tab">  
  <div class="d-sm-flex align-items-center justify-content-between border-bottom">
    <div>
      <h4>Stock Adjustment</h4>
    </div>
    <div>
      <div class="btn-wrapper">
        <!-- <a href="#" class="btn btn-otline-dark align-items-center"><i class="icon-share"></i> Share</a> -->
        <a href="#" class="btn btn-success text-white" data-bs-toggle="modal" data-bs-target="#modalImportStockAdjustment"><i class="icon-upload"></i> Import</a>
      </div>
    </div>
  </div>  

  <form action="<?= base_url('/warehouse/c_stockAdjustment/insertStockAdjustment') ?>" method="post">
    <div class="info">
      <div class="row mt-3">
        <div class="form-group col-md-6">
          <label for="date">Date</label>
          <input style="padding-left: 12px" type="date" value="<?= date('Y-m-d'); ?>" class="form-control <?= $validation->hasError('date') ? 'is-invalid' : '' ?>" name="date" id="date" placeholder="Product Code..." autocomplete="off" required>
          <div class="invalid-feedback">
              <?= $validation->getError('date') ?>
          </div>
        </div>
        <div class="form-group col-md-6">
          <label for="reason">Reason</label>
          <!-- <input style="padding-left: 12px" type="text" class="form-control" name="productCode" id="reason" placeholder="Product Code..." autocomplete="off" required> -->
            <select class="form-control form-select text-dark <?= $validation->hasError('reason') ? 'is-invalid' : '' ?>" name="reason" required>
              <option disabled selected value="">Choose...</option>
              <option value="1">Stock</option>
              <option value="2">Adjustment From Log to Timber</option>
              <option value="3">Error Item</option>
              <option value="4">Kiln Dry to Lumberyard</option>
              <option value="5">Material Return Slip</option>
              <option value="6">Monthly Inventory</option>
              <option value="7">Negative Quantity</option>
              <option value="8">Receiving Report</option>
              <option value="9">RR Revision</option>
              <option value="10">Sample Stock Out</option>
              <option value="11">Stock Opname</option>
              <option value="12">Wet Wood to Kiln Dry</option>
              <option value="13">Wrong Unit Price</option>
              <option value="14">Wrong Unit Quantity</option>
              <option value="15">Zero Quantity</option>
            </select>
            <div class="invalid-feedback">
                <?= $validation->getError('reason') ?>
            </div>
        </div>
      </div>
      <div class="row">
        <div class="form-group col-md-6">
          <label for="memo">Memo</label>
          <textarea name="memo" id="memo" class="form-control form-control-lg <?= $validation->hasError('memo') ? 'is-invalid' : '' ?>" required></textarea>
          <div class="invalid-feedback">
              <?= $validation->getError('memo') ?>
          </div>
        </div>
      </div>  
    </div>

    <div class="table-responsive mt-4 text-nowrap">
      <table id="tabel1" class="table">
        <thead>
          <tr>
            <th>No</th>
            <th>Product/Assembly Code</th>
            <th>Product Name</th>
            <th>Description</th>
            <th>UoM</th>
            <th>Adjustment Type</th>
            <th>Default Location</th>
            <th>Stock Available</th>
            <th>Quantity</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <tr id="row">
            <td scope="row">1</td>
            <td>
              <input style="padding-left: 12px; width: 300px" class="form-control autocomplete_text-sa productCode_1 <?= $validation->hasError('productCode') ? 'is-invalid' : '' ?>" name="productCode[]" id="productCode_1" placeholder="Product Code..." required>
              <div class="invalid-feedback">
                <?= $validation->getError('productCode') ?>
              </div>
            </td>
            <td>
              <input style="padding-left: 12px; width: 300px" type="text" class="form-control productName_1" name="productName[]" id="productName_1" placeholder="Product Name..." readonly>
            </td>
            <td>
              <input style="padding-left: 12px; width: 300px" type="text" class="form-control description_1" name="description[]" id="description_1" placeholder="Description..." readonly>
            </td>
            <td>
              <input style="padding-left: 12px; width: 70px" type="text" class="form-control uom_1" name="uom[]" id="uom_1" placeholder="UoM..." readonly>
            </td>
            <td>
              <select class="form-control form-select text-dark <?= $validation->hasError('adjType') ? 'is-invalid' : '' ?>" name="adjType[]" required>
                <option disabled selected value="">Choose...</option>
                <option value="1">Stock In</option>
                <option value="2">Stock Out</option>
              </select>
              <div class="invalid-feedback">
                <?= $validation->getError('adjType') ?>
              </div>
            </td>
            <td>
              <input style="padding-left: 12px; width: 200px" type="hidden" class="form-control defLocHidden_1" name="defLoc[]" id="defLocHidden_1" placeholder="Warehouse Loc..." readonly>
              <input style="padding-left: 12px; width: 200px" type="text" class="form-control defLoc_1" id="defLoc_1" placeholder="Warehouse Loc..." readonly>
            </td>
            <td>
              <input style="padding-left: 12px; width: 100px" type="text" class="form-control stock_1" name="stock[]" id="stock_1" placeholder="Available..." readonly>
            </td>
            <td>
              <input style="padding-left: 12px; width: 100px" type="text" class="form-control qty_1 <?= $validation->hasError('qty') ? 'is-invalid' : '' ?>" name="qty[]" id="qty_1" placeholder="Quantity..." required>
              <div class="invalid-feedback">
                <?= $validation->getError('qty') ?>
              </div>
            </td>
            <td>
            <button id="add-sa" type="button" class="btn btn-success text-white mt-2">Add</button>
            </td>
          </tr>
        </tbody>
      </table>

    </div>
    <div class="row mt-4 px-3">
        <button type="submit" class="btn btn-info text-white">Submit</button>
    </div>
  </form>



</div>

<?php $this->endSection('content'); ?>