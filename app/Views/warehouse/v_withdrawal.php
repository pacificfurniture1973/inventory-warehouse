
<?php $this->extend('/layout/template'); ?>
<?php $this->section('content'); ?>

  <!-- Logo alert -->
  <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
    <symbol id="check-circle-fill" fill="currentColor" viewBox="0 0 16 16">
      <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
    </symbol>
    <symbol id="info-fill" fill="currentColor" viewBox="0 0 16 16">
      <path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412-1 4.705c-.07.34.029.533.304.533.194 0 .487-.07.686-.246l-.088.416c-.287.346-.92.598-1.465.598-.703 0-1.002-.422-.808-1.319l.738-3.468c.064-.293.006-.399-.287-.47l-.451-.081.082-.381 2.29-.287zM8 5.5a1 1 0 1 1 0-2 1 1 0 0 1 0 2z"/>
    </symbol>
    <symbol id="exclamation-triangle-fill" fill="currentColor" viewBox="0 0 16 16">
      <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
    </symbol>
  </svg>

  <!-- show alert -->
  <?php if(session()->getFlashdata('success_message')) : ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>
      <?= session()->getFlashdata('success_message'); ?>
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
  <?php elseif(session()->getFlashdata('error_message')) : ?>
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
      <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
      <?= session()->getFlashdata('error_message'); ?>
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
  <?php endif; ?>

<div class="home-tab">  
  <div class="d-sm-flex align-items-center justify-content-between border-bottom">
    <div>
      <h4>Withdrawal Request</h4>
    </div>
    <div>
      <div class="btn-wrapper">
        <!-- <a href="#" class="btn btn-otline-dark align-items-center"><i class="icon-share"></i> Share</a> -->
        <a href="#" class="btn btn-success text-white" data-bs-toggle="modal" data-bs-target="#modalImportWithdrawal"><i class="icon-upload"></i> Import</a>
      </div>
    </div>
  </div>  

  <form action="<?= base_url('/warehouse/c_withdrawal/insertWithdrawal') ?>" method="post">
    <div class="info">
      <div class="row mt-3">
        <div class="form-group col-md-3">
          <label for="reason">Warehouse Destination</label>
            <select class="form-control form-select text-dark wsToWhs <?= ($validation->hasError('wsToWhs') ? 'is-invalid' : '') ?>" name="wsToWhs" onchange="JORequiredBaseLoc()" required>
              <option disabled selected value="">Choose...</option>
              <?php foreach($defLoc as $dl) : ?>
                <option value="<?= $dl['whs_code'] ?>"><?= $dl['whs_code'] ?> - <?= $dl['whs_name'] ?></option>
              <?php endforeach; ?>
            </select>
            <div class="invalid-feedback">
                <?= $validation->getError('wsToWhs') ?>
            </div>
        </div>
        <div class="form-group col-md-3">
          <label for="reason">Department Handle</label>
            <select class="form-control form-select text-dark <?= ($validation->hasError('wsDept') ? 'is-invalid' : '') ?>" name="wsDept" required>
              <option disabled selected value="">Choose...</option>
              <?php foreach($combine as $id => $name) : ?>
                <option value="<?= $id ?>"><?= $id ?> - <?= $name ?></option>
              <?php endforeach; ?>
            </select>
            <div class="invalid-feedback">
                <?= $validation->getError('wsDept') ?>
            </div>
        </div>
        <div class="form-group col-md-3">
          <label for="reason">With JO ?</label>
            <select class="form-control form-select text-dark <?= ($validation->hasError('wsWithJo') ? 'is-invalid' : '') ?>" name="wsWithJo" id="wsWithJo" onchange="JORequired()" required>
              <option disabled selected value="">Choose...</option>
              <option value="1">1 - With JO</option>
              <option value="2">2 - Without JO</option>
            </select>
            <div class="invalid-feedback">
                <?= $validation->getError('wsWithJo') ?>
            </div>
        </div>
        <div class="form-group col-md-3">
          <label for="date">Date</label>
          <input style="padding-left: 12px" type="date" value="<?= date('Y-m-d'); ?>" class="form-control" name="wsDate" id="wsDate" placeholder="Today..." autocomplete="off" required readonly>
        </div>
      </div> 
    </div>

    <div class="table-responsive mt-4 text-nowrap">
      <table id="tabel1" class="table">
        <thead>
          <tr>
            <th>No</th>
            <th>Product/Assembly Code</th>
            <th>Product Name</th>
            <th>Description</th>
            <th>UoM</th>
            <th>Stock Available</th>
            <th>From Warehouse</th>
            <th>Customer</th>
            <th>JO Number</th>
            <th>Assembly Product</th>
            <th>BOM Quantity</th>
            <th>Request Quantity</th>
            <th>Remark</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <tr id="row">
            <td scope="row">1</td>
            <td>
              <input style="padding-left: 12px; width: 250px" type="text" class="form-control autocomplete_text-ws <?= $validation->hasError('wsProductCode') ? 'is-invalid' : '' ?>" name="wsProductCode[]" id="wsProductCode_1" placeholder="Product/Assembly Code..." autocomplete="off">
              <div class="invalid-feedback mt-2">
                  <?= $validation->getError('wsProductCode') ?>
              </div>
            </td>
            <td>
              <input style="padding-left: 12px; width: 300px" type="text" class="form-control" name="wsProductName[]" id="wsProductName_1" placeholder="Product Name..." autocomplete="off" readonly>
            </td>
            <td>
              <input style="padding-left: 12px; width: 300px" type="text" class="form-control" name="wsDescription[]" id="wsDescription_1" placeholder="Description..." autocomplete="off" readonly>
            </td>
            <td>
              <input style="padding-left: 12px; width: 70px" type="text" class="form-control" name="wsUom[]" id="wsUom_1" placeholder="UoM..." autocomplete="off" readonly>
            </td>
            <td>
              <input style="padding-left: 12px; width: 120px" type="text" class="form-control" name="wsStockAvailable[]" id="wsStockAvailable_1" placeholder="Stock Available..." autocomplete="off" readonly>
            </td>
            <td>
              <input style="padding-left: 12px; width: 200px" type="hidden" class="form-control wsDefLocHidden_1" name="wsDefLoc[]" id="wsDefLocHidden_1" placeholder="Warehouse Loc..." readonly>
              <input style="padding-left: 12px; width: 200px" type="text" class="form-control" id="wsDefLoc_1" placeholder="Warehouse Loc..." autocomplete="off" readonly>
            </td>
            <td>
              <input style="padding-left: 12px; width: 200px" type="text" class="form-control autocomplete_text-ws-customer <?= $validation->hasError('wsCustomer') ? 'is-invalid' : '' ?>" name="wsCustomer[]" id="wsCustomer_1" placeholder="Customer..." autocomplete="off">
              <div class="invalid-feedback mt-2">
                  <?= $validation->getError('wsCustomer') ?>
              </div>
            </td>
            <td style="display: none">
              <input style="padding-left: 12px; width: 200px" type="hidden" class="form-control hiddenCustomerCode" id="hiddenCustomerCode_1" placeholder="Customer Code..." autocomplete="off">
            </td>
            <td>
              <input style="padding-left: 12px; width: 200px" type="text" class="form-control autocomplete_text-ws-jo <?= $validation->hasError('wsJoNumber') ? 'is-invalid' : '' ?>" name="wsJoNumber[]" id="wsJoNumber_1" placeholder="Search JO..." autocomplete="off">
              <div class="invalid-feedback mt-2">
                  <?= $validation->getError('wsJoNumber') ?>
              </div>
            </td>
            <td> 
              <input style="padding-left: 12px; width: 200px" type="text" class="form-control" name="wsAssemblyProduct[]" id="wsAssemblyProduct_1" placeholder="Assembly Product..." autocomplete="off" required onkeypress="return false;">
            </td>
            <td>
              <input style="padding-left: 12px; width: 150px" type="text" class="form-control" name="wsBomQuantity[]" id="wsBomQuantity_1" placeholder="BOM Quantity..." autocomplete="off" readonly>
            </td>
            <td>
              <input style="padding-left: 12px; width: 150px" type="number" step="any" class="form-control <?= $validation->hasError('wsRequestQty') ? 'is-invalid' : '' ?>" name="wsRequestQty[]" id="wsRequestQty_1" placeholder="Request Quantity..." autocomplete="off" required>
              <div class="invalid-feedback mt-2">
                  <?= $validation->getError('wsRequestQty') ?>
              </div>
            </td>
            <td>
              <input style="padding-left: 12px; width: 300px" type="text" class="form-control <?= $validation->hasError('wsRemark') ? 'is-invalid' : '' ?>" name="wsRemark[]" id="wsRemark_1" placeholder="Remark..." autocomplete="off" required>
              <div class="invalid-feedback mt-2">
                  <?= $validation->getError('wsRemark') ?>
              </div>
            </td>
            <td>
            <button id="add-ws" type="button" class="btn btn-success text-white mt-2">Add</button>
            </td>
          </tr>
        </tbody>
      </table>

    </div>
    <div class="row mt-4 px-3">
        <button type="submit" class="btn btn-info text-white">Submit</button>
    </div>
  </form>



</div>

<?php $this->endSection('content'); ?>